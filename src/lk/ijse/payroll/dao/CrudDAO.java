/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao;

import java.util.ArrayList;
import lk.ijse.payroll.entity.SuperEntity;

/**
 *
 * @author udara
 */
public interface CrudDAO<T extends SuperEntity,ID>extends SuperDAO{
    public boolean add(T entity)throws Exception;
    public boolean update(T entity)throws Exception;
    public boolean delete(ID Key)throws Exception;
    public T search(ID key)throws Exception;
    public ArrayList<T> getAll()throws Exception;
    
}
