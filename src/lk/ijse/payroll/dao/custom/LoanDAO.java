/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom;

import lk.ijse.payroll.dao.CrudDAO;
import lk.ijse.payroll.entity.Loan;


/**
 *
 * @author udara
 */
public interface LoanDAO extends CrudDAO<Loan,String>{

    public Loan searchLoanByName(String name)throws Exception;

    public Loan search()throws Exception;
    
}
