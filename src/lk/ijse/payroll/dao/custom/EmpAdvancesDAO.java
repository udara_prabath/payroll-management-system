/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudDAO;
import lk.ijse.payroll.entity.BasicSalary;
import lk.ijse.payroll.entity.EmpAdvances;

/**
 *
 * @author udara
 */
public interface EmpAdvancesDAO extends CrudDAO<EmpAdvances,String>{

    public ArrayList<EmpAdvances> searchByName(String adv)throws Exception;

    public EmpAdvances searchLastId()throws Exception;

    public ArrayList<EmpAdvances> searchByemp(String empid)throws Exception;

    public String searchMonth(String emp);

    public String searchyear(String emp);
    
}
