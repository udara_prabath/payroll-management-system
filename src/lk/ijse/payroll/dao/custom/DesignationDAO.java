/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom;

import lk.ijse.payroll.dao.CrudDAO;
import lk.ijse.payroll.entity.BasicSalary;
import lk.ijse.payroll.entity.Designation;

/**
 *
 * @author udara
 */
public interface DesignationDAO extends CrudDAO<Designation,String>{

    public Designation searchId(String Key) throws Exception;

    public Designation searchName(String deId)throws Exception;

    public Designation search()throws Exception;
    
}
