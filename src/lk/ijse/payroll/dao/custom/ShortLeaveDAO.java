/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudDAO;
import lk.ijse.payroll.entity.shortLeave;

/**
 *
 * @author udara
 */
public interface ShortLeaveDAO extends CrudDAO<shortLeave,String> {

    public ArrayList<shortLeave> getAll(String Id)throws Exception;

    public ArrayList<shortLeave> getAll(String id, String date)throws Exception;
    
}
