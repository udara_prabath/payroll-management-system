/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudDAO;
import lk.ijse.payroll.entity.BasicSalary;
import lk.ijse.payroll.entity.EmpLoan;

/**
 *
 * @author udara
 */
public interface EmpLoanDAO extends CrudDAO<EmpLoan,String>{

    public ArrayList<EmpLoan> searchByemp(String empid)throws Exception;

    public ArrayList<EmpLoan> searchByid(String lid)throws Exception;

    public EmpLoan searchId()throws Exception;

    public String getDate(String e);
    
}
