/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom;

import java.math.BigDecimal;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudDAO;
import lk.ijse.payroll.entity.BasicSalary;
import lk.ijse.payroll.entity.Payroll;

/**
 *
 * @author udara
 */
public interface PayrollDAO extends CrudDAO<Payroll,String>{

    public ArrayList<Payroll> getAllByEmpId(String id)throws Exception;

    public ArrayList<Payroll> getAllPayroll(String Id, int y, int m)throws Exception;

    public ArrayList<Payroll> getAllPayroll(int y, int m)throws Exception;

    public ArrayList<String> getAll(String date)throws Exception;
    
    public Payroll calculatePayroll(int NOT, int DOT, int leaves, int shot, BigDecimal basic, BigDecimal epf8, BigDecimal lon, BigDecimal ad, BigDecimal bon)throws Exception;
    
}
