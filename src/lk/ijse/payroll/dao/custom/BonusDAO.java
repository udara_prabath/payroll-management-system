/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom;

import lk.ijse.payroll.dao.CrudDAO;
import lk.ijse.payroll.entity.BasicSalary;
import lk.ijse.payroll.entity.Bonus;

/**
 *
 * @author udara
 */
public interface BonusDAO extends CrudDAO<Bonus,String>{

    public Bonus search()throws Exception;
    
}
