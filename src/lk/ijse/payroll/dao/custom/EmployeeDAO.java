/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudDAO;
import lk.ijse.payroll.entity.BankDetails;
import lk.ijse.payroll.entity.BasicSalary;
import lk.ijse.payroll.entity.Employee;
import lk.ijse.payroll.entity.SalaryDetails;

/**
 *
 * @author udara
 */
public interface EmployeeDAO extends CrudDAO<Employee, String> {

    public Employee searchEmpId() throws Exception;

    public Employee searchEmpID(String Id) throws Exception;

    public ArrayList<Employee> getAll(String deId) throws Exception;

    public boolean addEmployee(Employee employee, BankDetails bankdetails, SalaryDetails salarydetails) throws Exception;

    public boolean updateEmp(Employee e, BankDetails bank, SalaryDetails salary) throws Exception;

}
