/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudDAO;
import lk.ijse.payroll.entity.BasicSalary;
import lk.ijse.payroll.entity.OT;

/**
 *
 * @author udara
 */
public interface OTDAO extends CrudDAO<OT,String>{

    public ArrayList<OT> getAll(String id)throws Exception;
    
}
