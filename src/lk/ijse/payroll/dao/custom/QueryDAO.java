/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.SuperDAO;
import lk.ijse.payroll.dto.BankDetailsDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.entity.BasicSalary;
import lk.ijse.payroll.entity.CustomEntity;
import lk.ijse.payroll.entity.Query;
import lk.ijse.payroll.entity.SuperEntity;

/**
 *
 * @author udara
 */
public interface QueryDAO {

    public BasicSalary search(String ID) throws Exception;

    public Query getBankDetails(String ID)throws Exception;

    public ArrayList<Query> getAllAteendanceDetails(String id,String date)throws Exception;

    public Query searchAdvancesDetails(String id, String date)throws Exception;

    public Query searchLoanDetails(String id, String date)throws Exception;

    public ArrayList<Query> serachDetails(String id)throws Exception;

    public ArrayList<Query> getAdvances(String id)throws Exception;

}
