/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudDAO;
import lk.ijse.payroll.entity.BasicSalary;
import lk.ijse.payroll.entity.Leaves;

/**
 *
 * @author udara
 */
public interface LeavesDAO extends CrudDAO<Leaves,String>{

    public ArrayList<Leaves> getAll(String Id)throws Exception;
    
}
