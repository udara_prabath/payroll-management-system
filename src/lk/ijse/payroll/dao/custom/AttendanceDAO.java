/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudDAO;
import lk.ijse.payroll.entity.Attendance;
import lk.ijse.payroll.entity.BasicSalary;

/**
 *
 * @author udara
 */
public interface AttendanceDAO extends CrudDAO<Attendance,String>{

    public ArrayList<Attendance> getAll(String Id, int y, int m)throws Exception;

    public ArrayList<Attendance> getAll(int y, int m)throws Exception;

    public Attendance searchLastId()throws Exception;
    public boolean add(Attendance entity,String s) throws Exception;
    
}
