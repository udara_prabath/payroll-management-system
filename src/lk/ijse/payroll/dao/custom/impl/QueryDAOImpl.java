/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.QueryDAO;
import lk.ijse.payroll.db.DBConnection;
import lk.ijse.payroll.dto.BankDetailsDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.entity.BasicSalary;
import lk.ijse.payroll.entity.CustomEntity;
import lk.ijse.payroll.entity.Designation;
import lk.ijse.payroll.entity.Query;

/**
 *
 * @author udara
 */
public class QueryDAOImpl implements QueryDAO {

    @Override
    public BasicSalary search(String empId) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchamountByJoinquery(?); ", empId);

        if (rst.next()) {
            String id = rst.getString(1);
            ResultSet rst2 = CrudUtil.executeQuery("call searchbsidByJoinquery(?); ", id);
            return new BasicSalary(rst.getString(1), new BigDecimal(id));
        }
        return null;
    }

    @Override
    public Query getBankDetails(String id) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchBankDetailsByJoinquery(?); ", id);
        if (rst.next()) {
            return new Query(rst.getString(1), rst.getString(2));
        }
        return null;

    }

    @Override
    public ArrayList<Query> getAllAteendanceDetails(String id, String date) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchYearMonthByDate(?);",date);
        if (rst.next()) {

            String year = rst.getString(1);
            String month = rst.getString(2);

            ResultSet r = CrudUtil.executeQuery("call searchpayrollByjoin(?,?,?);", year, month, id);
            ArrayList<Query> all = new ArrayList<>();
            while (r.next()) {
                all.add(new Query(r.getString(1), r.getString(2), r.getString(3), r.getInt(4), r.getInt(5), r.getInt(6)));
            }

            return all;
        }
        return null;
    }

    @Override
    public Query searchAdvancesDetails(String id, String date) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchYearMonthByDate(?);",date);
        if (rst.next()) {
            String year = rst.getString(1);
            String month = rst.getString(2);
            int mon = Integer.parseInt(month) - 1;

            ResultSet r = CrudUtil.executeQuery("call searchAdNameAndM_I(?,?,?);", mon, year, id);
            if (r.next()) {

                return new Query(r.getString(1), r.getBigDecimal(2));
            }
            return null;
        }
        return null;
    }

    @Override
    public Query searchLoanDetails(String id, String date) throws Exception {

        ResultSet rst = CrudUtil.executeQuery("call searchLoanDetails(?,?); ",date, id);
        if (rst.next()) {

            return new Query(rst.getString(2), rst.getBigDecimal(3), rst.getString(1));
        }
        return null;
    }

    @Override
    public ArrayList<Query> serachDetails(String id) throws Exception {

        ResultSet r = CrudUtil.executeQuery("call searchLoanDetails2(?); ", id);
        ArrayList<Query> all = new ArrayList<>();
        while (r.next()) {
            all.add(new Query(r.getString(1), r.getString(2), r.getBigDecimal(3)));
        }

        return all;
    }

    @Override
    public ArrayList<Query> getAdvances(String id) throws Exception {
        ResultSet r = CrudUtil.executeQuery("call searchDetailsByEmp(?); ", id);
        ArrayList<Query> all = new ArrayList<>();
        while (r.next()) {
            all.add(new Query(r.getString(1), r.getString(2),r.getString(3), r.getBigDecimal(4)));
        }

        return all;
    }

}
