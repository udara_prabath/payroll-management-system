/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.BankDAO;
import lk.ijse.payroll.entity.Bank;

/**
 *
 * @author udara
 */
public class BankDAOImpl implements BankDAO {

    @Override
    public boolean add(Bank entity) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select AddBank(?,?)", entity.getBankId(), entity.getBankName());
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public boolean update(Bank entity) throws Exception {
        ResultSet rst =  CrudUtil.executeQuery("select updateBank(?,?); ", entity.getBankName(), entity.getBankId()) ;
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        ResultSet rst =  CrudUtil.executeQuery("select deleteBank(?);") ;
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public Bank search(String key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchBankById(?); ", key);
        if (rst.next()) {
            return new Bank(key, rst.getString(2));
        }
        return null;
    }

    @Override
    public ArrayList<Bank> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllBank();");
        ArrayList<Bank> allsalary = new ArrayList<>();
        while(rst.next()) {
            allsalary.add(new Bank(rst.getString(1), rst.getString(2)));
        }
        return allsalary;
    }

    @Override
    public Bank searchId(String id) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchBankByName(?); ", id);
        if (rst.next()) {
            return new Bank( rst.getString(1),id);
        }
        return null;
    }

    @Override
    public Bank searchId() throws Exception {
         ResultSet rst = CrudUtil.executeQuery("call searchBankBybankDescLimit1();");
         if (rst.next()) {
            return new Bank( rst.getString(1),rst.getString(2));
        }
        return null;
    }

}
