/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.OTDAO;
import lk.ijse.payroll.entity.OT;

/**
 *
 * @author udara
 */
public class OTDAOImpl implements OTDAO {

    @Override
    public boolean add(OT entity) throws Exception {
        return false;
    }

    @Override
    public boolean update(OT entity) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        return CrudUtil.executeUpdate("call deleteOT(?);",Key) > 0;
    }

    @Override
    public OT search(String key) throws Exception {
        return null;
    }

    @Override
    public ArrayList<OT> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllOT();");
        ArrayList<OT> allsalary = new ArrayList<>();

        while (rst.next()) {
            allsalary.add(new OT(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4),rst.getInt(5),rst.getInt(6),rst.getBigDecimal(7)));
        }
        return allsalary;
    }

    @Override
    public ArrayList<OT> getAll(String id) throws Exception {
         ResultSet rst = CrudUtil.executeQuery("call searchAllOTByEmp(?); ",id);
        ArrayList<OT> allsalary = new ArrayList<>();

        while (rst.next()) {
            allsalary.add(new OT(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4),rst.getInt(5),rst.getInt(6),rst.getBigDecimal(7)));
        }
        return allsalary;
    }

}
