/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.EmpAdvancesDAO;
import lk.ijse.payroll.entity.EmpAdvances;

/**
 *
 * @author udara
 */
public class EmpAdvancesDAOImpl implements EmpAdvancesDAO {

    @Override
    public boolean add(EmpAdvances entity) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select addEmpAdvance(?,?,?,?,?,?);", entity.geteAdId(), entity.getEmpId(), entity.getaDId(), entity.getMonth(), entity.getYear(), entity.getMonthlyInstallment()) ;
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public boolean update(EmpAdvances entity) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select deleteEmpAdvance(?);", Key);
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public EmpAdvances search(String key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchEmpAdvanceById(?); ", key);

        if (rst.next()) {
            return new EmpAdvances(key, rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getBigDecimal(6));
        }
        return null;
    }

    @Override
    public ArrayList<EmpAdvances> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchEmpAllAdvance();");
        ArrayList<EmpAdvances> allsalary = new ArrayList<>();

        while (rst.next()) {
//            System.out.println(rst.getString(1));
            allsalary.add(new EmpAdvances(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getBigDecimal(6)));
        }
        return allsalary;
    }

    @Override
    public ArrayList<EmpAdvances> searchByName(String adv) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchEmpAdvanceByadId(?); ", adv);
        ArrayList<EmpAdvances> all = new ArrayList<>();

        while (rst.next()) {

            all.add(new EmpAdvances(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getBigDecimal(6)));
        }
        return all;
    }

    @Override
    public EmpAdvances searchLastId() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchEmpAdvanceByAdidDescLimit1(); ");
        if (rst.next()) {
            return new EmpAdvances(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getBigDecimal(6));
        }
        return null;
    }

    @Override
    public ArrayList<EmpAdvances> searchByemp(String empid) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchEmpAdvanceByempId(?); ", empid);
        ArrayList<EmpAdvances> all = new ArrayList<>();

        while (rst.next()) {

            all.add(new EmpAdvances(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getBigDecimal(6)));
        }
        return all;
    }

    @Override
    public String searchMonth(String emp) {
        try {
            ResultSet rst = CrudUtil.executeQuery("call searchMonthByempId(?); ",emp);
            if(rst.next()){
                return rst.getString(1);
            }
            
        } catch (Exception ex) {
            return null;
        }
        return null;
    }

    @Override
    public String searchyear(String emp) {
        try {
            ResultSet rst = CrudUtil.executeQuery("call searchYearByempId(?); ",emp);
            if(rst.next()){
                return rst.getString(1);
            }
            
        } catch (Exception ex) {
            return null;
        }
        return null;
    }

}
