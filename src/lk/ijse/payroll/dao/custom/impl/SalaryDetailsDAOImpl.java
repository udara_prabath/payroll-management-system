/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.SalaryDetailsDAO;
import lk.ijse.payroll.entity.SalaryDetails;

/**
 *
 * @author udara
 */
public class SalaryDetailsDAOImpl implements SalaryDetailsDAO {

    @Override
    public boolean add(SalaryDetails entity) throws Exception {
//        return CrudUtil.executeUpdate("insert into SalaryDetails values(?,?,?)", entity.getSDId(), entity.getEmpId(), entity.getBSId()) > 0;
        return false;
    }

    @Override
    public boolean update(SalaryDetails entity) throws Exception {
//        return CrudUtil.executeUpdate("update SalaryDetails set empid=?, bsid=? where sdId=? ", entity.getEmpId(), entity.getBSId(), entity.getSDId()) > 0;
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
//        return CrudUtil.executeUpdate("delete from SalaryDetails where sdId='" + Key + "'") > 0;
        return false;
    }

    @Override
    public SalaryDetails search(String key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchSDById(?); ", key);
        if (rst.next()) {
            return new SalaryDetails(rst.getInt(1), rst.getString(2), rst.getString(3));
        }
        return null;
    }

    @Override
    public ArrayList<SalaryDetails> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllSD();");
        ArrayList<SalaryDetails> allsalary = new ArrayList<>();

        while (rst.next()) {
            allsalary.add(new SalaryDetails(rst.getInt(1), rst.getString(2), rst.getString(3)));
        }
        return allsalary;
    }
    
    public SalaryDetails searchId() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchSDByAdidDescLimit1(); ");
        if (rst.next()) {
            
            return new SalaryDetails(rst.getInt(1), rst.getString(2), rst.getString(3));
        }
        return null;
    }

    @Override
    public SalaryDetails getSalaryDetailId(String id) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchSDByName(?); ", id);
        if (rst.next()) {
            return new SalaryDetails(rst.getInt(1),id, rst.getString(3));
        }
        return null;
    }

    
}

