/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.PayrollDAO;
import lk.ijse.payroll.entity.Payroll;

/**
 *
 * @author udara
 */
public class PayrollDAOImpl implements PayrollDAO {

    @Override
    public boolean add(Payroll a) throws Exception {
        return CrudUtil.executeUpdate("call addpayroll(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", a.getPayId(), a.getEmpId(), a.getDate(), a.getDesignation(), a.getBank(), a.getAcc_no(), a.getBasic_Salary(), a.getDay_worked(), a.getNormal_OT(), a.getDouble_OT(), a.getTotal_Leaves(), a.getShort_Leaves(), a.getEPF_8(), a.getEPF_12(), a.getETF_3(), a.getLoan(), a.getAdvances(), a.getBonus(), a.getGross_Salary(), a.getTotal_Deduction(), a.getSalary()) > 0;
    }

    @Override
    public boolean update(Payroll a) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        return CrudUtil.executeUpdate("call deletePayroll(?);",Key ) > 0;
    }

    @Override
    public Payroll search(String key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchpayrollById(?); ", key);

        if (rst.next()) {
            return new Payroll(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getBigDecimal(7), rst.getString(8), rst.getBigDecimal(9), rst.getBigDecimal(10), rst.getString(11), rst.getString(12), rst.getBigDecimal(13), rst.getBigDecimal(14), rst.getBigDecimal(15), rst.getBigDecimal(16), rst.getBigDecimal(17), rst.getBigDecimal(18), rst.getBigDecimal(19), rst.getBigDecimal(20), rst.getBigDecimal(21));
        }
        return null;

    }

    @Override
    public ArrayList<Payroll> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllpayroll();");
        ArrayList<Payroll> allsalary = new ArrayList<>();

        while (rst.next()) {
            allsalary.add(new Payroll(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getBigDecimal(7), rst.getString(8), rst.getBigDecimal(9), rst.getBigDecimal(10), rst.getString(11), rst.getString(12), rst.getBigDecimal(13), rst.getBigDecimal(14), rst.getBigDecimal(15), rst.getBigDecimal(16), rst.getBigDecimal(17), rst.getBigDecimal(18), rst.getBigDecimal(19), rst.getBigDecimal(20), rst.getBigDecimal(21)));
        }
        return allsalary;
    }

    @Override
    public ArrayList<Payroll> getAllByEmpId(String id) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchpayrollByemp(?); ", id);
        ArrayList<Payroll> allsalary = new ArrayList<>();

        while (rst.next()) {
            allsalary.add(new Payroll(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getBigDecimal(7), rst.getString(8), rst.getBigDecimal(9), rst.getBigDecimal(10), rst.getString(11), rst.getString(12), rst.getBigDecimal(13), rst.getBigDecimal(14), rst.getBigDecimal(15), rst.getBigDecimal(16), rst.getBigDecimal(17), rst.getBigDecimal(18), rst.getBigDecimal(19), rst.getBigDecimal(20), rst.getBigDecimal(21)));
        }
        return allsalary;
    }

    @Override
    public ArrayList<Payroll> getAllPayroll(String Id, int y, int m) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchpayrollByYearMonthEmp(?,?,?); ", y, m, Id);
        ArrayList<Payroll> allsalary = new ArrayList<>();

        while (rst.next()) {
            allsalary.add(new Payroll(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getBigDecimal(7), rst.getString(8), rst.getBigDecimal(9), rst.getBigDecimal(10), rst.getString(11), rst.getString(12), rst.getBigDecimal(13), rst.getBigDecimal(14), rst.getBigDecimal(15), rst.getBigDecimal(16), rst.getBigDecimal(17), rst.getBigDecimal(18), rst.getBigDecimal(19), rst.getBigDecimal(20), rst.getBigDecimal(21)));
        }
        return allsalary;
    }

    @Override
    public ArrayList<Payroll> getAllPayroll(int y, int m) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchpayrollByYearMonth(?,?); ", y, m);
        ArrayList<Payroll> allsalary = new ArrayList<>();

        while (rst.next()) {
            allsalary.add(new Payroll(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getBigDecimal(7), rst.getString(8), rst.getBigDecimal(9), rst.getBigDecimal(10), rst.getString(11), rst.getString(12), rst.getBigDecimal(13), rst.getBigDecimal(14), rst.getBigDecimal(15), rst.getBigDecimal(16), rst.getBigDecimal(17), rst.getBigDecimal(18), rst.getBigDecimal(19), rst.getBigDecimal(20), rst.getBigDecimal(21)));
        }
        return allsalary;
    }

    @Override
    public ArrayList<String> getAll(String date) throws Exception {
        ResultSet rst1 = CrudUtil.executeQuery("call searchYearMonthByDate(?);",date);
        if (rst1.next()) {

            String year = rst1.getString(1);
            String month = rst1.getString(2);
            
            ResultSet rst = CrudUtil.executeQuery("call searchpayrollByDate(?,?); ", year, month);
            ArrayList<String> allsalary = new ArrayList<>();

            while (rst.next()) {
                allsalary.add(rst.getString(1));
                
            }
            return allsalary;
        }
        return null;

    }

    @Override
    public Payroll calculatePayroll(int NOT, int DOT, int leaves, int shot, BigDecimal basic, BigDecimal epf8, BigDecimal lon, BigDecimal ad, BigDecimal bon)throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call CalculatePayroll(?,?,?,?,?,?,?,?,?); ",NOT,DOT,leaves,shot,basic,epf8,lon,ad,bon);

        if (rst.next()) {
            return new Payroll( rst.getBigDecimal(1), rst.getBigDecimal(2), rst.getBigDecimal(3));
        }
        return null;
    }

    

}
