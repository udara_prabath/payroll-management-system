/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.AdvancesDAO;
import lk.ijse.payroll.entity.Advances;


/**
 *
 * @author udara
 */
public class AdvancesDAOImpl implements AdvancesDAO {

    @Override
    public boolean add(Advances entity) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select add_Advance(?,?,?); ", entity.getAdId(), entity.getAdName(), entity.getAmount());
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public boolean update(Advances entity) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select updateAdvance(?,?,?);", entity.getAdName(), entity.getAmount(), entity.getAdId());
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select deleteAdvance(?);", Key );
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public Advances search(String key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAdvanceById(?); ", key);
        if (rst.next()) {
            
            return new Advances(rst.getString(1), rst.getString(2), rst.getBigDecimal(3));
        }
        return null;
    }

    @Override
    public ArrayList<Advances> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllAdvance(); ");
        ArrayList<Advances> allad = new ArrayList<>();
        while (rst.next()) {
            allad.add(new Advances(rst.getString(1), rst.getString(2), rst.getBigDecimal(3)));
        }
        return allad;
    }

    @Override
    public Advances searchAdvance(String adv) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAdvanceByName(?); ", adv);
        if (rst.next()) {
            return new Advances(rst.getString(1), rst.getString(2), rst.getBigDecimal(3));
        }
        return null;
    }

    @Override
    public Advances search() throws Exception {
         ResultSet rst = CrudUtil.executeQuery("call searchAdvanceByAdidDescLimit1() ");
         if (rst.next()) {
            return new Advances(rst.getString(1), rst.getString(2), rst.getBigDecimal(3));
        }
        return null;
    }

  

}
