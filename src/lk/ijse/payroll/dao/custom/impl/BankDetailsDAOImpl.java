/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.BankDetailsDAO;
import lk.ijse.payroll.entity.BankDetails;

/**
 *
 * @author udara
 */
public class BankDetailsDAOImpl implements BankDetailsDAO {

    @Override
    public boolean add(BankDetails entity) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select addbankdetails(?,?,?,?);", entity.getBdId(), entity.getEmpId(), entity.getBankId(), entity.getAccNo());
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public boolean update(BankDetails entity) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select updateBankDetails(?,?,?,?); ", entity.getEmpId(), entity.getBankId(), entity.getAccNo(), entity.getBdId());
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select deleteBankDetails(?);",Key) ;
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public BankDetails search(String key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchBankDetailsById(?); ", key);
        if (rst.next()) {
            return new BankDetails(Integer.parseInt(key), rst.getString(2), rst.getString(3), rst.getString(4));
        }
        return null;
    }

    @Override
    public ArrayList<BankDetails> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllBankDetails();");
        ArrayList<BankDetails> allsalary = new ArrayList<>();
        while (rst.next()) {
            allsalary.add(new BankDetails(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4)));
        }
        return allsalary;
    }

    @Override
    public BankDetails searchBankId() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchBankDetailsByDescLimit1(); ");
        if (rst.next()) {

            return new BankDetails(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4));
        }
        return null;
    }

    @Override
    public BankDetails getbankId(String id) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchBankDetailsByemp(?); ", id);
        if (rst.next()) {
            return new BankDetails(rst.getInt(1),id, rst.getString(3), rst.getString(4));
        }
        return null;
    }

}
