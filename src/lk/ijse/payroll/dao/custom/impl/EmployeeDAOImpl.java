/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.EmpLoanDAO;
import lk.ijse.payroll.dao.custom.EmployeeDAO;
import lk.ijse.payroll.db.DBConnection;
import lk.ijse.payroll.entity.BankDetails;
import lk.ijse.payroll.entity.Employee;
import lk.ijse.payroll.entity.SalaryDetails;

/**
 *
 * @author udara
 */
public class EmployeeDAOImpl implements EmployeeDAO {

    @Override
    public boolean add(Employee entity) throws Exception {
        return true;
    }
    @Override
    public boolean update(Employee entity) throws Exception {
        return true;
    }

    @Override
    public boolean addEmployee(Employee e, BankDetails bank, SalaryDetails salary)throws Exception{
        return CrudUtil.executeUpdate("call addEmployee(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", e.getEmpId(), e.getEmpName(), e.getAddress(), e.getGender(), e.getNIC(), e.getD_O_B(), e.getPhone_No(), e.getJoin_Date(), e.getDeId(), e.getTot_Leaves(),bank.getBdId(), bank.getBankId(), bank.getAccNo(),salary.getSDId(), salary.getBSId() ) > 0;
    }
    @Override
    public boolean updateEmp(Employee e, BankDetails bank, SalaryDetails salary) throws Exception {
        return CrudUtil.executeUpdate("call updateEmployee(?,?,?,?,?,?,?,?,?,?,?,?,?,?);", e.getEmpId(), e.getEmpName(), e.getAddress(), e.getGender(), e.getNIC(), e.getD_O_B(), e.getPhone_No(), e.getJoin_Date(), e.getDeId(),bank.getBdId(), bank.getBankId(), bank.getAccNo(),salary.getSDId(), salary.getBSId() ) > 0;
    }

     @Override
    public boolean delete(String Key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select deleteEmployee(?);", Key ) ;
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public Employee search(String key) throws Exception {
        Connection connection = DBConnection.getInstance().getConnection();
        PreparedStatement pstm = connection.prepareStatement("call searchEmployeeBYID('" + key + "');");
        ResultSet rst = pstm.executeQuery();
        while (rst.next()) {

            Employee tem = new Employee(rst.getString("empid"), rst.getString("empname"), rst.getString("address"), rst.getString("gender"), rst.getString("nic"), rst.getString("d_o_b"), rst.getInt("phone_no"), rst.getString("join_date"), rst.getString("deid"), rst.getInt("tot_leaves"));
            return tem;
        }
        return null;
    }

    @Override
    public ArrayList<Employee> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllEmployee();");
        ArrayList<Employee> allsalary = new ArrayList<>();

        while (rst.next()) {
            allsalary.add(new Employee(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getInt(7), rst.getString(8), rst.getString(9), rst.getInt(10)));
        }
        return allsalary;
    }

    @Override
    public Employee searchEmpId() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchEmployeeByDescLimit1(); ");
        if (rst.next()) {

            return new Employee(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getInt(7), rst.getString(8), rst.getString(9), rst.getInt(10));
        }
        return null;
    }

    @Override
    public Employee searchEmpID(String key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchEmployeeBYName(?); ", key);

        if (rst.next()) {
            return new Employee(rst.getString(1), key, rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getInt(7), rst.getString(8), rst.getString(9), rst.getInt(10));
        }
        return null;
    }

    

    @Override
    public ArrayList<Employee> getAll(String deId) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchEmployeeBYDes(?); ", deId);
        ArrayList<Employee> allsalary = new ArrayList<>();

        while (rst.next()) {
            allsalary.add(new Employee(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getInt(7), rst.getString(8), rst.getString(9), rst.getInt(10)));
        }
        return allsalary;
    }

    

}
