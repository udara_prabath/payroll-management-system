/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.BasicSalaryDAO;
import lk.ijse.payroll.entity.BasicSalary;

/**
 *
 * @author udara
 */
public class BasicSalaryDAOImpl implements BasicSalaryDAO {

    @Override
    public boolean add(BasicSalary entity) throws Exception {
        ResultSet rst =  CrudUtil.executeQuery("select addBasicsalary(?,?);", entity.getBsId(), entity.getAmount()) ;
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public boolean update(BasicSalary entity) throws Exception {
        ResultSet rst =  CrudUtil.executeQuery("select updateBasicsalary(?,?); ", entity.getAmount(), entity.getBsId()) ;
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        ResultSet rst =  CrudUtil.executeQuery("select deleteBasicsalary(?);",Key) ;
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public BasicSalary search(String key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchBasicsalaryById(?); ", key);
        if (rst.next()) {
            return new BasicSalary(key, rst.getBigDecimal(2));
        }
        return null;
    }

    @Override
    public ArrayList<BasicSalary> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllBasicsalary();");
        ArrayList<BasicSalary> allsalary = new ArrayList<>();
        while (rst.next()) {
            allsalary.add(new BasicSalary(rst.getString(1), rst.getBigDecimal(2)));
        }
        return allsalary;
    }

    @Override
    public BasicSalary searchId(String id) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchBasicsalaryByname(?); ",id);
        if (rst.next()) {
            return new BasicSalary(rst.getString(1),new BigDecimal(id));
        }
        return null;
    }

    @Override
    public BasicSalary searchId() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchBasicsalaryByDescLimit1(); ");
        if (rst.next()) {

            return new BasicSalary(rst.getString(1),new BigDecimal(2));
        }
        return null;
    }

}
