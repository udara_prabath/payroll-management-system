/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.BonusDAO;
import lk.ijse.payroll.entity.Bonus;

/**
 *
 * @author udara
 */
public class BonusDAOImpl implements BonusDAO {

    @Override
    public boolean add(Bonus entity) throws Exception {
        ResultSet rst =  CrudUtil.executeQuery("select addBonus(?,?,?)", entity.getBonusId(), entity.getBonusName(), entity.getAmount());
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public boolean update(Bonus entity) throws Exception {
        ResultSet rst =  CrudUtil.executeQuery("select updateBonus(?,?,?);", entity.getBonusName(), entity.getAmount(), entity.getBonusId());
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        ResultSet rst =  CrudUtil.executeQuery("select deleteBonus(?);",Key) ;
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public Bonus search(String key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchBonusById(?); ", key);

        if (rst.next()) {
            return new Bonus(key, rst.getString(2), rst.getBigDecimal(3));
        }
        return null;
    }

    @Override
    public ArrayList<Bonus> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllBonus();");
        ArrayList<Bonus> allsalary = new ArrayList<>();

        while (rst.next()) {
            allsalary.add(new Bonus(rst.getString(1), rst.getString(2), rst.getBigDecimal(3)));
        }
        return allsalary;
    }

    @Override
    public Bonus search() throws Exception {
         ResultSet rst = CrudUtil.executeQuery("call searchBonusBybankDescLimit1(); ");
         if (rst.next()) {
            return new Bonus(rst.getString(1), rst.getString(2), rst.getBigDecimal(3));
        }
        return null;
    }

}
