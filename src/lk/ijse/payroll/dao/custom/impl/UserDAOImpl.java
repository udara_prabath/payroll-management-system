/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.UserDAO;
import lk.ijse.payroll.entity.User;

/**
 *
 * @author udara
 */
public class UserDAOImpl implements UserDAO {

    @Override
    public boolean add(User e) throws Exception {
        return true;
    }

    @Override
    public boolean update(User e) throws Exception {
        return CrudUtil.executeUpdate("call updateUser(?,?); ", e.getPassword(), e.getUserName()) > 0;
    }
    
    @Override
    public User search(String un, String pw) throws Exception {
         
        ResultSet rst = CrudUtil.executeQuery("call searchUserByNameAndPassword(?,?); ",pw,un);
        if (rst.next()) {
           System.out.println(rst.getString(1));
            return new User(rst.getString(1), rst.getString(2));
        }
        return null;
    }
    

    @Override
    public boolean delete(String Key) throws Exception {
        return false;
    }

    @Override
    public User search(String key) throws Exception {
        return null;
    }

    @Override
    public ArrayList<User> getAll() throws Exception {
        return null;
    }

    

    

    
}


