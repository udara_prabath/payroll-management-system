/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.EmpLoanDAO;
import lk.ijse.payroll.entity.EmpLoan;

/**
 *
 * @author udara
 */
public class EmpLoanDAOImpl implements EmpLoanDAO {

    @Override
    public boolean add(EmpLoan entity) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select addEmpLoan(?,?,?,?,? );", entity.getElId(), entity.getEmpId(), entity.getLoanId(), entity.getDate(), entity.getInstallment());
        if (rst.next()) {
            return rst.getInt(1) > 0;
        }
        return false;
    }

    @Override
    public boolean update(EmpLoan entity) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select deleteEmpLoan(?);", Key);
        if (rst.next()) {
            return rst.getInt(1) > 0;
        }
        return false;
    }

    @Override
    public EmpLoan search(String key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchEmpLoanById(?); ", key);

        if (rst.next()) {
            return new EmpLoan(key, rst.getString(2), rst.getString(3), rst.getString(4), rst.getBigDecimal(5));
        }
        return null;
    }

    @Override
    public ArrayList<EmpLoan> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllEmpLoan();");
        ArrayList<EmpLoan> allsalary = new ArrayList<>();

        while (rst.next()) {
            allsalary.add(new EmpLoan(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getBigDecimal(5)));
        }
        return allsalary;
    }

    @Override
    public ArrayList<EmpLoan> searchByemp(String empid) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchEmpLoanByEmpId(?); ", empid);
        ArrayList<EmpLoan> all = new ArrayList<>();

        while (rst.next()) {

            all.add(new EmpLoan(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getBigDecimal(5)));
        }
        return all;
    }

    @Override
    public ArrayList<EmpLoan> searchByid(String lid) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchEmpLoanByLId(?); ", lid);
        ArrayList<EmpLoan> all = new ArrayList<>();

        while (rst.next()) {

            all.add(new EmpLoan(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getBigDecimal(5)));
        }
        return all;
    }

    @Override
    public EmpLoan searchId() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchEmpLoanByAdidDescLimit1(); ");
        if (rst.next()) {
            return new EmpLoan(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getBigDecimal(5));
        }
        return null;
    }

    @Override
    public String getDate(String id) {
        try {
            ResultSet rst = CrudUtil.executeQuery("call searchLoanDate(?)", id);
            if (rst.next()) {
                return rst.getString(1);
            }

        } catch (Exception ex) {
            return "366";
        }
        return "366";
    }

}
