/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.ShortLeaveDAO;
import lk.ijse.payroll.entity.shortLeave;

/**
 *
 * @author udara
 */
public class ShortLeaveDAOImpl implements ShortLeaveDAO {

    @Override
    public boolean add(shortLeave e) throws Exception {
        return CrudUtil.executeUpdate("call addshortleave(?,?,?,?,?,?); ", e.getSLId(), e.getEmpId(), e.getDate(), e.getTimeOut(), e.getTimeIn(), e.getDuration()) > 0;

    }

    @Override
    public boolean update(shortLeave entity) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        return false;
    }

    @Override
    public shortLeave search(String key) throws Exception {
        return null;
    }

    @Override
    public ArrayList<shortLeave> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllshortleave(); ");
        ArrayList<shortLeave> all = new ArrayList<>();

        while (rst.next()) {

            all.add(new shortLeave(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getInt(6)));
        }
        return all;
    }

    @Override
    public ArrayList<shortLeave> getAll(String Id) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllshortleave2(?); ", Id);
        ArrayList<shortLeave> all = new ArrayList<>();

        while (rst.next()) {

            all.add(new shortLeave(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getInt(6)));
        }
        return all;
    }

    @Override
    public ArrayList<shortLeave> getAll(String id, String date) throws Exception {
        ResultSet r = CrudUtil.executeQuery("call searchYearMonthByDate(?);",date);
        if (r.next()) {

            String year = r.getString(1);
            String month = r.getString(2);
            

            ResultSet rst = CrudUtil.executeQuery("call searchAllshortleave3(?,?,?);",year,month,id);
            ArrayList<shortLeave> all = new ArrayList<>();
            while (rst.next()) {
               
                all.add(new shortLeave(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getInt(6)));
            }

            return all;
        }
        return null;
    }

}
