/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.DesignationDAO;
import lk.ijse.payroll.entity.Designation;

/**
 *
 * @author udara
 */
public class DesignationDAOImpl implements DesignationDAO {

    @Override
    public boolean add(Designation entity) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select addDes(?,?);", entity.getDeId(), entity.getDeName()) ;
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public boolean update(Designation entity) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select updateDes(?,?); ", entity.getDeName(), entity.getDeId());
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select deleteDes(?);" ,Key);
        if(rst.next()){
            return rst.getInt(1)>0;
        }
        return false;
    }

    @Override
    public Designation search(String key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchDesById(?); ", key);

        if (rst.next()) {
            return new Designation(key, rst.getString(2));
        }
        return null;
    }

    @Override
    public Designation searchId(String Key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchDesByName(?); ", Key);
        if (rst.next()) {
            return new Designation(rst.getString(1), Key);
        }
        return null;
    }

    @Override
    public ArrayList<Designation> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllDes();");
        ArrayList<Designation> allsalary = new ArrayList<>();

        while (rst.next()) {
            allsalary.add(new Designation(rst.getString(1), rst.getString(2)));
        }
        return allsalary;
    }

    @Override
    public Designation searchName(String deId) throws Exception {
        return null;
    }

    @Override
    public Designation search() throws Exception {
         ResultSet rst = CrudUtil.executeQuery("call searchDesByIdDescLimit1(); ");
         if (rst.next()) {
            return new Designation(rst.getString(1), rst.getString(2));
        }
        return null;
    }

}
