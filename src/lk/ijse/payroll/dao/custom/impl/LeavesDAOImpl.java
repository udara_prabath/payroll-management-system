/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.LeavesDAO;
import lk.ijse.payroll.entity.Leaves;

/**
 *
 * @author udara
 */
public class LeavesDAOImpl implements LeavesDAO {

    @Override
    public boolean add(Leaves entity) throws Exception {
        return CrudUtil.executeUpdate("call addleave(?,?,?,?,?);", entity.getLId(), entity.getEmpId(), entity.getDate(), entity.getTotal_Absants(), entity.getFree_Leaves()) > 0;
    }

    @Override
    public boolean update(Leaves entity) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        return false;
    }

    @Override
    public Leaves search(String key) throws Exception {
        
        return null;
    }

    @Override
    public ArrayList<Leaves> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllLeaves();");
        ArrayList<Leaves> all = new ArrayList<>();

        while (rst.next()) {
            //System.out.println(rst.getString(1) + "gh");
            all.add(new Leaves(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getInt(4), rst.getInt(5)));
        }
        return all;
    }

    @Override
    public ArrayList<Leaves> getAll(String Id) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllLeavesByEmp(?); ",Id);
        ArrayList<Leaves> all = new ArrayList<>();

        while (rst.next()) {
            
            all.add(new Leaves(rst.getInt(1), rst.getString(2), rst.getString(3), rst.getInt(4), rst.getInt(5)));
        }
        return all;

    }

}
