/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.LoanDAO;
import lk.ijse.payroll.entity.Loan;

/**
 *
 * @author udara
 */
public class LoanDAOImpl implements LoanDAO {

    @Override
    public boolean add(Loan entity) throws Exception {
        ResultSet rst =  CrudUtil.executeQuery("select addLoan(?,?,?);", entity.getLoanId(), entity.getLoanName(), entity.getAmount());
        if (rst.next()) {
            return rst.getInt(1) > 0;
        }
        return false;
    }

    @Override
    public boolean update(Loan entity) throws Exception {
        ResultSet rst =  CrudUtil.executeQuery("select updateLoan(?,?,?);", entity.getLoanName(), entity.getAmount(), entity.getLoanId());
        if (rst.next()) {
            return rst.getInt(1) > 0;
        }
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        ResultSet rst =  CrudUtil.executeQuery("select deleteLoan(?);", Key);
        if (rst.next()) {
            return rst.getInt(1) > 0;
        }
        return false;
    }

    @Override
    public Loan search(String key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchLoanById(?);", key);
        if (rst.next()) {
            return new Loan(key, rst.getString(2), rst.getBigDecimal(3));
        }
        return null;
    }

    @Override
    public ArrayList<Loan> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllLoan();");
        ArrayList<Loan> allLoan = new ArrayList<>();
        while (rst.next()) {
            allLoan.add(new Loan(rst.getString(1), rst.getString(2), rst.getBigDecimal(3)));
        }
        return allLoan;
    }

    @Override
    public Loan searchLoanByName(String name) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchLoanByName(?); ", name);
        if (rst.next()) {
            System.out.println(rst.getString(1) + "kk");
            return new Loan(rst.getString(1), rst.getString(2), rst.getBigDecimal(3));
        }
        return null;
    }

    @Override
    public Loan search() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchLoanByDescLimit1(); ");
        if (rst.next()) {
            return new Loan(rst.getString(1), rst.getString(2), rst.getBigDecimal(3));
        }
        return null;
    }

}
