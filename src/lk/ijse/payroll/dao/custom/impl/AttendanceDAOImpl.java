/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom.impl;

import java.sql.ResultSet;
import java.util.ArrayList;
import lk.ijse.payroll.dao.CrudUtil;
import lk.ijse.payroll.dao.custom.AttendanceDAO;
import lk.ijse.payroll.entity.Attendance;

/**
 *
 * @author udara
 */
public class AttendanceDAOImpl implements AttendanceDAO {

    
    public boolean add(Attendance entity,String s) throws Exception {
        return CrudUtil.executeUpdate("call addAttendance(?,?,?,?,?,?,?,?,?)", s,entity.getAttId(), entity.getEmpId(), entity.getDay(), entity.getDateIn(), entity.getTimeIn(), entity.getDateOut(), entity.getTimeOut(), entity.getWorkedHourse()) > 0;
    }

    @Override
    public boolean update(Attendance entity) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Key) throws Exception {
        return CrudUtil.executeUpdate("call deleteAttendance(?);",Key) > 0;
    }

    @Override
    public Attendance search(String key) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAttendanceById(?); ", key);
        if (rst.next()) {
            return new Attendance(key, rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getString(7), rst.getInt(8));
        }
        return null;
    }

    @Override
    public ArrayList<Attendance> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAllAttendance();");
        ArrayList<Attendance> allatt = new ArrayList<>();
        while (rst.next()) {

            allatt.add(new Attendance(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getString(7), rst.getInt(8)));

        }
        return allatt;
    }

    @Override
    public ArrayList<Attendance> getAll(String Id, int y, int m) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAttendanceByYearMonthEmp(?,?,?);", y, m, Id);
        ArrayList<Attendance> allatt = new ArrayList<>();
        while (rst.next()) {

            allatt.add(new Attendance(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getString(7), rst.getInt(8)));

        }
        return allatt;
    }

    @Override
    public ArrayList<Attendance> getAll(int y, int m) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAttendanceByYearMonth(?,?); ", y, m);
        ArrayList<Attendance> allatt = new ArrayList<>();
        while (rst.next()) {
            
            allatt.add(new Attendance(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getString(7), rst.getInt(8)));

        }
        return allatt;
    }

    @Override
    public Attendance searchLastId() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("call searchAttendanceByattidDescLimit1();");
        if (rst.next()) {

            return new Attendance(rst.getString(1), rst.getString(2), rst.getString(3), rst.getString(4), rst.getString(5), rst.getString(6), rst.getString(7), rst.getInt(8));
        }
        return null;
    }

    @Override
    public boolean add(Attendance entity) throws Exception {
        return false;
    }

}
