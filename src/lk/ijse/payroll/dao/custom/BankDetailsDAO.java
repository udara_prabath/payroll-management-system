/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao.custom;

import lk.ijse.payroll.dao.CrudDAO;
import lk.ijse.payroll.entity.BankDetails;
import lk.ijse.payroll.entity.BasicSalary;

/**
 *
 * @author udara
 */
public interface BankDetailsDAO extends CrudDAO<BankDetails,String>{

    public BankDetails searchBankId()throws Exception;

    public BankDetails getbankId(String id)throws Exception;
    
}
