/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao;

import com.lowagie.toolbox.plugins.Split;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import lk.ijse.payroll.db.DBConnection;

/**
 *
 * @author udara
 */
public class CrudUtil {
    public static PreparedStatement getPreparedStatement(String SQL,Object...parms)throws Exception{
        int parmsCount=SQL.split("[?]").length-1;
        if(parmsCount!=parms.length){
            throw new RuntimeException("parameters Count mismatch");
        }
        Connection connection=DBConnection.getInstance().getConnection();
        PreparedStatement stm = connection.prepareStatement(SQL);
        for (int i = 0; i < parms.length; i++) {
            stm.setObject(i+1, parms[i]);
        }
        return stm;
    }
    public static ResultSet executeQuery(String SQL,Object...parms) throws Exception{
        return getPreparedStatement(SQL, parms).executeQuery();
    }
    public static int executeUpdate(String SQL,Object...parms) throws Exception{
        return getPreparedStatement(SQL, parms).executeUpdate();
    }
    
}
