/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dao;

import lk.ijse.payroll.dao.custom.impl.AdvancesDAOImpl;
import lk.ijse.payroll.dao.custom.impl.AttendanceDAOImpl;
import lk.ijse.payroll.dao.custom.impl.BankDAOImpl;
import lk.ijse.payroll.dao.custom.impl.BankDetailsDAOImpl;
import lk.ijse.payroll.dao.custom.impl.BasicSalaryDAOImpl;
import lk.ijse.payroll.dao.custom.impl.BonusDAOImpl;
import lk.ijse.payroll.dao.custom.impl.DesignationDAOImpl;
import lk.ijse.payroll.dao.custom.impl.EmpAdvancesDAOImpl;


import lk.ijse.payroll.dao.custom.impl.EmpLoanDAOImpl;
import lk.ijse.payroll.dao.custom.impl.EmployeeDAOImpl;

import lk.ijse.payroll.dao.custom.impl.LeavesDAOImpl;
import lk.ijse.payroll.dao.custom.impl.LoanDAOImpl;
import lk.ijse.payroll.dao.custom.impl.OTDAOImpl;
import lk.ijse.payroll.dao.custom.impl.PayrollDAOImpl;
import lk.ijse.payroll.dao.custom.impl.QueryDAOImpl;
import lk.ijse.payroll.dao.custom.impl.SalaryDetailsDAOImpl;
import lk.ijse.payroll.dao.custom.impl.ShortLeaveDAOImpl;
import lk.ijse.payroll.dao.custom.impl.UserDAOImpl;

/**
 *
 * @author udara
 */
public class DAOFactory {

    public enum DAOTypes {
        Advances, Attendance, Bank, BankDetails, BasicSalary, Bonus, Designation, EmpAdvances,  EmpBonus, EmpLoan, Employee, Leaves, Loan, OT, Payroll, SalaryDetails, Query, ShortLeave,User;
    }
    public static DAOFactory daoFactory;

    private DAOFactory() {

    }

    public static DAOFactory getInstance() {
        if (daoFactory == null) {
            daoFactory = new DAOFactory();
        }
        return daoFactory;
    }

    public <T> T getDAO(DAOTypes daoTypes) {
        switch (daoTypes) {
            case Advances:
                return (T) new AdvancesDAOImpl();
            case SalaryDetails:
                return (T) new SalaryDetailsDAOImpl();
            case Attendance:
                return (T) new AttendanceDAOImpl();
            case Bank:
                return (T) new BankDAOImpl();
            case BankDetails:
                return (T) new BankDetailsDAOImpl();
            case BasicSalary:
                return (T) new BasicSalaryDAOImpl();
            case Bonus:
                return (T) new BonusDAOImpl();
            case Designation:
                return (T) new DesignationDAOImpl();
            case EmpAdvances:
                return (T) new EmpAdvancesDAOImpl();
           
            case EmpLoan:
                return (T) new EmpLoanDAOImpl();
            case Employee:
                return (T) new EmployeeDAOImpl();
            case Leaves:
                return (T) new LeavesDAOImpl();
            case Loan:
                return (T) new LoanDAOImpl();
            
            case OT:
                return (T) new OTDAOImpl();
            case Payroll:
                return (T) new PayrollDAOImpl();
            case Query:
                return (T) new QueryDAOImpl();
            case ShortLeave:
                return (T) new ShortLeaveDAOImpl();
            case User:
                return (T) new UserDAOImpl();

            default:
                return null;

        }
    }
}
