/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.entity;

import java.math.BigDecimal;

/**
 *
 * @author udara
 */
public class Loan implements SuperEntity{
    private String loanId;
    private String loanName;
    private BigDecimal amount;

    public Loan() {
    }

    public Loan(String loanId, String loanName, BigDecimal amount) {
        this.loanId = loanId;
        this.loanName = loanName;
        this.amount = amount;
    }

    /**
     * @return the loanId
     */
    public String getLoanId() {
        return loanId;
    }

    /**
     * @param loanId the loanId to set
     */
    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    /**
     * @return the loanName
     */
    public String getLoanName() {
        return loanName;
    }

    /**
     * @param loanName the loanName to set
     */
    public void setLoanName(String loanName) {
        this.loanName = loanName;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Loan{" + "loanId=" + loanId + ", loanName=" + loanName + ", amount=" + amount + '}';
    }
    
}
