/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.entity;

import java.math.BigDecimal;
import lk.ijse.payroll.dto.*;
import lk.ijse.payroll.dao.custom.impl.*;
import lk.ijse.payroll.dao.custom.EmpLoanDAO;

/**
 *
 * @author udara
 */
public class EmpLoan implements SuperEntity{
    private String elId;
    private String empId;

    
    private String loanId;
    
    private String date;
    private BigDecimal installment;

    public EmpLoan() {
    }

    @Override
    public String toString() {
        return "EmpLoan{" + "elId=" + elId + ", empId=" + empId + ", loanId=" + loanId + ", date=" + date + ", installment=" + installment + '}';
    }

    public EmpLoan(String elId, String empId, String loanId, String date, BigDecimal installment) {
        this.elId = elId;
        this.empId = empId;
        this.loanId = loanId;
        this.date = date;
        this.installment = installment;
    }

    /**
     * @return the elId
     */
    public String getElId() {
        return elId;
    }

    /**
     * @param elId the elId to set
     */
    public void setElId(String elId) {
        this.elId = elId;
    }

    /**
     * @return the empId
     */
    public String getEmpId() {
        return empId;
    }

    /**
     * @param empId the empId to set
     */
    public void setEmpId(String empId) {
        this.empId = empId;
    }

    /**
     * @return the loanId
     */
    public String getLoanId() {
        return loanId;
    }

    /**
     * @param loanId the loanId to set
     */
    public void setLoanId(String loanId) {
        this.loanId = loanId;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the installment
     */
    public BigDecimal getInstallment() {
        return installment;
    }

    /**
     * @param installment the installment to set
     */
    public void setInstallment(BigDecimal installment) {
        this.installment = installment;
    }

}
