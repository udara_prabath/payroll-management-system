/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.entity;

import java.math.BigDecimal;
import java.util.logging.Logger;

/**
 *
 * @author udara
 */
public class Query implements SuperEntity {

    

    private String BankName;
    private String Acc_no;
    private String empName;
    private String year;
    private String month;
    private int worked_hours;
    private int nOT;
    private int dOT;

    private String adName;
    private BigDecimal amount;
    
    private String LoanName;
    private BigDecimal installment;
    private String empId;
    private String date;
    private BigDecimal loanAmount;

    public Query() {
    }
    
    public Query(String adName,String year,String month, BigDecimal amount) {
        this.adName = adName;
        this.amount = amount;
        this.year=year;
        this.month=month;
    }
    
    public Query(String loanName,String date,BigDecimal amount){
        this.date=date;
        this.LoanName=loanName;
        this.loanAmount=amount;
    }

    public Query(String adName, BigDecimal amount) {
        this.adName = adName;
        this.amount = amount;
    }

    public Query(String BankName, String Acc_no) {
        this.BankName = BankName;
        this.Acc_no = Acc_no;
    }



    

    
    

    /**
     * @return the BankName
     */
    public String getBankName() {
        return BankName;
    }

    /**
     * @param BankName the BankName to set
     */
    public void setBankName(String BankName) {
        this.BankName = BankName;
    }

    /**
     * @return the Acc_no
     */
    public String getAcc_no() {
        return Acc_no;
    }

    /**
     * @param Acc_no the Acc_no to set
     */
    public void setAcc_no(String Acc_no) {
        this.Acc_no = Acc_no;
    }
//
    public Query(String empName, String year, String month, int worked_hours, int nOT, int dOT) {
        this.empName = empName;
        this.year = year;
        this.month = month;
        this.worked_hours = worked_hours;
        this.nOT = nOT;
        this.dOT = dOT;
    }

    /**
     * @return the empName
     */
    public String getEmpName() {
        return empName;
    }

    /**
     * @param empName the empName to set
     */
    public void setEmpName(String empName) {
        this.empName = empName;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * @return the month
     */
    public String getMonth() {
        return month;
    }

    /**
     * @param month the month to set
     */
    public void setMonth(String month) {
        this.month = month;
    }

    /**
     * @return the worked_hours
     */
    public int getWorked_hours() {
        return worked_hours;
    }

    /**
     * @param worked_hours the worked_hours to set
     */
    public void setWorked_hours(int worked_hours) {
        this.worked_hours = worked_hours;
    }

    /**
     * @return the nOT
     */
    public int getnOT() {
        return nOT;
    }

    /**
     * @param nOT the nOT to set
     */
    public void setnOT(int nOT) {
        this.nOT = nOT;
    }

    /**
     * @return the dOT
     */
    public int getdOT() {
        return dOT;
    }

    /**
     * @param dOT the dOT to set
     */
    public void setdOT(int dOT) {
        this.dOT = dOT;
    }

    /**
     * @return the adName
     */
    public String getAdName() {
        return adName;
    }

    /**
     * @param adName the adName to set
     */
    public void setAdName(String adName) {
        this.adName = adName;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the LoanName
     */
    public String getLoanName() {
        return LoanName;
    }

    /**
     * @return the installment
     */
    public BigDecimal getInstallment() {
        return installment;
    }

    /**
     * @return the empId
     */
    public String getEmpId() {
        return empId;
    }

    public Query(String LoanName, BigDecimal installment, String empId) {
        this.LoanName = LoanName;
        this.installment = installment;
        this.empId = empId;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @return the loanAmount
     */
    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    @Override
    public String toString() {
        return "Query{" + "BankName=" + BankName + ", Acc_no=" + Acc_no + ", empName=" + empName + ", year=" + year + ", month=" + month + ", worked_hours=" + worked_hours + ", nOT=" + nOT + ", dOT=" + dOT + ", adName=" + adName + ", amount=" + amount + ", LoanName=" + LoanName + ", installment=" + installment + ", empId=" + empId + ", date=" + date + ", loanAmount=" + loanAmount + ", PayDate=" + PayDate + ", normalOT=" + normalOT + ", doubleOT=" + doubleOT + ", EPF12=" + EPF12 + ", EPF8=" + EPF8 + ", EtF3=" + EtF3 + ", loan=" + loan + ", advance=" + advance + ", salary=" + salary + '}';
    }

   
    
    
    private String PayDate;
    private BigDecimal normalOT;
    private BigDecimal doubleOT;
    private BigDecimal EPF12;
    private BigDecimal EPF8;
    private BigDecimal EtF3;
    private BigDecimal loan;
    private BigDecimal advance;
    private BigDecimal salary;

    /**
     * @return the PayDate
     */
    public String getPayDate() {
        return PayDate;
    }

    /**
     * @return the normalOT
     */
    public BigDecimal getNormalOT() {
        return normalOT;
    }

    /**
     * @return the doubleOT
     */
    public BigDecimal getDoubleOT() {
        return doubleOT;
    }

    /**
     * @return the EPF12
     */
    public BigDecimal getEPF12() {
        return EPF12;
    }

    /**
     * @return the EPF8
     */
    public BigDecimal getEPF8() {
        return EPF8;
    }

    /**
     * @return the EtF3
     */
    public BigDecimal getEtF3() {
        return EtF3;
    }

    /**
     * @return the loan
     */
    public BigDecimal getLoan() {
        return loan;
    }

    /**
     * @return the advance
     */
    public BigDecimal getAdvance() {
        return advance;
    }

    /**
     * @return the salary
     */
    public BigDecimal getSalary() {
        return salary;
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }



    public Query(String PayDate, BigDecimal normalOT, BigDecimal doubleOT, BigDecimal EPF12, BigDecimal EPF8, BigDecimal EtF3, BigDecimal loan, BigDecimal advance, BigDecimal salary) {
        this.PayDate = PayDate;
        this.normalOT = normalOT;
        this.doubleOT = doubleOT;
        this.EPF12 = EPF12;
        this.EPF8 = EPF8;
        this.EtF3 = EtF3;
        this.loan = loan;
        this.advance = advance;
        this.salary = salary;
    }
}
