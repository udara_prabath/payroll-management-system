/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.entity;

/**
 *
 * @author udara
 */
 public class Employee implements SuperEntity{
    private String empId;
    private String empName;
    private String address;
    private String gender;
    private String NIC;
    private String D_O_B;
    private int phone_No;
    private String join_Date;
    private String deId;
    private int tot_Leaves;

    public Employee() {
    }

    @Override
    public String toString() {
        return "Employee{" + "empId=" + empId + ", empName=" + empName + ", address=" + address + ", gender=" + gender + ", NIC=" + NIC + ", D_O_B=" + D_O_B + ", phone_No=" + phone_No + ", join_Date=" + join_Date + ", deId=" + deId + ", tot_Leaves=" + tot_Leaves + '}';
    }

    public Employee(String empId,int tot_Leaves){
        this.empId = empId;
        this.tot_Leaves = tot_Leaves;
    }
    
    public Employee(String empId, String empName, String address, String gender, String NIC, String D_O_B, int phone_No, String join_Date, String deId, int tot_Leaves) {
        this.empId = empId;
        this.empName = empName;
        this.address = address;
        this.gender = gender;
        this.NIC = NIC;
        this.D_O_B = D_O_B;
        this.phone_No = phone_No;
        this.join_Date = join_Date;
        this.deId = deId;
        this.tot_Leaves = tot_Leaves;
    }

    /**
     * @return the empId
     */
    public String getEmpId() {
        return empId;
    }

    /**
     * @param empId the empId to set
     */
    public void setEmpId(String empId) {
        this.empId = empId;
    }

    /**
     * @return the empName
     */
    public String getEmpName() {
        return empName;
    }

    /**
     * @param empName the empName to set
     */
    public void setEmpName(String empName) {
        this.empName = empName;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the NIC
     */
    public String getNIC() {
        return NIC;
    }

    /**
     * @param NIC the NIC to set
     */
    public void setNIC(String NIC) {
        this.NIC = NIC;
    }

    /**
     * @return the D_O_B
     */
    public String getD_O_B() {
        return D_O_B;
    }

    /**
     * @param D_O_B the D_O_B to set
     */
    public void setD_O_B(String D_O_B) {
        this.D_O_B = D_O_B;
    }

    /**
     * @return the phone_No
     */
    public int getPhone_No() {
        return phone_No;
    }

    /**
     * @param phone_No the phone_No to set
     */
    public void setPhone_No(int phone_No) {
        this.phone_No = phone_No;
    }

    /**
     * @return the join_Date
     */
    public String getJoin_Date() {
        return join_Date;
    }

    /**
     * @param join_Date the join_Date to set
     */
    public void setJoin_Date(String join_Date) {
        this.join_Date = join_Date;
    }

    /**
     * @return the deId
     */
    public String getDeId() {
        return deId;
    }

    /**
     * @param deId the deId to set
     */
    public void setDeId(String deId) {
        this.deId = deId;
    }

    /**
     * @return the tot_Leaves
     */
    public int getTot_Leaves() {
        return tot_Leaves;
    }

    /**
     * @param tot_Leaves the tot_Leaves to set
     */
    public void setTot_Leaves(int tot_Leaves) {
        this.tot_Leaves = tot_Leaves;
    }

    
  
    
    
}
