/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.entity;

import java.math.BigDecimal;

/**
 *
 * @author udara
 */
public class EmpAdvances implements SuperEntity{

    @Override
    public String toString() {
        return "EmpAdvances{" + "eAdId=" + eAdId + ", empId=" + empId + ", aDId=" + aDId + ", month=" + month + ", year=" + year + ", monthlyInstallment=" + monthlyInstallment + '}';
    }

    private String eAdId;
    private String empId;
    private String aDId;
    private String month;
    private String year;
    private BigDecimal monthlyInstallment;

    public EmpAdvances() {
    }

    public EmpAdvances(String eAdId, String empId, String aDId, String month, String year, BigDecimal monthlyInstallment) {
        this.eAdId = eAdId;
        this.empId = empId;
        this.aDId = aDId;
        this.month = month;
        this.year = year;
        this.monthlyInstallment = monthlyInstallment;
    }

    /**
     * @return the eAdId
     */
    public String geteAdId() {
        return eAdId;
    }

    /**
     * @param eAdId the eAdId to set
     */
    public void seteAdId(String eAdId) {
        this.eAdId = eAdId;
    }

    /**
     * @return the empId
     */
    public String getEmpId() {
        return empId;
    }

    /**
     * @param empId the empId to set
     */
    public void setEmpId(String empId) {
        this.empId = empId;
    }

    /**
     * @return the aDId
     */
    public String getaDId() {
        return aDId;
    }

    /**
     * @param aDId the aDId to set
     */
    public void setaDId(String aDId) {
        this.aDId = aDId;
    }

    /**
     * @return the month
     */
    public String getMonth() {
        return month;
    }

    /**
     * @param month the month to set
     */
    public void setMonth(String month) {
        this.month = month;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * @return the monthlyInstallment
     */
    public BigDecimal getMonthlyInstallment() {
        return monthlyInstallment;
    }

    /**
     * @param monthlyInstallment the monthlyInstallment to set
     */
    public void setMonthlyInstallment(BigDecimal monthlyInstallment) {
        this.monthlyInstallment = monthlyInstallment;
    }
    
}
