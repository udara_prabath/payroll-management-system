/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.entity;

import java.math.BigDecimal;
import lk.ijse.payroll.dto.*;
import lk.ijse.payroll.dao.custom.impl.*;
import lk.ijse.payroll.dao.custom.SalaryDetailsDAO;
import sun.nio.cs.ext.Big5;

/**
 *
 * @author udara
 */
public class SalaryDetails implements SuperEntity{
   private int SDId;
   private String EmpId;
   private String BSId;

    public SalaryDetails() {
    }

    @Override
    public String toString() {
        return "SalaryDetails{" + "SDId=" + SDId + ", EmpId=" + EmpId + ", BSId=" + BSId + '}';
    }

    public SalaryDetails(int SDId, String EmpId, String BSId) {
        this.SDId = SDId;
        this.EmpId = EmpId;
        this.BSId = BSId;
    }

    /**
     * @return the SDId
     */
    public int getSDId() {
        return SDId;
    }

    /**
     * @param SDId the SDId to set
     */
    public void setSDId(int SDId) {
        this.SDId = SDId;
    }

    /**
     * @return the EmpId
     */
    public String getEmpId() {
        return EmpId;
    }

    /**
     * @param EmpId the EmpId to set
     */
    public void setEmpId(String EmpId) {
        this.EmpId = EmpId;
    }

    /**
     * @return the BSId
     */
    public String getBSId() {
        return BSId;
    }

    /**
     * @param BSId the BSId to set
     */
    public void setBSId(String BSId) {
        this.BSId = BSId;
    }

    
}