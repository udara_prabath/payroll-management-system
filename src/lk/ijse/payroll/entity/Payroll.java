/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.entity;

import java.math.BigDecimal;
import lk.ijse.payroll.dto.*;
import lk.ijse.payroll.dao.custom.impl.*;
import lk.ijse.payroll.dao.custom.PayrollDAO;

/**
 *
 * @author udara
 */
public class Payroll implements SuperEntity {

    private int PayId;
    private String EmpId;
    private String Date;
    private String Designation;
    private String Bank;
    private String Acc_no;
    private BigDecimal Basic_Salary;
    private String Day_worked;
    private BigDecimal Normal_OT;
    private BigDecimal Double_OT;
    private String Total_Leaves;
    private String Short_Leaves;
    private BigDecimal EPF_8;
    private BigDecimal EPF_12;
    private BigDecimal ETF_3;
    private BigDecimal Loan;
    private BigDecimal Advances;
    private BigDecimal Bonus;
    private BigDecimal Gross_Salary;
    private BigDecimal Total_Deduction;
    private BigDecimal Salary;

    public Payroll() {
    }

    public Payroll(int PayId, String EmpId, String Date, String Designation, String Bank, String Acc_no, BigDecimal Basic_Salary, String Day_worked, BigDecimal Normal_OT, BigDecimal Double_OT, String Total_Leaves, String Short_Leaves, BigDecimal EPF_8, BigDecimal EPF_12, BigDecimal ETF_3, BigDecimal Loan, BigDecimal Advances, BigDecimal Bonus, BigDecimal Gross_Salary, BigDecimal Total_Deduction, BigDecimal Salary) {
        this.PayId = PayId;
        this.EmpId = EmpId;
        this.Date = Date;
        this.Designation = Designation;
        this.Bank = Bank;
        this.Acc_no = Acc_no;
        this.Basic_Salary = Basic_Salary;
        this.Day_worked = Day_worked;
        this.Normal_OT = Normal_OT;
        this.Double_OT = Double_OT;
        this.Total_Leaves = Total_Leaves;
        this.Short_Leaves = Short_Leaves;
        this.EPF_8 = EPF_8;
        this.EPF_12 = EPF_12;
        this.ETF_3 = ETF_3;
        this.Loan = Loan;
        this.Advances = Advances;
        this.Bonus = Bonus;
        this.Gross_Salary = Gross_Salary;
        this.Total_Deduction = Total_Deduction;
        this.Salary = Salary;
    }
    
    public Payroll(BigDecimal Gross_Salary,BigDecimal Total_Deduction,BigDecimal salary) {
        this.Gross_Salary=Gross_Salary;
        this.Total_Deduction=Total_Deduction;
        this.Salary=salary;
    }
    /**
     * @return the PayId
     */
    public int getPayId() {
        return PayId;
    }

    /**
     * @param PayId the PayId to set
     */
    public void setPayId(int PayId) {
        this.PayId = PayId;
    }

    /**
     * @return the EmpId
     */
    public String getEmpId() {
        return EmpId;
    }

    /**
     * @param EmpId the EmpId to set
     */
    public void setEmpId(String EmpId) {
        this.EmpId = EmpId;
    }

    /**
     * @return the Date
     */
    public String getDate() {
        return Date;
    }

    /**
     * @param Date the Date to set
     */
    public void setDate(String Date) {
        this.Date = Date;
    }

    /**
     * @return the Designation
     */
    public String getDesignation() {
        return Designation;
    }

    /**
     * @param Designation the Designation to set
     */
    public void setDesignation(String Designation) {
        this.Designation = Designation;
    }

    /**
     * @return the Bank
     */
    public String getBank() {
        return Bank;
    }

    /**
     * @param Bank the Bank to set
     */
    public void setBank(String Bank) {
        this.Bank = Bank;
    }

    /**
     * @return the Acc_no
     */
    public String getAcc_no() {
        return Acc_no;
    }

    /**
     * @param Acc_no the Acc_no to set
     */
    public void setAcc_no(String Acc_no) {
        this.Acc_no = Acc_no;
    }

    /**
     * @return the Basic_Salary
     */
    public BigDecimal getBasic_Salary() {
        return Basic_Salary;
    }

    /**
     * @param Basic_Salary the Basic_Salary to set
     */
    public void setBasic_Salary(BigDecimal Basic_Salary) {
        this.Basic_Salary = Basic_Salary;
    }

    /**
     * @return the Day_worked
     */
    public String getDay_worked() {
        return Day_worked;
    }

    /**
     * @param Day_worked the Day_worked to set
     */
    public void setDay_worked(String Day_worked) {
        this.Day_worked = Day_worked;
    }

    /**
     * @return the Normal_OT
     */
    public BigDecimal getNormal_OT() {
        return Normal_OT;
    }

    /**
     * @param Normal_OT the Normal_OT to set
     */
    public void setNormal_OT(BigDecimal Normal_OT) {
        this.Normal_OT = Normal_OT;
    }

    /**
     * @return the Double_OT
     */
    public BigDecimal getDouble_OT() {
        return Double_OT;
    }

    /**
     * @param Double_OT the Double_OT to set
     */
    public void setDouble_OT(BigDecimal Double_OT) {
        this.Double_OT = Double_OT;
    }

    /**
     * @return the Total_Leaves
     */
    public String getTotal_Leaves() {
        return Total_Leaves;
    }

    /**
     * @param Total_Leaves the Total_Leaves to set
     */
    public void setTotal_Leaves(String Total_Leaves) {
        this.Total_Leaves = Total_Leaves;
    }

    /**
     * @return the Short_Leaves
     */
    public String getShort_Leaves() {
        return Short_Leaves;
    }

    /**
     * @param Short_Leaves the Short_Leaves to set
     */
    public void setShort_Leaves(String Short_Leaves) {
        this.Short_Leaves = Short_Leaves;
    }

    /**
     * @return the EPF_8
     */
    public BigDecimal getEPF_8() {
        return EPF_8;
    }

    /**
     * @param EPF_8 the EPF_8 to set
     */
    public void setEPF_8(BigDecimal EPF_8) {
        this.EPF_8 = EPF_8;
    }

    /**
     * @return the EPF_12
     */
    public BigDecimal getEPF_12() {
        return EPF_12;
    }

    /**
     * @param EPF_12 the EPF_12 to set
     */
    public void setEPF_12(BigDecimal EPF_12) {
        this.EPF_12 = EPF_12;
    }

    /**
     * @return the ETF_3
     */
    public BigDecimal getETF_3() {
        return ETF_3;
    }

    /**
     * @param ETF_3 the ETF_3 to set
     */
    public void setETF_3(BigDecimal ETF_3) {
        this.ETF_3 = ETF_3;
    }

    /**
     * @return the Loan
     */
    public BigDecimal getLoan() {
        return Loan;
    }

    /**
     * @param Loan the Loan to set
     */
    public void setLoan(BigDecimal Loan) {
        this.Loan = Loan;
    }

    /**
     * @return the Advances
     */
    public BigDecimal getAdvances() {
        return Advances;
    }

    /**
     * @param Advances the Advances to set
     */
    public void setAdvances(BigDecimal Advances) {
        this.Advances = Advances;
    }

    /**
     * @return the Bonus
     */
    public BigDecimal getBonus() {
        return Bonus;
    }

    /**
     * @param Bonus the Bonus to set
     */
    public void setBonus(BigDecimal Bonus) {
        this.Bonus = Bonus;
    }

    /**
     * @return the Gross_Salary
     */
    public BigDecimal getGross_Salary() {
        return Gross_Salary;
    }

    /**
     * @param Gross_Salary the Gross_Salary to set
     */
    public void setGross_Salary(BigDecimal Gross_Salary) {
        this.Gross_Salary = Gross_Salary;
    }

    /**
     * @return the Total_Deduction
     */
    public BigDecimal getTotal_Deduction() {
        return Total_Deduction;
    }

    /**
     * @param Total_Deduction the Total_Deduction to set
     */
    public void setTotal_Deduction(BigDecimal Total_Deduction) {
        this.Total_Deduction = Total_Deduction;
    }

    /**
     * @return the Salary
     */
    public BigDecimal getSalary() {
        return Salary;
    }

    /**
     * @param Salary the Salary to set
     */
    public void setSalary(BigDecimal Salary) {
        this.Salary = Salary;
    }

    

}
