/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.entity;

import java.math.BigDecimal;
import lk.ijse.payroll.dto.*;
import lk.ijse.payroll.dao.custom.impl.*;
import lk.ijse.payroll.dao.custom.OTDAO;

/**
 *
 * @author udara
 */
public class OT implements SuperEntity{
     private int OTId;
     private String Attid;
    private String EmpId;
    private String Date;
    private int Normal_OT;
    private int Double_OT;
    private BigDecimal Amount;

    public OT() {
    }

    public OT(int OTId, String Attid, String EmpId, String Date, int Normal_OT, int Double_OT, BigDecimal Amount) {
        this.OTId = OTId;
        this.Attid = Attid;
        this.EmpId = EmpId;
        this.Date = Date;
        this.Normal_OT = Normal_OT;
        this.Double_OT = Double_OT;
        this.Amount = Amount;
    }

    @Override
    public String toString() {
        return "OT{" + "OTId=" + OTId + ", Attid=" + Attid + ", EmpId=" + EmpId + ", Date=" + Date + ", Normal_OT=" + Normal_OT + ", Double_OT=" + Double_OT + ", Amount=" + Amount + '}';
    }
    

    /**
     * @return the OTId
     */
    public int getOTId() {
        return OTId;
    }

    /**
     * @param OTId the OTId to set
     */
    public void setOTId(int OTId) {
        this.OTId = OTId;
    }

    /**
     * @return the Attid
     */
    public String getAttid() {
        return Attid;
    }

    /**
     * @param Attid the Attid to set
     */
    public void setAttid(String Attid) {
        this.Attid = Attid;
    }

    /**
     * @return the EmpId
     */
    public String getEmpId() {
        return EmpId;
    }

    /**
     * @param EmpId the EmpId to set
     */
    public void setEmpId(String EmpId) {
        this.EmpId = EmpId;
    }

    /**
     * @return the Date
     */
    public String getDate() {
        return Date;
    }

    /**
     * @param Date the Date to set
     */
    public void setDate(String Date) {
        this.Date = Date;
    }

    /**
     * @return the Normal_OT
     */
    public int getNormal_OT() {
        return Normal_OT;
    }

    /**
     * @param Normal_OT the Normal_OT to set
     */
    public void setNormal_OT(int Normal_OT) {
        this.Normal_OT = Normal_OT;
    }

    /**
     * @return the Double_OT
     */
    public int getDouble_OT() {
        return Double_OT;
    }

    /**
     * @param Double_OT the Double_OT to set
     */
    public void setDouble_OT(int Double_OT) {
        this.Double_OT = Double_OT;
    }

    /**
     * @return the Amount
     */
    public BigDecimal getAmount() {
        return Amount;
    }

    /**
     * @param Amount the Amount to set
     */
    public void setAmount(BigDecimal Amount) {
        this.Amount = Amount;
    }

   
    
    
}
