/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.entity;

import lk.ijse.payroll.dto.*;
import lk.ijse.payroll.dao.custom.impl.*;
import lk.ijse.payroll.dao.custom.DesignationDAO;

/**
 *
 * @author udara
 */
public class Designation implements SuperEntity{

    private String DeId;
    private String DeName;

    @Override
    public String toString() {
        return "Designation{" + "DeId=" + DeId + ", DeName=" + DeName + '}';
    }

    public Designation(String DeId, String DeName) {
        this.DeId = DeId;
        this.DeName = DeName;
    }

    public Designation() {
    }

    /**
     * @return the DeId
     */
    public String getDeId() {
        return DeId;
    }

    /**
     * @param DeId the DeId to set
     */
    public void setDeId(String DeId) {
        this.DeId = DeId;
    }

    /**
     * @return the DeName
     */
    public String getDeName() {
        return DeName;
    }

    /**
     * @param DeName the DeName to set
     */
    public void setDeName(String DeName) {
        this.DeName = DeName;
    }
}
