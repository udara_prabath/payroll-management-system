/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.entity;

/**
 *
 * @author udara
 */
public class CustomEntity implements SuperEntity{
    private String sdid;
    private String adid;
    private String loanid;

    @Override
    public String toString() {
        return "CustomEntity{" + "sdid=" + sdid + ", adid=" + adid + ", loanid=" + loanid + '}';
    }

    public CustomEntity(String sdid, String adid, String loanid) {
        this.sdid = sdid;
        this.adid = adid;
        this.loanid = loanid;
    }

    public CustomEntity() {
    }

    /**
     * @return the sdid
     */
    public String getSdid() {
        return sdid;
    }

    /**
     * @param sdid the sdid to set
     */
    public void setSdid(String sdid) {
        this.sdid = sdid;
    }

    /**
     * @return the adid
     */
    public String getAdid() {
        return adid;
    }

    /**
     * @param adid the adid to set
     */
    public void setAdid(String adid) {
        this.adid = adid;
    }

    /**
     * @return the loanid
     */
    public String getLoanid() {
        return loanid;
    }

    /**
     * @param loanid the loanid to set
     */
    public void setLoanid(String loanid) {
        this.loanid = loanid;
    }
    
    
}
