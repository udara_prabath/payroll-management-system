/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.entity;

import java.math.BigDecimal;
import lk.ijse.payroll.dto.*;
import lk.ijse.payroll.dao.custom.impl.*;
import lk.ijse.payroll.dao.custom.BonusDAO;

/**
 *
 * @author udara
 */
public class Bonus implements SuperEntity{
    private String bonusId;
    private String bonusName;
    private BigDecimal amount;

    public Bonus() {
    }

    @Override
    public String toString() {
        return "Bonus{" + "bonusId=" + bonusId + ", bonusName=" + bonusName + ", amount=" + amount + '}';
    }

    public Bonus(String bonusId, String bonusName, BigDecimal amount) {
        this.bonusId = bonusId;
        this.bonusName = bonusName;
        this.amount = amount;
    }

    /**
     * @return the bonusId
     */
    public String getBonusId() {
        return bonusId;
    }

    /**
     * @param bonusId the bonusId to set
     */
    public void setBonusId(String bonusId) {
        this.bonusId = bonusId;
    }

    /**
     * @return the bonusName
     */
    public String getBonusName() {
        return bonusName;
    }

    /**
     * @param bonusName the bonusName to set
     */
    public void setBonusName(String bonusName) {
        this.bonusName = bonusName;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
