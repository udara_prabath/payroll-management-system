/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.entity;

import lk.ijse.payroll.dto.*;
import lk.ijse.payroll.dao.custom.impl.*;
import lk.ijse.payroll.dao.custom.LeavesDAO;

/**
 *
 * @author udara
 */
public class Leaves implements SuperEntity{
    private int LId;
    private String EmpId;
    private String Date;
    private int Total_Absants;
    private int Free_Leaves;

    public Leaves() {
    }

    public Leaves(int LId, String EmpId, String Date, int Total_Absants, int Free_Leaves) {
        this.LId = LId;
        this.EmpId = EmpId;
        this.Date = Date;
        this.Total_Absants = Total_Absants;
        this.Free_Leaves = Free_Leaves;
    }

    @Override
    public String toString() {
        return "Leaves{" + "LId=" + LId + ", EmpId=" + EmpId + ", Date=" + Date + ", Total_Absants=" + Total_Absants + ", Free_Leaves=" + Free_Leaves + '}';
    }

    /**
     * @return the LId
     */
    public int getLId() {
        return LId;
    }

    /**
     * @param LId the LId to set
     */
    public void setLId(int LId) {
        this.LId = LId;
    }

    /**
     * @return the EmpId
     */
    public String getEmpId() {
        return EmpId;
    }

    /**
     * @param EmpId the EmpId to set
     */
    public void setEmpId(String EmpId) {
        this.EmpId = EmpId;
    }

    /**
     * @return the Date
     */
    public String getDate() {
        return Date;
    }

    /**
     * @param Date the Date to set
     */
    public void setDate(String Date) {
        this.Date = Date;
    }

    /**
     * @return the Total_Absants
     */
    public int getTotal_Absants() {
        return Total_Absants;
    }

    /**
     * @param Total_Absants the Total_Absants to set
     */
    public void setTotal_Absants(int Total_Absants) {
        this.Total_Absants = Total_Absants;
    }

    /**
     * @return the Free_Leaves
     */
    public int getFree_Leaves() {
        return Free_Leaves;
    }

    /**
     * @param Free_Leaves the Free_Leaves to set
     */
    public void setFree_Leaves(int Free_Leaves) {
        this.Free_Leaves = Free_Leaves;
    }

   

    
}
