/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.entity;

import java.math.BigDecimal;

/**
 *
 * @author udara
 */
public class BasicSalary implements SuperEntity {
    private String bsId;
    private BigDecimal amount;

    public BasicSalary() {
    }

    @Override
    public String toString() {
        return "BasicSalary{" + "bsId=" + bsId + ", amount=" + amount + '}';
    }

    public BasicSalary(String bsId, BigDecimal amount) {
        this.bsId = bsId;
        this.amount = amount;
    }

    /**
     * @return the bsId
     */
    public String getBsId() {
        return bsId;
    }

    /**
     * @param bsId the bsId to set
     */
    public void setBsId(String bsId) {
        this.bsId = bsId;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    
}
