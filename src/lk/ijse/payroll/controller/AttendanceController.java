/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.AttendanceBO;
import lk.ijse.payroll.business.custom.DesignationBO;
import lk.ijse.payroll.business.custom.EmployeeBO;
import lk.ijse.payroll.business.custom.LeavesBO;
import lk.ijse.payroll.business.custom.ShortLeaveBO;
import lk.ijse.payroll.dto.AttendanceDTO;
import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.dto.LeavesDTO;
import lk.ijse.payroll.dto.shortLeaveDTO;

/**
 *
 * @author udara
 */
public class AttendanceController {

    private EmployeeBO emp;
    private DesignationBO des;
    private AttendanceBO att;
    private LeavesBO lev;
    private ShortLeaveBO sl;

    public AttendanceController() {
        emp = BOFactory.getInstance().getBO(BOFactory.BOTypes.Employee);
        des = BOFactory.getInstance().getBO(BOFactory.BOTypes.Designation);
        att = BOFactory.getInstance().getBO(BOFactory.BOTypes.Attendance);
        lev = BOFactory.getInstance().getBO(BOFactory.BOTypes.Leaves);
        sl = BOFactory.getInstance().getBO(BOFactory.BOTypes.ShortLeave);
    }

    public ArrayList<String> LoaDAllemployeeId() throws Exception {
        return emp.getAllemployee();
    }

    public ArrayList<String> LoadallemployeeName() throws Exception {
        return emp.getAllemployeeName();
    }

    public ArrayList<String> allDesignation() throws Exception {
        return des.getAllDesignations();
    }

    public EmployeeDTO searchempDetailsByName(String id) throws Exception {
        return emp.searchEmpDetails(id);
    }

    public String searchDesignation(String deId) throws Exception {
        return des.searchDes(deId);
    }

    public EmployeeDTO searchEmpByName(String name) throws Exception {
        return emp.searchEmployeeId(name);
    }

    public boolean saveAttendance(String AttendanceId, String EmpId, String Day, String DateIn, String TimeIn, String DateOut, String TimeOut, String designation) throws Exception {
        String dateStart = DateIn + " " + TimeIn;
        String dateEnd = DateOut + " " + TimeOut;
        SimpleDateFormat format = new SimpleDateFormat("yyyy:MM:dd HH.mm");

        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = d2.getTime() - d1.getTime();
        long diffHours = diff / (60 * 60 * 1000);
        System.out.println(diffHours);

        AttendanceDTO attendance = new AttendanceDTO(AttendanceId, EmpId, Day, DateIn, TimeIn, DateOut, TimeOut, (int) diffHours);
        return att.add(attendance, designation);

    }

    public ArrayList<AttendanceDTO> getAllAttendance() throws Exception {
        return att.getAll();
    }

    public boolean markLeaves(int leaveId, String empId, String leavedate, int ab, int free) throws Exception {
        return lev.markLeave(leaveId, empId, leavedate, ab, free);
    }

    public ArrayList<LeavesDTO> getAllLeaves() throws Exception {
        return lev.getAll();
    }

    public boolean delete(String toString) throws Exception {
        return att.delete(toString);
    }

    public ArrayList<LeavesDTO> getAllLeaves(String Id) throws Exception {
        return lev.getAll(Id);
    }

    public boolean markLeaves(int i, String empId, String date, String out, String in) throws Exception {
        String dateStart = date + " " + out;
        String dateEnd = date + " " + in;
        SimpleDateFormat format = new SimpleDateFormat("yyyy:MM:dd HH.mm");

        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse(dateStart);
            d2 = format.parse(dateEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = d2.getTime() - d1.getTime();
        long diffHours = diff / (60 * 60 * 1000);
        System.out.println(diffHours);

        shortLeaveDTO a = new shortLeaveDTO(0, empId, date, out, in, (int) diffHours);

        return sl.add(a);

    }

    public ArrayList<shortLeaveDTO> getAllShortLeaves(String Id) throws Exception {
        return sl.getAll(Id);
    }

    public ArrayList<shortLeaveDTO> getAllShortLeaves() throws Exception {
        return sl.getAll();
    }

    public ArrayList<AttendanceDTO> getAttendanceDetails(String Id, int y, int m) throws Exception {
        return att.getAll(Id, y, m);
    }

    public ArrayList<AttendanceDTO> getAttendanceDetails(int y, int m)throws Exception  {
        return att.getAll(y, m);
    }

    public String searchAttId() throws Exception {
        return att.search();
    }

}
