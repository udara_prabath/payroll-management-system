/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.util.ArrayList;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.BankBO;
import lk.ijse.payroll.business.custom.Impl.BankBOImpl;
import lk.ijse.payroll.dto.BankDTO;
import lk.ijse.payroll.entity.Bank;

/**
 *
 * @author udara
 */
public class BankController {
    private BankBO bo;

    public BankController() {
        bo=BOFactory.getInstance().getBO(BOFactory.BOTypes.Bank);
    }
    public ArrayList<BankDTO> getAll() throws Exception{
        return bo.getAll();
    }
    public boolean delete(String id) throws Exception{
        return bo.delete(id);
    }
    public boolean add(BankDTO bank) throws Exception{
        return bo.add(bank);
    }
    public boolean update(BankDTO bank) throws Exception{
        return bo.update(bank);
    }
    public Bank search(String id) throws Exception{
        return bo.search(id);
    }

    public String searchId()throws Exception {
        return bo.search();
    }
    
}
