/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.util.ArrayList;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.DesignationBO;
import lk.ijse.payroll.business.custom.EmployeeBO;
import lk.ijse.payroll.business.custom.OTBO;
import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.dto.OTDTO;

/**
 *
 * @author udara
 */
public class OTController {

    private OTBO bo;
    private EmployeeBO emp;
    private DesignationBO des;

    public OTController() {
        bo = BOFactory.getInstance().getBO(BOFactory.BOTypes.OT);
        emp = BOFactory.getInstance().getBO(BOFactory.BOTypes.Employee);
        des = BOFactory.getInstance().getBO(BOFactory.BOTypes.Designation);
    }

    public boolean delete(String id) throws Exception {
        return bo.delete(id);
    }

    public ArrayList<OTDTO> getAll() throws Exception {
        return bo.getAll();
    }

    public ArrayList<String> LoaDAllemployeeId() throws Exception {
        return emp.getAllemployee();
    }

    public ArrayList<String> LoadallemployeeName() throws Exception {
        return emp.getAllemployeeName();
    }

    public EmployeeDTO searchempDetailsByName(String id) throws Exception {
        return emp.searchEmpDetails(id);
    }

    public String searchDesignation(String deId) throws Exception {
        return des.searchDes(deId);
    }

    public EmployeeDTO searchEmpByName(String name) throws Exception {
         return emp.searchEmployeeId(name);
    }

    public ArrayList<OTDTO> getAll(String id) throws Exception {
        return bo.getAll(id);
    }
}
