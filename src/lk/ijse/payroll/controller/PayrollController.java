/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.BankDetailsBO;
import lk.ijse.payroll.business.custom.BasicSalaryBO;
import lk.ijse.payroll.business.custom.DesignationBO;
import lk.ijse.payroll.business.custom.EmpAdvancesBO;
import lk.ijse.payroll.business.custom.EmpLoanBO;
import lk.ijse.payroll.business.custom.EmployeeBO;
import lk.ijse.payroll.business.custom.PayrollBO;
import lk.ijse.payroll.business.custom.ShortLeaveBO;
import lk.ijse.payroll.dto.BankDetailsDTO;
import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.dto.PayrollDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.dto.shortLeaveDTO;
import lk.ijse.payroll.entity.Query;

/**
 *
 * @author udara
 */
public class PayrollController {

    private EmployeeBO emp;
    private BasicSalaryBO sal;
    private BankDetailsBO bd;
    private PayrollBO pay;
    private EmpAdvancesBO EAD;
    private EmpLoanBO el;
    private DesignationBO de;
    private ShortLeaveBO sl;

    public PayrollController() {
        emp = BOFactory.getInstance().getBO(BOFactory.BOTypes.Employee);
        sal = BOFactory.getInstance().getBO(BOFactory.BOTypes.BasicSalary);
        bd = BOFactory.getInstance().getBO(BOFactory.BOTypes.BankDetails);
        pay = BOFactory.getInstance().getBO(BOFactory.BOTypes.Payroll);
        EAD = BOFactory.getInstance().getBO(BOFactory.BOTypes.EmpAdvances);
        el = BOFactory.getInstance().getBO(BOFactory.BOTypes.EmpLoan);
        de = BOFactory.getInstance().getBO(BOFactory.BOTypes.Designation);
        sl = BOFactory.getInstance().getBO(BOFactory.BOTypes.ShortLeave);
    }

    public String searchPayrollId() {
        return null;
    }

    public EmployeeDTO searchemp(String id) throws Exception {
        return emp.searchEmpDetails(id);
    }

    public String searchBasicSalary(String id) throws Exception {
        return sal.searchSalaryName(id);
    }

    public ArrayList<String> LoaDAllemployeeId() throws Exception {
        return emp.getAllemployee();
    }

    public ArrayList<String> LoadallemployeeName() throws Exception {
        return emp.getAllemployeeName();
    }

    public EmployeeDTO searchEmpId(String name) throws Exception {
        return emp.searchEmployeeId(name);
    }

    public QueryDTO searchBankDetails(String id) throws Exception {
        return bd.getBankDetails(id);
    }

    public ArrayList<QueryDTO> getAllDetails(String id, String date) throws Exception {
        return pay.getAllDetails(id, date);
    }

    public String calculateAmount(BigDecimal f) {

        BigDecimal ne = f.multiply(new BigDecimal(8));
        BigDecimal nn = ne.divide(new BigDecimal(100), RoundingMode.HALF_UP);
        String n = nn.toString();
        return n;

    }

    public QueryDTO searchAdvancesDetail(String id, String date) throws Exception {
        return EAD.searchAdvanceDetails(id, date);
    }

    public String searchDes(String deId) throws Exception {
        return de.searchDes(deId);
    }

    public QueryDTO searchForBonus(String de, int a) {
        if (de.equals("pumper") & a >= 10) {
            int x = a - 10;
            int z = (810 * x) + 4500;
            String amount = "" + z;
            return new QueryDTO(null, "Attendance Bonus", amount);
        } else if (a >= 25) {
            int x = a - 25;
            int z = (810 * x) + 5000;
            String amount = "" + z;
            return new QueryDTO(null, "Attendance Bonus", amount);
        }
        return null;
    }

    public QueryDTO seachLoanDetails(String id, String date) throws Exception {
        return el.getLoanDetails(id, date);
    }

    public ArrayList<shortLeaveDTO> getAllSLDetails(String id, String date) throws Exception {
        return sl.getdetails(id, date);
    }

    public String calculateep12(BigDecimal f) {
        BigDecimal ne = f.multiply(new BigDecimal(12));
        BigDecimal nn = ne.divide(new BigDecimal(100), RoundingMode.HALF_UP);
        String n = nn.toString();
        return n;
    }

    public String calculateet3(BigDecimal f) {
        BigDecimal ne = f.multiply(new BigDecimal(3));
        BigDecimal nn = ne.divide(new BigDecimal(100), RoundingMode.HALF_UP);
        String n = nn.toString();
        return n;
    }

    public PayrollDTO calculateSalary(String bs, String not, String dot, String ep8, String nol, String shl, String bonus, String adv, String loan) throws Exception {
        return pay.calculateSalary(bs, not, dot, ep8, nol, shl, bonus, adv, loan);

    }

    public boolean savePayroll(int i, String id, String date, String designation, String empbank, String accno, String bs, String wd, String not, String dot, String totlevs, String shl, String ep8, String ep12, String et3, String loan, String adv, String bonus, String gross, String totDed, String Net) throws Exception {
        BigDecimal basic = new BigDecimal(bs);
        BigDecimal Not = new BigDecimal(not);
        BigDecimal Dot = new BigDecimal(dot);
        BigDecimal epf8 = new BigDecimal(ep8);
        BigDecimal epf12 = new BigDecimal(ep12);
        BigDecimal etf3 = new BigDecimal(et3);
        BigDecimal l = new BigDecimal(loan);
        BigDecimal ad = new BigDecimal(adv);
        BigDecimal b = new BigDecimal(bonus);
        BigDecimal gro = new BigDecimal(gross);
        BigDecimal tot = new BigDecimal(totDed);
        BigDecimal net = new BigDecimal(Net);

        
        BigDecimal Namount = new BigDecimal(0.00);
        BigDecimal Damount = new BigDecimal(0.00);

        BigDecimal n = new BigDecimal(90.00);
        n.precision();
        BigDecimal d = new BigDecimal(180.00);
        d.precision();

        
        Namount = n.multiply(Not);
        Damount=d.multiply(Dot);

        return pay.add(new PayrollDTO(i, id, date, designation, empbank, accno, basic, wd, Namount, Damount, totlevs, shl, epf8, epf12, etf3, l, ad, b, gro, tot, net));
    }

    public ArrayList<String> getEmployeeIds(String date) throws Exception{
        return pay.getEmployeeIds(date);
    }

}
