/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.util.ArrayList;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.BankBO;
import lk.ijse.payroll.business.custom.BankDetailsBO;
import lk.ijse.payroll.business.custom.BasicSalaryBO;
import lk.ijse.payroll.business.custom.DesignationBO;
import lk.ijse.payroll.business.custom.EmpAdvancesBO;
import lk.ijse.payroll.business.custom.EmpLoanBO;
import lk.ijse.payroll.business.custom.EmployeeBO;
import lk.ijse.payroll.business.custom.PayrollBO;
import lk.ijse.payroll.business.custom.SalaryDetailsBO;
import lk.ijse.payroll.dto.BankDTO;
import lk.ijse.payroll.dto.BankDetailsDTO;
import lk.ijse.payroll.dto.DesignationDTO;
import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.dto.PayrollDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.dto.SalaryDetailsDTO;

/**
 *
 * @author udara
 */
public class EmployeeReviewController {

    private EmployeeBO bo;
    private DesignationBO de;
    private BankBO bank;
    private BasicSalaryBO sal;
    private SalaryDetailsBO sd;
    private BankDetailsBO bd;
    private EmpLoanBO q;
    private EmpAdvancesBO a;
    private PayrollBO p;

    public EmployeeReviewController() {
        bo = BOFactory.getInstance().getBO(BOFactory.BOTypes.Employee);
        de = BOFactory.getInstance().getBO(BOFactory.BOTypes.Designation);
        bank = BOFactory.getInstance().getBO(BOFactory.BOTypes.Bank);
        sal = BOFactory.getInstance().getBO(BOFactory.BOTypes.BasicSalary);
        sd = BOFactory.getInstance().getBO(BOFactory.BOTypes.SalaryDetails);
        bd = BOFactory.getInstance().getBO(BOFactory.BOTypes.BankDetails);
        q=BOFactory.getInstance().getBO(BOFactory.BOTypes.EmpLoan);
        p=BOFactory.getInstance().getBO(BOFactory.BOTypes.Payroll);
        a=BOFactory.getInstance().getBO(BOFactory.BOTypes.EmpAdvances);
    }

    public boolean add(EmployeeDTO employee, BankDetailsDTO bank, SalaryDetailsDTO salary) throws Exception {
        return bo.add(employee, bank, salary);
    }

    public DesignationDTO search(String id) throws Exception {
        return de.searchId(id);
    }

    public ArrayList<String> allDesignation() throws Exception {
        return de.getAllDesignations();
    }

    public ArrayList<String> allBanks() throws Exception {
        return bank.getAllBanks();
    }

    public ArrayList<String> allSalary() throws Exception {
        return sal.getAllsalary();
    }

    public String searchsalaryId(String toString) throws Exception {

        return sal.searchSalaryId(toString);
    }

    public BankDTO searchbankId(String toString) throws Exception {
        return bank.searchBankId(toString);
    }

    public String searchId() throws Exception {
        return sd.searchId();
    }

    public String searchbdId() throws Exception {
        return bd.searchbank();
    }

    public String searchempId() throws Exception {
        return bo.search();
    }

    public ArrayList<String> allemployee() throws Exception {
        return bo.getAllemployee();
    }

    public EmployeeDTO searchemp(String id) throws Exception {
        return bo.searchEmpDetails(id);
    }

    public String searchDes(String deId) throws Exception {
        return de.searchDes(deId);
    }

    public String searchSal(String empId) throws Exception {
        return sal.searchSalaryName(empId);
    }

    public String searchBankDetailId(String toString) throws Exception {
        return bd.getBdid(toString);
    }

    public String searchSalaryDetailId(String toString) throws Exception {
        return sd.getSalarydetailId(toString);
    }

    public boolean updateemployee(EmployeeDTO employee, SalaryDetailsDTO salary, BankDetailsDTO bd) throws Exception {
        return bo.update(employee, salary, bd);
    }

    public ArrayList<String> allemployeeName() throws Exception {
        return bo.getAllemployeeName();
    }

    public EmployeeDTO searchempid(String id) throws Exception {
        return bo.searchEmpID(id);
    }

    public ArrayList<EmployeeDTO> getAll() throws Exception {
        return bo.getAll();
    }

    public boolean delete(String toString) throws Exception {
        return bo.delete(toString);
    }

    public ArrayList<EmployeeDTO> searchempBydes(String deId) throws Exception {
        return bo.getAll(deId);
    }

    public QueryDTO serachBankDetails(String empId) throws Exception {
        return bd.getBankDetails(empId);
    }

    public ArrayList<QueryDTO> getLoan(String id)throws Exception  {
        return q.searchempLoans(id);
    }

    public ArrayList<QueryDTO> getAllAdvances(String id) throws Exception {
        return a.getDetails(id);
    }

    public ArrayList<PayrollDTO> getAllPayroll(String id) throws Exception{
        return p.getAlByEmpId(id);
    }

}
