/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.util.ArrayList;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.DesignationBO;
import lk.ijse.payroll.dto.DesignationDTO;
import lk.ijse.payroll.entity.Designation;

/**
 *
 * @author udara
 */
public class DesignationController {
    private DesignationBO bo;

    public DesignationController() {
        bo=BOFactory.getInstance().getBO(BOFactory.BOTypes.Designation);
    }
    
    public ArrayList<DesignationDTO> getAll() throws Exception{
        return bo.getAll();
    }
    public boolean delete(String id) throws Exception{
        return bo.delete(id);
    }
    public boolean add(DesignationDTO advance) throws Exception{
        return bo.add(advance);
    }
    public boolean update(DesignationDTO advance) throws Exception{
        return bo.update(advance);
    }
    public Designation search(String id) throws Exception{
        return bo.search(id);
    }

    public String searchId()throws Exception {
        return bo.search();
    }
}
