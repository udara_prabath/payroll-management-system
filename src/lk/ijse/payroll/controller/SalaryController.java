/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.util.ArrayList;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.BasicSalaryBO;
import lk.ijse.payroll.dto.BasicSalaryDTO;
import lk.ijse.payroll.dto.LoanDTO;
import lk.ijse.payroll.entity.BasicSalary;

/**
 *
 * @author udara
 */
public class SalaryController {
    private BasicSalaryBO salary;

    public SalaryController() {
        salary=BOFactory.getInstance().getBO(BOFactory.BOTypes.BasicSalary);
    }
    public boolean addSalary(BasicSalaryDTO basic) throws Exception{
        return salary.addSalary(basic);
    }
    public boolean updateSalary(BasicSalaryDTO basic) throws Exception{
        return salary.updateSalary(basic);
    }
    public boolean deleteSalary(String Id) throws Exception{
        return salary.deleteSalary(Id);
    }
    public ArrayList<BasicSalaryDTO>getAll() throws Exception{
        return salary.getAllSalary();
    }
    public BasicSalary searchSalary(String Id) throws Exception{
        return salary.searchSalary(Id);
    }

    public String searchId() throws Exception {
        return salary.searchSalaryId();
    }
    
}
