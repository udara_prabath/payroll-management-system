/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.util.ArrayList;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.AdvancesBO;
import lk.ijse.payroll.dto.AdvancesDTO;
import lk.ijse.payroll.entity.Advances;

/**
 *
 * @author udara
 */
public class AdvancesController {
    private AdvancesBO bo;

    public AdvancesController() {
        bo=BOFactory.getInstance().getBO(BOFactory.BOTypes.Advances);
    }
    
    public ArrayList<AdvancesDTO> getAll() throws Exception{
        return bo.getAll();
    }
    public boolean delete(String id) throws Exception{
        return bo.delete(id);
    }
    public boolean add(AdvancesDTO advance) throws Exception{
        return bo.add(advance);
    }
    public boolean update(AdvancesDTO advance) throws Exception{
        return bo.update(advance);
    }
    public Advances search(String id) throws Exception{
        return bo.search(id);
    }

    public String searchId()  throws Exception{
       return bo.search();
    }
    
}
