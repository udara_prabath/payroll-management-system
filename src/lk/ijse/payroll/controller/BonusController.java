/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.util.ArrayList;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.BonusBO;
import lk.ijse.payroll.dto.BonusDTO;
import lk.ijse.payroll.entity.Bonus;

/**
 *
 * @author udara
 */
public class BonusController {
     private BonusBO ba;

    public BonusController() {
        ba=BOFactory.getInstance().getBO(BOFactory.BOTypes.Bonus);
    }
    public boolean add(BonusDTO basic) throws Exception{
        return ba.add(basic);
    }
    public boolean update(BonusDTO basic) throws Exception{
        return ba.update(basic);
    }
    public boolean delete(String Id) throws Exception{
        return ba.delete(Id);
    }
    public ArrayList<BonusDTO>getAll() throws Exception{
        return ba.getAll();
    }
    public Bonus search(String Id) throws Exception{
        return ba.search(Id);
    }

    public String searchId() throws Exception {
        return ba.search();
    }
}
