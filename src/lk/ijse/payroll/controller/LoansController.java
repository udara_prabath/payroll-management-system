/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.util.ArrayList;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.LoanBO;
import lk.ijse.payroll.dto.LoanDTO;
import lk.ijse.payroll.entity.Loan;

/**
 *
 * @author udara
 */
public class LoansController {

    private LoanBO loanBO;

    public LoansController() {
        loanBO = BOFactory.getInstance().getInstance().getBO(BOFactory.BOTypes.Loan);
    }

    public boolean addLoan(LoanDTO loan) throws Exception {
        return loanBO.addLoan(loan);
    }

    public boolean updateLoan(LoanDTO loan) throws Exception {
        return loanBO.updateLoan(loan);
    }

    public boolean deleteLoan(String loanId) throws Exception {
        return loanBO.deleteLoan(loanId);
    }

    public ArrayList<LoanDTO> getAll() throws Exception {
        return loanBO.getAllLoans();
    }

    public String searchId()throws Exception  {
        return loanBO.search();
    }

}
