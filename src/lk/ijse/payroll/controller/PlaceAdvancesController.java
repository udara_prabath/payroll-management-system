/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.AdvancesBO;
import lk.ijse.payroll.business.custom.DesignationBO;
import lk.ijse.payroll.business.custom.EmpAdvancesBO;
import lk.ijse.payroll.business.custom.EmployeeBO;
import lk.ijse.payroll.dto.AdvancesDTO;
import lk.ijse.payroll.dto.EmpAdvancesDTO;
import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.entity.Advances;

/**
 *
 * @author udara
 */
public class PlaceAdvancesController {

    private EmployeeBO emp;
    private DesignationBO des;
    private AdvancesBO adv;

    private EmpAdvancesBO EAD;

    public PlaceAdvancesController() {
        emp = BOFactory.getInstance().getBO(BOFactory.BOTypes.Employee);
        des = BOFactory.getInstance().getBO(BOFactory.BOTypes.Designation);
        adv = BOFactory.getInstance().getBO(BOFactory.BOTypes.Advances);

        EAD = BOFactory.getInstance().getBO(BOFactory.BOTypes.EmpAdvances);
    }

    public ArrayList<String> LoadallemployeeName() throws Exception {
        return emp.getAllemployeeName();
    }

    public ArrayList<String> LoaDAllemployeeId() throws Exception {
        return emp.getAllemployee();
    }

    public ArrayList<String> loadAllAdvances() throws Exception {
        return adv.getAllAdvanceNames();
    }

    public EmployeeDTO searchemp(String id) throws Exception {
        return emp.searchEmpDetails(id);
    }

    public String searchDes(String deId) throws Exception {
        return des.searchDes(deId);
    }

    public EmployeeDTO searchEmpId(String name) throws Exception {
        return emp.searchEmployeeId(name);
    }

    public AdvancesDTO searchAdvance(String ad) throws Exception {
        return adv.searchAdvance(ad);
    }

    public boolean addAdvance(String eadid, String empid, String adid, String mon, String yer, BigDecimal amount) throws Exception {

        

        EmpAdvancesDTO advance = new EmpAdvancesDTO(eadid, empid, adid, mon, yer, amount);
        return EAD.add(advance);
    }

    public ArrayList<EmpAdvancesDTO> getAllAdvances() throws Exception {
        return EAD.getAll();
    }

    public boolean delete(String toString) throws Exception {
        return EAD.delete(toString);
    }

    public ArrayList<EmpAdvancesDTO> getEmpAdvances(String adv) throws Exception {
        return EAD.getEmpAdVances(adv);
    }

    public String searchLastId() throws Exception {
        return EAD.searchLastId();
    }

    public String searchAdvanceId(String adId) throws Exception {

        return adv.searchadid(adId);
    }

    public ArrayList<EmpAdvancesDTO> getAdvances(String empid) throws Exception {
        return EAD.getAdvances(empid);
    }

    public String searchAttId() throws Exception {
        return EAD.searchLastId();
    }

    public boolean checkAvailabilety(int year, int month,String emp) {
        return EAD.checkAvailabiity(year,month, emp);
    }

}
