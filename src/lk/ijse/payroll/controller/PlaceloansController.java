/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import javax.swing.JTextField;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.DesignationBO;
import lk.ijse.payroll.business.custom.EmpLoanBO;
import lk.ijse.payroll.business.custom.EmployeeBO;
import lk.ijse.payroll.business.custom.LoanBO;
import lk.ijse.payroll.dto.EmpAdvancesDTO;
import lk.ijse.payroll.dto.EmpLoanDTO;
import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.dto.LoanDTO;

/**
 *
 * @author udara
 */
public class PlaceloansController {

    private EmployeeBO emp;
    private DesignationBO des;
    private LoanBO loan;
    private EmpLoanBO el;

    public PlaceloansController() {
        emp = BOFactory.getInstance().getBO(BOFactory.BOTypes.Employee);
        des = BOFactory.getInstance().getBO(BOFactory.BOTypes.Designation);
        this.loan = BOFactory.getInstance().getBO(BOFactory.BOTypes.Loan);
        el = BOFactory.getInstance().getBO(BOFactory.BOTypes.EmpLoan);
    }

    public EmployeeDTO searchempDetailsByName(String id) throws Exception {
        return emp.searchEmpDetails(id);
    }

    public String searchDesignation(String deId) throws Exception {
        return des.searchDes(deId);
    }

    public ArrayList<String> LoaDAllemployeeId() throws Exception {
        return emp.getAllemployee();
    }

    public ArrayList<String> LoadallemployeeName() throws Exception {
        return emp.getAllemployeeName();
    }

    public ArrayList<String> loadAllLoans() throws Exception {
        return loan.getAllloanNames();

    }

    public EmployeeDTO searchEmpByName(String name) throws Exception {
        return emp.searchEmployeeId(name);
    }

    public LoanDTO searchLoanDetailsByName(String name) throws Exception {
        return loan.searchLoanByname(name);
    }

    public boolean addLoan(String elid, String empid, String loanid, String date, BigDecimal amount) throws Exception {

        BigDecimal divide = amount.divide(new BigDecimal(12), RoundingMode.HALF_UP);

        EmpLoanDTO advance = new EmpLoanDTO(elid, empid, loanid, date, divide);
        return loan.addLoan(advance);
    }

    public ArrayList<EmpLoanDTO> getAllLoans() throws Exception {
        return el.getAll();
    }

    public String searchLoanName(String loanId) throws Exception {
        return loan.searchLoanName(loanId);
    }

    public EmployeeDTO searchemp(String empId) throws Exception {
        return emp.searchEmpName(empId);
    }

    public boolean delete(String toString) throws Exception {
        return el.delete(toString);
    }

    public EmployeeDTO searchEmpId(String name) throws Exception {
        return emp.searchEmployeeId(name);
    }

    public ArrayList<EmpLoanDTO> getLoan(String empid) throws Exception {
        return el.getLoans(empid);
    }

    public ArrayList<EmpLoanDTO> searchLoanDetailsByloanId(String lid) throws Exception {
        return el.getLoanById(lid);
    }

    public String searchLoanId() throws Exception{
        return el.search();
    }

    public boolean cheackAvailability(String date,String empid){
       return el.checkAvailability(date,empid);
    }

}
