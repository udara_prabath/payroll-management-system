/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.util.ArrayList;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.EmployeeBO;
import lk.ijse.payroll.business.custom.PayrollBO;
import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.dto.PayrollDTO;

/**
 *
 * @author udara
 */
public class PayrollReviewController {
    private EmployeeBO emp;
    private PayrollBO pa;
    
    public PayrollReviewController() {
        emp = BOFactory.getInstance().getBO(BOFactory.BOTypes.Employee);
        pa=BOFactory.getInstance().getBO(BOFactory.BOTypes.Payroll);
    }
    
    public EmployeeDTO searchempDetailsByName(String id) throws Exception{
       return emp.searchEmpDetails(id); 
    }

    public EmployeeDTO searchEmpByName(String name) throws Exception{
        return emp.searchEmployeeId(name);
    }

    public ArrayList<String> LoaDAllemployeeId()throws Exception {
         return emp.getAllemployee();
    }

    public ArrayList<String> LoadallemployeeName()throws Exception {
        return emp.getAllemployeeName();
    }

    public ArrayList<PayrollDTO> getAllPayroll() throws Exception {
        return pa.getAll();
    }

    public ArrayList<PayrollDTO> getPayrollDetails(int y, int m) throws Exception {
        return pa.getAll(y,m);
    }

    public ArrayList<PayrollDTO> getPayrollDetails(String Id, int y, int m) throws Exception {
       return pa.getAll(Id,y,m);
    }
    
}
