/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import lk.ijse.payroll.business.BOFactory;
import lk.ijse.payroll.business.custom.UserBO;
import lk.ijse.payroll.dto.UserDTO;

/**
 *
 * @author udara
 */
public class SettingController {

    private UserBO u;

    public SettingController() {
        u = BOFactory.getInstance().getBO(BOFactory.BOTypes.User);
    }

    public boolean matchPassword(UserDTO us) {
        UserDTO searched = null;
        try {
            searched = u.search(us.getUserName(), us.getPassword());

        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        if (searched != null) {
            return true;
        }

        return false;
    }

    public boolean updatePassword(UserDTO userDTO)throws Exception {
        boolean update = u.update(userDTO);
        return update;
    }
}
