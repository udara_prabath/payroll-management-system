/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.view.util;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author udara
 */
public class ComboBoxRender extends JLabel implements ListCellRenderer {

    private String _title;

    public ComboBoxRender(String title) {
        _title = title;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus) {
        if (index == -1 && value == null) {
            setText(_title);
        } else {
            setText(value.toString());
        }
        if (isSelected) {
            setBackground(new Color(51,102,255)); //the background color of the selected item
            setForeground(Color.red); //the textcolor of the selected item
        } else {
            setBackground(new Color(255, 255, 255)); //the background color of the other items
            setForeground(Color.black); //the textcolor of the other items
        }
        return this;
    }

}
