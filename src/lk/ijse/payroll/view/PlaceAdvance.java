/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.view;

import com.sun.javafx.fxml.expression.BinaryExpression;
import static com.sun.javafx.fxml.expression.Expression.divide;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import lk.ijse.payroll.controller.PlaceAdvancesController;
import lk.ijse.payroll.dto.AdvancesDTO;
import lk.ijse.payroll.dto.EmpAdvancesDTO;
import lk.ijse.payroll.dto.EmployeeDTO;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import lk.ijse.payroll.view.util.ComboBoxRender;
import sun.util.calendar.CalendarUtils;

/**
 *
 * @author udara
 */
public class PlaceAdvance extends javax.swing.JFrame {

    private PlaceAdvancesController ctrl;

    /**
     * Creates new form PlaceAdvance
     */
    public PlaceAdvance() {
        initComponents();
        setCellsAlignment(advancesTable);
        setDate();
        setTime();
        ctrl = new PlaceAdvancesController();
        ImageIcon img = new ImageIcon("src/lk/ijse/payroll/asset/payloku.png");
        this.setIconImage(img.getImage());
        loadItems();
        loadAllAdvance();
        EmployeeAdvanceId.setText(loadNextId());
        employeeId.requestFocus();
        lb9.setForeground(new Color(0, 204, 0));

    }

    public void loadAllAdvance() {
        try {
            ArrayList<EmpAdvancesDTO> all = ctrl.getAllAdvances();
            DefaultTableModel dtm = (DefaultTableModel) advancesTable.getModel();
            dtm.setRowCount(0);

            for (EmpAdvancesDTO ad : all) {
                String AdvanceName = ctrl.searchAdvanceId(ad.getaDId());
                EmployeeDTO employeeId = ctrl.searchemp(ad.getEmpId());

                Object Rowdata[] = {ad.geteAdId(), employeeId.getEmpName(), AdvanceName, ad.getMonth(), ad.getYear(), ad.getMonthlyInstallment()};
                dtm.addRow(Rowdata);

            }
            int i = dtm.getRowCount();
            count.setText("" + i);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void setCellsAlignment(JTable table) {
        ((DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        table.getTableHeader().setFont(new Font("Tahome", Font.BOLD, 15));
        table.setFont(new Font("Tahome", 1, 13));
    }

    public void loadItems() {
        try {
            ArrayList<String> emp = ctrl.LoaDAllemployeeId();
            for (String e : emp) {

                //employeeId3.addItem(e);
                employeeId.addItem(e);
                empId.addItem(e);

            }

        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            ArrayList<String> emp = ctrl.LoadallemployeeName();
            for (String e : emp) {

                employeeName.addItem(e);
                empName.addItem(e);

            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ArrayList<String> advance = ctrl.loadAllAdvances();
            for (String e : advance) {

                advanceName.addItem(e);
                this.advance.addItem(e);

            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);

        }
        employeeId.setRenderer(new ComboBoxRender("Select EmployeeId"));
        employeeId.setSelectedIndex(-1);
        empId.setRenderer(new ComboBoxRender("Select EmployeeId"));
        empId.setSelectedIndex(-1);

        employeeName.setRenderer(new ComboBoxRender("Select Employee"));
        employeeName.setSelectedIndex(-1);
        empName.setRenderer(new ComboBoxRender("Select Employee"));
        empName.setSelectedIndex(-1);

        advanceName.setRenderer(new ComboBoxRender("Select advance"));
        advanceName.setSelectedIndex(-1);
        advance.setRenderer(new ComboBoxRender("Select advance"));
        advance.setSelectedIndex(-1);

        employeeDesiignation.setText("");
        advanceAmount.setText("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        main = new javax.swing.JPanel();
        menu = new javax.swing.JLabel();
        sp1 = new javax.swing.JSeparator();
        lb3 = new javax.swing.JLabel();
        lb1 = new javax.swing.JLabel();
        lb5 = new javax.swing.JLabel();
        lb7 = new javax.swing.JLabel();
        lb8 = new javax.swing.JLabel();
        lb9 = new javax.swing.JLabel();
        lb17 = new javax.swing.JLabel();
        lb16 = new javax.swing.JLabel();
        path = new javax.swing.JPanel();
        home = new javax.swing.JLabel();
        dash = new javax.swing.JLabel();
        next = new javax.swing.JLabel();
        lb15 = new javax.swing.JLabel();
        lb18 = new javax.swing.JLabel();
        lb19 = new javax.swing.JLabel();
        card = new javax.swing.JPanel();
        newad = new javax.swing.JPanel();
        p1 = new javax.swing.JPanel();
        lb20 = new javax.swing.JLabel();
        advanceAmount = new javax.swing.JTextField();
        advanceName = new javax.swing.JComboBox<>();
        employeeId = new javax.swing.JComboBox<>();
        lb21 = new javax.swing.JLabel();
        lb22 = new javax.swing.JLabel();
        month = new com.toedter.calendar.JMonthChooser();
        year = new com.toedter.calendar.JYearChooser();
        jSeparator3 = new javax.swing.JSeparator();
        cancel = new javax.swing.JLabel();
        save = new javax.swing.JLabel();
        employeeName = new javax.swing.JComboBox<>();
        lb23 = new javax.swing.JLabel();
        employeeDesiignation = new javax.swing.JTextField();
        jSeparator4 = new javax.swing.JSeparator();
        lb24 = new javax.swing.JLabel();
        lb25 = new javax.swing.JLabel();
        lb26 = new javax.swing.JLabel();
        EmployeeAdvanceId = new javax.swing.JTextField();
        status = new javax.swing.JLabel();
        view = new javax.swing.JPanel();
        p2 = new javax.swing.JPanel();
        fil = new javax.swing.JPanel();
        p3 = new javax.swing.JPanel();
        lbfilter = new javax.swing.JLabel();
        advance = new javax.swing.JComboBox<>();
        apply = new javax.swing.JLabel();
        empId = new javax.swing.JComboBox<>();
        empName = new javax.swing.JComboBox<>();
        cancel1 = new javax.swing.JLabel();
        remove = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        advancesTable = new javax.swing.JTable();
        empcoount = new javax.swing.JLabel();
        count = new javax.swing.JTextField();
        re = new javax.swing.JLabel();
        placenew = new javax.swing.JLabel();
        viewad = new javax.swing.JLabel();
        lb6 = new javax.swing.JLabel();
        lb11 = new javax.swing.JLabel();
        lb4 = new javax.swing.JLabel();
        lb10 = new javax.swing.JLabel();
        sp2 = new javax.swing.JSeparator();
        lblDate = new javax.swing.JLabel();
        lblTime = new javax.swing.JLabel();
        frame = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        main.setBackground(new java.awt.Color(255, 255, 255));
        main.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        menu.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 14)); // NOI18N
        menu.setForeground(new java.awt.Color(255, 255, 255));
        menu.setText("Menu");
        main.add(menu, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 60, -1));

        sp1.setBackground(new java.awt.Color(204, 204, 255));
        sp1.setForeground(new java.awt.Color(204, 255, 204));
        main.add(sp1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 310, 10));

        lb3.setBackground(new java.awt.Color(242, 241, 239));
        lb3.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb3.setForeground(new java.awt.Color(255, 255, 255));
        lb3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/home2.png"))); // NOI18N
        lb3.setText(" DashBoard");
        lb3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb3.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lb3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb3MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb3MouseExited(evt);
            }
        });
        main.add(lb3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 240, 70));

        lb1.setBackground(new java.awt.Color(242, 241, 239));
        lb1.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb1.setForeground(new java.awt.Color(255, 255, 255));
        lb1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/admin1.png"))); // NOI18N
        lb1.setText(" Administration");
        lb1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb1MouseExited(evt);
            }
        });
        main.add(lb1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 230, 290, 70));

        lb5.setBackground(new java.awt.Color(242, 241, 239));
        lb5.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb5.setForeground(new java.awt.Color(255, 255, 255));
        lb5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/employee.png"))); // NOI18N
        lb5.setText("  Employee");
        lb5.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb5MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb5MouseExited(evt);
            }
        });
        main.add(lb5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 320, 240, 70));

        lb7.setBackground(new java.awt.Color(242, 241, 239));
        lb7.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb7.setForeground(new java.awt.Color(255, 255, 255));
        lb7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/attendance.png"))); // NOI18N
        lb7.setText("  Attendance");
        lb7.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb7MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb7MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb7MouseExited(evt);
            }
        });
        main.add(lb7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 410, 260, 70));

        lb8.setBackground(new java.awt.Color(242, 241, 239));
        lb8.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb8.setForeground(new java.awt.Color(255, 255, 255));
        lb8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/loan.png"))); // NOI18N
        lb8.setText("  Loan");
        lb8.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb8MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb8MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb8MouseExited(evt);
            }
        });
        main.add(lb8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 500, 190, 70));

        lb9.setBackground(new java.awt.Color(242, 241, 239));
        lb9.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb9.setForeground(new java.awt.Color(0, 204, 0));
        lb9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/advance.png"))); // NOI18N
        lb9.setText("  Advances");
        lb9.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb9MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb9MouseExited(evt);
            }
        });
        main.add(lb9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 590, 240, 70));

        lb17.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 30)); // NOI18N
        lb17.setForeground(new java.awt.Color(153, 153, 153));
        lb17.setText("Advances");
        main.add(lb17, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 40, 150, 60));

        lb16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lb16.setForeground(new java.awt.Color(204, 204, 204));
        lb16.setText("List");
        main.add(lb16, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 60, 30, 30));

        path.setBackground(new java.awt.Color(255, 255, 255));
        path.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        home.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/home.png"))); // NOI18N
        path.add(home, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 30, 30));

        dash.setForeground(new java.awt.Color(153, 153, 153));
        dash.setText("DashBoard");
        dash.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        dash.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dashMouseClicked(evt);
            }
        });
        path.add(dash, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 16, -1, 20));

        next.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/next.png"))); // NOI18N
        path.add(next, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 0, 20, 50));

        lb15.setText("Place New Advance");
        lb15.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        path.add(lb15, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, -1, 30));

        lb18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/next.png"))); // NOI18N
        path.add(lb18, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 0, 20, 50));

        lb19.setForeground(new java.awt.Color(153, 153, 153));
        lb19.setText("Advances");
        lb19.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb19MouseClicked(evt);
            }
        });
        path.add(lb19, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 16, 70, 20));

        main.add(path, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 90, 830, 50));

        card.setBackground(new java.awt.Color(255, 255, 255));
        card.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        card.setLayout(new java.awt.CardLayout());

        newad.setBackground(new java.awt.Color(255, 255, 255));
        newad.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        newad.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p1.setBackground(new java.awt.Color(0, 0, 102));
        p1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        newad.add(p1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1550, 40));

        lb20.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lb20.setText("Employee Advance ID*");
        newad.add(lb20, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 150, 150, 30));

        advanceAmount.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        advanceAmount.setFocusable(false);
        advanceAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                advanceAmountKeyPressed(evt);
            }
        });
        newad.add(advanceAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 440, 310, 40));

        advanceName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        advanceName.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                advanceNameItemStateChanged(evt);
            }
        });
        advanceName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                advanceNameActionPerformed(evt);
            }
        });
        advanceName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                advanceNameKeyPressed(evt);
            }
        });
        newad.add(advanceName, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 440, 410, 40));

        employeeId.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeId.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                employeeIdItemStateChanged(evt);
            }
        });
        employeeId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeIdActionPerformed(evt);
            }
        });
        employeeId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeIdKeyPressed(evt);
            }
        });
        newad.add(employeeId, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 260, 210, 40));

        lb21.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lb21.setText("Advance Amount");
        newad.add(lb21, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 420, -1, -1));

        lb22.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lb22.setText("year*");
        newad.add(lb22, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 520, 50, 20));

        month.setFocusable(false);
        month.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                monthKeyPressed(evt);
            }
        });
        newad.add(month, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 550, 110, 30));

        year.setFocusable(false);
        year.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                yearKeyPressed(evt);
            }
        });
        newad.add(year, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 550, 130, 30));
        newad.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 390, 1410, 10));

        cancel.setBackground(new java.awt.Color(153, 0, 0));
        cancel.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        cancel.setForeground(new java.awt.Color(255, 255, 255));
        cancel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cancel.setText("Cancel");
        cancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        cancel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancel.setOpaque(true);
        cancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelMouseClicked(evt);
            }
        });
        newad.add(cancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(1170, 780, 150, 40));

        save.setBackground(new java.awt.Color(0, 204, 0));
        save.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        save.setForeground(new java.awt.Color(255, 255, 255));
        save.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        save.setText("Save");
        save.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 0)));
        save.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        save.setOpaque(true);
        save.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                saveFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                saveFocusLost(evt);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveMouseClicked(evt);
            }
        });
        save.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                saveKeyPressed(evt);
            }
        });
        newad.add(save, new org.netbeans.lib.awtextra.AbsoluteConstraints(1350, 780, 150, 40));

        employeeName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeName.setFocusable(false);
        employeeName.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                employeeNameItemStateChanged(evt);
            }
        });
        employeeName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeNameActionPerformed(evt);
            }
        });
        employeeName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeNameKeyPressed(evt);
            }
        });
        newad.add(employeeName, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 260, 370, 40));

        lb23.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lb23.setText("Employee Designation");
        newad.add(lb23, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 240, -1, -1));

        employeeDesiignation.setEditable(false);
        employeeDesiignation.setBackground(new java.awt.Color(255, 255, 255));
        employeeDesiignation.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeDesiignation.setFocusable(false);
        newad.add(employeeDesiignation, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 260, 350, 40));
        newad.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 110, 1420, 10));

        lb24.setText("Employee Details");
        newad.add(lb24, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, -1, -1));

        lb25.setText("Advance Details");
        newad.add(lb25, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 370, -1, -1));

        lb26.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lb26.setText("Month*");
        newad.add(lb26, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 520, 90, 20));

        EmployeeAdvanceId.setEditable(false);
        EmployeeAdvanceId.setBackground(new java.awt.Color(255, 255, 255));
        EmployeeAdvanceId.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        EmployeeAdvanceId.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        EmployeeAdvanceId.setFocusable(false);
        newad.add(EmployeeAdvanceId, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 180, 210, 40));

        status.setBackground(new java.awt.Color(255, 255, 255));
        status.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        status.setForeground(new java.awt.Color(153, 0, 0));
        newad.add(status, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 800, 400, 20));

        card.add(newad, "card2");

        view.setBackground(new java.awt.Color(255, 255, 255));
        view.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        view.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p2.setBackground(new java.awt.Color(0, 0, 102));
        p2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        view.add(p2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1550, 40));

        fil.setBackground(new java.awt.Color(255, 255, 255));
        fil.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        fil.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p3.setBackground(new java.awt.Color(0, 102, 204));
        p3.setForeground(new java.awt.Color(255, 255, 255));
        p3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbfilter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbfilter.setForeground(new java.awt.Color(255, 255, 255));
        lbfilter.setText("Filters");
        p3.add(lbfilter, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 0, 70, 40));

        fil.add(p3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1480, 40));

        advance.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        advance.setForeground(new java.awt.Color(51, 51, 51));
        advance.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        advance.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        advance.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                advanceKeyPressed(evt);
            }
        });
        fil.add(advance, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 60, 340, 40));

        apply.setBackground(new java.awt.Color(0, 153, 0));
        apply.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        apply.setForeground(new java.awt.Color(255, 255, 255));
        apply.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        apply.setText("Apply Filters");
        apply.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        apply.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        apply.setOpaque(true);
        apply.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                applyMouseClicked(evt);
            }
        });
        apply.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                applyKeyPressed(evt);
            }
        });
        fil.add(apply, new org.netbeans.lib.awtextra.AbsoluteConstraints(1030, 60, 140, 40));

        empId.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        empId.setForeground(new java.awt.Color(51, 51, 51));
        empId.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        empId.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        empId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empIdActionPerformed(evt);
            }
        });
        empId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                empIdKeyPressed(evt);
            }
        });
        fil.add(empId, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 60, 270, 40));

        empName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        empName.setForeground(new java.awt.Color(51, 51, 51));
        empName.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        empName.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        empName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empNameActionPerformed(evt);
            }
        });
        empName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                empNameKeyPressed(evt);
            }
        });
        fil.add(empName, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 60, 240, 40));

        cancel1.setBackground(new java.awt.Color(153, 0, 0));
        cancel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        cancel1.setForeground(new java.awt.Color(255, 255, 255));
        cancel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cancel1.setText("Cancel");
        cancel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        cancel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancel1.setOpaque(true);
        cancel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancel1MouseClicked(evt);
            }
        });
        fil.add(cancel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 60, 140, 40));

        view.add(fil, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 90, 1330, 140));

        remove.setBackground(new java.awt.Color(153, 0, 0));
        remove.setFont(new java.awt.Font("Trajan Pro", 1, 18)); // NOI18N
        remove.setForeground(new java.awt.Color(255, 255, 255));
        remove.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        remove.setText("Remove Advances");
        remove.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        remove.setOpaque(true);
        remove.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                removeMouseClicked(evt);
            }
        });
        view.add(remove, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 330, 200, 30));

        advancesTable.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        advancesTable.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        advancesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Employee ", "Advance Name", "Month", "Year", "Monthly_Installment"
            }
        ));
        advancesTable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        advancesTable.setGridColor(new java.awt.Color(51, 102, 255));
        advancesTable.setRowHeight(25);
        advancesTable.setSelectionBackground(new java.awt.Color(102, 102, 102));
        jScrollPane1.setViewportView(advancesTable);

        view.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 380, 1390, 340));

        empcoount.setFont(new java.awt.Font("Trajan Pro", 1, 14)); // NOI18N
        empcoount.setText("empcount");
        view.add(empcoount, new org.netbeans.lib.awtextra.AbsoluteConstraints(1170, 330, 180, 30));

        count.setEditable(false);
        count.setBackground(new java.awt.Color(255, 255, 255));
        count.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        count.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        view.add(count, new org.netbeans.lib.awtextra.AbsoluteConstraints(1360, 330, 130, 30));

        re.setBackground(new java.awt.Color(255, 255, 255));
        re.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        re.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/refresh.png"))); // NOI18N
        re.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        re.setOpaque(true);
        re.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                reMouseClicked(evt);
            }
        });
        view.add(re, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 330, 60, 30));

        card.add(view, "card3");

        main.add(card, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 210, 1550, 840));

        placenew.setBackground(new java.awt.Color(0, 0, 102));
        placenew.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 18)); // NOI18N
        placenew.setForeground(new java.awt.Color(255, 255, 255));
        placenew.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        placenew.setText("Place New Advances");
        placenew.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        placenew.setOpaque(true);
        placenew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                placenewMouseClicked(evt);
            }
        });
        main.add(placenew, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 170, 210, 40));

        viewad.setBackground(new java.awt.Color(250, 250, 250));
        viewad.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 18)); // NOI18N
        viewad.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        viewad.setText("View Employee Advances Details");
        viewad.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        viewad.setOpaque(true);
        viewad.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                viewadMouseClicked(evt);
            }
        });
        main.add(viewad, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 170, 350, 40));

        lb6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/closesudu.png"))); // NOI18N
        lb6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb6MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb6MouseExited(evt);
            }
        });
        main.add(lb6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1870, 0, 40, 40));

        lb11.setBackground(new java.awt.Color(242, 241, 239));
        lb11.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb11.setForeground(new java.awt.Color(255, 255, 255));
        lb11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lb11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/payroll.png"))); // NOI18N
        lb11.setText("  Payroll Review");
        lb11.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb11.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lb11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb11MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb11MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb11MouseExited(evt);
            }
        });
        main.add(lb11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 780, 250, 70));

        lb4.setBackground(new java.awt.Color(242, 241, 239));
        lb4.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb4.setForeground(new java.awt.Color(255, 255, 255));
        lb4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/reports.png"))); // NOI18N
        lb4.setText("  Reports");
        lb4.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb4MouseExited(evt);
            }
        });
        main.add(lb4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 860, 230, 70));

        lb10.setBackground(new java.awt.Color(242, 241, 239));
        lb10.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb10.setForeground(new java.awt.Color(255, 255, 255));
        lb10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/payroll.png"))); // NOI18N
        lb10.setText("  Payroll");
        lb10.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb10MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb10MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb10MouseExited(evt);
            }
        });
        main.add(lb10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 680, 210, 70));
        main.add(sp2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 940, 290, 10));

        lblDate.setFont(new java.awt.Font("DigifaceWide", 0, 18)); // NOI18N
        lblDate.setForeground(new java.awt.Color(255, 255, 255));
        main.add(lblDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 960, 220, 40));

        lblTime.setFont(new java.awt.Font("DigifaceWide", 0, 18)); // NOI18N
        lblTime.setForeground(new java.awt.Color(255, 255, 255));
        main.add(lblTime, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 1010, 220, 40));

        frame.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/employee.jpg"))); // NOI18N
        main.add(frame, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1920, 1080));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1920, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(main, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1080, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(main, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lb3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb3MouseClicked
        DashBoard d1 = new DashBoard();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb3MouseClicked

    private void lb3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb3MouseEntered
        lb3.setForeground(Color.RED);
    }//GEN-LAST:event_lb3MouseEntered

    private void lb3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb3MouseExited
        lb3.setForeground(Color.WHITE);
    }//GEN-LAST:event_lb3MouseExited

    private void lb1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseEntered
        lb1.setForeground(Color.RED);
    }//GEN-LAST:event_lb1MouseEntered

    private void lb1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseExited
        lb1.setForeground(Color.white);
    }//GEN-LAST:event_lb1MouseExited

    private void lb5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseClicked
        EmployeeReview d1 = new EmployeeReview();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb5MouseClicked

    private void lb5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseEntered
        lb5.setForeground(Color.RED);
    }//GEN-LAST:event_lb5MouseEntered

    private void lb5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseExited
        lb5.setForeground(Color.white);
    }//GEN-LAST:event_lb5MouseExited

    private void lb7MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseEntered
        lb7.setForeground(Color.RED);
    }//GEN-LAST:event_lb7MouseEntered

    private void lb7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseExited
        lb7.setForeground(Color.white);
    }//GEN-LAST:event_lb7MouseExited

    private void lb8MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseEntered
        lb8.setForeground(Color.RED);
    }//GEN-LAST:event_lb8MouseEntered

    private void lb8MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseExited
        lb8.setForeground(Color.white);
    }//GEN-LAST:event_lb8MouseExited

    private void lb9MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseEntered
        lb9.setForeground(Color.RED);
    }//GEN-LAST:event_lb9MouseEntered

    private void lb9MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseExited
        lb9.setForeground(new Color(0, 204, 0));
    }//GEN-LAST:event_lb9MouseExited

    private void dashMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dashMouseClicked
        DashBoard d1 = new DashBoard();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_dashMouseClicked

    private void lb19MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb19MouseClicked
        lb15.setText("Place New Advance Advances");
        card.removeAll();
        card.repaint();
        card.revalidate();
        card.add(newad);
        card.repaint();
        card.revalidate();
        placenew.setBackground(new Color(0, 0, 102));
        viewad.setBackground(new Color(255, 255, 255));
        viewad.setForeground(new Color(0, 0, 0));
        placenew.setForeground(new Color(255, 255, 255));
    }//GEN-LAST:event_lb19MouseClicked

    private void viewadMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_viewadMouseClicked
        lb15.setText("View Employee Advances");
        card.removeAll();
        card.repaint();
        card.revalidate();
        card.add(view);
        card.repaint();
        card.revalidate();
        viewad.setBackground(new Color(0, 0, 102));
        placenew.setBackground(new Color(255, 255, 255));
        placenew.setForeground(new Color(0, 0, 0));
        viewad.setForeground(new Color(255, 255, 255));
    }//GEN-LAST:event_viewadMouseClicked

    private void lb8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseClicked
        placeLoans d1 = new placeLoans();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();

    }//GEN-LAST:event_lb8MouseClicked

    private void placenewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_placenewMouseClicked
        lb15.setText("Place New Advance");
        card.removeAll();
        card.repaint();
        card.revalidate();
        card.add(newad);
        card.repaint();
        card.revalidate();
        placenew.setBackground(new Color(0, 0, 102));
        viewad.setBackground(new Color(255, 255, 255));
        viewad.setForeground(new Color(0, 0, 0));
        placenew.setForeground(new Color(255, 255, 255));
    }//GEN-LAST:event_placenewMouseClicked

    private void lb6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseClicked
        int result = JOptionPane.showConfirmDialog(this, "Do You Want To Exit?", "Exit:", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            this.dispose();
        } else if (result == JOptionPane.NO_OPTION) {
            this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        }
    }//GEN-LAST:event_lb6MouseClicked

    private void lb6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseEntered
        ImageIcon icon = new ImageIcon("src/lk/ijse/payroll/asset/closerathu.png");
        lb6.setIcon(icon);
    }//GEN-LAST:event_lb6MouseEntered

    private void lb6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseExited
        ImageIcon icon = new ImageIcon("src/lk/ijse/payroll/asset/closesudu.png");
        lb6.setIcon(icon);
    }//GEN-LAST:event_lb6MouseExited

    private void lb1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseClicked
        Administation d1 = new Administation();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb1MouseClicked

    private void advanceNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_advanceNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_advanceNameActionPerformed

    private void employeeIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeIdActionPerformed
        if (employeeId.getSelectedIndex() != -1) {
            try {
                String id = (String) employeeId.getSelectedItem();

                EmployeeDTO e = ctrl.searchemp(id);
                if (e != null) {
                    String de = ctrl.searchDes(e.getDeId());
                    String d = e.getEmpName();

                    employeeName.setSelectedItem(d);

                    employeeDesiignation.setText(de);

                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeIdActionPerformed

    private void employeeNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeNameActionPerformed
        if (employeeName.getSelectedIndex() != -1) {
            try {
                String name = employeeName.getSelectedItem().toString();
                // System.out.println(name);
                EmployeeDTO e = ctrl.searchEmpId(name);
                if (e != null) {

                    String des = ctrl.searchDes(e.getDeId());
                    String empid = e.getEmpId();

                    employeeId.setSelectedItem(empid);
                    employeeDesiignation.setText(des);

                }
            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeNameActionPerformed

    private void lb7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseClicked
        Attendance d1 = new Attendance();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb7MouseClicked

    private void employeeIdItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_employeeIdItemStateChanged

    }//GEN-LAST:event_employeeIdItemStateChanged

    private void employeeNameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_employeeNameItemStateChanged

    }//GEN-LAST:event_employeeNameItemStateChanged

    private void advanceNameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_advanceNameItemStateChanged
        if (advanceName.getSelectedIndex() != -1) {
            try {
                String adv = advanceName.getSelectedItem().toString();
                System.out.println(adv);
                AdvancesDTO ad = ctrl.searchAdvance(adv);

                if (ad != null) {
                    advanceAmount.setText(ad.getAmount().toString());

                }

            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_advanceNameItemStateChanged

    private void saveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseClicked
        int i = year.getYear();
        int x = month.getMonth() + 1;
        boolean available=false;
        if (employeeId.getSelectedItem() != null) {
            String empi = employeeId.getSelectedItem().toString();
             available = ctrl.checkAvailabilety(i, x, empi);
        }
        
        if (employeeId.getSelectedIndex() == -1) {
            status.setText("Please Select Employee!");
            employeeId.requestFocus();

        } else if (advanceName.getSelectedIndex() == -1) {
            status.setText("Please Select a Advance before save!");
            advanceName.requestFocus();
        } else if (i < 2018 & x < 6) {
            status.setText("Please Select Valid  date before save!");
            year.requestFocus();

        } else if (!available) {
            status.setText("Cannot Launch Advance For this Employee");
            employeeId.requestFocus();
            clean();
            loadItems();
        } else {
            try {
                String adv = (String) advanceName.getSelectedItem();
                AdvancesDTO ad = ctrl.searchAdvance(adv);
                int m = month.getMonth() + 1;
                int y = year.getYear();

                String z = EmployeeAdvanceId.getText();

                String empid = employeeId.getSelectedItem().toString();
                String adid = ad.getAdId();
                String mon = "" + m;
                String yer = "" + y;
                System.out.println(mon);
                System.out.println(yer);

                BigDecimal amount = new BigDecimal(advanceAmount.getText());
                amount.precision();

                boolean added = ctrl.addAdvance(z, empid, adid, mon, yer, amount);
                if (added) {
                    JOptionPane.showMessageDialog(this, "added");
                }

            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
            loadAllAdvance();
            clean();
            loadItems();
            EmployeeAdvanceId.setText(loadNextId());
            status.setText("");
        }
    }//GEN-LAST:event_saveMouseClicked

    private void removeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_removeMouseClicked
        int result1 = JOptionPane.showConfirmDialog(this, "Do You Want To Delete This Date?", "Warning:", JOptionPane.YES_NO_OPTION);
        if (result1 == JOptionPane.YES_OPTION) {
            try {
                if (advancesTable.getSelectedRow() == -1) {
                    return;
                }

                boolean result = ctrl.delete(advancesTable.getValueAt(advancesTable.getSelectedRow(), 0).toString());

                if (result) {
                    JOptionPane.showMessageDialog(this, "Advance has been deleted successfully");
                } else {
                    JOptionPane.showMessageDialog(this, "Failed to delete the Advance");
                }

                advancesTable.getSelectionModel().clearSelection();
                loadAllAdvance();
                clean();

                loadItems();

            } catch (Exception ex) {
                Logger.getLogger(Loans.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (result1 == JOptionPane.NO_OPTION) {

        }

    }//GEN-LAST:event_removeMouseClicked

    private void applyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_applyMouseClicked
        DefaultTableModel dtm = (DefaultTableModel) advancesTable.getModel();
        if (advance.getSelectedIndex() == -1 & empId.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Please Select Only One Category");
            advance.requestFocus();
        } else if (advance.getSelectedIndex() == -1) {
            try {
                EmployeeDTO employeeId = ctrl.searchemp(empId.getSelectedItem().toString());
                String empid = employeeId.getEmpName();

                ArrayList<EmpAdvancesDTO> Alladvances = ctrl.getAdvances(employeeId.getEmpId());
                for (EmpAdvancesDTO ad : Alladvances) {
                    String AdvanceName = ctrl.searchAdvanceId(ad.getaDId());
                    Object Rowdata[] = {ad.geteAdId(), employeeId.getEmpName(), AdvanceName, ad.getMonth(), ad.getYear(), ad.getMonthlyInstallment()};
                    dtm.setRowCount(0);
                    dtm.addRow(Rowdata);

                }
                int i = dtm.getRowCount();
                count.setText("" + i);

            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
            clean();
            loadItems();

        } else if (empId.getSelectedIndex() == -1) {
            try {

                String adv = advance.getSelectedItem().toString();
                dtm.setRowCount(0);
                AdvancesDTO advances = ctrl.searchAdvance(adv);
                String adid = advances.getAdId();

                ArrayList<EmpAdvancesDTO> Alladvances = ctrl.getEmpAdvances(adid);
                for (EmpAdvancesDTO ad : Alladvances) {
                    String AdvanceName = ctrl.searchAdvanceId(ad.getaDId());
                    EmployeeDTO employeeId = ctrl.searchemp(ad.getEmpId());

                    Object Rowdata[] = {ad.geteAdId(), employeeId.getEmpName(), AdvanceName, ad.getMonth(), ad.getYear(), ad.getMonthlyInstallment()};

                    dtm.setRowCount(0);
                    dtm.addRow(Rowdata);

                }
                int i = dtm.getRowCount();
                count.setText("" + i);

            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
            clean();
            loadItems();
        } else if (empId.getSelectedIndex() != -1 & advance.getSelectedIndex() != -1) {
            JOptionPane.showMessageDialog(this, "Please Select Only One Category");
            clean();
            loadItems();
            loadAllAdvance();
        }

    }//GEN-LAST:event_applyMouseClicked

    private void empIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empIdActionPerformed
        if (empId.getSelectedIndex() != -1) {
            try {
                String id = (String) empId.getSelectedItem();

                EmployeeDTO e = ctrl.searchemp(id);
                if (e != null) {
                    String de = ctrl.searchDes(e.getDeId());
                    String d = e.getEmpName();

                    empName.setSelectedItem(d);

                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_empIdActionPerformed

    private void empNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empNameActionPerformed
        if (empName.getSelectedIndex() != -1) {
            try {
                String name = empName.getSelectedItem().toString();
                // System.out.println(name);
                EmployeeDTO e = ctrl.searchEmpId(name);
                if (e != null) {

                    String empid = e.getEmpId();

                    empId.setSelectedItem(empid);

                }
            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_empNameActionPerformed

    private void cancelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelMouseClicked
        clean();
        loadItems();
    }//GEN-LAST:event_cancelMouseClicked

    private void reMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reMouseClicked
        loadAllAdvance();
    }//GEN-LAST:event_reMouseClicked

    private void employeeIdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeIdKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (employeeId.getSelectedIndex() == -1) {
                    status.setText("Please select employee! ");
                    employeeId.requestFocus();
                } else {
                    KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                    manager.getFocusOwner().transferFocus();
                }
                break;
        }
    }//GEN-LAST:event_employeeIdKeyPressed

    private void employeeNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeNameKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (employeeId.getSelectedIndex() == -1) {
                    status.setText("Please select employee! ");
                    employeeId.requestFocus();
                } else {
                    KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                    manager.getFocusOwner().transferFocus();
                }
                break;
        }
    }//GEN-LAST:event_employeeNameKeyPressed

    private void advanceNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_advanceNameKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (employeeId.getSelectedIndex() == -1) {
                    status.setText("Please select employee! ");
                    employeeId.requestFocus();
                } else if (advanceName.getSelectedIndex() == -1) {
                    status.setText("Please select Advance! ");
                    advanceName.requestFocus();
                } else {
                    save.requestFocus();
                }
                break;
        }
    }//GEN-LAST:event_advanceNameKeyPressed

    private void monthKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_monthKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:

                year.requestFocus();
                break;
        }
    }//GEN-LAST:event_monthKeyPressed

    private void yearKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_yearKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:

                save.requestFocus();
                break;
        }
    }//GEN-LAST:event_yearKeyPressed

    private void saveFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_saveFocusGained
        save.setBackground(new Color(31, 191, 21));
        save.setForeground(Color.white);
    }//GEN-LAST:event_saveFocusGained

    private void saveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_saveKeyPressed
        int x = month.getMonth() + 1;
        int i = year.getYear();
        String empi = employeeId.getSelectedItem().toString();
        System.out.println(empi + "aswfaf");
        boolean available = ctrl.checkAvailabilety(i, x, empi);
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (!available) {
                    status.setText("Cannot Launch Advance For this Employee");
                    employeeId.requestFocus();
                    clean();
                    loadItems();
                } else {
                    try {
                        String adv = (String) advanceName.getSelectedItem();
                        AdvancesDTO ad = ctrl.searchAdvance(adv);
                        int m = month.getMonth() + 1;
                        int y = year.getYear();

                        String z = EmployeeAdvanceId.getText();

                        String empid = employeeId.getSelectedItem().toString();
                        String adid = ad.getAdId();
                        String mon = "" + m;
                        String yer = "" + y;
                        System.out.println(mon);
                        System.out.println(yer);

                        BigDecimal amount = new BigDecimal(advanceAmount.getText());
                        amount.precision();

                        boolean added = ctrl.addAdvance(z, empid, adid, mon, yer, amount);
                        if (added) {
                            JOptionPane.showMessageDialog(this, "added");
                        }

                    } catch (Exception ex) {
                        Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    loadAllAdvance();
                    clean();
                    loadItems();
                    EmployeeAdvanceId.setText(loadNextId());
                    status.setText("");
                }
                break;
        }
    }//GEN-LAST:event_saveKeyPressed

    private void advanceKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_advanceKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (advance.getSelectedIndex() == -1) {
                    status.setText("Please select Advance! ");
                    advance.requestFocus();
                } else {
                    apply.requestFocus();
                }
                break;
        }
    }//GEN-LAST:event_advanceKeyPressed

    private void empIdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_empIdKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (empId.getSelectedIndex() == -1) {
                    status.setText("Please select Advance! ");
                    empId.requestFocus();
                } else {
                    apply.requestFocus();
                }
                break;
        }
    }//GEN-LAST:event_empIdKeyPressed

    private void empNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_empNameKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (empName.getSelectedIndex() == -1) {
                    status.setText("Please select Advance! ");
                    empName.requestFocus();
                } else {
                    apply.requestFocus();
                }
                break;
        }
    }//GEN-LAST:event_empNameKeyPressed

    private void applyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_applyKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                DefaultTableModel dtm = (DefaultTableModel) advancesTable.getModel();
                if (advance.getSelectedIndex() == -1) {
                    try {
                        EmployeeDTO employeeId = ctrl.searchemp(empId.getSelectedItem().toString());
                        String empid = employeeId.getEmpName();

                        ArrayList<EmpAdvancesDTO> Alladvances = ctrl.getAdvances(employeeId.getEmpId());
                        for (EmpAdvancesDTO ad : Alladvances) {
                            String AdvanceName = ctrl.searchAdvanceId(ad.getaDId());
                            Object Rowdata[] = {ad.geteAdId(), employeeId.getEmpName(), AdvanceName, ad.getMonth(), ad.getYear(), ad.getMonthlyInstallment()};
                            dtm.setRowCount(0);
                            dtm.addRow(Rowdata);

                        }
                        int i = dtm.getRowCount();
                        count.setText("" + i);

                    } catch (Exception ex) {
                        Logger.getLogger(PlaceAdvance.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                    clean();
                    loadItems();

                } else if (empId.getSelectedIndex() == -1) {
                    try {

                        String adv = advance.getSelectedItem().toString();
                        dtm.setRowCount(0);
                        AdvancesDTO advances = ctrl.searchAdvance(adv);
                        String adid = advances.getAdId();

                        ArrayList<EmpAdvancesDTO> Alladvances = ctrl.getEmpAdvances(adid);
                        for (EmpAdvancesDTO ad : Alladvances) {
                            String AdvanceName = ctrl.searchAdvanceId(ad.getaDId());
                            EmployeeDTO employeeId = ctrl.searchemp(ad.getEmpId());

                            Object Rowdata[] = {ad.geteAdId(), employeeId.getEmpName(), AdvanceName, ad.getMonth(), ad.getYear(), ad.getMonthlyInstallment()};

                            dtm.setRowCount(0);
                            dtm.addRow(Rowdata);

                        }
                        int i = dtm.getRowCount();
                        count.setText("" + i);

                    } catch (Exception ex) {
                        Logger.getLogger(PlaceAdvance.class
                                .getName()).log(Level.SEVERE, null, ex);
                    }
                    clean();
                    loadItems();
                } else if (empId.getSelectedIndex() != -1 & advance.getSelectedIndex() != -1) {
                    JOptionPane.showMessageDialog(this, "Plese Select Only One Category");
                    clean();
                    loadItems();
                    loadAllAdvance();
                }
                break;
        }
    }//GEN-LAST:event_applyKeyPressed

    private void advanceAmountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_advanceAmountKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_advanceAmountKeyPressed

    private void saveFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_saveFocusLost
        save.setBackground(new Color(255, 255, 255));
        save.setForeground(Color.black);
    }//GEN-LAST:event_saveFocusLost

    private void cancel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancel1MouseClicked
        clean();
        loadItems();
    }//GEN-LAST:event_cancel1MouseClicked

    private void lb11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb11MouseClicked
        PayrollReview d1 = new PayrollReview();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb11MouseClicked

    private void lb11MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb11MouseEntered
        lb11.setForeground(Color.red);
    }//GEN-LAST:event_lb11MouseEntered

    private void lb11MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb11MouseExited
        lb11.setForeground(Color.white);
    }//GEN-LAST:event_lb11MouseExited

    private void lb4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseClicked
        Reports d1 = new Reports();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb4MouseClicked

    private void lb4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseEntered
        lb4.setForeground(Color.RED);
    }//GEN-LAST:event_lb4MouseEntered

    private void lb4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseExited
        lb4.setForeground(Color.white);
    }//GEN-LAST:event_lb4MouseExited

    private void lb10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseClicked
        Payroll d1 = new Payroll();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb10MouseClicked

    private void lb10MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseEntered
        lb10.setForeground(Color.RED);
    }//GEN-LAST:event_lb10MouseEntered

    private void lb10MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseExited
        lb10.setForeground(Color.white);
    }//GEN-LAST:event_lb10MouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PlaceAdvance.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PlaceAdvance.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PlaceAdvance.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PlaceAdvance.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PlaceAdvance().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField EmployeeAdvanceId;
    private javax.swing.JComboBox<String> advance;
    private javax.swing.JTextField advanceAmount;
    private javax.swing.JComboBox<String> advanceName;
    private javax.swing.JTable advancesTable;
    private javax.swing.JLabel apply;
    private javax.swing.JLabel cancel;
    private javax.swing.JLabel cancel1;
    private javax.swing.JPanel card;
    private javax.swing.JTextField count;
    private javax.swing.JLabel dash;
    private javax.swing.JComboBox<String> empId;
    private javax.swing.JComboBox<String> empName;
    private javax.swing.JLabel empcoount;
    private javax.swing.JTextField employeeDesiignation;
    private javax.swing.JComboBox<String> employeeId;
    private javax.swing.JComboBox<String> employeeName;
    private javax.swing.JPanel fil;
    private javax.swing.JLabel frame;
    private javax.swing.JLabel home;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel lb1;
    private javax.swing.JLabel lb10;
    private javax.swing.JLabel lb11;
    private javax.swing.JLabel lb15;
    private javax.swing.JLabel lb16;
    private javax.swing.JLabel lb17;
    private javax.swing.JLabel lb18;
    private javax.swing.JLabel lb19;
    private javax.swing.JLabel lb20;
    private javax.swing.JLabel lb21;
    private javax.swing.JLabel lb22;
    private javax.swing.JLabel lb23;
    private javax.swing.JLabel lb24;
    private javax.swing.JLabel lb25;
    private javax.swing.JLabel lb26;
    private javax.swing.JLabel lb3;
    private javax.swing.JLabel lb4;
    private javax.swing.JLabel lb5;
    private javax.swing.JLabel lb6;
    private javax.swing.JLabel lb7;
    private javax.swing.JLabel lb8;
    private javax.swing.JLabel lb9;
    private javax.swing.JLabel lbfilter;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblTime;
    private javax.swing.JPanel main;
    private javax.swing.JLabel menu;
    private com.toedter.calendar.JMonthChooser month;
    private javax.swing.JPanel newad;
    private javax.swing.JLabel next;
    private javax.swing.JPanel p1;
    private javax.swing.JPanel p2;
    private javax.swing.JPanel p3;
    private javax.swing.JPanel path;
    private javax.swing.JLabel placenew;
    private javax.swing.JLabel re;
    private javax.swing.JLabel remove;
    private javax.swing.JLabel save;
    private javax.swing.JSeparator sp1;
    private javax.swing.JSeparator sp2;
    private javax.swing.JLabel status;
    private javax.swing.JPanel view;
    private javax.swing.JLabel viewad;
    private com.toedter.calendar.JYearChooser year;
    // End of variables declaration//GEN-END:variables
private void setTime() {
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("hh-mm-ss a");
                lblTime.setText(sdf.format(date));
            }
        }).start();
    }

    private void setDate() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        lblDate.setText(sdf.format(date));
    }

    public void clean() {

        employeeName.removeAllItems();
        employeeId.removeAllItems();
        employeeDesiignation.setText("");
        advanceName.removeAllItems();
        advanceAmount.setText("");

        month.setMonth(1);
        year.setYear(2018);
        advance.removeAllItems();
        empId.removeAllItems();
        empName.removeAllItems();
        employeeName.removeAllItems();

    }

    private String loadNextId() {
        String id = getLastId();
        if (id == null) {
            id = "ADV0";
        }
        char ch = id.charAt(id.length() - 1);
        int a = Character.getNumericValue(ch) + 1;
        return id = id.substring(0, id.length() - 1) + a;

    }

    private String getLastId() {
        String lastId = null;
        try {
            lastId = ctrl.searchAttId();
        } catch (Exception e) {
            // e.printStackTrace();
        }
        return lastId;
    }
}
