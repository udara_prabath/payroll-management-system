/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import lk.ijse.payroll.controller.EmployeeReviewController;
import lk.ijse.payroll.dto.BankDTO;
import lk.ijse.payroll.dto.BankDetailsDTO;
import lk.ijse.payroll.dto.DesignationDTO;
import lk.ijse.payroll.dto.EmpLoanDTO;
import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.dto.PayrollDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.dto.SalaryDetailsDTO;
import lk.ijse.payroll.view.util.ComboBoxRender;

/**
 *
 * @author udara
 */
public class EmployeeReview extends javax.swing.JFrame {

    private EmployeeReviewController ctrl;
    DefaultTableModel dtm;
    DefaultTableModel dtm1;
    DefaultTableModel dtm2;

    /**
     * Creates new form Employee
     */
    public EmployeeReview() {
        initComponents();
        setSize(1920, 1080);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
        transparent();
        setTime();
        setDate();
        setCellsAlignment(EmployeeTable);
        setCellsAlignment(advances);
        setCellsAlignment(loantable);
        setCellsAlignment(payroll);
        ImageIcon img = new ImageIcon("src/lk/ijse/payroll/asset/payloku.png");
        this.setIconImage(img.getImage());
        EmployeeId.requestFocus();
        ctrl = new EmployeeReviewController();
        loadAllItems();
        loadAllEmployee();
        employeeId2.setText(loadNextempId());
        joinDate.setDate(new Date());
        dtm = (DefaultTableModel) loantable.getModel();
        dtm1 = (DefaultTableModel) advances.getModel();
        dtm2 = (DefaultTableModel) payroll.getModel();
        dtm.setRowCount(0);
        dtm1.setRowCount(0);
        dtm2.setRowCount(0);
        status.setText("");
        lb5.setForeground(new Color(0, 204, 0));
    }

    public String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {

                return button.getText();
            }
        }

        return null;
    }

    public void loadAllItems() {

        try {
            ArrayList<String> desigation = ctrl.allDesignation();
            for (String des : desigation) {
                EmployeeDesignation2.addItem(des);
                employeeDesignation3.addItem(des);
                EmployeeDesignation.addItem(des);
            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            ArrayList<String> bank = ctrl.allBanks();
            for (String ba : bank) {
                employeeBank.addItem(ba);
                employeeBank2.addItem(ba);
            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            ArrayList<String> salary = ctrl.allSalary();
            for (String sal : salary) {
                EmployeeSalary2.addItem(sal);

                employeeSalary3.addItem(sal);
            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ArrayList<String> emp = ctrl.allemployee();
            for (String e : emp) {

                employeeId3.addItem(e);
                EmployeeId.addItem(e);
                employeeId.addItem(e);

            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ArrayList<String> emp = ctrl.allemployeeName();
            for (String e : emp) {

                EmployeeName.addItem(e);
                employeeName.addItem(e);

            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }

        EmployeeDesignation.setRenderer(new ComboBoxRender("Select Designation"));
        EmployeeDesignation.setSelectedIndex(-1);
        EmployeeDesignation2.setRenderer(new ComboBoxRender("Select Designation"));
        EmployeeDesignation2.setSelectedIndex(-1);
        employeeDesignation3.setRenderer(new ComboBoxRender("Select Designation"));
        employeeDesignation3.setSelectedIndex(-1);

        EmployeeName.setRenderer(new ComboBoxRender("Select Employee"));
        EmployeeName.setSelectedIndex(-1);
        EmployeeId.setRenderer(new ComboBoxRender("Select EmployeeID"));

        employeeId.setRenderer(new ComboBoxRender("Select EmployeeID"));
        employeeName.setRenderer(new ComboBoxRender("Select Employee"));
        employeeId.setSelectedIndex(-1);
        employeeName.setSelectedIndex(-1);

        EmployeeId.setSelectedIndex(-1);
        employeeId3.setRenderer(new ComboBoxRender("Select EmployeeID"));
        employeeId3.setSelectedIndex(-1);
        employeeSalary3.setRenderer(new ComboBoxRender("Select Salary"));
        employeeSalary3.setSelectedIndex(-1);
        EmployeeSalary2.setRenderer(new ComboBoxRender("Select Salary"));
        EmployeeSalary2.setSelectedIndex(-1);
        employeeBank.setRenderer(new ComboBoxRender("Select Bank"));
        employeeBank.setSelectedIndex(-1);
        employeeBank2.setRenderer(new ComboBoxRender("Select Bank"));
        employeeBank2.setSelectedIndex(-1);

        employeeAdress2.setText("Address");
        employeeId2.setText("Employee Id");
        employeeName2.setText("Employee Name");
        NIC.setText("NIC");
        bankAcc3.setText("Acc No");
        phoneNumber3.setText("Phone Number");
        bankAcc.setText("Acc No");
        employeeAddress3.setText("Address");
        phoneNumber.setText("Phone Number");
        joinDate.setCalendar(null);
        gender.clearSelection();
        employeeName3.setText("Employee Name");
        NIC3.setText("NIC");
        EmployeeDOB2.setText("D.O.B");
        status.setText("");
        des.setText("");
        basic.setText("");
        join.setText("");
        nis.setText("");
        gen.setText("");
        bd.setText("");
        tp.setText("");
        bank.setText("");
        dtm = (DefaultTableModel) loantable.getModel();
        dtm1 = (DefaultTableModel) advances.getModel();
        dtm2 = (DefaultTableModel) payroll.getModel();
        dtm.setRowCount(0);
        dtm1.setRowCount(0);
        dtm2.setRowCount(0);

    }

    public void loadAllEmployee() {
        try {
            ArrayList<EmployeeDTO> all = ctrl.getAll();
            DefaultTableModel dtm = (DefaultTableModel) EmployeeTable.getModel();
            dtm.setRowCount(0);

            for (EmployeeDTO ad : all) {
                String de = ctrl.searchDes(ad.getDeId());
                String bs = ctrl.searchSal(ad.getEmpId());

                Object Rowdata[] = {ad.getEmpName(), de, ad.getNIC(), ad.getJoin_Date(), bs};
                dtm.addRow(Rowdata);

            }
            int i = dtm.getRowCount();
            EmployeeCount.setText("" + i);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void setCellsAlignment(JTable table) {
        ((DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        table.getTableHeader().setFont(new Font("Tahome", Font.BOLD, 15));
        table.setFont(new Font("Tahome", 1, 13));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        gender = new javax.swing.ButtonGroup();
        main = new javax.swing.JPanel();
        lb6 = new javax.swing.JLabel();
        path = new javax.swing.JPanel();
        lb32 = new javax.swing.JLabel();
        lb33 = new javax.swing.JLabel();
        lb34 = new javax.swing.JLabel();
        lb35 = new javax.swing.JLabel();
        lb36 = new javax.swing.JLabel();
        lb37 = new javax.swing.JLabel();
        lb38 = new javax.swing.JLabel();
        lb26 = new javax.swing.JLabel();
        card = new javax.swing.JPanel();
        EmployeeReview = new javax.swing.JPanel();
        p1 = new javax.swing.JPanel();
        fil = new javax.swing.JPanel();
        p2 = new javax.swing.JPanel();
        lb40 = new javax.swing.JLabel();
        EmployeeDesignation = new javax.swing.JComboBox<>();
        applyFilter = new javax.swing.JLabel();
        EmployeeId = new javax.swing.JComboBox<>();
        EmployeeName = new javax.swing.JComboBox<>();
        cancel1 = new javax.swing.JLabel();
        remove = new javax.swing.JLabel();
        EmployeeCount = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        EmployeeTable = new javax.swing.JTable();
        jLabel20 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        UpdateEmployee = new javax.swing.JPanel();
        p3 = new javax.swing.JPanel();
        lb41 = new javax.swing.JLabel();
        lb42 = new javax.swing.JLabel();
        employeeName3 = new javax.swing.JTextField();
        employeeAddress3 = new javax.swing.JTextField();
        bankAcc3 = new javax.swing.JTextField();
        phoneNumber3 = new javax.swing.JTextField();
        male3 = new javax.swing.JRadioButton();
        female3 = new javax.swing.JRadioButton();
        NIC3 = new javax.swing.JTextField();
        jSeparator5 = new javax.swing.JSeparator();
        lb43 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        lb44 = new javax.swing.JLabel();
        cancel = new javax.swing.JLabel();
        update = new javax.swing.JLabel();
        employeeId3 = new javax.swing.JComboBox<>();
        employeeDesignation3 = new javax.swing.JComboBox<>();
        employeeSalary3 = new javax.swing.JComboBox<>();
        employeeBank2 = new javax.swing.JComboBox<>();
        joinData3 = new com.toedter.calendar.JDateChooser();
        EmployeeDOB2 = new javax.swing.JTextField();
        NewEmployeee = new javax.swing.JPanel();
        p4 = new javax.swing.JPanel();
        lb445 = new javax.swing.JLabel();
        employeeId2 = new javax.swing.JTextField();
        employeeName2 = new javax.swing.JTextField();
        employeeAdress2 = new javax.swing.JTextField();
        bankAcc = new javax.swing.JTextField();
        phoneNumber = new javax.swing.JTextField();
        male = new javax.swing.JRadioButton();
        female = new javax.swing.JRadioButton();
        NIC = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();
        lb46 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        lb47 = new javax.swing.JLabel();
        cancel2 = new javax.swing.JLabel();
        save = new javax.swing.JLabel();
        joinDate = new com.toedter.calendar.JDateChooser();
        EmployeeSalary2 = new javax.swing.JComboBox<>();
        EmployeeDesignation2 = new javax.swing.JComboBox<>();
        employeeBank = new javax.swing.JComboBox<>();
        lb48 = new javax.swing.JLabel();
        EmployeeDOB = new javax.swing.JTextField();
        status = new javax.swing.JLabel();
        EmployeeSummary = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        EmployeeSummary1 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        advances = new javax.swing.JTable();
        lb15 = new javax.swing.JLabel();
        lb16 = new javax.swing.JLabel();
        lb17 = new javax.swing.JLabel();
        lb18 = new javax.swing.JLabel();
        lb19 = new javax.swing.JLabel();
        lb20 = new javax.swing.JLabel();
        lb21 = new javax.swing.JLabel();
        lb22 = new javax.swing.JLabel();
        lb23 = new javax.swing.JLabel();
        lb11 = new javax.swing.JLabel();
        lb12 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        loantable = new javax.swing.JTable();
        lb13 = new javax.swing.JLabel();
        tp = new javax.swing.JTextField();
        des = new javax.swing.JTextField();
        basic = new javax.swing.JTextField();
        join = new javax.swing.JTextField();
        nis = new javax.swing.JTextField();
        bank = new javax.swing.JTextField();
        gen = new javax.swing.JTextField();
        bd = new javax.swing.JTextField();
        lb14 = new javax.swing.JLabel();
        employeeId = new javax.swing.JComboBox<>();
        employeeName = new javax.swing.JComboBox<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        payroll = new javax.swing.JTable();
        jPanel8 = new javax.swing.JPanel();
        lb27 = new javax.swing.JLabel();
        lb28 = new javax.swing.JLabel();
        lb29 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        lb30 = new javax.swing.JLabel();
        lb3 = new javax.swing.JLabel();
        lb1 = new javax.swing.JLabel();
        lb5 = new javax.swing.JLabel();
        lb7 = new javax.swing.JLabel();
        lb8 = new javax.swing.JLabel();
        lb9 = new javax.swing.JLabel();
        lb31 = new javax.swing.JLabel();
        lb24 = new javax.swing.JLabel();
        lb4 = new javax.swing.JLabel();
        lb10 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        lblDate = new javax.swing.JLabel();
        lblTime = new javax.swing.JLabel();
        lb2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        main.setBackground(new java.awt.Color(255, 255, 255));
        main.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/closealu.png"))); // NOI18N
        lb6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb6MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb6MouseExited(evt);
            }
        });
        main.add(lb6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1870, 0, 40, 40));

        path.setBackground(new java.awt.Color(255, 255, 255));
        path.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/home.png"))); // NOI18N
        path.add(lb32, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 30, 30));

        lb33.setForeground(new java.awt.Color(153, 153, 153));
        lb33.setText("DashBoard");
        lb33.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb33.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb33MouseClicked(evt);
            }
        });
        path.add(lb33, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 16, -1, 20));

        lb34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/next.png"))); // NOI18N
        path.add(lb34, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 0, 20, 50));

        lb35.setText("Employee Review");
        lb35.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        path.add(lb35, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, -1, 30));

        lb36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/next.png"))); // NOI18N
        path.add(lb36, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 0, 20, 50));

        lb37.setForeground(new java.awt.Color(153, 153, 153));
        lb37.setText("Employee");
        lb37.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb37.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb37MouseClicked(evt);
            }
        });
        path.add(lb37, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 16, -1, 20));

        main.add(path, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 90, 830, 50));

        lb38.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 30)); // NOI18N
        lb38.setForeground(new java.awt.Color(153, 153, 153));
        lb38.setText("Employee");
        main.add(lb38, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 40, 190, 60));

        lb26.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lb26.setForeground(new java.awt.Color(204, 204, 204));
        lb26.setText("List");
        main.add(lb26, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 67, 30, 20));

        card.setBackground(new java.awt.Color(204, 255, 255));
        card.setLayout(new java.awt.CardLayout());

        EmployeeReview.setBackground(new java.awt.Color(255, 255, 255));
        EmployeeReview.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        EmployeeReview.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p1.setBackground(new java.awt.Color(0, 0, 102));
        p1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        EmployeeReview.add(p1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1550, 40));

        fil.setBackground(new java.awt.Color(255, 255, 255));
        fil.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        fil.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p2.setBackground(new java.awt.Color(0, 102, 204));
        p2.setForeground(new java.awt.Color(255, 255, 255));
        p2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb40.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lb40.setForeground(new java.awt.Color(255, 255, 255));
        lb40.setText("Filters");
        p2.add(lb40, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 0, 70, 40));

        fil.add(p2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1480, 40));

        EmployeeDesignation.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        EmployeeDesignation.setForeground(new java.awt.Color(51, 51, 51));
        EmployeeDesignation.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EmployeeDesignation.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        EmployeeDesignation.setFocusable(false);
        EmployeeDesignation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EmployeeDesignationActionPerformed(evt);
            }
        });
        EmployeeDesignation.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EmployeeDesignationKeyPressed(evt);
            }
        });
        fil.add(EmployeeDesignation, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 70, 270, 40));

        applyFilter.setBackground(new java.awt.Color(0, 153, 0));
        applyFilter.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        applyFilter.setForeground(new java.awt.Color(255, 255, 255));
        applyFilter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        applyFilter.setText("Apply Filters");
        applyFilter.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        applyFilter.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        applyFilter.setOpaque(true);
        applyFilter.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                applyFilterMouseClicked(evt);
            }
        });
        applyFilter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                applyFilterKeyReleased(evt);
            }
        });
        fil.add(applyFilter, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 70, 140, 40));

        EmployeeId.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        EmployeeId.setForeground(new java.awt.Color(51, 51, 51));
        EmployeeId.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EmployeeId.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        EmployeeId.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                EmployeeIdItemStateChanged(evt);
            }
        });
        EmployeeId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EmployeeIdActionPerformed(evt);
            }
        });
        EmployeeId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EmployeeIdKeyPressed(evt);
            }
        });
        fil.add(EmployeeId, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 70, 270, 40));

        EmployeeName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        EmployeeName.setForeground(new java.awt.Color(51, 51, 51));
        EmployeeName.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EmployeeName.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        EmployeeName.setFocusable(false);
        EmployeeName.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                EmployeeNameItemStateChanged(evt);
            }
        });
        EmployeeName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EmployeeNameActionPerformed(evt);
            }
        });
        EmployeeName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EmployeeNameKeyPressed(evt);
            }
        });
        fil.add(EmployeeName, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 70, 270, 40));

        cancel1.setBackground(new java.awt.Color(153, 0, 0));
        cancel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        cancel1.setForeground(new java.awt.Color(255, 255, 255));
        cancel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cancel1.setText("Cancel");
        cancel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        cancel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancel1.setOpaque(true);
        cancel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancel1MouseClicked(evt);
            }
        });
        fil.add(cancel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1210, 70, 140, 40));

        EmployeeReview.add(fil, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 90, 1380, 150));

        remove.setBackground(new java.awt.Color(153, 0, 0));
        remove.setFont(new java.awt.Font("Trajan Pro", 1, 18)); // NOI18N
        remove.setForeground(new java.awt.Color(255, 255, 255));
        remove.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        remove.setText("Remove Employee");
        remove.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        remove.setOpaque(true);
        remove.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                removeMouseClicked(evt);
            }
        });
        EmployeeReview.add(remove, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 330, 200, 30));

        EmployeeCount.setEditable(false);
        EmployeeCount.setBackground(new java.awt.Color(255, 255, 255));
        EmployeeCount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        EmployeeCount.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        EmployeeReview.add(EmployeeCount, new org.netbeans.lib.awtextra.AbsoluteConstraints(1360, 330, 130, 30));

        EmployeeTable.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EmployeeTable.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        EmployeeTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Employee Name", "Designation", "N.I.C", "Join Date", "Basic Salary"
            }
        ));
        EmployeeTable.setGridColor(new java.awt.Color(51, 102, 255));
        EmployeeTable.setRowHeight(25);
        EmployeeTable.setSelectionBackground(new java.awt.Color(102, 102, 102));
        jScrollPane1.setViewportView(EmployeeTable);

        EmployeeReview.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 380, 1390, 410));

        jLabel20.setFont(new java.awt.Font("Trajan Pro", 1, 14)); // NOI18N
        jLabel20.setText("Count of Employee :");
        EmployeeReview.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(1170, 330, 180, 30));

        jLabel7.setBackground(new java.awt.Color(255, 255, 255));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/refresh.png"))); // NOI18N
        jLabel7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel7.setOpaque(true);
        jLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel7MouseClicked(evt);
            }
        });
        EmployeeReview.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 330, 60, 30));

        card.add(EmployeeReview, "card2");

        UpdateEmployee.setBackground(new java.awt.Color(255, 255, 255));
        UpdateEmployee.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        UpdateEmployee.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p3.setBackground(new java.awt.Color(0, 0, 102));
        p3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        UpdateEmployee.add(p3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1550, 40));

        lb41.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 14)); // NOI18N
        lb41.setText("Gender");
        UpdateEmployee.add(lb41, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 370, -1, -1));

        lb42.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 14)); // NOI18N
        lb42.setText("Join Date *");
        UpdateEmployee.add(lb42, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 540, -1, -1));

        employeeName3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeName3.setText("Employee Name");
        employeeName3.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        employeeName3.setFocusable(false);
        employeeName3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                employeeName3FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                employeeName3FocusLost(evt);
            }
        });
        UpdateEmployee.add(employeeName3, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 160, 390, 40));

        employeeAddress3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeAddress3.setText("Address");
        employeeAddress3.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        employeeAddress3.setFocusable(false);
        employeeAddress3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                employeeAddress3FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                employeeAddress3FocusLost(evt);
            }
        });
        UpdateEmployee.add(employeeAddress3, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 230, 390, 40));

        bankAcc3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        bankAcc3.setText("Acc No");
        bankAcc3.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        bankAcc3.setFocusable(false);
        bankAcc3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                bankAcc3FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                bankAcc3FocusLost(evt);
            }
        });
        UpdateEmployee.add(bankAcc3, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 690, 290, 40));

        phoneNumber3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        phoneNumber3.setText("Phone Number");
        phoneNumber3.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        phoneNumber3.setFocusable(false);
        phoneNumber3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                phoneNumber3FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                phoneNumber3FocusLost(evt);
            }
        });
        UpdateEmployee.add(phoneNumber3, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 310, 390, 40));

        male3.setBackground(new java.awt.Color(255, 255, 255));
        gender.add(male3);
        male3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        male3.setText("Male");
        male3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        male3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        male3.setFocusable(false);
        UpdateEmployee.add(male3, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 410, 80, -1));

        female3.setBackground(new java.awt.Color(255, 255, 255));
        gender.add(female3);
        female3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        female3.setText("Female");
        female3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        female3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        female3.setFocusable(false);
        UpdateEmployee.add(female3, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 410, 90, -1));

        NIC3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        NIC3.setText("NIC");
        NIC3.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        NIC3.setFocusable(false);
        NIC3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                NIC3FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                NIC3FocusLost(evt);
            }
        });
        NIC3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NIC3ActionPerformed(evt);
            }
        });
        UpdateEmployee.add(NIC3, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 230, 390, 40));
        UpdateEmployee.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 82, 1460, 10));

        lb43.setText("Basic Info ");
        UpdateEmployee.add(lb43, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, -1, -1));
        UpdateEmployee.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 520, 1460, 10));

        lb44.setText("Official Info ");
        UpdateEmployee.add(lb44, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 500, -1, -1));

        cancel.setBackground(new java.awt.Color(153, 0, 0));
        cancel.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        cancel.setForeground(new java.awt.Color(255, 255, 255));
        cancel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cancel.setText("Cancel");
        cancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        cancel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancel.setOpaque(true);
        cancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelMouseClicked(evt);
            }
        });
        UpdateEmployee.add(cancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(1140, 780, 150, 40));

        update.setBackground(new java.awt.Color(31, 191, 21));
        update.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        update.setForeground(new java.awt.Color(255, 255, 255));
        update.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        update.setText("Update & Save");
        update.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        update.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        update.setOpaque(true);
        update.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                updateMouseClicked(evt);
            }
        });
        UpdateEmployee.add(update, new org.netbeans.lib.awtextra.AbsoluteConstraints(1300, 780, 220, 40));

        employeeId3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeId3.setForeground(new java.awt.Color(51, 51, 51));
        employeeId3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        employeeId3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeId3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                employeeId3ItemStateChanged(evt);
            }
        });
        employeeId3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeId3ActionPerformed(evt);
            }
        });
        UpdateEmployee.add(employeeId3, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 90, 240, 40));

        employeeDesignation3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeDesignation3.setForeground(new java.awt.Color(51, 51, 51));
        employeeDesignation3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        employeeDesignation3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeDesignation3.setFocusable(false);
        UpdateEmployee.add(employeeDesignation3, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 560, 390, 40));

        employeeSalary3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeSalary3.setForeground(new java.awt.Color(51, 51, 51));
        employeeSalary3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        employeeSalary3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeSalary3.setFocusable(false);
        UpdateEmployee.add(employeeSalary3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 620, 390, 40));

        employeeBank2.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeBank2.setForeground(new java.awt.Color(51, 51, 51));
        employeeBank2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        employeeBank2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeBank2.setFocusable(false);
        UpdateEmployee.add(employeeBank2, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 690, 390, 40));

        joinData3.setFocusable(false);
        UpdateEmployee.add(joinData3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 560, 390, 40));

        EmployeeDOB2.setEditable(false);
        EmployeeDOB2.setBackground(new java.awt.Color(255, 255, 255));
        EmployeeDOB2.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        EmployeeDOB2.setText("D.O.B");
        EmployeeDOB2.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        EmployeeDOB2.setFocusable(false);
        EmployeeDOB2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                EmployeeDOB2FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                EmployeeDOB2FocusLost(evt);
            }
        });
        UpdateEmployee.add(EmployeeDOB2, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 310, 390, 40));

        card.add(UpdateEmployee, "card4");

        NewEmployeee.setBackground(new java.awt.Color(255, 255, 255));
        NewEmployeee.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        NewEmployeee.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p4.setBackground(new java.awt.Color(0, 0, 102));
        p4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        NewEmployeee.add(p4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1550, 40));

        lb445.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 14)); // NOI18N
        lb445.setText("Gender");
        NewEmployeee.add(lb445, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 410, -1, -1));

        employeeId2.setEditable(false);
        employeeId2.setBackground(new java.awt.Color(255, 255, 255));
        employeeId2.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeId2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        employeeId2.setText("Employee Id");
        employeeId2.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        employeeId2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                employeeId2FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                employeeId2FocusLost(evt);
            }
        });
        NewEmployeee.add(employeeId2, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 110, 180, 40));

        employeeName2.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeName2.setText("Employee Name");
        employeeName2.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        employeeName2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                employeeName2FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                employeeName2FocusLost(evt);
            }
        });
        employeeName2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeName2KeyPressed(evt);
            }
        });
        NewEmployeee.add(employeeName2, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 180, 390, 40));

        employeeAdress2.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeAdress2.setText("Address");
        employeeAdress2.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        employeeAdress2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                employeeAdress2FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                employeeAdress2FocusLost(evt);
            }
        });
        employeeAdress2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeAdress2ActionPerformed(evt);
            }
        });
        employeeAdress2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeAdress2KeyPressed(evt);
            }
        });
        NewEmployeee.add(employeeAdress2, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 250, 390, 40));

        bankAcc.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        bankAcc.setText("Acc No");
        bankAcc.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        bankAcc.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                bankAccFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                bankAccFocusLost(evt);
            }
        });
        bankAcc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bankAccActionPerformed(evt);
            }
        });
        bankAcc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bankAccKeyPressed(evt);
            }
        });
        NewEmployeee.add(bankAcc, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 720, 250, 40));

        phoneNumber.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        phoneNumber.setText("Phone Number");
        phoneNumber.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        phoneNumber.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                phoneNumberFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                phoneNumberFocusLost(evt);
            }
        });
        phoneNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                phoneNumberActionPerformed(evt);
            }
        });
        phoneNumber.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                phoneNumberKeyPressed(evt);
            }
        });
        NewEmployeee.add(phoneNumber, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 340, 390, 40));

        male.setBackground(new java.awt.Color(255, 255, 255));
        gender.add(male);
        male.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        male.setText("Male");
        male.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        male.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        male.setFocusable(false);
        male.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maleActionPerformed(evt);
            }
        });
        NewEmployeee.add(male, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 440, 80, -1));

        female.setBackground(new java.awt.Color(255, 255, 255));
        gender.add(female);
        female.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        female.setText("Female");
        female.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        female.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        female.setFocusable(false);
        NewEmployeee.add(female, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 440, 90, -1));

        NIC.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        NIC.setText("NIC");
        NIC.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        NIC.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                NICFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                NICFocusLost(evt);
            }
        });
        NIC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NICActionPerformed(evt);
            }
        });
        NIC.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                NICKeyPressed(evt);
            }
        });
        NewEmployeee.add(NIC, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 250, 270, 40));
        NewEmployeee.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 82, 1460, 10));

        lb46.setText("Basic Info ");
        NewEmployeee.add(lb46, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, -1, -1));
        NewEmployeee.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 520, 1460, 10));

        lb47.setText("Official Info ");
        NewEmployeee.add(lb47, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 500, -1, -1));

        cancel2.setBackground(new java.awt.Color(153, 0, 0));
        cancel2.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        cancel2.setForeground(new java.awt.Color(255, 255, 255));
        cancel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cancel2.setText("Cancel");
        cancel2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        cancel2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancel2.setOpaque(true);
        cancel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancel2MouseClicked(evt);
            }
        });
        NewEmployeee.add(cancel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1170, 780, 150, 40));

        save.setBackground(new java.awt.Color(255, 255, 255));
        save.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        save.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        save.setText("Save");
        save.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 204, 0), 2));
        save.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        save.setOpaque(true);
        save.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                saveFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                saveFocusLost(evt);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                saveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                saveMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                saveMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                saveMouseReleased(evt);
            }
        });
        NewEmployeee.add(save, new org.netbeans.lib.awtextra.AbsoluteConstraints(1350, 780, 150, 40));

        joinDate.setFocusable(false);
        joinDate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                joinDateKeyPressed(evt);
            }
        });
        NewEmployeee.add(joinDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 560, 390, 40));

        EmployeeSalary2.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        EmployeeSalary2.setForeground(new java.awt.Color(51, 51, 51));
        EmployeeSalary2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EmployeeSalary2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        EmployeeSalary2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EmployeeSalary2KeyPressed(evt);
            }
        });
        NewEmployeee.add(EmployeeSalary2, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 640, 390, 40));

        EmployeeDesignation2.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        EmployeeDesignation2.setForeground(new java.awt.Color(51, 51, 51));
        EmployeeDesignation2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        EmployeeDesignation2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        EmployeeDesignation2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EmployeeDesignation2ActionPerformed(evt);
            }
        });
        EmployeeDesignation2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EmployeeDesignation2KeyPressed(evt);
            }
        });
        NewEmployeee.add(EmployeeDesignation2, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 560, 420, 40));

        employeeBank.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeBank.setForeground(new java.awt.Color(51, 51, 51));
        employeeBank.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        employeeBank.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeBank.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                employeeBankItemStateChanged(evt);
            }
        });
        employeeBank.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeBankKeyPressed(evt);
            }
        });
        NewEmployeee.add(employeeBank, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 720, 390, 40));

        lb48.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 14)); // NOI18N
        lb48.setText("Join Date *");
        NewEmployeee.add(lb48, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 540, -1, -1));

        EmployeeDOB.setEditable(false);
        EmployeeDOB.setBackground(new java.awt.Color(255, 255, 255));
        EmployeeDOB.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        EmployeeDOB.setText("D.O.B");
        EmployeeDOB.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        EmployeeDOB.setFocusable(false);
        EmployeeDOB.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                EmployeeDOBFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                EmployeeDOBFocusLost(evt);
            }
        });
        NewEmployeee.add(EmployeeDOB, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 340, 270, 40));

        status.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        status.setForeground(new java.awt.Color(204, 0, 0));
        NewEmployeee.add(status, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 800, 460, 30));

        card.add(NewEmployeee, "card3");

        EmployeeSummary.setBackground(new java.awt.Color(255, 255, 255));
        EmployeeSummary.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        EmployeeSummary.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel10.setBackground(new java.awt.Color(0, 0, 102));
        jPanel10.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        EmployeeSummary.add(jPanel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1550, 40));

        jScrollPane2.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane2.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        EmployeeSummary1.setBackground(new java.awt.Color(255, 255, 255));
        EmployeeSummary1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        EmployeeSummary1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        advances.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        advances.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        advances.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Advance Name", "Year", "Month", "Amount"
            }
        ));
        advances.setGridColor(new java.awt.Color(51, 102, 255));
        advances.setRowHeight(25);
        advances.setSelectionBackground(new java.awt.Color(102, 102, 102));
        jScrollPane4.setViewportView(advances);

        EmployeeSummary1.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 680, 1000, 160));

        lb15.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb15.setText("Select employee          :");
        EmployeeSummary1.add(lb15, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 30, 180, 30));

        lb16.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb16.setText("Employee Designation :");
        EmployeeSummary1.add(lb16, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 90, 190, 30));

        lb17.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb17.setText("Basic Salary               :");
        EmployeeSummary1.add(lb17, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 160, 180, 30));

        lb18.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb18.setText("Join date                    :");
        EmployeeSummary1.add(lb18, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 210, 170, 30));

        lb19.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb19.setText("N.I.C                        :");
        EmployeeSummary1.add(lb19, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 260, 170, 30));

        lb20.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb20.setText("BirthDay               :");
        EmployeeSummary1.add(lb20, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 160, 150, 30));

        lb21.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb21.setText("Gender                  :");
        EmployeeSummary1.add(lb21, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 100, 160, 30));

        lb22.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb22.setText("Employee Salary Details :");
        EmployeeSummary1.add(lb22, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 870, 210, 30));

        lb23.setBackground(new java.awt.Color(204, 0, 0));
        lb23.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        lb23.setForeground(new java.awt.Color(255, 255, 255));
        lb23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb23.setText("Clear");
        lb23.setOpaque(true);
        lb23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb23MouseClicked(evt);
            }
        });
        EmployeeSummary1.add(lb23, new org.netbeans.lib.awtextra.AbsoluteConstraints(1420, 20, 100, 30));

        lb11.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb11.setText("Bank Acc                  :");
        EmployeeSummary1.add(lb11, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 320, 170, 30));

        lb12.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb12.setText("Employee Loans        :");
        EmployeeSummary1.add(lb12, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 420, 170, 30));

        loantable.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        loantable.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        loantable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Loan Name", "Date", "Amount"
            }
        ));
        loantable.setGridColor(new java.awt.Color(51, 102, 255));
        loantable.setRowHeight(25);
        loantable.setSelectionBackground(new java.awt.Color(102, 102, 102));
        jScrollPane5.setViewportView(loantable);

        EmployeeSummary1.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 460, 910, 160));

        lb13.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb13.setText("Employee Advances     :");
        EmployeeSummary1.add(lb13, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 650, 180, 30));

        tp.setEditable(false);
        tp.setBackground(new java.awt.Color(255, 255, 255));
        tp.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        EmployeeSummary1.add(tp, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 220, 320, 40));

        des.setEditable(false);
        des.setBackground(new java.awt.Color(255, 255, 255));
        des.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        EmployeeSummary1.add(des, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 90, 320, 40));

        basic.setEditable(false);
        basic.setBackground(new java.awt.Color(255, 255, 255));
        basic.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        EmployeeSummary1.add(basic, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 150, 320, 40));

        join.setEditable(false);
        join.setBackground(new java.awt.Color(255, 255, 255));
        join.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        EmployeeSummary1.add(join, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 210, 320, 40));

        nis.setEditable(false);
        nis.setBackground(new java.awt.Color(255, 255, 255));
        nis.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        EmployeeSummary1.add(nis, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 270, 320, 40));

        bank.setEditable(false);
        bank.setBackground(new java.awt.Color(255, 255, 255));
        bank.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        EmployeeSummary1.add(bank, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 330, 320, 40));

        gen.setEditable(false);
        gen.setBackground(new java.awt.Color(255, 255, 255));
        gen.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        EmployeeSummary1.add(gen, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 100, 320, 40));

        bd.setEditable(false);
        bd.setBackground(new java.awt.Color(255, 255, 255));
        bd.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        EmployeeSummary1.add(bd, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 160, 320, 40));

        lb14.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb14.setText("Telephone Number :");
        EmployeeSummary1.add(lb14, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 230, 170, 30));

        employeeId.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeId.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                employeeIdItemStateChanged(evt);
            }
        });
        employeeId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeIdActionPerformed(evt);
            }
        });
        EmployeeSummary1.add(employeeId, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 30, 220, 40));

        employeeName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeName.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                employeeNameItemStateChanged(evt);
            }
        });
        employeeName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeNameActionPerformed(evt);
            }
        });
        EmployeeSummary1.add(employeeName, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 30, 330, 40));

        payroll.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "NormalOT", "Double OT", "E.P.F 12%", "E.P.F 8%", "E.T.F 3%", "Loan", "Advances", "Monthly Salary"
            }
        ));
        jScrollPane3.setViewportView(payroll);

        EmployeeSummary1.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 910, 1440, 210));

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1480, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        EmployeeSummary1.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 1170, 1480, 40));

        jScrollPane2.setViewportView(EmployeeSummary1);

        EmployeeSummary.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 1550, 800));

        card.add(EmployeeSummary, "card3");

        main.add(card, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 210, 1550, 840));

        lb27.setBackground(new java.awt.Color(250, 250, 250));
        lb27.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 18)); // NOI18N
        lb27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb27.setText("Update Employee");
        lb27.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb27.setOpaque(true);
        lb27.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb27MouseClicked(evt);
            }
        });
        main.add(lb27, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 170, 200, 40));

        lb28.setBackground(new java.awt.Color(0, 0, 102));
        lb28.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 18)); // NOI18N
        lb28.setForeground(new java.awt.Color(255, 255, 255));
        lb28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb28.setText("Employee Review");
        lb28.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb28.setOpaque(true);
        lb28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb28MouseClicked(evt);
            }
        });
        main.add(lb28, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 170, 200, 40));

        lb29.setBackground(new java.awt.Color(250, 250, 250));
        lb29.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 18)); // NOI18N
        lb29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb29.setText("New Employee");
        lb29.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb29.setOpaque(true);
        lb29.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb29MouseClicked(evt);
            }
        });
        main.add(lb29, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 170, 200, 40));

        jSeparator1.setBackground(new java.awt.Color(204, 204, 255));
        jSeparator1.setForeground(new java.awt.Color(204, 255, 204));
        main.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 310, 10));

        lb30.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 14)); // NOI18N
        lb30.setForeground(new java.awt.Color(255, 255, 255));
        lb30.setText("Menu");
        main.add(lb30, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 60, -1));

        lb3.setBackground(new java.awt.Color(242, 241, 239));
        lb3.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb3.setForeground(new java.awt.Color(255, 255, 255));
        lb3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/home2.png"))); // NOI18N
        lb3.setText("DashBoard");
        lb3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb3.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lb3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb3MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb3MouseExited(evt);
            }
        });
        main.add(lb3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 240, 70));

        lb1.setBackground(new java.awt.Color(242, 241, 239));
        lb1.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb1.setForeground(new java.awt.Color(255, 255, 255));
        lb1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/admin1.png"))); // NOI18N
        lb1.setText(" Administration");
        lb1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb1MouseExited(evt);
            }
        });
        main.add(lb1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 230, 290, 70));

        lb5.setBackground(new java.awt.Color(242, 241, 239));
        lb5.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb5.setForeground(new java.awt.Color(0, 204, 0));
        lb5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/employee.png"))); // NOI18N
        lb5.setText("  Employee");
        lb5.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb5MouseExited(evt);
            }
        });
        main.add(lb5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 320, 240, 70));

        lb7.setBackground(new java.awt.Color(242, 241, 239));
        lb7.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb7.setForeground(new java.awt.Color(255, 255, 255));
        lb7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/attendance.png"))); // NOI18N
        lb7.setText("  Attendance");
        lb7.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb7MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb7MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb7MouseExited(evt);
            }
        });
        main.add(lb7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 410, 260, 70));

        lb8.setBackground(new java.awt.Color(242, 241, 239));
        lb8.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb8.setForeground(new java.awt.Color(255, 255, 255));
        lb8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/loan.png"))); // NOI18N
        lb8.setText("  Loan");
        lb8.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb8MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb8MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb8MouseExited(evt);
            }
        });
        main.add(lb8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 500, 190, 70));

        lb9.setBackground(new java.awt.Color(242, 241, 239));
        lb9.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb9.setForeground(new java.awt.Color(255, 255, 255));
        lb9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/advance.png"))); // NOI18N
        lb9.setText("  Advances");
        lb9.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb9MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb9MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb9MouseExited(evt);
            }
        });
        main.add(lb9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 590, 240, 70));

        lb31.setBackground(new java.awt.Color(250, 250, 250));
        lb31.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 18)); // NOI18N
        lb31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb31.setText("Employee Summary");
        lb31.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb31.setOpaque(true);
        lb31.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb31MouseClicked(evt);
            }
        });
        main.add(lb31, new org.netbeans.lib.awtextra.AbsoluteConstraints(1000, 170, 200, 40));

        lb24.setBackground(new java.awt.Color(242, 241, 239));
        lb24.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb24.setForeground(new java.awt.Color(255, 255, 255));
        lb24.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lb24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/payroll.png"))); // NOI18N
        lb24.setText("  Payroll Review");
        lb24.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb24.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb24.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lb24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb24MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb24MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb24MouseExited(evt);
            }
        });
        main.add(lb24, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 770, 260, 70));

        lb4.setBackground(new java.awt.Color(242, 241, 239));
        lb4.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb4.setForeground(new java.awt.Color(255, 255, 255));
        lb4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/reports.png"))); // NOI18N
        lb4.setText("  Reports");
        lb4.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb4MouseExited(evt);
            }
        });
        main.add(lb4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 860, 230, 70));

        lb10.setBackground(new java.awt.Color(242, 241, 239));
        lb10.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb10.setForeground(new java.awt.Color(255, 255, 255));
        lb10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/payroll.png"))); // NOI18N
        lb10.setText("  Payroll");
        lb10.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb10MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb10MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb10MouseExited(evt);
            }
        });
        main.add(lb10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 680, 210, 70));
        main.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 940, 290, 10));

        lblDate.setFont(new java.awt.Font("DigifaceWide", 0, 18)); // NOI18N
        lblDate.setForeground(new java.awt.Color(255, 255, 255));
        main.add(lblDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 960, 220, 40));

        lblTime.setFont(new java.awt.Font("DigifaceWide", 0, 18)); // NOI18N
        lblTime.setForeground(new java.awt.Color(255, 255, 255));
        main.add(lblTime, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 1010, 220, 40));

        lb2.setBackground(new java.awt.Color(0, 0, 153));
        lb2.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        lb2.setForeground(new java.awt.Color(255, 255, 255));
        lb2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/employee.jpg"))); // NOI18N
        lb2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        main.add(lb2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1960, 1080));

        getContentPane().add(main, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lb6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseClicked
        int result = JOptionPane.showConfirmDialog(this, "Do You Want To Exit?", "Exit:", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            this.dispose();
        } else if (result == JOptionPane.NO_OPTION) {
            this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        }
    }//GEN-LAST:event_lb6MouseClicked

    private void lb6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseEntered
        ImageIcon icon = new ImageIcon("src/lk/ijse/payroll/asset/closerathu.png");
        lb6.setIcon(icon);
    }//GEN-LAST:event_lb6MouseEntered

    private void lb6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseExited
        ImageIcon icon = new ImageIcon("src/lk/ijse/payroll/asset/closesudu.png");
        lb6.setIcon(icon);
    }//GEN-LAST:event_lb6MouseExited

    private void lb33MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb33MouseClicked
        DashBoard d1 = new DashBoard();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb33MouseClicked

    private void lb37MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb37MouseClicked
        lb35.setText("Employee Review");
        card.removeAll();
        card.repaint();
        card.revalidate();
        card.add(EmployeeReview);
        card.repaint();
        card.revalidate();
        lb28.setBackground(new Color(0, 0, 102));
        lb27.setBackground(new Color(255, 255, 255));
        lb29.setBackground(new Color(255, 255, 255));
        lb31.setBackground(new Color(255, 255, 255));
        lb31.setForeground(new Color(0, 0, 0));
        lb27.setForeground(new Color(0, 0, 0));
        lb28.setForeground(new Color(255, 255, 255));
        lb29.setForeground(new Color(0, 0, 0));
    }//GEN-LAST:event_lb37MouseClicked

    private void lb29MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb29MouseClicked
        lb35.setText("New Employee");
        employeeName2.requestFocus();
        card.removeAll();
        card.repaint();
        card.revalidate();
        card.add(NewEmployeee);
        card.repaint();
        card.revalidate();
        lb29.setBackground(new Color(0, 0, 102));
        lb28.setBackground(new Color(255, 255, 255));
        lb27.setBackground(new Color(255, 255, 255));
        lb31.setBackground(new Color(255, 255, 255));
        lb31.setForeground(new Color(0, 0, 0));
        lb28.setForeground(new Color(0, 0, 0));
        lb27.setForeground(new Color(0, 0, 0));
        lb29.setForeground(new Color(255, 255, 255));
    }//GEN-LAST:event_lb29MouseClicked

    private void lb27MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb27MouseClicked
        lb35.setText("Update Employee");
        card.removeAll();
        card.repaint();
        card.revalidate();
        card.add(UpdateEmployee);
        card.repaint();
        card.revalidate();
        lb27.setBackground(new Color(0, 0, 102));
        lb28.setBackground(new Color(255, 255, 255));
        lb29.setBackground(new Color(255, 255, 255));
        lb28.setForeground(new Color(0, 0, 0));
        lb27.setForeground(new Color(255, 255, 255));
        lb29.setForeground(new Color(0, 0, 0));
        lb31.setBackground(new Color(255, 255, 255));
        lb31.setForeground(new Color(0, 0, 0));
    }//GEN-LAST:event_lb27MouseClicked

    private void lb3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb3MouseClicked
        DashBoard d1 = new DashBoard();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb3MouseClicked

    private void lb3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb3MouseEntered
        lb3.setForeground(Color.RED);
    }//GEN-LAST:event_lb3MouseEntered

    private void lb3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb3MouseExited
        lb3.setForeground(Color.WHITE);
    }//GEN-LAST:event_lb3MouseExited

    private void lb1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseEntered
        lb1.setForeground(Color.RED);
    }//GEN-LAST:event_lb1MouseEntered

    private void lb1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseExited
        lb1.setForeground(Color.white);
    }//GEN-LAST:event_lb1MouseExited

    private void lb5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseEntered
        lb5.setForeground(Color.RED);
    }//GEN-LAST:event_lb5MouseEntered

    private void lb5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseExited
        lb5.setForeground(new Color(0, 204, 0));
    }//GEN-LAST:event_lb5MouseExited

    private void lb7MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseEntered
        lb7.setForeground(Color.RED);
    }//GEN-LAST:event_lb7MouseEntered

    private void lb7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseExited
        lb7.setForeground(Color.white);
    }//GEN-LAST:event_lb7MouseExited

    private void lb8MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseEntered
        lb8.setForeground(Color.RED);
    }//GEN-LAST:event_lb8MouseEntered

    private void lb8MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseExited
        lb8.setForeground(Color.white);
    }//GEN-LAST:event_lb8MouseExited

    private void lb9MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseEntered
        lb9.setForeground(Color.RED);
    }//GEN-LAST:event_lb9MouseEntered

    private void lb9MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseExited
        lb9.setForeground(Color.white);
    }//GEN-LAST:event_lb9MouseExited

    private void lb8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseClicked
        placeLoans d1 = new placeLoans();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb8MouseClicked

    private void lb9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseClicked
        PlaceAdvance d1 = new PlaceAdvance();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb9MouseClicked

    private void EmployeeDesignation2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EmployeeDesignation2ActionPerformed

    }//GEN-LAST:event_EmployeeDesignation2ActionPerformed

    private void lb28MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb28MouseClicked
        lb35.setText("Employee Review");
        card.removeAll();
        card.repaint();
        card.revalidate();
        card.add(EmployeeReview);
        card.repaint();
        card.revalidate();
        lb28.setBackground(new Color(0, 0, 102));
        lb27.setBackground(new Color(255, 255, 255));
        lb29.setBackground(new Color(255, 255, 255));
        lb31.setBackground(new Color(255, 255, 255));
        lb27.setForeground(new Color(0, 0, 0));
        lb28.setForeground(new Color(255, 255, 255));
        lb29.setForeground(new Color(0, 0, 0));
        lb31.setForeground(new Color(0, 0, 0));
    }//GEN-LAST:event_lb28MouseClicked

    private void lb1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseClicked
        Administation d1 = new Administation();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb1MouseClicked

    private void lb7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseClicked
        Attendance d1 = new Attendance();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb7MouseClicked

    private void maleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_maleActionPerformed

    private void NICActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NICActionPerformed
        if (!(NIC.getText().matches("^[0-9]{9}[vVxX]$"))) {
            status.setText("Invalid NIC ");
            NIC.selectAll();
            NIC.requestFocus();
        } else if (NIC.getText().isEmpty()) {
            EmployeeDOB.setText("");
            status.setText("Please Enter NIC No");
            gender.clearSelection();
        } else if (NIC.getText() == "NIC") {
            EmployeeDOB.setText("");
            gender.clearSelection();
            status.setText("Please Enter NIC No");
        } else {
            String nIC = NIC.getText();
            DOB d = new DOB(nIC);
            String a = d.getYear();
            String g = d.getSex();
            System.out.println(a);
            System.out.println(g);
            EmployeeDOB.setText(a);
            if (g == "Male") {
                male.setSelected(true);
                gender.setSelected(male.getModel(), true);
                female.setEnabled(false);
            } else if (g == "Female") {
                female.setSelected(true);
                gender.setSelected(female.getModel(), true);
                male.setEnabled(false);
            }
            employeeAdress2.requestFocus();
        }

    }//GEN-LAST:event_NICActionPerformed

    private void employeeAdress2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeAdress2ActionPerformed
        if (!validateLetters(employeeAdress2.getText())) {
            status.setText("Plese Enter Correct Address");
            employeeAdress2.requestFocus();
            employeeAdress2.selectAll();

        } else if (employeeAdress2.getText().trim().isEmpty()) {
            status.setText("Plese Enter Employee Address");
            employeeAdress2.requestFocus();
            employeeAdress2.selectAll();
        } else {
            phoneNumber.requestFocus();
        }

    }//GEN-LAST:event_employeeAdress2ActionPerformed

    private void employeeId2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_employeeId2FocusLost
        if (employeeId2.getText().isEmpty()) {
            employeeId2.setText("Employee Id");
        }
        employeeId2.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));

    }//GEN-LAST:event_employeeId2FocusLost

    private void employeeId2FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_employeeId2FocusGained
        if (employeeId2.getText().equals("Employee Id")) {
            employeeId2.setText("");
            employeeId2.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }
    }//GEN-LAST:event_employeeId2FocusGained

    private void employeeName2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_employeeName2FocusLost
        if (employeeName2.getText().isEmpty()) {
            employeeName2.setText("Employee Name");
        }
        employeeName2.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));

    }//GEN-LAST:event_employeeName2FocusLost

    private void employeeName2FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_employeeName2FocusGained
        if (employeeName2.getText().equals("Employee Name")) {
            employeeName2.setText("");
            employeeName2.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }
    }//GEN-LAST:event_employeeName2FocusGained

    private void NICFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_NICFocusLost
        if (NIC.getText().isEmpty()) {
            NIC.setText("NIC");
        }
        NIC.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));

    }//GEN-LAST:event_NICFocusLost

    private void NICFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_NICFocusGained
        if (NIC.getText().equals("NIC")) {
            NIC.setText("");
            NIC.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }
    }//GEN-LAST:event_NICFocusGained

    private void employeeAdress2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_employeeAdress2FocusLost
        if (employeeAdress2.getText().isEmpty()) {
            employeeAdress2.setText("Address");
        }
        employeeAdress2.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));


    }//GEN-LAST:event_employeeAdress2FocusLost

    private void employeeAdress2FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_employeeAdress2FocusGained
        if (employeeAdress2.getText().equals("Address")) {
            employeeAdress2.setText("");
            employeeAdress2.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }
    }//GEN-LAST:event_employeeAdress2FocusGained

    private void phoneNumberFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_phoneNumberFocusLost
        if (phoneNumber.getText().isEmpty()) {
            phoneNumber.setText("Phone Number");
        }
        phoneNumber.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));


    }//GEN-LAST:event_phoneNumberFocusLost

    private void phoneNumberFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_phoneNumberFocusGained
        if (phoneNumber.getText().equals("Phone Number")) {
            phoneNumber.setText("");
            phoneNumber.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }
    }//GEN-LAST:event_phoneNumberFocusGained

    private void bankAccFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_bankAccFocusLost
        if (bankAcc.getText().isEmpty()) {
            bankAcc.setText("Acc No");
        }
        bankAcc.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));


    }//GEN-LAST:event_bankAccFocusLost

    private void bankAccFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_bankAccFocusGained
        if (bankAcc.getText().equals("Acc No")) {
            bankAcc.setText("");
            bankAcc.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }
    }//GEN-LAST:event_bankAccFocusGained

    private void NIC3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NIC3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NIC3ActionPerformed

    private void employeeId3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeId3ActionPerformed
        if (employeeId3.getSelectedIndex() != -1) {
            try {
                String id = (String) employeeId3.getSelectedItem();
                EmployeeDTO e = ctrl.searchemp(id);
                String de = ctrl.searchDes(e.getDeId());
                String sal = ctrl.searchSal(e.getEmpId());
                QueryDTO ban = ctrl.serachBankDetails(id);
                if (e != null) {
                    if (e.getGender() == "Male") {
                        male3.setSelected(true);
                        gender.setSelected(male3.getModel(), true);

                    } else if (e.getGender() == "Female") {
                        female3.setSelected(true);
                        gender.setSelected(female3.getModel(), true);

                    }

                    employeeName3.setText(e.getEmpName());
                    NIC3.setText(e.getNIC());
                    employeeAddress3.setText(e.getAddress());
                    phoneNumber3.setText("" + e.getPhone_No());
                    EmployeeDOB2.setText(e.getD_O_B());
                    joinData3.setDate(new Date(e.getJoin_Date()));
                    employeeDesignation3.setSelectedItem(de);
                    employeeSalary3.setSelectedItem(sal);
                    bankAcc3.setText(ban.getAcc_no());
                    employeeBank2.setSelectedItem(ban.getBankName());

                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeId3ActionPerformed

    private void employeeName3FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_employeeName3FocusGained
        if (employeeName3.getText().equals("Employee Name")) {
            employeeName3.setText("");
            employeeName3.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));

        }
    }//GEN-LAST:event_employeeName3FocusGained

    private void employeeName3FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_employeeName3FocusLost
        if (employeeName3.getText().isEmpty()) {
            employeeName3.setText("Employee Name");
        }
        employeeName3.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));

    }//GEN-LAST:event_employeeName3FocusLost

    private void NIC3FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_NIC3FocusGained
        if (NIC3.getText().equals("NIC")) {
            NIC3.setText("");
            NIC3.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }
    }//GEN-LAST:event_NIC3FocusGained

    private void NIC3FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_NIC3FocusLost
        if (NIC3.getText().isEmpty()) {
            NIC3.setText("NIC");
        }
        NIC3.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));

    }//GEN-LAST:event_NIC3FocusLost

    private void employeeAddress3FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_employeeAddress3FocusGained
        if (employeeAddress3.getText().equals("Address")) {
            employeeAddress3.setText("");
            employeeAddress3.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }


    }//GEN-LAST:event_employeeAddress3FocusGained

    private void employeeAddress3FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_employeeAddress3FocusLost
        if (employeeAddress3.getText().isEmpty()) {
            employeeAddress3.setText("Address");
        }
        employeeAddress3.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));

    }//GEN-LAST:event_employeeAddress3FocusLost

    private void phoneNumber3FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_phoneNumber3FocusGained
        if (phoneNumber3.getText().equals("Phone Number")) {
            phoneNumber3.setText("");
            phoneNumber3.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }


    }//GEN-LAST:event_phoneNumber3FocusGained

    private void phoneNumber3FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_phoneNumber3FocusLost
        if (phoneNumber3.getText().isEmpty()) {
            phoneNumber3.setText("Phone Number");
        }
        phoneNumber3.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));

    }//GEN-LAST:event_phoneNumber3FocusLost

    private void bankAcc3FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_bankAcc3FocusGained
        if (bankAcc3.getText().equals("Acc No")) {
            bankAcc3.setText("");
            bankAcc3.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }

    }//GEN-LAST:event_bankAcc3FocusGained

    private void bankAcc3FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_bankAcc3FocusLost
        if (bankAcc3.getText().isEmpty()) {
            bankAcc3.setText("Acc No");
        }
        bankAcc3.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));

    }//GEN-LAST:event_bankAcc3FocusLost

    private void saveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseClicked
        if ("Employee Name".equals(employeeName2.getText())) {
            employeeName2.requestFocus();

            status.setText("Please Enter Name For Employee");
        } else if (!validateLetters(employeeName2.getText())) {
            employeeName2.requestFocus();
            employeeName2.selectAll();

            status.setText("Invaild Name For Employee");

        } else if (employeeName2.getText().equals("")) {

            status.setText("please Enter Employee Name");
            employeeName2.requestFocus();
        } else if (!(NIC.getText().matches("^[0-9]{9}[vVxX]$"))) {
            status.setText("Invalid NIC ");
            NIC.selectAll();
            NIC.requestFocus();
        } else if (NIC.getText().isEmpty()) {
            EmployeeDOB.setText("");
            status.setText("Please Enter NIC No");
            gender.clearSelection();
        } else if (NIC.getText() == "NIC") {
            EmployeeDOB.setText("");
            gender.clearSelection();
            status.setText("Please Enter NIC No");
        } else if (!validateLetters(employeeAdress2.getText())) {
            status.setText("Plese Enter Correct Address");
            employeeAdress2.requestFocus();
            employeeAdress2.selectAll();

        } else if (employeeAdress2.getText().trim().isEmpty()) {
            status.setText("Plese Enter Employee Address");
            employeeAdress2.requestFocus();
            employeeAdress2.selectAll();
        } else if (!(phoneNumber.getText().matches("^[0-9]{10}"))) {
            status.setText("Invalid Mobile Number");
            phoneNumber.requestFocus();
            phoneNumber.selectAll();
        } else if (phoneNumber.getText().trim().isEmpty()) {
            status.setText("Please Enter Mobile Number");
            phoneNumber.requestFocus();
        } else if (EmployeeDesignation2.getSelectedIndex() == -1) {
            status.setText("PLease Select Designation");
            EmployeeDesignation2.requestFocus();

        } else if (EmployeeSalary2.getSelectedIndex() == -1) {
            status.setText("PLease Select Basic Salary For Employee");
            EmployeeSalary2.requestFocus();

        } else if (employeeBank.getSelectedIndex() == -1) {
            status.setText("PLease Select Employee's Service Bank");
            employeeBank.requestFocus();

        } else if (bankAcc.getText().trim().isEmpty()) {
            status.setText("PLease Enter Bank Acc No");
            bankAcc.requestFocus();
        } else if (validateLetters(bankAcc.getText())) {
            status.setText("PLease Enter valid Bank AccNo");
            bankAcc.requestFocus();
        } else {
            try {
                String empId = employeeId2.getText();
                String empName = employeeName2.getText();
                String address = employeeAdress2.getText();
                String gender = getSelectedButtonText(this.gender);
                String nIC = NIC.getText();
                String D_O_B = EmployeeDOB.getText();
                String phone_No = phoneNumber.getText();
                String join_Date = ((JTextField) joinDate.getDateEditor().getUiComponent()).getText();
                String deId = (String) EmployeeDesignation2.getSelectedItem();
                String acc = bankAcc.getText();
                DesignationDTO designation = ctrl.search(deId);

                String da = designation.getDeId();
                String id = ctrl.searchsalaryId(EmployeeSalary2.getSelectedItem().toString());

                BankDTO bankid = ctrl.searchbankId(employeeBank.getSelectedItem().toString());
                String bankId = bankid.getBankId();

                SalaryDetailsDTO salary = new SalaryDetailsDTO(0, empId, id);
                BankDetailsDTO bd = new BankDetailsDTO(0, empId, bankId, acc);
                EmployeeDTO employee = new EmployeeDTO(empId, empName, address, gender, nIC, D_O_B, phone_No, join_Date, da, 21);

                boolean added = ctrl.add(employee, bd, salary);
                if (added) {
                    JOptionPane.showMessageDialog(this, "Employee Added faild");
                } else {
                    JOptionPane.showMessageDialog(this, "Employee Added Succes");
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage());
            }
            clear();
            remove();

            loadAllItems();
            loadAllEmployee();
            employeeId2.setText(loadNextempId());
        }


    }//GEN-LAST:event_saveMouseClicked

    private void cancel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancel2MouseClicked
        int result = JOptionPane.showConfirmDialog(this, "Do You Want To Clear Data?", "Warning:", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            clear();
            remove();
            loadAllItems();
        } else if (result == JOptionPane.NO_OPTION) {

        }


    }//GEN-LAST:event_cancel2MouseClicked

    private void bankAccActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bankAccActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bankAccActionPerformed

    private void employeeId3ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_employeeId3ItemStateChanged


    }//GEN-LAST:event_employeeId3ItemStateChanged

    private void updateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_updateMouseClicked
        if (employeeId3.getSelectedIndex() != -1) {
            try {
                String empName = employeeName3.getText();
                String address = employeeAddress3.getText();
                String gender = getSelectedButtonText(this.gender);
                String nIC = NIC3.getText();
                String D_O_B = EmployeeDOB2.getText();
                String phone_No = phoneNumber3.getText();
                String join_Date = ((JTextField) joinData3.getDateEditor().getUiComponent()).getText();
                String deId = (String) employeeDesignation3.getSelectedItem();
                String acc = bankAcc3.getText();
                DesignationDTO designation = ctrl.search(deId);
                String da = designation.getDeId();

                String id = ctrl.searchsalaryId(employeeSalary3.getSelectedItem().toString());

                BankDTO bankid = ctrl.searchbankId(employeeBank2.getSelectedItem().toString());
                String bankId = bankid.getBankId();
                String empid = employeeId3.getSelectedItem().toString();
                String bank = ctrl.searchBankDetailId(employeeId3.getSelectedItem().toString());

                String salarydetail = ctrl.searchSalaryDetailId(employeeId3.getSelectedItem().toString());

                SalaryDetailsDTO salary = new SalaryDetailsDTO(Integer.parseInt(salarydetail), empid, id);
                BankDetailsDTO bd = new BankDetailsDTO(Integer.parseInt(bank), empid, bankId, acc);
                EmployeeDTO employee = new EmployeeDTO(empid, empName, address, gender, nIC, D_O_B, phone_No, join_Date, da, 21);
                System.out.println(empid);
                boolean update = ctrl.updateemployee(employee, salary, bd);
                if (update) {
                    JOptionPane.showMessageDialog(this, "Employee updated Succes");
                } else {
                    JOptionPane.showMessageDialog(this, "Employee updated faild");
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage());
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
            clear();
//            loadAllEmployee();
            remove();

//            loadAllItems();
        }
    }//GEN-LAST:event_updateMouseClicked

    private void EmployeeDesignationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EmployeeDesignationActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EmployeeDesignationActionPerformed

    private void EmployeeIdItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_EmployeeIdItemStateChanged

    }//GEN-LAST:event_EmployeeIdItemStateChanged

    public void searchItem(String id) {
        try {
            EmployeeDTO searchemp = ctrl.searchemp(id);
            JOptionPane.showMessageDialog(rootPane, searchemp.getAddress());

        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    private void EmployeeNameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_EmployeeNameItemStateChanged

    }//GEN-LAST:event_EmployeeNameItemStateChanged

    private void EmployeeIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EmployeeIdActionPerformed
        if (EmployeeId.getSelectedIndex() != -1) {
            try {
                String id = (String) EmployeeId.getSelectedItem();
                //searchItem(id);
                EmployeeDTO e = ctrl.searchemp(id);
                if (e != null) {
                    String de = ctrl.searchDes(e.getDeId());
                    String d = e.getEmpName();
                    //System.out.println(d);
                    //JOptionPane.showMessageDialog(this, e.getAddress());
                    EmployeeName.setSelectedItem(d);

                    EmployeeDesignation.setSelectedItem(de);

                    //bankAcc3.setText();
                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_EmployeeIdActionPerformed

    private void removeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_removeMouseClicked
        try {
            if (EmployeeTable.getSelectedRow() == -1) {
                return;
            }

            boolean result = ctrl.delete(EmployeeTable.getValueAt(EmployeeTable.getSelectedRow(), 0).toString());

            if (result) {
                JOptionPane.showMessageDialog(this, "Employee has been deleted successfully");
            } else {
                JOptionPane.showMessageDialog(this, "Failed to delete the Employee");
            }

            clear();
            EmployeeTable.getSelectionModel().clearSelection();
            loadAllEmployee();
            remove();

            loadAllItems();

        } catch (Exception ex) {
            Logger.getLogger(Loans.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_removeMouseClicked

    private void applyFilterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_applyFilterMouseClicked
        if (EmployeeId.getSelectedIndex() == -1 & EmployeeDesignation.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Please Select One Category");
            employeeId.requestFocus();
        } else if (EmployeeId.getSelectedIndex() != -1) {
            try {
                DefaultTableModel dtm = (DefaultTableModel) EmployeeTable.getModel();
                dtm.setRowCount(0);

                String EmployeeID = EmployeeId.getSelectedItem().toString();
                EmployeeDTO e = ctrl.searchemp(EmployeeID);
                String basicSalary = ctrl.searchSal(e.getEmpId());
                System.out.println(basicSalary);
                String de = ctrl.searchDes(e.getDeId());

                if (e != null) {
                    Object[] employee = {e.getEmpName(), de, e.getNIC(), e.getJoin_Date(), basicSalary};
                    dtm.addRow(employee);
                }

                int i = dtm.getRowCount();
                EmployeeCount.setText("" + i);

            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
            EmployeeId.setSelectedIndex(-1);
            EmployeeName.setSelectedIndex(-1);
            EmployeeDesignation.setSelectedIndex(-1);
        } else if (EmployeeId.getSelectedIndex() == -1) {
            try {
                DefaultTableModel dtm = (DefaultTableModel) EmployeeTable.getModel();
                dtm.setRowCount(0);
                String des = EmployeeDesignation.getSelectedItem().toString();
                DesignationDTO designation = ctrl.search(des);
                String deId = designation.getDeId();
                ArrayList<EmployeeDTO> a = ctrl.searchempBydes(deId);
                for (EmployeeDTO e : a) {
                    String basicSalary = ctrl.searchSal(e.getEmpId());
                    String de = ctrl.searchDes(e.getDeId());
                    Object[] employee = {e.getEmpName(), de, e.getNIC(), e.getJoin_Date(), basicSalary};
                    dtm.addRow(employee);

                }
                int i = dtm.getRowCount();
                EmployeeCount.setText("" + i);
            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
            EmployeeId.setSelectedIndex(-1);
            EmployeeName.setSelectedIndex(-1);
            EmployeeDesignation.setSelectedIndex(-1);

        }

    }//GEN-LAST:event_applyFilterMouseClicked

    private void cancelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelMouseClicked
        int result = JOptionPane.showConfirmDialog(this, "Do You Want To Clear Data?", "Warning:", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            clear();
            remove();
            loadAllItems();
        } else if (result == JOptionPane.NO_OPTION) {

        }


    }//GEN-LAST:event_cancelMouseClicked

    private void EmployeeNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EmployeeNameActionPerformed
        if (EmployeeName.getSelectedIndex() != -1) {
            try {
                String name = (String) EmployeeName.getSelectedItem();
                EmployeeDTO em = ctrl.searchempid(name);

                if (em != null) {
                    String dep = ctrl.searchDes(em.getDeId());

                    String d = em.getEmpId();
                    //System.out.println(d);

                    EmployeeId.setSelectedItem(d);
                    EmployeeDesignation.setSelectedItem(dep);

                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_EmployeeNameActionPerformed

    private void jLabel7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseClicked
        loadAllEmployee();
    }//GEN-LAST:event_jLabel7MouseClicked

    private void EmployeeDOB2FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_EmployeeDOB2FocusGained
        if (EmployeeDOB2.getText().equals("D.O.B")) {
            EmployeeDOB2.setText("");
            EmployeeDOB2.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }
    }//GEN-LAST:event_EmployeeDOB2FocusGained

    private void EmployeeDOB2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_EmployeeDOB2FocusLost
        if (EmployeeDOB2.getText().isEmpty()) {
            EmployeeDOB2.setText("D.O.B");

        }
        EmployeeDOB2.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));
    }//GEN-LAST:event_EmployeeDOB2FocusLost

    private void EmployeeDOBFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_EmployeeDOBFocusGained
        if (EmployeeDOB.getText().equals("D.O.B")) {
            EmployeeDOB.setText("");
            EmployeeDOB.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }
    }//GEN-LAST:event_EmployeeDOBFocusGained

    private void EmployeeDOBFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_EmployeeDOBFocusLost
        if (EmployeeDOB.getText().isEmpty()) {
            EmployeeDOB.setText("D.O.B");

        }
        EmployeeDOB.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));
    }//GEN-LAST:event_EmployeeDOBFocusLost

    private void EmployeeIdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EmployeeIdKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {
            case KeyEvent.VK_RIGHT:
                employeeName.requestFocus();
            case KeyEvent.VK_LEFT:
                EmployeeDesignation.requestFocus();
            case KeyEvent.VK_ENTER:

                applyFilter.requestFocus();
                break;
        }
    }//GEN-LAST:event_EmployeeIdKeyPressed

    private void employeeName2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeName2KeyPressed

        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (!validateLetters(employeeName2.getText())) {
                    employeeName2.requestFocus();
                    employeeName2.selectAll();

                    status.setText("Invaild Name For Employee");

                } else if (employeeName2.getText().equals("")) {

                    status.setText("please Enter Employee Name");
                    employeeName2.requestFocus();
                } else {
                    KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                    manager.getFocusOwner().transferFocus();
                    status.setText("");
                }
                break;
        }

    }//GEN-LAST:event_employeeName2KeyPressed

    private void NICKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NICKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_NICKeyPressed

    private void employeeAdress2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeAdress2KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (!validateLetters(employeeAdress2.getText())) {
                    status.setText("Plese Enter Correct Address");
                    employeeAdress2.requestFocus();
                    employeeAdress2.selectAll();

                } else if (employeeAdress2.getText().trim().isEmpty()) {
                    status.setText("Plese Enter Employee Address");
                    employeeAdress2.requestFocus();
                    employeeAdress2.selectAll();
                } else {
                    KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                    manager.getFocusOwner().transferFocus();
                    phoneNumber.requestFocus();
                    status.setText("");
                }

                break;
        }
    }//GEN-LAST:event_employeeAdress2KeyPressed

    private void phoneNumberKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_phoneNumberKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (!(phoneNumber.getText().matches("^[0-9]{10}"))) {
                    status.setText("Invalid Mobile Number");
                    phoneNumber.requestFocus();
                    phoneNumber.selectAll();
                } else if (phoneNumber.getText().trim().isEmpty()) {
                    status.setText("Please Enter Mobile Number");
                    phoneNumber.requestFocus();
                } else {
                    KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                    manager.getFocusOwner().transferFocus();
                    EmployeeDesignation2.requestFocus();
                    status.setText("");
                }

                break;
        }
    }//GEN-LAST:event_phoneNumberKeyPressed

    private void joinDateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_joinDateKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_joinDateKeyPressed

    private void EmployeeDesignation2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EmployeeDesignation2KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (EmployeeDesignation2.getSelectedIndex() == -1) {
                    status.setText("PLease Select Designation");
                    EmployeeDesignation2.requestFocus();

                } else {
                    status.setText("");
                    EmployeeSalary2.requestFocus();
                }

                break;
        }
    }//GEN-LAST:event_EmployeeDesignation2KeyPressed

    private void EmployeeSalary2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EmployeeSalary2KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (EmployeeSalary2.getSelectedIndex() == -1) {
                    status.setText("PLease Select Basic Salary For Employee");
                    EmployeeSalary2.requestFocus();

                } else {
                    status.setText("");
                    employeeBank.requestFocus();
                }

                break;
        }
    }//GEN-LAST:event_EmployeeSalary2KeyPressed

    private void employeeBankKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeBankKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (employeeBank.getSelectedIndex() == -1) {
                    status.setText("PLease Select Employee's Service Bank");
                    employeeBank.requestFocus();

                } else {
                    status.setText("");
                    bankAcc.requestFocus();
                }

                break;
        }
    }//GEN-LAST:event_employeeBankKeyPressed

    private void bankAccKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bankAccKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (bankAcc.getText().trim().isEmpty()) {
                    status.setText("PLease Enter Bank Acc No");
                    bankAcc.requestFocus();
                } else if (validateLetters(bankAcc.getText())) {
                    status.setText("PLease Enter valid Bank AccNo");
                    bankAcc.requestFocus();
                } else {
                    status.setText("");
                    save.requestFocus();
                }

                break;
        }
    }//GEN-LAST:event_bankAccKeyPressed

    private void lb31MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb31MouseClicked
        lb35.setText(" Employee Summary");
        card.removeAll();
        card.repaint();
        card.revalidate();
        card.add(EmployeeSummary);
        card.repaint();
        card.revalidate();
        lb31.setBackground(new Color(0, 0, 102));
        lb28.setBackground(new Color(255, 255, 255));
        lb29.setBackground(new Color(255, 255, 255));
        lb27.setBackground(new Color(255, 255, 255));
        lb28.setForeground(new Color(0, 0, 0));
        lb31.setForeground(new Color(255, 255, 255));
        lb29.setForeground(new Color(0, 0, 0));
        lb27.setForeground(new Color(0, 0, 0));

    }//GEN-LAST:event_lb31MouseClicked

    private void cancel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancel1MouseClicked
        clear();
        remove();
        loadAllItems();
    }//GEN-LAST:event_cancel1MouseClicked

    private void EmployeeDesignationKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EmployeeDesignationKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {
            case KeyEvent.VK_RIGHT:
                employeeId.requestFocus();
            case KeyEvent.VK_LEFT:
                employeeName.requestFocus();

            case KeyEvent.VK_ENTER:

                applyFilter.requestFocus();
                break;
        }
    }//GEN-LAST:event_EmployeeDesignationKeyPressed

    private void applyFilterKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_applyFilterKeyReleased
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (EmployeeId.getSelectedIndex() == -1 & EmployeeDesignation.getSelectedIndex() == -1) {
                    JOptionPane.showMessageDialog(this, "Please Select One Category");
                    employeeId.requestFocus();
                } else if (EmployeeId.getSelectedIndex() != -1) {
                    try {
                        DefaultTableModel dtm = (DefaultTableModel) EmployeeTable.getModel();
                        dtm.setRowCount(0);

                        String EmployeeID = EmployeeId.getSelectedItem().toString();
                        EmployeeDTO e = ctrl.searchemp(EmployeeID);
                        String basicSalary = ctrl.searchSal(e.getEmpId());
                        System.out.println(basicSalary);
                        String de = ctrl.searchDes(e.getDeId());

                        if (e != null) {
                            Object[] employee = {e.getEmpName(), de, e.getNIC(), e.getJoin_Date(), basicSalary};
                            dtm.addRow(employee);
                        }

                        int i = dtm.getRowCount();
                        EmployeeCount.setText("" + i);

                    } catch (Exception ex) {
                        Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    EmployeeId.setSelectedIndex(-1);
                    EmployeeName.setSelectedIndex(-1);
                    EmployeeDesignation.setSelectedIndex(-1);
                } else if (EmployeeId.getSelectedIndex() == -1) {
                    try {
                        DefaultTableModel dtm = (DefaultTableModel) EmployeeTable.getModel();
                        dtm.setRowCount(0);
                        String des = EmployeeDesignation.getSelectedItem().toString();
                        DesignationDTO designation = ctrl.search(des);
                        String deId = designation.getDeId();
                        ArrayList<EmployeeDTO> a = ctrl.searchempBydes(deId);
                        for (EmployeeDTO e : a) {
                            String basicSalary = ctrl.searchSal(e.getEmpId());
                            String de = ctrl.searchDes(e.getDeId());
                            Object[] employee = {e.getEmpName(), de, e.getNIC(), e.getJoin_Date(), basicSalary};
                            dtm.addRow(employee);

                        }
                        int i = dtm.getRowCount();
                        EmployeeCount.setText("" + i);
                    } catch (Exception ex) {
                        Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    EmployeeId.setSelectedIndex(-1);
                    EmployeeName.setSelectedIndex(-1);
                    EmployeeDesignation.setSelectedIndex(-1);
                    EmployeeId.requestFocus();
                }

                break;
        }
    }//GEN-LAST:event_applyFilterKeyReleased

    private void EmployeeNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EmployeeNameKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {
            case KeyEvent.VK_LEFT:
                employeeId.requestFocus();
            case KeyEvent.VK_RIGHT:
                EmployeeDesignation.requestFocus();
            case KeyEvent.VK_ENTER:

                applyFilter.requestFocus();
                break;
        }
    }//GEN-LAST:event_EmployeeNameKeyPressed

    private void phoneNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_phoneNumberActionPerformed
        if (!(phoneNumber.getText().matches("^[0-9]{10}"))) {
            status.setText("Invalid Mobile Number");
            phoneNumber.requestFocus();
            phoneNumber.selectAll();
        } else if (phoneNumber.getText().trim().isEmpty()) {
            status.setText("Please Enter Mobile Number");
            phoneNumber.requestFocus();
        } else {
            EmployeeDesignation2.requestFocus();

        }
    }//GEN-LAST:event_phoneNumberActionPerformed

    private void saveFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_saveFocusGained
        save.setBackground(new Color(0, 102, 0));
        save.setForeground(Color.white);
    }//GEN-LAST:event_saveFocusGained

    private void saveFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_saveFocusLost
        save.setBackground(new Color(255, 255, 255));
        save.setForeground(Color.black);
    }//GEN-LAST:event_saveFocusLost

    private void saveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseEntered
        save.setBackground(new Color(0, 102, 0));
        save.setForeground(Color.white);
    }//GEN-LAST:event_saveMouseEntered

    private void saveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseExited
        save.setBackground(new Color(255, 255, 255));
        save.setForeground(Color.black);
    }//GEN-LAST:event_saveMouseExited

    private void saveMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMousePressed
        save.setBackground(new Color(17, 119, 45));
        save.setForeground(Color.white);
    }//GEN-LAST:event_saveMousePressed

    private void saveMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseReleased
        save.setBackground(new Color(0, 102, 0));
        save.setForeground(Color.white);
    }//GEN-LAST:event_saveMouseReleased

    private void employeeBankItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_employeeBankItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_employeeBankItemStateChanged

    private void lb23MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb23MouseClicked
        clear();
        remove();
        loadAllItems();
    }//GEN-LAST:event_lb23MouseClicked

    private void employeeIdItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_employeeIdItemStateChanged
        if (employeeId.getSelectedIndex() != -1) {
            try {
                String id = (String) employeeId.getSelectedItem();
                System.out.println(id + "yur");
                EmployeeDTO e = ctrl.searchemp(id);
                if (e != null) {
                    QueryDTO ban = ctrl.serachBankDetails(e.getEmpId());
                    String sal = ctrl.searchSal(e.getEmpId());
                    String de = ctrl.searchDes(e.getDeId());
                    String d = e.getEmpName();
                    String b = ban.getBankName() + " " + ban.getAcc_no();

                    employeeName.setSelectedItem(d);
                    des.setText(de);
                    basic.setText(sal);
                    join.setText(e.getJoin_Date());
                    nis.setText(e.getNIC());
                    gen.setText(e.getGender());
                    bd.setText(e.getD_O_B());
                    tp.setText(e.getPhone_No());
                    bank.setText(b);
                    loadAllLoans(id);
                    loadAllAdvances(id);
                    loadAllPayroll(id);

                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeIdItemStateChanged

    private void employeeIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeIdActionPerformed

    }//GEN-LAST:event_employeeIdActionPerformed

    private void employeeNameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_employeeNameItemStateChanged
        if (employeeName.getSelectedIndex() != -1) {
            try {
                String name = (String) employeeName.getSelectedItem();
                EmployeeDTO em = ctrl.searchempid(name);

                if (em != null) {
                    String dep = ctrl.searchDes(em.getDeId());
                    String d = em.getEmpId();
                    String id = em.getEmpId();
                    employeeId.setSelectedItem(d);
                    des.setText(dep);

                    QueryDTO ban = ctrl.serachBankDetails(d);
                    String sal = ctrl.searchSal(d);
                    String b = ban.getBankName() + " " + ban.getAcc_no();

                    basic.setText(sal);
                    join.setText(em.getJoin_Date());
                    nis.setText(em.getNIC());
                    gen.setText(em.getGender());
                    bd.setText(em.getD_O_B());
                    tp.setText(em.getPhone_No());
                    bank.setText(b);
                    loadAllLoans(id);
                    loadAllAdvances(id);
                    loadAllPayroll(id);
                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeNameItemStateChanged

    private void employeeNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeNameActionPerformed

    }//GEN-LAST:event_employeeNameActionPerformed

    private void lb24MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb24MouseClicked
        PayrollReview d1 = new PayrollReview();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb24MouseClicked

    private void lb24MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb24MouseEntered
        lb24.setForeground(Color.red);
    }//GEN-LAST:event_lb24MouseEntered

    private void lb24MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb24MouseExited
        lb24.setForeground(Color.white);
    }//GEN-LAST:event_lb24MouseExited

    private void lb4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseClicked
        Reports d1 = new Reports();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb4MouseClicked

    private void lb4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseEntered
        lb4.setForeground(Color.RED);
    }//GEN-LAST:event_lb4MouseEntered

    private void lb4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseExited
        lb4.setForeground(Color.white);
    }//GEN-LAST:event_lb4MouseExited

    private void lb10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseClicked
        Payroll d1 = new Payroll();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb10MouseClicked

    private void lb10MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseEntered
        lb10.setForeground(Color.RED);
    }//GEN-LAST:event_lb10MouseEntered

    private void lb10MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseExited
        lb10.setForeground(Color.white);
    }//GEN-LAST:event_lb10MouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EmployeeReview.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EmployeeReview.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EmployeeReview.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EmployeeReview.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EmployeeReview().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField EmployeeCount;
    private javax.swing.JTextField EmployeeDOB;
    private javax.swing.JTextField EmployeeDOB2;
    private javax.swing.JComboBox<String> EmployeeDesignation;
    private javax.swing.JComboBox<String> EmployeeDesignation2;
    private javax.swing.JComboBox<String> EmployeeId;
    private javax.swing.JComboBox<String> EmployeeName;
    private javax.swing.JPanel EmployeeReview;
    private javax.swing.JComboBox<String> EmployeeSalary2;
    private javax.swing.JPanel EmployeeSummary;
    private javax.swing.JPanel EmployeeSummary1;
    private javax.swing.JTable EmployeeTable;
    private javax.swing.JTextField NIC;
    private javax.swing.JTextField NIC3;
    private javax.swing.JPanel NewEmployeee;
    private javax.swing.JPanel UpdateEmployee;
    private javax.swing.JTable advances;
    private javax.swing.JLabel applyFilter;
    private javax.swing.JTextField bank;
    private javax.swing.JTextField bankAcc;
    private javax.swing.JTextField bankAcc3;
    private javax.swing.JTextField basic;
    private javax.swing.JTextField bd;
    private javax.swing.JLabel cancel;
    private javax.swing.JLabel cancel1;
    private javax.swing.JLabel cancel2;
    private javax.swing.JPanel card;
    private javax.swing.JTextField des;
    private javax.swing.JTextField employeeAddress3;
    private javax.swing.JTextField employeeAdress2;
    private javax.swing.JComboBox<String> employeeBank;
    private javax.swing.JComboBox<String> employeeBank2;
    private javax.swing.JComboBox<String> employeeDesignation3;
    private javax.swing.JComboBox<String> employeeId;
    private javax.swing.JTextField employeeId2;
    private javax.swing.JComboBox<String> employeeId3;
    private javax.swing.JComboBox<String> employeeName;
    private javax.swing.JTextField employeeName2;
    private javax.swing.JTextField employeeName3;
    private javax.swing.JComboBox<String> employeeSalary3;
    private javax.swing.JRadioButton female;
    private javax.swing.JRadioButton female3;
    private javax.swing.JPanel fil;
    private javax.swing.JTextField gen;
    private javax.swing.ButtonGroup gender;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JTextField join;
    private com.toedter.calendar.JDateChooser joinData3;
    private com.toedter.calendar.JDateChooser joinDate;
    private javax.swing.JLabel lb1;
    private javax.swing.JLabel lb10;
    private javax.swing.JLabel lb11;
    private javax.swing.JLabel lb12;
    private javax.swing.JLabel lb13;
    private javax.swing.JLabel lb14;
    private javax.swing.JLabel lb15;
    private javax.swing.JLabel lb16;
    private javax.swing.JLabel lb17;
    private javax.swing.JLabel lb18;
    private javax.swing.JLabel lb19;
    private javax.swing.JLabel lb2;
    private javax.swing.JLabel lb20;
    private javax.swing.JLabel lb21;
    private javax.swing.JLabel lb22;
    private javax.swing.JLabel lb23;
    private javax.swing.JLabel lb24;
    private javax.swing.JLabel lb26;
    private javax.swing.JLabel lb27;
    private javax.swing.JLabel lb28;
    private javax.swing.JLabel lb29;
    private javax.swing.JLabel lb3;
    private javax.swing.JLabel lb30;
    private javax.swing.JLabel lb31;
    private javax.swing.JLabel lb32;
    private javax.swing.JLabel lb33;
    private javax.swing.JLabel lb34;
    private javax.swing.JLabel lb35;
    private javax.swing.JLabel lb36;
    private javax.swing.JLabel lb37;
    private javax.swing.JLabel lb38;
    private javax.swing.JLabel lb4;
    private javax.swing.JLabel lb40;
    private javax.swing.JLabel lb41;
    private javax.swing.JLabel lb42;
    private javax.swing.JLabel lb43;
    private javax.swing.JLabel lb44;
    private javax.swing.JLabel lb445;
    private javax.swing.JLabel lb46;
    private javax.swing.JLabel lb47;
    private javax.swing.JLabel lb48;
    private javax.swing.JLabel lb5;
    private javax.swing.JLabel lb6;
    private javax.swing.JLabel lb7;
    private javax.swing.JLabel lb8;
    private javax.swing.JLabel lb9;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblTime;
    private javax.swing.JTable loantable;
    private javax.swing.JPanel main;
    private javax.swing.JRadioButton male;
    private javax.swing.JRadioButton male3;
    private javax.swing.JTextField nis;
    private javax.swing.JPanel p1;
    private javax.swing.JPanel p2;
    private javax.swing.JPanel p3;
    private javax.swing.JPanel p4;
    private javax.swing.JPanel path;
    private javax.swing.JTable payroll;
    private javax.swing.JTextField phoneNumber;
    private javax.swing.JTextField phoneNumber3;
    private javax.swing.JLabel remove;
    private javax.swing.JLabel save;
    private javax.swing.JLabel status;
    private javax.swing.JTextField tp;
    private javax.swing.JLabel update;
    // End of variables declaration//GEN-END:variables
private void setTime() {
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("hh-mm-ss a");
                lblTime.setText(sdf.format(date));
            }
        }).start();
    }

    private void setDate() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        lblDate.setText(sdf.format(date));
    }

    private void transparent() {

        EmployeeDesignation.setBackground(new Color(255, 255, 255, 10));

        EmployeeDesignation2.setBackground(new Color(255, 255, 255, 10));
        EmployeeSalary2.setBackground(new Color(255, 255, 255, 10));
        employeeBank.setBackground(new Color(255, 255, 255, 10));
        employeeId3.setBackground(new Color(255, 255, 255, 10));
        employeeDesignation3.setBackground(new Color(255, 255, 255, 10));
        employeeSalary3.setBackground(new Color(255, 255, 255, 10));
        employeeBank2.setBackground(new Color(255, 255, 255, 10));
    }

    public void clear() {
        dtm.setRowCount(0);
        dtm1.setRowCount(0);
        dtm2.setRowCount(0);

        employeeAdress2.setText("Address");
        employeeId2.setText("Employee Id");
        employeeName2.setText("Employee Name");
        NIC.setText("NIC");
        bankAcc3.setText("Acc No");
        phoneNumber3.setText("Phone Number");
        bankAcc.setText("Acc No");
        employeeAddress3.setText("Address");
        phoneNumber.setText("Phone Number");
        joinDate.setCalendar(null);
        EmployeeDOB2.setText("D.O.B");
        gender.clearSelection();

        employeeName3.setText("Employee Name");
        NIC3.setText("NIC");

        joinData3.setCalendar(null);

        gender.clearSelection();
        des.setText("");
        basic.setText("");
        join.setText("");
        nis.setText("");
        gen.setText("");
        bd.setText("");
        tp.setText("");
        bank.setText("");

    }

    public void remove() {
        EmployeeDesignation2.removeAllItems();
        EmployeeSalary2.removeAllItems();
        employeeBank.removeAllItems();
        employeeDesignation3.removeAllItems();
        employeeSalary3.removeAllItems();
        employeeBank2.removeAllItems();
        employeeId3.removeAllItems();
        employeeDesignation3.removeAllItems();
        EmployeeId.removeAllItems();
        employeeId.removeAllItems();
        EmployeeName.removeAllItems();
        employeeName.removeAllItems();
        EmployeeDesignation.removeAllItems();

    }

    private String loadNextempId() {
        String id = getLastempId();
        if (id == null) {
            id = "Emp00";
        }
        char ch = id.charAt(id.length() - 1);
        int a = Character.getNumericValue(ch) + 1;
        return id = id.substring(0, id.length() - 1) + a;

    }

    private String getLastempId() {
        String lastId = null;
        try {
            lastId = ctrl.searchempId();
        } catch (Exception e) {
//            e.printStackTrace();

        }
        return lastId;
    }

    public void loadAllLoans(String id) {

        try {
            DefaultTableModel dtm = (DefaultTableModel) loantable.getModel();

            System.out.println(id);
            ArrayList<QueryDTO> All = ctrl.getLoan(id);
            dtm.setRowCount(0);
            for (QueryDTO l : All) {

                Object Rowdata[] = {l.getDate(), l.getLoanName(), l.getLoanAmount()};

                dtm.addRow(Rowdata);

            }

        } catch (Exception ex) {
            Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void loadAllAdvances(String id) {

        try {
            DefaultTableModel dtm = (DefaultTableModel) advances.getModel();

            ArrayList<QueryDTO> all = ctrl.getAllAdvances(id);
            dtm.setRowCount(0);
            for (QueryDTO q : all) {
                Object RowData[] = {q.getAdName(), q.getYear(), q.getMonth(), q.getAmount()};
                dtm.addRow(RowData);
            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static boolean validateLetters(String txt) {

        String regx = "^[\\p{L} .'-]+$";
        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        return matcher.find();

    }

    private void loadAllPayroll(String id) {
        try {
            DefaultTableModel dtm = (DefaultTableModel) payroll.getModel();

            ArrayList<PayrollDTO> all = ctrl.getAllPayroll(id);
            dtm.setRowCount(0);
            for (PayrollDTO a : all) {
                Object RowData[] = {a.getDate(), a.getNormal_OT(), a.getDouble_OT(), a.getEPF_12(), a.getEPF_8(), a.getETF_3(), a.getLoan(), a.getAdvances(), a.getSalary()};
                dtm.addRow(RowData);
            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}

class DOB {

    String id;
    int month[] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public DOB(String id) {
        this.id = id;
    }

    public String getYear() {
        int year = 1900 + Integer.parseInt(id.substring(0, 2));
        String month = setMonth();
        String DOB = year + "." + month;
        return DOB;
    }

    public int getDays() {
        int d = Integer.parseInt(id.substring(2, 5));
        if (d > 500) {
            return (d - 500);
        } else {
            return d;
        }
    }

    public String setMonth() {
        int mo = 0, da = 0;
        int days = getDays();

        for (int i = 0; i < month.length; i++) {
            if (days < month[i]) {
                mo = i + 1;
                da = days;
                break;
            } else {
                days = days - month[i];
            }
        }
        //System.out.println("Month : " + mo + "\nDate : " + da);
        String month = mo + "." + da;
        return month;
    }

    public String getSex() {
        String M = "Male", F = "Female";
        int d = Integer.parseInt(id.substring(2, 5));
        if (d > 500) {
            return F;
        } else {
            return M;
        }
    }

}
