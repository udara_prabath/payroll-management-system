/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import lk.ijse.payroll.controller.PlaceloansController;
import lk.ijse.payroll.dto.EmpLoanDTO;
import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.dto.LoanDTO;
import lk.ijse.payroll.view.util.ComboBoxRender;

/**
 *
 * @author udara
 */
public class placeLoans extends javax.swing.JFrame {

    private PlaceloansController ctrl;

    /**
     * Creates new form placeLoans
     */
    public placeLoans() {
        ctrl = new PlaceloansController();
        initComponents();
        setTime();
        setDate();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setCellsAlignment(loanTable);
        loadAllLoans();
        loadItems();
        lb8.setForeground(new Color(0,204,0));
        loandate.setDateFormatString("yyyy:MM:dd");
        loandate.setDate(new Date());
        loanId.setText(loadNextId());
        employeeId.requestFocus();

        employeeId.setRenderer(new ComboBoxRender("Select EmployeeId"));
        employeeId.setSelectedIndex(-1);
        empId.setRenderer(new ComboBoxRender("Select EmployeeId"));
        empId.setSelectedIndex(-1);

        employeeName.setRenderer(new ComboBoxRender("Select Employee"));
        employeeName.setSelectedIndex(-1);
        empName.setRenderer(new ComboBoxRender("Select Employee"));
        empName.setSelectedIndex(-1);

        loan.setRenderer(new ComboBoxRender("Select Loan"));
        loan.setSelectedIndex(-1);
        loanName.setRenderer(new ComboBoxRender("Select Loan"));
        loanName.setSelectedIndex(-1);
        ImageIcon img=new ImageIcon("src/lk/ijse/payroll/asset/payloku.png");
        this.setIconImage(img.getImage());
        

    }

    public void loadAllLoans() {
        try {
            ArrayList<EmpLoanDTO> all = ctrl.getAllLoans();
            DefaultTableModel dtm = (DefaultTableModel) loanTable.getModel();
            dtm.setRowCount(0);

            for (EmpLoanDTO ad : all) {
                String loanName = ctrl.searchLoanName(ad.getLoanId());
                EmployeeDTO employeeId = ctrl.searchemp(ad.getEmpId());

                Object Rowdata[] = {ad.getElId(), employeeId.getEmpName(), loanName, ad.getDate(), ad.getInstallment()};
                dtm.addRow(Rowdata);

            }
            int i = dtm.getRowCount();
            count.setText("" + i);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void setCellsAlignment(JTable table) {
        ((DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        table.getTableHeader().setFont(new Font("Tahome", Font.BOLD, 15));
        table.setFont(new Font("Tahome", 1, 13));
    }

    public void loadItems() {
        try {
            ArrayList<String> emp = ctrl.LoaDAllemployeeId();
            for (String e : emp) {

                //employeeId3.addItem(e);
                employeeId.addItem(e);
                empId.addItem(e);

            }

        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            ArrayList<String> emp = ctrl.LoadallemployeeName();
            for (String e : emp) {

                employeeName.addItem(e);
                empName.addItem(e);

            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ArrayList<String> l = ctrl.loadAllLoans();
            for (String e : l) {

                loan.addItem(e);
                loanName.addItem(e);

            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);

        }
        employeeId.setRenderer(new ComboBoxRender("Select EmployeeId"));
        employeeId.setSelectedIndex(-1);
        empId.setRenderer(new ComboBoxRender("Select EmployeeId"));
        empId.setSelectedIndex(-1);

        employeeName.setRenderer(new ComboBoxRender("Select Employee"));
        employeeName.setSelectedIndex(-1);
        empName.setRenderer(new ComboBoxRender("Select Employee"));
        empName.setSelectedIndex(-1);

        loan.setRenderer(new ComboBoxRender("Select Loan"));
        loan.setSelectedIndex(-1);
        loanName.setRenderer(new ComboBoxRender("Select Loan"));
        loanName.setSelectedIndex(-1);

        employeeDesignation.setText("");
        amount.setText("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        main = new javax.swing.JPanel();
        menu = new javax.swing.JLabel();
        sp1 = new javax.swing.JSeparator();
        lb3 = new javax.swing.JLabel();
        lb1 = new javax.swing.JLabel();
        lb5 = new javax.swing.JLabel();
        lb7 = new javax.swing.JLabel();
        lb8 = new javax.swing.JLabel();
        lb9 = new javax.swing.JLabel();
        lb20 = new javax.swing.JLabel();
        lb21 = new javax.swing.JLabel();
        path = new javax.swing.JPanel();
        home = new javax.swing.JLabel();
        dash = new javax.swing.JLabel();
        lb22 = new javax.swing.JLabel();
        lb23 = new javax.swing.JLabel();
        lb24 = new javax.swing.JLabel();
        lb25 = new javax.swing.JLabel();
        newtab = new javax.swing.JLabel();
        viewtab = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        newLoan = new javax.swing.JPanel();
        p1 = new javax.swing.JPanel();
        sp2 = new javax.swing.JSeparator();
        lb26 = new javax.swing.JLabel();
        lb27 = new javax.swing.JLabel();
        loanId = new javax.swing.JTextField();
        lb28 = new javax.swing.JLabel();
        employeeId = new javax.swing.JComboBox<>();
        employeeName = new javax.swing.JComboBox<>();
        lb29 = new javax.swing.JLabel();
        employeeDesignation = new javax.swing.JTextField();
        lb30 = new javax.swing.JLabel();
        amount = new javax.swing.JTextField();
        lb31 = new javax.swing.JLabel();
        sp3 = new javax.swing.JSeparator();
        lb32 = new javax.swing.JLabel();
        lb33 = new javax.swing.JLabel();
        loanName = new javax.swing.JComboBox<>();
        lb34 = new javax.swing.JLabel();
        lb35 = new javax.swing.JLabel();
        duration = new javax.swing.JTextField();
        cancel = new javax.swing.JLabel();
        save = new javax.swing.JLabel();
        loandate = new com.toedter.calendar.JDateChooser();
        status = new javax.swing.JLabel();
        viewLoans = new javax.swing.JPanel();
        p2 = new javax.swing.JPanel();
        filpanel = new javax.swing.JPanel();
        p3 = new javax.swing.JPanel();
        lb36 = new javax.swing.JLabel();
        empName = new javax.swing.JComboBox<>();
        loan = new javax.swing.JComboBox<>();
        filter = new javax.swing.JLabel();
        empId = new javax.swing.JComboBox<>();
        cancel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        loanTable = new javax.swing.JTable();
        remove = new javax.swing.JLabel();
        lb37 = new javax.swing.JLabel();
        count = new javax.swing.JTextField();
        lb38 = new javax.swing.JLabel();
        lb6 = new javax.swing.JLabel();
        lb11 = new javax.swing.JLabel();
        lb4 = new javax.swing.JLabel();
        lb10 = new javax.swing.JLabel();
        sp4 = new javax.swing.JSeparator();
        lblDate = new javax.swing.JLabel();
        lblTime = new javax.swing.JLabel();
        lbframe = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        main.setBackground(new java.awt.Color(255, 255, 255));
        main.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        menu.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 14)); // NOI18N
        menu.setForeground(new java.awt.Color(255, 255, 255));
        menu.setText("Menu");
        main.add(menu, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 60, -1));

        sp1.setBackground(new java.awt.Color(204, 204, 255));
        sp1.setForeground(new java.awt.Color(204, 255, 204));
        main.add(sp1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 310, 10));

        lb3.setBackground(new java.awt.Color(242, 241, 239));
        lb3.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb3.setForeground(new java.awt.Color(255, 255, 255));
        lb3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/home2.png"))); // NOI18N
        lb3.setText("DashBoard");
        lb3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb3.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lb3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb3MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb3MouseExited(evt);
            }
        });
        main.add(lb3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 240, 70));

        lb1.setBackground(new java.awt.Color(242, 241, 239));
        lb1.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb1.setForeground(new java.awt.Color(255, 255, 255));
        lb1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/admin1.png"))); // NOI18N
        lb1.setText(" Administration");
        lb1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb1MouseExited(evt);
            }
        });
        main.add(lb1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 230, 300, 70));

        lb5.setBackground(new java.awt.Color(242, 241, 239));
        lb5.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb5.setForeground(new java.awt.Color(255, 255, 255));
        lb5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/employee.png"))); // NOI18N
        lb5.setText("  Employee");
        lb5.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb5MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb5MouseExited(evt);
            }
        });
        main.add(lb5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 320, 240, 70));

        lb7.setBackground(new java.awt.Color(242, 241, 239));
        lb7.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb7.setForeground(new java.awt.Color(255, 255, 255));
        lb7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/attendance.png"))); // NOI18N
        lb7.setText("  Attendance");
        lb7.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb7MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb7MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb7MouseExited(evt);
            }
        });
        main.add(lb7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 410, 260, 70));

        lb8.setBackground(new java.awt.Color(242, 241, 239));
        lb8.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb8.setForeground(new java.awt.Color(0, 204, 0));
        lb8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/loan.png"))); // NOI18N
        lb8.setText("  Loan");
        lb8.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb8MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb8MouseExited(evt);
            }
        });
        main.add(lb8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 500, 180, 70));

        lb9.setBackground(new java.awt.Color(242, 241, 239));
        lb9.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb9.setForeground(new java.awt.Color(255, 255, 255));
        lb9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/advance.png"))); // NOI18N
        lb9.setText("  Advances");
        lb9.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb9MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb9MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb9MouseExited(evt);
            }
        });
        main.add(lb9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 590, 240, 70));

        lb20.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 30)); // NOI18N
        lb20.setForeground(new java.awt.Color(153, 153, 153));
        lb20.setText("Loan");
        lb20.setFocusable(false);
        main.add(lb20, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 40, 90, 60));

        lb21.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lb21.setForeground(new java.awt.Color(204, 204, 204));
        lb21.setText("List");
        lb21.setFocusable(false);
        main.add(lb21, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 60, 30, 30));

        path.setBackground(new java.awt.Color(255, 255, 255));
        path.setFocusable(false);
        path.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        home.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/home.png"))); // NOI18N
        home.setFocusable(false);
        path.add(home, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 30, 30));

        dash.setForeground(new java.awt.Color(153, 153, 153));
        dash.setText("DashBoard");
        dash.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        dash.setFocusable(false);
        dash.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dashMouseClicked(evt);
            }
        });
        path.add(dash, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 16, -1, 20));

        lb22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/next.png"))); // NOI18N
        path.add(lb22, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 0, 20, 50));

        lb23.setText("Place New Loan");
        lb23.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb23.setFocusable(false);
        path.add(lb23, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 10, 160, 30));

        lb24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/next.png"))); // NOI18N
        lb24.setFocusable(false);
        path.add(lb24, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 0, 20, 50));

        lb25.setForeground(new java.awt.Color(153, 153, 153));
        lb25.setText("Loan");
        lb25.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb25.setFocusable(false);
        lb25.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb25MouseClicked(evt);
            }
        });
        path.add(lb25, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 16, 40, 20));

        main.add(path, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 90, 830, 50));

        newtab.setBackground(new java.awt.Color(0, 0, 102));
        newtab.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 18)); // NOI18N
        newtab.setForeground(new java.awt.Color(255, 255, 255));
        newtab.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        newtab.setText("Place New Loan");
        newtab.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        newtab.setFocusable(false);
        newtab.setOpaque(true);
        newtab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                newtabMouseClicked(evt);
            }
        });
        main.add(newtab, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 170, 200, 40));

        viewtab.setBackground(new java.awt.Color(250, 250, 250));
        viewtab.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 18)); // NOI18N
        viewtab.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        viewtab.setText("View Employee Loan Details");
        viewtab.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        viewtab.setFocusable(false);
        viewtab.setOpaque(true);
        viewtab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                viewtabMouseClicked(evt);
            }
        });
        main.add(viewtab, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 170, 300, 40));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel5.setLayout(new java.awt.CardLayout());

        newLoan.setBackground(new java.awt.Color(255, 255, 255));
        newLoan.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        newLoan.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p1.setBackground(new java.awt.Color(0, 0, 102));
        p1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        newLoan.add(p1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1550, 40));
        newLoan.add(sp2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 92, 1420, 10));

        lb26.setText("Employee Details");
        lb26.setFocusable(false);
        newLoan.add(lb26, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 70, -1, -1));

        lb27.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lb27.setText("Employee Loan ID*");
        lb27.setFocusable(false);
        newLoan.add(lb27, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 110, 150, 30));

        loanId.setEditable(false);
        loanId.setBackground(new java.awt.Color(255, 255, 255));
        loanId.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        loanId.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        loanId.setFocusable(false);
        newLoan.add(loanId, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 140, 240, 40));

        lb28.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lb28.setText("Employee ID");
        lb28.setFocusable(false);
        newLoan.add(lb28, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 200, -1, -1));

        employeeId.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        employeeId.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        employeeId.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeIdActionPerformed(evt);
            }
        });
        employeeId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeIdKeyPressed(evt);
            }
        });
        newLoan.add(employeeId, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 220, 220, 40));

        employeeName.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        employeeName.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeName.setFocusable(false);
        employeeName.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                employeeNameItemStateChanged(evt);
            }
        });
        employeeName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeNameActionPerformed(evt);
            }
        });
        employeeName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeNameKeyPressed(evt);
            }
        });
        newLoan.add(employeeName, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 220, 370, 40));

        lb29.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lb29.setText("Employee Name");
        lb29.setFocusable(false);
        newLoan.add(lb29, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 200, 100, -1));

        employeeDesignation.setEditable(false);
        employeeDesignation.setBackground(new java.awt.Color(255, 255, 255));
        employeeDesignation.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        employeeDesignation.setFocusable(false);
        newLoan.add(employeeDesignation, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 220, 290, 40));

        lb30.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lb30.setText("Employee Designation");
        lb30.setFocusable(false);
        newLoan.add(lb30, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 200, -1, -1));

        amount.setEditable(false);
        amount.setBackground(new java.awt.Color(255, 255, 255));
        amount.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        amount.setFocusable(false);
        newLoan.add(amount, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 380, 260, 40));

        lb31.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lb31.setText("Loan Amount");
        newLoan.add(lb31, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 360, -1, -1));
        newLoan.add(sp3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 340, 1410, 10));

        lb32.setText("Loan Details");
        lb32.setFocusable(false);
        newLoan.add(lb32, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 320, -1, -1));

        lb33.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lb33.setText("Loan Name");
        lb33.setFocusable(false);
        newLoan.add(lb33, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 350, -1, -1));

        loanName.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        loanName.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        loanName.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                loanNameItemStateChanged(evt);
            }
        });
        loanName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loanNameActionPerformed(evt);
            }
        });
        loanName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                loanNameKeyPressed(evt);
            }
        });
        newLoan.add(loanName, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 370, 410, 40));

        lb34.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lb34.setText("Date*");
        lb34.setFocusable(false);
        newLoan.add(lb34, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 450, 90, 20));

        lb35.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        lb35.setText("Loan Duration");
        lb35.setFocusable(false);
        newLoan.add(lb35, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 550, 100, 20));

        duration.setEditable(false);
        duration.setBackground(new java.awt.Color(255, 255, 255));
        duration.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        duration.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        duration.setText("12 Months");
        duration.setFocusable(false);
        newLoan.add(duration, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 580, 320, 40));

        cancel.setBackground(new java.awt.Color(153, 0, 0));
        cancel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        cancel.setForeground(new java.awt.Color(255, 255, 255));
        cancel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cancel.setText("Cancel");
        cancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        cancel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancel.setOpaque(true);
        cancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelMouseClicked(evt);
            }
        });
        newLoan.add(cancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(1170, 780, 150, 40));

        save.setBackground(new java.awt.Color(255, 255, 255));
        save.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        save.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        save.setText("Save");
        save.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 204, 0), 2, true));
        save.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        save.setOpaque(true);
        save.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                saveFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                saveFocusLost(evt);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                saveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                saveMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                saveMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                saveMouseReleased(evt);
            }
        });
        save.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                saveKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                saveKeyReleased(evt);
            }
        });
        newLoan.add(save, new org.netbeans.lib.awtextra.AbsoluteConstraints(1350, 780, 150, 40));

        loandate.setFocusable(false);
        loandate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                loandateKeyPressed(evt);
            }
        });
        newLoan.add(loandate, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 470, 310, 40));

        status.setBackground(new java.awt.Color(255, 255, 255));
        status.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        status.setForeground(new java.awt.Color(153, 0, 0));
        newLoan.add(status, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 800, 400, 20));

        jPanel5.add(newLoan, "card2");

        viewLoans.setBackground(new java.awt.Color(255, 255, 255));
        viewLoans.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        viewLoans.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p2.setBackground(new java.awt.Color(0, 0, 102));
        p2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        viewLoans.add(p2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1550, 40));

        filpanel.setBackground(new java.awt.Color(255, 255, 255));
        filpanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        filpanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p3.setBackground(new java.awt.Color(0, 102, 204));
        p3.setForeground(new java.awt.Color(255, 255, 255));
        p3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb36.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lb36.setForeground(new java.awt.Color(255, 255, 255));
        lb36.setText("Filters");
        p3.add(lb36, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 0, 70, 40));

        filpanel.add(p3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1480, 40));

        empName.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        empName.setForeground(new java.awt.Color(153, 153, 153));
        empName.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        empName.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        empName.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                empNameItemStateChanged(evt);
            }
        });
        empName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empNameActionPerformed(evt);
            }
        });
        empName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                empNameKeyPressed(evt);
            }
        });
        filpanel.add(empName, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 70, 270, 40));

        loan.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        loan.setForeground(new java.awt.Color(153, 153, 153));
        loan.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        loan.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        loan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                loanKeyPressed(evt);
            }
        });
        filpanel.add(loan, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 70, 340, 40));

        filter.setBackground(new java.awt.Color(0, 153, 0));
        filter.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        filter.setForeground(new java.awt.Color(255, 255, 255));
        filter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        filter.setText("Apply Filters");
        filter.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        filter.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        filter.setOpaque(true);
        filter.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                filterMouseClicked(evt);
            }
        });
        filter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                filterKeyPressed(evt);
            }
        });
        filpanel.add(filter, new org.netbeans.lib.awtextra.AbsoluteConstraints(1080, 70, 140, 40));

        empId.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        empId.setForeground(new java.awt.Color(153, 153, 153));
        empId.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        empId.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        empId.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                empIdItemStateChanged(evt);
            }
        });
        empId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empIdActionPerformed(evt);
            }
        });
        empId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                empIdKeyPressed(evt);
            }
        });
        filpanel.add(empId, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 70, 310, 40));

        cancel1.setBackground(new java.awt.Color(153, 0, 0));
        cancel1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        cancel1.setForeground(new java.awt.Color(255, 255, 255));
        cancel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cancel1.setText("Cancel");
        cancel1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        cancel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancel1.setOpaque(true);
        cancel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancel1MouseClicked(evt);
            }
        });
        filpanel.add(cancel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1230, 70, 140, 40));

        viewLoans.add(filpanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 80, 1380, 160));

        loanTable.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        loanTable.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        loanTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Loan ID", "Employee Name", "Loan Name", "Date", "Monthly_Installment"
            }
        ));
        loanTable.setGridColor(new java.awt.Color(51, 102, 255));
        loanTable.setRowHeight(25);
        loanTable.setSelectionBackground(new java.awt.Color(102, 102, 102));
        jScrollPane1.setViewportView(loanTable);

        viewLoans.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 380, 1390, 330));

        remove.setBackground(new java.awt.Color(153, 0, 0));
        remove.setFont(new java.awt.Font("Trajan Pro", 1, 18)); // NOI18N
        remove.setForeground(new java.awt.Color(255, 255, 255));
        remove.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        remove.setText("Remove Loan");
        remove.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        remove.setOpaque(true);
        remove.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                removeMouseClicked(evt);
            }
        });
        viewLoans.add(remove, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 330, 200, 30));

        lb37.setFont(new java.awt.Font("Trajan Pro", 1, 14)); // NOI18N
        lb37.setText("Count of Employee :");
        viewLoans.add(lb37, new org.netbeans.lib.awtextra.AbsoluteConstraints(1170, 330, 180, 30));

        count.setEditable(false);
        count.setBackground(new java.awt.Color(255, 255, 255));
        count.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        count.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        viewLoans.add(count, new org.netbeans.lib.awtextra.AbsoluteConstraints(1360, 330, 130, 30));

        lb38.setBackground(new java.awt.Color(255, 255, 255));
        lb38.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb38.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/refresh.png"))); // NOI18N
        lb38.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb38.setOpaque(true);
        lb38.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb38MouseClicked(evt);
            }
        });
        viewLoans.add(lb38, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 330, 60, 30));

        jPanel5.add(viewLoans, "card3");

        main.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 210, 1550, 840));

        lb6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/closesudu.png"))); // NOI18N
        lb6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb6MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb6MouseExited(evt);
            }
        });
        main.add(lb6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1870, 0, 40, 40));

        lb11.setBackground(new java.awt.Color(242, 241, 239));
        lb11.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb11.setForeground(new java.awt.Color(255, 255, 255));
        lb11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lb11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/payroll.png"))); // NOI18N
        lb11.setText("  Payroll Review");
        lb11.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb11.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lb11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb11MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb11MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb11MouseExited(evt);
            }
        });
        main.add(lb11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 770, 260, 70));

        lb4.setBackground(new java.awt.Color(242, 241, 239));
        lb4.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb4.setForeground(new java.awt.Color(255, 255, 255));
        lb4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/reports.png"))); // NOI18N
        lb4.setText("  Reports");
        lb4.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb4MouseExited(evt);
            }
        });
        main.add(lb4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 860, 230, 70));

        lb10.setBackground(new java.awt.Color(242, 241, 239));
        lb10.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb10.setForeground(new java.awt.Color(255, 255, 255));
        lb10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/payroll.png"))); // NOI18N
        lb10.setText("  Payroll");
        lb10.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb10MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb10MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb10MouseExited(evt);
            }
        });
        main.add(lb10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 680, 210, 70));
        main.add(sp4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 940, 290, 10));

        lblDate.setFont(new java.awt.Font("DigifaceWide", 0, 18)); // NOI18N
        lblDate.setForeground(new java.awt.Color(255, 255, 255));
        main.add(lblDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 960, 220, 40));

        lblTime.setFont(new java.awt.Font("DigifaceWide", 0, 18)); // NOI18N
        lblTime.setForeground(new java.awt.Color(255, 255, 255));
        main.add(lblTime, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 1010, 220, 40));

        lbframe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/employee.jpg"))); // NOI18N
        main.add(lbframe, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1920, 1080));

        getContentPane().add(main, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1920, 1080));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lb3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb3MouseClicked
        DashBoard d1 = new DashBoard();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb3MouseClicked

    private void lb3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb3MouseEntered
        lb3.setForeground(Color.RED);
    }//GEN-LAST:event_lb3MouseEntered

    private void lb3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb3MouseExited
        lb3.setForeground(Color.WHITE);
    }//GEN-LAST:event_lb3MouseExited

    private void lb1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseEntered
        lb1.setForeground(Color.RED);
    }//GEN-LAST:event_lb1MouseEntered

    private void lb1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseExited
        lb1.setForeground(Color.white);
    }//GEN-LAST:event_lb1MouseExited

    private void lb5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseEntered
        lb5.setForeground(Color.RED);
    }//GEN-LAST:event_lb5MouseEntered

    private void lb5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseExited
        lb5.setForeground(Color.white);
    }//GEN-LAST:event_lb5MouseExited

    private void lb7MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseEntered
        lb7.setForeground(Color.RED);
    }//GEN-LAST:event_lb7MouseEntered

    private void lb7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseExited
        lb7.setForeground(Color.white);
    }//GEN-LAST:event_lb7MouseExited

    private void lb8MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseEntered
        lb8.setForeground(Color.RED);
    }//GEN-LAST:event_lb8MouseEntered

    private void lb8MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseExited
        lb8.setForeground(new Color(0,204,0));
    }//GEN-LAST:event_lb8MouseExited

    private void lb9MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseEntered
        lb9.setForeground(Color.RED);
    }//GEN-LAST:event_lb9MouseEntered

    private void lb9MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseExited
        lb9.setForeground(Color.white);
    }//GEN-LAST:event_lb9MouseExited

    private void dashMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dashMouseClicked
        DashBoard d1 = new DashBoard();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_dashMouseClicked

    private void lb25MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb25MouseClicked
        lb23.setText("Place New Loan");
        jPanel5.removeAll();
        jPanel5.repaint();
        jPanel5.revalidate();
        jPanel5.add(newLoan);
        jPanel5.repaint();
        jPanel5.revalidate();
        newtab.setBackground(new Color(0, 0, 102));
        viewtab.setBackground(new Color(255, 255, 255));
        viewtab.setForeground(new Color(0, 0, 0));
        newtab.setForeground(new Color(255, 255, 255));
    }//GEN-LAST:event_lb25MouseClicked

    private void viewtabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_viewtabMouseClicked
        lb23.setText("View Employee Loan Details");
        jPanel5.removeAll();
        jPanel5.repaint();
        jPanel5.revalidate();
        jPanel5.add(viewLoans);
        jPanel5.repaint();
        jPanel5.revalidate();
        viewtab.setBackground(new Color(0, 0, 102));
        newtab.setBackground(new Color(255, 255, 255));
        newtab.setForeground(new Color(0, 0, 0));
        viewtab.setForeground(new Color(255, 255, 255));
    }//GEN-LAST:event_viewtabMouseClicked

    private void lb5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseClicked
        EmployeeReview d1 = new EmployeeReview();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb5MouseClicked

    private void lb9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseClicked
        PlaceAdvance d1 = new PlaceAdvance();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb9MouseClicked

    private void newtabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_newtabMouseClicked
        lb23.setText("Place New Loan");
        jPanel5.removeAll();
        jPanel5.repaint();
        jPanel5.revalidate();
        jPanel5.add(newLoan);
        jPanel5.repaint();
        jPanel5.revalidate();
        newtab.setBackground(new Color(0, 0, 102));
        viewtab.setBackground(new Color(255, 255, 255));
        viewtab.setForeground(new Color(0, 0, 0));
        newtab.setForeground(new Color(255, 255, 255));
    }//GEN-LAST:event_newtabMouseClicked

    private void lb6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseClicked
        int result = JOptionPane.showConfirmDialog(this, "Do You Want To Exit?", "Exit:", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            this.dispose();
        } else if (result == JOptionPane.NO_OPTION) {
            this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        }
    }//GEN-LAST:event_lb6MouseClicked

    private void lb6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseEntered
        ImageIcon icon = new ImageIcon("src/lk/ijse/payroll/asset/closerathu.png");
        lb6.setIcon(icon);
    }//GEN-LAST:event_lb6MouseEntered

    private void lb6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseExited
        ImageIcon icon = new ImageIcon("src/lk/ijse/payroll/asset/closesudu.png");
        lb6.setIcon(icon);
    }//GEN-LAST:event_lb6MouseExited

    private void lb1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseClicked
        Administation d1 = new Administation();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb1MouseClicked

    private void employeeIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeIdActionPerformed
        boolean b = employeeId.getSelectedIndex() != -1;
        if (b) {

            try {
                String id = (String) employeeId.getSelectedItem();

                EmployeeDTO e = ctrl.searchempDetailsByName(id);
                if (e != null) {
                    String de = ctrl.searchDesignation(e.getDeId());
                    String d = e.getEmpName();

                    employeeName.setSelectedItem(d);

                    employeeDesignation.setText(de);

                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_employeeIdActionPerformed

    private void employeeNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeNameActionPerformed

    }//GEN-LAST:event_employeeNameActionPerformed

    private void loanNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loanNameActionPerformed

    }//GEN-LAST:event_loanNameActionPerformed

    private void lb7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseClicked
        Attendance d1 = new Attendance();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb7MouseClicked

    private void empNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empNameActionPerformed

    }//GEN-LAST:event_empNameActionPerformed

    private void employeeNameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_employeeNameItemStateChanged
        boolean b = employeeName.getSelectedIndex() != -1;
        if (b) {
            try {
                String name = employeeName.getSelectedItem().toString();
                // System.out.println(name);
                EmployeeDTO e = ctrl.searchEmpByName(name);
                if (e != null) {

                    String des = ctrl.searchDesignation(e.getDeId());
                    String empid = e.getEmpId();

                    employeeId.setSelectedItem(empid);
                    employeeDesignation.setText(des);

                }
            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeNameItemStateChanged

    private void loanNameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_loanNameItemStateChanged
        boolean b = loanName.getSelectedIndex() != -1;
        if (b) {
            try {
                String adv = loanName.getSelectedItem().toString();

                LoanDTO ad = ctrl.searchLoanDetailsByName(adv);

                if (ad != null) {
                    amount.setText(ad.getAmount().toString());

                }

            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_loanNameItemStateChanged

    private void saveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseClicked
        String empid1=employeeId.getSelectedItem().toString();
        String yr = ((JTextField) loandate.getDateEditor().getUiComponent()).getText();
        boolean date = ctrl.cheackAvailability(((JTextField) loandate.getDateEditor().getUiComponent()).getText(),empid1);
        if (employeeId.getSelectedIndex() == -1) {
            status.setText("Please Select Employee!");
            employeeId.requestFocus();

        } else if (loanName.getSelectedIndex() == -1) {
            status.setText("Please Select a Loan before save!");
            loanName.requestFocus();
        } else if (yr.equals("")) {
            status.setText("Please Select Valid Date before save!");
            loandate.requestFocus();

//        } else if (!yr.equals(new Date())) {
//            status.setText("Please Select Valid  date before save!");
//            loandate.requestFocus();
        } else if (!date) {
            JOptionPane.showMessageDialog(this, "Cannont Launch Loan For This Employee");
        } else {
            try {
                String adv = loanName.getSelectedItem().toString();
                LoanDTO ad = ctrl.searchLoanDetailsByName(adv);

                String elid = loanId.getText();
                String empid = employeeId.getSelectedItem().toString();
                String loanid = ad.getLoanId();

                String yer = ((JTextField) loandate.getDateEditor().getUiComponent()).getText();
                BigDecimal amount1 = new BigDecimal(amount.getText());
                amount1.precision();

                boolean added = ctrl.addLoan(elid, empid, loanid, yer, amount1);
                if (added) {
                    JOptionPane.showMessageDialog(this, "added");
                } else {
                    JOptionPane.showMessageDialog(this, "failed");
                }

                loadAllLoans();
                clean();
                loadItems();
                loanId.setText(loadNextId());
            } catch (Exception ex) {
                Logger.getLogger(Loans.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, ex.getMessage());
            }
            employeeId.requestFocus();
        }
    }//GEN-LAST:event_saveMouseClicked

    private void cancelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelMouseClicked
        int result = JOptionPane.showConfirmDialog(this, "Do You Want To Clear Filled Data?", "Warning", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            clean();
            loadItems();
        } else if (result == JOptionPane.NO_OPTION) {

        }


    }//GEN-LAST:event_cancelMouseClicked

    private void removeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_removeMouseClicked
        int result1 = JOptionPane.showConfirmDialog(this, "Do You Want To Clear Filled Data?", "Warning", JOptionPane.YES_NO_OPTION);
        if (result1 == JOptionPane.YES_OPTION) {
            try {
                if (loanTable.getSelectedRow() == -1) {
                    return;
                }

                boolean result = ctrl.delete(loanTable.getValueAt(loanTable.getSelectedRow(), 0).toString());

                if (result) {
                    JOptionPane.showMessageDialog(this, "Loan has been deleted successfully");
                } else {
                    JOptionPane.showMessageDialog(this, "Failed to delete the Loan");
                }

                loanTable.getSelectionModel().clearSelection();
                loadAllLoans();
                clean();
                loadItems();

            } catch (Exception ex) {
                Logger.getLogger(Loans.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (result1 == JOptionPane.NO_OPTION) {

        }


    }//GEN-LAST:event_removeMouseClicked

    private void empIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empIdActionPerformed
        boolean b = empId.getSelectedIndex() != -1;
        if (b) {
            try {
                String id = (String) empId.getSelectedItem();

                EmployeeDTO e = ctrl.searchemp(id);
                if (e != null) {
                    String de = ctrl.searchDesignation(e.getDeId());
                    String d = e.getEmpName();

                    empName.setSelectedItem(d);

                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_empIdActionPerformed

    private void filterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_filterMouseClicked
        DefaultTableModel dtm = (DefaultTableModel) loanTable.getModel();
        if (loan.getSelectedIndex() == -1 & empName.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Select one Catogary!");
        } else if (loan.getSelectedIndex() == -1) {
            try {

                EmployeeDTO employeeId = ctrl.searchemp(empId.getSelectedItem().toString());
                String empid = employeeId.getEmpName();
                System.out.println(empid);
                ArrayList<EmpLoanDTO> Alladvances = ctrl.getLoan(employeeId.getEmpId());
                dtm.setRowCount(0);
                for (EmpLoanDTO ad : Alladvances) {
                    String loanName = ctrl.searchLoanName(ad.getLoanId());
                    System.out.println(ad.getLoanId());
                    Object Rowdata[] = {ad.getElId(), employeeId.getEmpName(), loanName, ad.getDate(), ad.getInstallment()};

                    dtm.addRow(Rowdata);

                }
                int i = dtm.getRowCount();
                count.setText("" + i);

            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
            clean();
            loadItems();

        } else if (empId.getSelectedIndex() == -1) {
            try {

                String adv = loan.getSelectedItem().toString();
                dtm.setRowCount(0);
                LoanDTO l = ctrl.searchLoanDetailsByName(adv);
                String lid = l.getLoanId();

                ArrayList<EmpLoanDTO> Alladvances = ctrl.searchLoanDetailsByloanId(lid);

                for (EmpLoanDTO ad : Alladvances) {
                    String loanName = ctrl.searchLoanName(ad.getLoanId());
                    EmployeeDTO employeeId = ctrl.searchemp(ad.getEmpId());

                    Object Rowdata[] = {ad.getElId(), employeeId.getEmpName(), loanName, ad.getDate(), ad.getInstallment()};
                    dtm.addRow(Rowdata);

                }
                int i = dtm.getRowCount();
                count.setText("" + i);

            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
            clean();
            loadItems();
        } else if (empId.getSelectedIndex() != -1 & loan.getSelectedIndex() != -1) {
            JOptionPane.showMessageDialog(this, "Plese Select One Catogary");
            clean();
            loadItems();
            loadAllLoans();
        }
    }//GEN-LAST:event_filterMouseClicked

    private void empIdItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_empIdItemStateChanged

    }//GEN-LAST:event_empIdItemStateChanged

    private void empNameItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_empNameItemStateChanged

        boolean b = empName.getSelectedIndex() != -1;
        if (b) {
            try {
                String name = empName.getSelectedItem().toString();
                // System.out.println(name);
                EmployeeDTO e = ctrl.searchEmpId(name);
                if (e != null) {

                    String empid = e.getEmpId();

                    empId.setSelectedItem(empid);

                }
            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_empNameItemStateChanged

    private void lb38MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb38MouseClicked
        loadAllLoans();
    }//GEN-LAST:event_lb38MouseClicked

    private void employeeIdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeIdKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (employeeId.getSelectedIndex() == -1) {
                    status.setText("Please select employee! ");
                    employeeId.requestFocus();
                } else {
                    KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                    manager.getFocusOwner().transferFocus();
                }
                break;
        }
    }//GEN-LAST:event_employeeIdKeyPressed

    private void employeeNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeNameKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (employeeId.getSelectedIndex() == -1) {
                    status.setText("Please select employee! ");
                    employeeId.requestFocus();
                } else {
                    KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                    manager.getFocusOwner().transferFocus();
                }
                break;
        }
    }//GEN-LAST:event_employeeNameKeyPressed

    private void loanNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_loanNameKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (employeeId.getSelectedIndex() == -1) {
                    status.setText("Please select employee! ");
                    employeeId.requestFocus();
                } else if (loanName.getSelectedIndex() == -1) {
                    status.setText("Please select Loan! ");
                    loanName.requestFocus();
                } else {
                    save.requestFocus();
                }
                break;
        }
    }//GEN-LAST:event_loanNameKeyPressed

    private void loandateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_loandateKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                save.requestFocus();
                break;
        }
    }//GEN-LAST:event_loandateKeyPressed

    private void saveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_saveKeyPressed
        String empid1=employeeId.getSelectedItem().toString();
        String yr = ((JTextField) loandate.getDateEditor().getUiComponent()).getText();
        boolean date = ctrl.cheackAvailability(((JTextField) loandate.getDateEditor().getUiComponent()).getText(),empid1);
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (employeeId.getSelectedIndex() == -1) {
                    status.setText("Please select employee! ");
                    employeeId.requestFocus();
                } else if (loanName.getSelectedIndex() == -1) {
                    status.setText("Please select Loan! ");
                    loanName.requestFocus();
                } else if (!date) {
                    JOptionPane.showMessageDialog(this, "Cannont Launch Loan For This Employee");
                } else {
                    try {
                        String adv = loanName.getSelectedItem().toString();
                        LoanDTO ad = ctrl.searchLoanDetailsByName(adv);

                        String elid = loanId.getText();
                        String empid = employeeId.getSelectedItem().toString();
                        String loanid = ad.getLoanId();

                        String yer = ((JTextField) loandate.getDateEditor().getUiComponent()).getText();
                        BigDecimal amount1 = new BigDecimal(amount.getText());
                        amount1.precision();

                        boolean added = ctrl.addLoan(elid, empid, loanid, yer, amount1);
                        if (added) {
                            JOptionPane.showMessageDialog(this, "added");
                        } else {
                            JOptionPane.showMessageDialog(this, "failed");
                        }

                        loadAllLoans();
                        clean();
                        loadItems();
                        loanId.setText(loadNextId());
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(this, ex.getMessage());
                    }
                    employeeId.requestFocus();
                }
                break;
        }
    }//GEN-LAST:event_saveKeyPressed

    private void loanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_loanKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                filter.requestFocus();
                break;
        }
    }//GEN-LAST:event_loanKeyPressed

    private void empIdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_empIdKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                filter.requestFocus();
                break;
        }
    }//GEN-LAST:event_empIdKeyPressed

    private void empNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_empNameKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                filter.requestFocus();
                break;
        }
    }//GEN-LAST:event_empNameKeyPressed

    private void filterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_filterKeyPressed
        DefaultTableModel dtm = (DefaultTableModel) loanTable.getModel();
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (loan.getSelectedIndex() == -1 & empName.getSelectedIndex() == -1) {
                    JOptionPane.showMessageDialog(this, "Select one Catogary!");
                } else if (loan.getSelectedIndex() == -1) {
                    try {

                        EmployeeDTO employeeId = ctrl.searchemp(empId.getSelectedItem().toString());
                        String empid = employeeId.getEmpName();
                        System.out.println(empid);
                        ArrayList<EmpLoanDTO> All = ctrl.getLoan(employeeId.getEmpId());
                        dtm.setRowCount(0);
                        for (EmpLoanDTO ad : All) {
                            String loanName = ctrl.searchLoanName(ad.getLoanId());
                            System.out.println(ad.getLoanId());
                            Object Rowdata[] = {ad.getElId(), employeeId.getEmpName(), loanName, ad.getDate(), ad.getInstallment()};

                            dtm.addRow(Rowdata);

                        }
                        int i = dtm.getRowCount();
                        count.setText("" + i);

                    } catch (Exception ex) {
                        Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    clean();
                    loadItems();

                } else if (empId.getSelectedIndex() == -1) {
                    try {

                        String adv = loan.getSelectedItem().toString();
                        dtm.setRowCount(0);
                        LoanDTO l = ctrl.searchLoanDetailsByName(adv);
                        String lid = l.getLoanId();

                        ArrayList<EmpLoanDTO> Alladvances = ctrl.searchLoanDetailsByloanId(lid);

                        for (EmpLoanDTO ad : Alladvances) {
                            String loanName = ctrl.searchLoanName(ad.getLoanId());
                            EmployeeDTO employeeId = ctrl.searchemp(ad.getEmpId());

                            Object Rowdata[] = {ad.getElId(), employeeId.getEmpName(), loanName, ad.getDate(), ad.getInstallment()};
                            dtm.addRow(Rowdata);

                        }
                        int i = dtm.getRowCount();
                        count.setText("" + i);

                    } catch (Exception ex) {
                        Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    clean();
                    loadItems();
                } else if (empId.getSelectedIndex() != -1 & loan.getSelectedIndex() != -1) {
                    JOptionPane.showMessageDialog(this, "Plese Select One Catogary");
                    clean();
                    loadItems();
                    loadAllLoans();
                }
                break;
        }
    }//GEN-LAST:event_filterKeyPressed

    private void cancel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancel1MouseClicked
        clean();
        loadItems();
    }//GEN-LAST:event_cancel1MouseClicked

    private void saveKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_saveKeyReleased
        String empid1=employeeId.getSelectedItem().toString();
        String yr = ((JTextField) loandate.getDateEditor().getUiComponent()).getText();
        boolean date = ctrl.cheackAvailability(((JTextField) loandate.getDateEditor().getUiComponent()).getText(),empid1);
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (employeeId.getSelectedIndex() == -1) {
                    status.setText("Please select employee! ");
                    employeeId.requestFocus();
                } else if (loanName.getSelectedIndex() == -1) {
                    status.setText("Please select Loan! ");
                    loanName.requestFocus();
                } else if (!date) {
                    JOptionPane.showMessageDialog(this, "Cannont Launch Loan For This Employee");
                } else {
                    try {
                        String adv = loanName.getSelectedItem().toString();
                        LoanDTO ad = ctrl.searchLoanDetailsByName(adv);

                        String elid = loanId.getText();
                        String empid = employeeId.getSelectedItem().toString();
                        String loanid = ad.getLoanId();

                        String yer = ((JTextField) loandate.getDateEditor().getUiComponent()).getText();
                        BigDecimal amount1 = new BigDecimal(amount.getText());
                        amount1.precision();

                        boolean added = ctrl.addLoan(elid, empid, loanid, yer, amount1);
                        if (added) {
                            JOptionPane.showMessageDialog(this, "added");
                        } else {
                            JOptionPane.showMessageDialog(this, "failed");
                        }

                        loadAllLoans();
                        clean();
                        loadItems();
                        loanId.setText(loadNextId());
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(this, ex.getMessage());
                    }
                    employeeId.requestFocus();
                }
                break;
        }
    }//GEN-LAST:event_saveKeyReleased

    private void saveFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_saveFocusGained
        save.setBackground(new Color(0, 102, 0));
        save.setForeground(Color.white);
    }//GEN-LAST:event_saveFocusGained

    private void saveFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_saveFocusLost
        save.setBackground(new Color(255, 255, 255));
        save.setForeground(Color.black);
    }//GEN-LAST:event_saveFocusLost

    private void saveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseEntered
        save.setBackground(new Color(0, 102, 0));
        save.setForeground(Color.white);
    }//GEN-LAST:event_saveMouseEntered

    private void saveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseExited
        save.setBackground(new Color(255, 255, 255));
        save.setForeground(Color.black);
    }//GEN-LAST:event_saveMouseExited

    private void saveMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMousePressed
        save.setBackground(new Color(17, 119, 45));
        save.setForeground(Color.white);
    }//GEN-LAST:event_saveMousePressed

    private void saveMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseReleased
        save.setBackground(new Color(0, 102, 0));
        save.setForeground(Color.white);
    }//GEN-LAST:event_saveMouseReleased

    private void lb11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb11MouseClicked
        PayrollReview d1 = new PayrollReview();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb11MouseClicked

    private void lb11MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb11MouseEntered
        lb11.setForeground(Color.red);
    }//GEN-LAST:event_lb11MouseEntered

    private void lb11MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb11MouseExited
        lb11.setForeground(Color.white);
    }//GEN-LAST:event_lb11MouseExited

    private void lb4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseClicked
        Reports d1 = new Reports();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb4MouseClicked

    private void lb4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseEntered
        lb4.setForeground(Color.RED);
    }//GEN-LAST:event_lb4MouseEntered

    private void lb4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseExited
        lb4.setForeground(Color.white);
    }//GEN-LAST:event_lb4MouseExited

    private void lb10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseClicked
        Payroll d1 = new Payroll();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb10MouseClicked

    private void lb10MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseEntered
        lb10.setForeground(Color.RED);
    }//GEN-LAST:event_lb10MouseEntered

    private void lb10MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseExited
        lb10.setForeground(Color.white);
    }//GEN-LAST:event_lb10MouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(placeLoans.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(placeLoans.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(placeLoans.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(placeLoans.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new placeLoans().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField amount;
    private javax.swing.JLabel cancel;
    private javax.swing.JLabel cancel1;
    private javax.swing.JTextField count;
    private javax.swing.JLabel dash;
    private javax.swing.JTextField duration;
    private javax.swing.JComboBox<String> empId;
    private javax.swing.JComboBox<String> empName;
    private javax.swing.JTextField employeeDesignation;
    private javax.swing.JComboBox<String> employeeId;
    private javax.swing.JComboBox<String> employeeName;
    private javax.swing.JPanel filpanel;
    private javax.swing.JLabel filter;
    private javax.swing.JLabel home;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lb1;
    private javax.swing.JLabel lb10;
    private javax.swing.JLabel lb11;
    private javax.swing.JLabel lb20;
    private javax.swing.JLabel lb21;
    private javax.swing.JLabel lb22;
    private javax.swing.JLabel lb23;
    private javax.swing.JLabel lb24;
    private javax.swing.JLabel lb25;
    private javax.swing.JLabel lb26;
    private javax.swing.JLabel lb27;
    private javax.swing.JLabel lb28;
    private javax.swing.JLabel lb29;
    private javax.swing.JLabel lb3;
    private javax.swing.JLabel lb30;
    private javax.swing.JLabel lb31;
    private javax.swing.JLabel lb32;
    private javax.swing.JLabel lb33;
    private javax.swing.JLabel lb34;
    private javax.swing.JLabel lb35;
    private javax.swing.JLabel lb36;
    private javax.swing.JLabel lb37;
    private javax.swing.JLabel lb38;
    private javax.swing.JLabel lb4;
    private javax.swing.JLabel lb5;
    private javax.swing.JLabel lb6;
    private javax.swing.JLabel lb7;
    private javax.swing.JLabel lb8;
    private javax.swing.JLabel lb9;
    private javax.swing.JLabel lbframe;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblTime;
    private javax.swing.JComboBox<String> loan;
    private javax.swing.JTextField loanId;
    private javax.swing.JComboBox<String> loanName;
    private javax.swing.JTable loanTable;
    private com.toedter.calendar.JDateChooser loandate;
    private javax.swing.JPanel main;
    private javax.swing.JLabel menu;
    private javax.swing.JPanel newLoan;
    private javax.swing.JLabel newtab;
    private javax.swing.JPanel p1;
    private javax.swing.JPanel p2;
    private javax.swing.JPanel p3;
    private javax.swing.JPanel path;
    private javax.swing.JLabel remove;
    private javax.swing.JLabel save;
    private javax.swing.JSeparator sp1;
    private javax.swing.JSeparator sp2;
    private javax.swing.JSeparator sp3;
    private javax.swing.JSeparator sp4;
    private javax.swing.JLabel status;
    private javax.swing.JPanel viewLoans;
    private javax.swing.JLabel viewtab;
    // End of variables declaration//GEN-END:variables
private void setTime() {
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("hh-mm-ss a");
                lblTime.setText(sdf.format(date));
            }
        }).start();
    }

    private void setDate() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        lblDate.setText(sdf.format(date));
    }

    public void clean() {

        employeeName.removeAllItems();
        employeeId.removeAllItems();

        employeeDesignation.setText("");
        loanName.removeAllItems();
        amount.setText("");
        loandate.setDate(new Date());
        loan.removeAllItems();
        empId.removeAllItems();
        empName.removeAllItems();

        employeeId.setSelectedIndex(-1);
        empName.setSelectedIndex(-1);
        loanName.setSelectedIndex(-1);
        loan.setSelectedIndex(-1);
        empId.setSelectedIndex(-1);
        empName.setSelectedIndex(-1);

    }

    private String loadNextId() {
        String id = getLastId();
        if (id == null) {
            id = "Lo0";
        }
        char ch = id.charAt(id.length() - 1);
        int a = Character.getNumericValue(ch) + 1;
        return id = id.substring(0, id.length() - 1) + a;

    }

    private String getLastId() {
        String lastId = null;
        try {
            lastId = ctrl.searchLoanId();
        } catch (Exception e) {
            // e.printStackTrace();
        }
        return lastId;
    }

}
