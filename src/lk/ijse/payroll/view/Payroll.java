/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import lk.ijse.payroll.controller.PayrollController;
import lk.ijse.payroll.dto.BankDetailsDTO;
import lk.ijse.payroll.dto.EmpLoanDTO;
import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.dto.PayrollDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.dto.shortLeaveDTO;

/**
 *
 * @author udara
 */
public class Payroll extends javax.swing.JFrame {

    private PayrollController ctrl;

    /**
     * Creates new form Payroll
     */
    public Payroll() {
        initComponents();
        transpareent();
        setCellsAlignment(attendanceTable);
        setDate();
        setTime();
        ctrl = new PayrollController();
        ImageIcon img = new ImageIcon("src/lk/ijse/payroll/asset/payloku.png");
        this.setIconImage(img.getImage());
        loadItems();
        payrollDate.setDateFormatString("yyyy:MM:dd");
        payrollDate.setDate(new Date());
        employeeId.requestFocus();
        lb10.setForeground(new Color(0, 204, 0));
    }

    public void loadAllattdetails() {
        int count = 0;
        try {
            String id = employeeId.getSelectedItem().toString();
            String date = ((JTextField) payrollDate.getDateEditor().getUiComponent()).getText();

            ArrayList<QueryDTO> all = ctrl.getAllDetails(id, date);
            DefaultTableModel dtm = (DefaultTableModel) attendanceTable.getModel();
            dtm.setRowCount(0);

            for (QueryDTO a : all) {

                Object Rowdata[] = {a.getEmpName(), a.getYear(), a.getMonth(), a.getWorked_hours(), a.getnOT(), a.getdOT()};
                dtm.addRow(Rowdata);

            }
            int a = dtm.getRowCount();

            int normalOt = 0;
            for (int i = 0; i < dtm.getRowCount(); i++) {
                normalOt += attendanceTable.getValueAt(i, 4).hashCode();
            }

            int doubleOt = 0;
            for (int i = 0; i < dtm.getRowCount(); i++) {
                doubleOt += attendanceTable.getValueAt(i, 5).hashCode();
            }

            BigDecimal f = new BigDecimal(basicSalary.getText());
            f.precision();
            String ep8 = ctrl.calculateAmount(f);
            String ep12 = ctrl.calculateep12(f);
            String et3 = ctrl.calculateet3(f);
            EmployeeDTO e = ctrl.searchemp(id);
            String des = e.getDeId();
            String de = ctrl.searchDes(e.getDeId());
            QueryDTO zz = ctrl.searchForBonus(de, a);
            if (zz != null) {
                bonusName.setText(zz.getBonusName());
                bonusAmount.setText(zz.getBonusAmount());
            } else {
                bonusName.setText("No Bonus Available");
                bonusAmount.setText("No Bonus Available");
            }

            workedDays.setText("" + a);
            normalOtAmount.setText("" + normalOt);
            doubleOtAmount.setText("" + doubleOt);

            epf8.setText(ep8);
            epf12.setText(ep12);
            etf3.setText(et3);

            ArrayList<shortLeaveDTO> slv = ctrl.getAllSLDetails(id, date);
            if (slv != null) {
                for (shortLeaveDTO s : slv) {
                    count += s.getDuration();
                }
                sl.setText("" + count);
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void loadAdvanceDetails() {
        try {
            String id = employeeId.getSelectedItem().toString();
            String date = ((JTextField) payrollDate.getDateEditor().getUiComponent()).getText();
            try {

                QueryDTO ad = ctrl.searchAdvancesDetail(id, date);
                String name = ad.getAdName();
                String adamount = ad.getAmount().toString();
                advanceName.setText(name);
                advanceAmount.setText(adamount);
            } catch (NullPointerException n) {
                advanceName.setText("No Advances yet");
                advanceAmount.setText("No Advances yet");

            }

        } catch (Exception ex) {

            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void setCellsAlignment(JTable table) {
        ((DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        table.getTableHeader().setFont(new Font("Tahome", Font.BOLD, 15));
        table.setFont(new Font("Tahome", 1, 13));
    }

    public void loadItems() {
        try {
            ArrayList<String> emp = ctrl.LoaDAllemployeeId();
            for (String e : emp) {

                employeeId.addItem(e);

            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            ArrayList<String> emp = ctrl.LoadallemployeeName();
            for (String e : emp) {

                employeeName.addItem(e);

            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        main = new javax.swing.JPanel();
        menu = new javax.swing.JLabel();
        sp1 = new javax.swing.JSeparator();
        lb5 = new javax.swing.JLabel();
        lb1 = new javax.swing.JLabel();
        lb6 = new javax.swing.JLabel();
        lb7 = new javax.swing.JLabel();
        lb8 = new javax.swing.JLabel();
        lb9 = new javax.swing.JLabel();
        lb12 = new javax.swing.JLabel();
        Payroll = new javax.swing.JPanel();
        lb13 = new javax.swing.JLabel();
        lb14 = new javax.swing.JLabel();
        lb15 = new javax.swing.JLabel();
        lb16 = new javax.swing.JLabel();
        lb17 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        attendanceTable = new javax.swing.JTable();
        lb18 = new javax.swing.JLabel();
        lb19 = new javax.swing.JLabel();
        lb20 = new javax.swing.JLabel();
        lb21 = new javax.swing.JLabel();
        lb22 = new javax.swing.JLabel();
        lb23 = new javax.swing.JLabel();
        basicSalary = new javax.swing.JTextField();
        payrollDate = new com.toedter.calendar.JDateChooser();
        employeeName = new javax.swing.JComboBox<>();
        employeeId = new javax.swing.JComboBox<>();
        doubleOtAmount = new javax.swing.JTextField();
        bank = new javax.swing.JTextField();
        accNo = new javax.swing.JTextField();
        lb24 = new javax.swing.JLabel();
        freeLeaves = new javax.swing.JTextField();
        normalOtAmount = new javax.swing.JTextField();
        lb25 = new javax.swing.JLabel();
        lb26 = new javax.swing.JLabel();
        lb27 = new javax.swing.JLabel();
        lb28 = new javax.swing.JLabel();
        lb29 = new javax.swing.JLabel();
        lb30 = new javax.swing.JLabel();
        lb32 = new javax.swing.JLabel();
        lb33 = new javax.swing.JLabel();
        lb34 = new javax.swing.JLabel();
        lb35 = new javax.swing.JLabel();
        workedDays = new javax.swing.JTextField();
        bonusName = new javax.swing.JTextField();
        bonusAmount = new javax.swing.JTextField();
        advanceName = new javax.swing.JTextField();
        advanceAmount = new javax.swing.JTextField();
        loanName = new javax.swing.JTextField();
        loanAmount = new javax.swing.JTextField();
        epf8 = new javax.swing.JTextField();
        netSalary = new javax.swing.JTextField();
        noOfLeaves = new javax.swing.JTextField();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        lb36 = new javax.swing.JLabel();
        lb37 = new javax.swing.JLabel();
        lb38 = new javax.swing.JLabel();
        etf3 = new javax.swing.JTextField();
        grossSalary = new javax.swing.JTextField();
        totalDeduction = new javax.swing.JTextField();
        lb39 = new javax.swing.JLabel();
        lb40 = new javax.swing.JLabel();
        pqth = new javax.swing.JPanel();
        lb41 = new javax.swing.JLabel();
        lb42 = new javax.swing.JLabel();
        lb43 = new javax.swing.JLabel();
        lb44 = new javax.swing.JLabel();
        lb45 = new javax.swing.JLabel();
        lb46 = new javax.swing.JLabel();
        sl = new javax.swing.JTextField();
        lb47 = new javax.swing.JLabel();
        lb48 = new javax.swing.JLabel();
        epf12 = new javax.swing.JTextField();
        lb11 = new javax.swing.JLabel();
        lb4 = new javax.swing.JLabel();
        lb10 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        lblDate = new javax.swing.JLabel();
        lblTime = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        main.setBackground(new java.awt.Color(255, 255, 255));
        main.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        menu.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 14)); // NOI18N
        menu.setForeground(new java.awt.Color(255, 255, 255));
        menu.setText("Menu");
        main.add(menu, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 60, -1));

        sp1.setBackground(new java.awt.Color(204, 204, 255));
        sp1.setForeground(new java.awt.Color(204, 255, 204));
        main.add(sp1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 310, 10));

        lb5.setBackground(new java.awt.Color(242, 241, 239));
        lb5.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb5.setForeground(new java.awt.Color(255, 255, 255));
        lb5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/home2.png"))); // NOI18N
        lb5.setText(" DashBoard");
        lb5.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb5.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lb5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb5MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb5MouseExited(evt);
            }
        });
        main.add(lb5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 240, 70));

        lb1.setBackground(new java.awt.Color(242, 241, 239));
        lb1.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb1.setForeground(new java.awt.Color(255, 255, 255));
        lb1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/admin1.png"))); // NOI18N
        lb1.setText(" Administration");
        lb1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb1MouseExited(evt);
            }
        });
        main.add(lb1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 230, 310, 70));

        lb6.setBackground(new java.awt.Color(242, 241, 239));
        lb6.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb6.setForeground(new java.awt.Color(255, 255, 255));
        lb6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/employee.png"))); // NOI18N
        lb6.setText("  Employee");
        lb6.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb6MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb6MouseExited(evt);
            }
        });
        main.add(lb6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 320, 240, 70));

        lb7.setBackground(new java.awt.Color(242, 241, 239));
        lb7.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb7.setForeground(new java.awt.Color(255, 255, 255));
        lb7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/attendance.png"))); // NOI18N
        lb7.setText("  Attendance");
        lb7.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb7MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb7MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb7MouseExited(evt);
            }
        });
        main.add(lb7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 410, 260, 70));

        lb8.setBackground(new java.awt.Color(242, 241, 239));
        lb8.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb8.setForeground(new java.awt.Color(255, 255, 255));
        lb8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/loan.png"))); // NOI18N
        lb8.setText("  Loan");
        lb8.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb8MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb8MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb8MouseExited(evt);
            }
        });
        main.add(lb8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 500, 190, 70));

        lb9.setBackground(new java.awt.Color(242, 241, 239));
        lb9.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb9.setForeground(new java.awt.Color(255, 255, 255));
        lb9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/advance.png"))); // NOI18N
        lb9.setText("  Advances");
        lb9.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb9MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb9MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb9MouseExited(evt);
            }
        });
        main.add(lb9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 590, 240, 70));

        lb12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/closealu.png"))); // NOI18N
        lb12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb12MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb12MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb12MouseExited(evt);
            }
        });
        main.add(lb12, new org.netbeans.lib.awtextra.AbsoluteConstraints(1870, 0, 40, 40));

        Payroll.setBackground(new java.awt.Color(255, 255, 255));
        Payroll.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb13.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb13.setText("Basic Salary");
        lb13.setFocusable(false);
        Payroll.add(lb13, new org.netbeans.lib.awtextra.AbsoluteConstraints(1220, 220, -1, -1));

        lb14.setText("Date");
        Payroll.add(lb14, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, -1, -1));

        lb15.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb15.setText("Double OT");
        lb15.setFocusable(false);
        Payroll.add(lb15, new org.netbeans.lib.awtextra.AbsoluteConstraints(1260, 490, 90, -1));

        lb16.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb16.setText("Bank ");
        lb16.setFocusable(false);
        Payroll.add(lb16, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 220, -1, -1));

        lb17.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb17.setText("Acc No");
        lb17.setFocusable(false);
        Payroll.add(lb17, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 220, -1, -1));

        attendanceTable.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        attendanceTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Employee Name", "Year", "Month", "Worked Hourse", "Normal OT", "Double OT"
            }
        ));
        attendanceTable.setRowHeight(25);
        attendanceTable.setSelectionBackground(new java.awt.Color(102, 102, 102));
        jScrollPane1.setViewportView(attendanceTable);

        Payroll.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 300, 1380, 170));

        lb18.setBackground(new java.awt.Color(255, 255, 255));
        lb18.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb18.setForeground(new java.awt.Color(0, 102, 0));
        lb18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb18.setText("Check For Leaves");
        lb18.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 0), 2));
        lb18.setFocusable(false);
        lb18.setOpaque(true);
        Payroll.add(lb18, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 780, 170, 30));

        lb19.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb19.setText("Worked Days");
        lb19.setFocusable(false);
        Payroll.add(lb19, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 490, -1, -1));

        lb20.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb20.setText("Advance Name");
        lb20.setFocusable(false);
        Payroll.add(lb20, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 710, -1, -1));

        lb21.setBackground(new java.awt.Color(255, 255, 255));
        lb21.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb21.setForeground(new java.awt.Color(0, 102, 0));
        lb21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb21.setText("Check For Loans");
        lb21.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 0), 2));
        lb21.setFocusable(false);
        lb21.setOpaque(true);
        Payroll.add(lb21, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 560, 160, 30));

        lb22.setBackground(new java.awt.Color(255, 255, 255));
        lb22.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb22.setForeground(new java.awt.Color(0, 102, 0));
        lb22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb22.setText("Check For Advances");
        lb22.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 0), 2));
        lb22.setFocusable(false);
        lb22.setOpaque(true);
        Payroll.add(lb22, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 670, 200, 30));

        lb23.setBackground(new java.awt.Color(255, 255, 255));
        lb23.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb23.setForeground(new java.awt.Color(0, 102, 0));
        lb23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb23.setText("Check For Funds");
        lb23.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 0), 2));
        lb23.setFocusable(false);
        lb23.setOpaque(true);
        Payroll.add(lb23, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 660, 160, 30));

        basicSalary.setEditable(false);
        basicSalary.setBackground(new java.awt.Color(255, 255, 255));
        basicSalary.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        basicSalary.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        basicSalary.setFocusable(false);
        Payroll.add(basicSalary, new org.netbeans.lib.awtextra.AbsoluteConstraints(1240, 240, 220, 30));
        Payroll.add(payrollDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 150, 320, 30));

        employeeName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeName.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "~Select employee~" }));
        employeeName.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeNameActionPerformed(evt);
            }
        });
        Payroll.add(employeeName, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 240, 210, 30));

        employeeId.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeId.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "~Select emp id~" }));
        employeeId.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeIdActionPerformed(evt);
            }
        });
        Payroll.add(employeeId, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 240, 180, 30));

        doubleOtAmount.setEditable(false);
        doubleOtAmount.setBackground(new java.awt.Color(255, 255, 255));
        doubleOtAmount.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        doubleOtAmount.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        doubleOtAmount.setFocusable(false);
        Payroll.add(doubleOtAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(1350, 480, 110, 30));

        bank.setEditable(false);
        bank.setBackground(new java.awt.Color(255, 255, 255));
        bank.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        bank.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        bank.setFocusable(false);
        Payroll.add(bank, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 240, 370, 30));

        accNo.setEditable(false);
        accNo.setBackground(new java.awt.Color(255, 255, 255));
        accNo.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        accNo.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        accNo.setFocusable(false);
        Payroll.add(accNo, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 240, 220, 30));

        lb24.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb24.setText("Normal OT ");
        lb24.setFocusable(false);
        Payroll.add(lb24, new org.netbeans.lib.awtextra.AbsoluteConstraints(1000, 490, -1, -1));

        freeLeaves.setEditable(false);
        freeLeaves.setBackground(new java.awt.Color(255, 255, 255));
        freeLeaves.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        freeLeaves.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        freeLeaves.setFocusable(false);
        Payroll.add(freeLeaves, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 890, 230, 30));

        normalOtAmount.setEditable(false);
        normalOtAmount.setBackground(new java.awt.Color(255, 255, 255));
        normalOtAmount.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        normalOtAmount.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        normalOtAmount.setFocusable(false);
        normalOtAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                normalOtAmountActionPerformed(evt);
            }
        });
        Payroll.add(normalOtAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 480, 120, 30));

        lb25.setBackground(new java.awt.Color(255, 255, 255));
        lb25.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb25.setForeground(new java.awt.Color(0, 102, 0));
        lb25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb25.setText("Check For Bonus");
        lb25.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 0), 2));
        lb25.setFocusable(false);
        lb25.setOpaque(true);
        Payroll.add(lb25, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 560, 170, 30));

        lb26.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb26.setText("Bonus Name");
        lb26.setFocusable(false);
        Payroll.add(lb26, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 600, -1, -1));

        lb27.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb27.setText("Amount");
        lb27.setFocusable(false);
        Payroll.add(lb27, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 630, -1, -1));

        lb28.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb28.setText("Monthly Installment");
        lb28.setFocusable(false);
        Payroll.add(lb28, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 620, -1, -1));

        lb29.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb29.setText("Mothly Installment");
        lb29.setFocusable(false);
        Payroll.add(lb29, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 740, -1, -1));

        lb30.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb30.setText("E.P.F 8%");
        lb30.setFocusable(false);
        Payroll.add(lb30, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 700, -1, -1));

        lb32.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb32.setText("Loan Name");
        lb32.setFocusable(false);
        Payroll.add(lb32, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 590, -1, -1));

        lb33.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb33.setText("Short Leaves");
        lb33.setFocusable(false);
        Payroll.add(lb33, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 860, -1, -1));

        lb34.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb34.setText("Net Salary");
        lb34.setFocusable(false);
        Payroll.add(lb34, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 910, 80, 30));

        lb35.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb35.setText("No of Leaves");
        lb35.setFocusable(false);
        Payroll.add(lb35, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 820, -1, -1));

        workedDays.setEditable(false);
        workedDays.setBackground(new java.awt.Color(255, 255, 255));
        workedDays.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        workedDays.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        workedDays.setFocusable(false);
        Payroll.add(workedDays, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 480, 70, 30));

        bonusName.setEditable(false);
        bonusName.setBackground(new java.awt.Color(255, 255, 255));
        bonusName.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        bonusName.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        bonusName.setFocusable(false);
        Payroll.add(bonusName, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 590, 230, 30));

        bonusAmount.setEditable(false);
        bonusAmount.setBackground(new java.awt.Color(255, 255, 255));
        bonusAmount.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        bonusAmount.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        bonusAmount.setFocusable(false);
        Payroll.add(bonusAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 630, 230, 30));

        advanceName.setEditable(false);
        advanceName.setBackground(new java.awt.Color(255, 255, 255));
        advanceName.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        advanceName.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        advanceName.setFocusable(false);
        Payroll.add(advanceName, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 700, 320, 30));

        advanceAmount.setEditable(false);
        advanceAmount.setBackground(new java.awt.Color(255, 255, 255));
        advanceAmount.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        advanceAmount.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        advanceAmount.setFocusable(false);
        Payroll.add(advanceAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 740, 230, 30));

        loanName.setEditable(false);
        loanName.setBackground(new java.awt.Color(255, 255, 255));
        loanName.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        loanName.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        loanName.setFocusable(false);
        Payroll.add(loanName, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 580, 230, 30));

        loanAmount.setEditable(false);
        loanAmount.setBackground(new java.awt.Color(255, 255, 255));
        loanAmount.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        loanAmount.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        loanAmount.setFocusable(false);
        Payroll.add(loanAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 620, 230, 30));

        epf8.setEditable(false);
        epf8.setBackground(new java.awt.Color(255, 255, 255));
        epf8.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        epf8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        epf8.setFocusable(false);
        Payroll.add(epf8, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 690, 230, 30));

        netSalary.setEditable(false);
        netSalary.setBackground(new java.awt.Color(255, 255, 255));
        netSalary.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        netSalary.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        netSalary.setFocusable(false);
        Payroll.add(netSalary, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 910, 230, 30));

        noOfLeaves.setEditable(false);
        noOfLeaves.setBackground(new java.awt.Color(255, 255, 255));
        noOfLeaves.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        noOfLeaves.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        noOfLeaves.setFocusable(false);
        Payroll.add(noOfLeaves, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 810, 230, 30));
        Payroll.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, 1540, 10));
        Payroll.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 530, 1540, 10));

        lb36.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb36.setText("E.T.F 3%");
        lb36.setFocusable(false);
        Payroll.add(lb36, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 770, -1, -1));

        lb37.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb37.setText("Gross Salary");
        lb37.setFocusable(false);
        Payroll.add(lb37, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 830, 100, 30));

        lb38.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb38.setText("Total deductions");
        lb38.setFocusable(false);
        Payroll.add(lb38, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 870, 140, 30));

        etf3.setEditable(false);
        etf3.setBackground(new java.awt.Color(255, 255, 255));
        etf3.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        etf3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        etf3.setFocusable(false);
        Payroll.add(etf3, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 770, 230, 30));

        grossSalary.setEditable(false);
        grossSalary.setBackground(new java.awt.Color(255, 255, 255));
        grossSalary.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        grossSalary.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        grossSalary.setFocusable(false);
        Payroll.add(grossSalary, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 830, 230, 30));

        totalDeduction.setEditable(false);
        totalDeduction.setBackground(new java.awt.Color(255, 255, 255));
        totalDeduction.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        totalDeduction.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        totalDeduction.setFocusable(false);
        Payroll.add(totalDeduction, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 870, 230, 30));

        lb39.setBackground(new java.awt.Color(31, 191, 21));
        lb39.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        lb39.setForeground(new java.awt.Color(255, 255, 255));
        lb39.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb39.setText("Save");
        lb39.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb39.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb39.setOpaque(true);
        lb39.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb39MouseClicked(evt);
            }
        });
        Payroll.add(lb39, new org.netbeans.lib.awtextra.AbsoluteConstraints(1420, 990, 150, 40));

        lb40.setBackground(new java.awt.Color(153, 0, 0));
        lb40.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        lb40.setForeground(new java.awt.Color(255, 255, 255));
        lb40.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb40.setText("Cancel");
        lb40.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb40.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb40.setOpaque(true);
        lb40.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb40MouseClicked(evt);
            }
        });
        Payroll.add(lb40, new org.netbeans.lib.awtextra.AbsoluteConstraints(1220, 990, 150, 40));

        pqth.setBackground(new java.awt.Color(255, 255, 255));
        pqth.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/home.png"))); // NOI18N
        pqth.add(lb41, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 30, 30));

        lb42.setForeground(new java.awt.Color(153, 153, 153));
        lb42.setText("DashBoard");
        lb42.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb42.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb42MouseClicked(evt);
            }
        });
        pqth.add(lb42, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 16, -1, 20));

        lb43.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/next.png"))); // NOI18N
        pqth.add(lb43, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 0, 20, 50));

        lb44.setForeground(new java.awt.Color(153, 153, 153));
        lb44.setText("Payroll");
        lb44.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb44.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb44MouseClicked(evt);
            }
        });
        pqth.add(lb44, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 16, 70, 20));

        Payroll.add(pqth, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 830, 50));

        lb45.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 30)); // NOI18N
        lb45.setForeground(new java.awt.Color(153, 153, 153));
        lb45.setText("Payroll");
        Payroll.add(lb45, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 150, 60));

        lb46.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lb46.setForeground(new java.awt.Color(204, 204, 204));
        lb46.setText("List");
        Payroll.add(lb46, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 30, 30, 30));

        sl.setEditable(false);
        sl.setBackground(new java.awt.Color(255, 255, 255));
        sl.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        sl.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        sl.setFocusable(false);
        Payroll.add(sl, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 850, 230, 30));

        lb47.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb47.setText("Free Leaves");
        lb47.setFocusable(false);
        Payroll.add(lb47, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 900, -1, -1));

        lb48.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        lb48.setText("E.P.F 12%");
        lb48.setFocusable(false);
        Payroll.add(lb48, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 730, -1, -1));

        epf12.setEditable(false);
        epf12.setBackground(new java.awt.Color(255, 255, 255));
        epf12.setFont(new java.awt.Font("Times New Roman", 0, 17)); // NOI18N
        epf12.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        epf12.setFocusable(false);
        Payroll.add(epf12, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 730, 230, 30));

        main.add(Payroll, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 30, 1590, 1050));

        lb11.setBackground(new java.awt.Color(242, 241, 239));
        lb11.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb11.setForeground(new java.awt.Color(255, 255, 255));
        lb11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lb11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/payroll.png"))); // NOI18N
        lb11.setText("  Payroll Review");
        lb11.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb11.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lb11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb11MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb11MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb11MouseExited(evt);
            }
        });
        main.add(lb11, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 770, 270, 70));

        lb4.setBackground(new java.awt.Color(242, 241, 239));
        lb4.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb4.setForeground(new java.awt.Color(255, 255, 255));
        lb4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/reports.png"))); // NOI18N
        lb4.setText("  Reports");
        lb4.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb4MouseExited(evt);
            }
        });
        main.add(lb4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 860, 230, 70));

        lb10.setBackground(new java.awt.Color(242, 241, 239));
        lb10.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb10.setForeground(new java.awt.Color(0, 204, 0));
        lb10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/payroll.png"))); // NOI18N
        lb10.setText("  Payroll");
        lb10.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb10MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb10MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb10MouseExited(evt);
            }
        });
        main.add(lb10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 680, 210, 70));
        main.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 940, 290, 10));

        lblDate.setFont(new java.awt.Font("DigifaceWide", 0, 18)); // NOI18N
        lblDate.setForeground(new java.awt.Color(255, 255, 255));
        main.add(lblDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 960, 220, 40));

        lblTime.setFont(new java.awt.Font("DigifaceWide", 0, 18)); // NOI18N
        lblTime.setForeground(new java.awt.Color(255, 255, 255));
        main.add(lblTime, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 1010, 220, 40));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/employee.jpg"))); // NOI18N
        main.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1920, 1080));

        getContentPane().add(main, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lb5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseClicked
        DashBoard d1 = new DashBoard();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb5MouseClicked

    private void lb5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseEntered
        lb5.setForeground(Color.RED);
    }//GEN-LAST:event_lb5MouseEntered

    private void lb5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseExited
        lb5.setForeground(Color.WHITE);
    }//GEN-LAST:event_lb5MouseExited

    private void lb1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseClicked
        Administation d1 = new Administation();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb1MouseClicked

    private void lb1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseEntered
        lb1.setForeground(Color.RED);
    }//GEN-LAST:event_lb1MouseEntered

    private void lb1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseExited
        lb1.setForeground(Color.white);
    }//GEN-LAST:event_lb1MouseExited

    private void lb6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseClicked
        EmployeeReview d1 = new EmployeeReview();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb6MouseClicked

    private void lb6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseEntered
        lb6.setForeground(Color.RED);
    }//GEN-LAST:event_lb6MouseEntered

    private void lb6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseExited
        lb6.setForeground(Color.white);
    }//GEN-LAST:event_lb6MouseExited

    private void lb7MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseEntered
        lb7.setForeground(Color.RED);
    }//GEN-LAST:event_lb7MouseEntered

    private void lb7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseExited
        lb7.setForeground(Color.white);
    }//GEN-LAST:event_lb7MouseExited

    private void lb8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseClicked
        placeLoans d1 = new placeLoans();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();

    }//GEN-LAST:event_lb8MouseClicked

    private void lb8MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseEntered
        lb8.setForeground(Color.RED);
    }//GEN-LAST:event_lb8MouseEntered

    private void lb8MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseExited
        lb8.setForeground(Color.white);
    }//GEN-LAST:event_lb8MouseExited

    private void lb9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseClicked
        PlaceAdvance d1 = new PlaceAdvance();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb9MouseClicked

    private void lb9MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseEntered
        lb9.setForeground(Color.RED);
    }//GEN-LAST:event_lb9MouseEntered

    private void lb9MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseExited
        lb9.setForeground(Color.white);
    }//GEN-LAST:event_lb9MouseExited

    private void lb42MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb42MouseClicked
        DashBoard d1 = new DashBoard();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb42MouseClicked

    private void lb44MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb44MouseClicked

    }//GEN-LAST:event_lb44MouseClicked

    private void lb12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb12MouseClicked
        int result = JOptionPane.showConfirmDialog(this, "Do You Want To Exit?", "Exit:", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            this.dispose();
        } else if (result == JOptionPane.NO_OPTION) {
            this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        }
    }//GEN-LAST:event_lb12MouseClicked

    private void lb12MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb12MouseEntered
        ImageIcon icon = new ImageIcon("src/lk/ijse/payroll/asset/closerathu.png");
        lb12.setIcon(icon);
    }//GEN-LAST:event_lb12MouseEntered

    private void lb12MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb12MouseExited
        ImageIcon icon = new ImageIcon("src/lk/ijse/payroll/asset/closealu.png");
        lb12.setIcon(icon);
    }//GEN-LAST:event_lb12MouseExited

    private void normalOtAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_normalOtAmountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_normalOtAmountActionPerformed

    private void employeeIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeIdActionPerformed
        String date1 = ((JTextField) payrollDate.getDateEditor().getUiComponent()).getText();
         System.out.println(date1);
        if (employeeId.getSelectedIndex() != 0) {

            try {
                ArrayList<String> empid = ctrl.getEmployeeIds(date1);
               
                if (empid != null) {
                    System.out.println(empid);
                    for (String s : empid) {
                        System.out.println(s+"as");
                        if (s.equals(employeeId.getSelectedItem().toString())) {
                            clear();
                            JOptionPane.showMessageDialog(this, "Cannot Launch Payroll For This Employee");

                        } else {
                            
                            if (employeeId.getSelectedIndex() != 0) {
                                try {
                                    String id = (String) employeeId.getSelectedItem();
                                    String date = ((JTextField) payrollDate.getDateEditor().getUiComponent()).getText();
                                    EmployeeDTO e = ctrl.searchemp(id);
                                   
                                            
                                    QueryDTO b = ctrl.searchBankDetails(id);

                                    if (e != null) {
                                        String d = e.getEmpName();
                                        String bs = ctrl.searchBasicSalary(id);
                                        String ab = 21 - e.getTot_leaves() + "";

                                        employeeName.setSelectedItem(d);
                                        bank.setText(b.getBankName());
                                        accNo.setText(b.getAcc_no());
                                        basicSalary.setText(bs);
                                        loadAllattdetails();
                                        freeLeaves.setText("" + e.getTot_leaves());
                                        noOfLeaves.setText(ab);

                                        loadAdvanceDetails();

                                        try {
                                            QueryDTO loan = ctrl.seachLoanDetails(id, date);
                                            loanName.setText(loan.getLoanName());
                                            loanAmount.setText(loan.getInstallment().toString());
                                        } catch (NullPointerException n) {
                                            loanName.setText("No loans yet");
                                            loanAmount.setText("No loans yet");
                                        }
                                        PayrollCalculation();

                                    }

                                } catch (Exception ex) {

                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
                clear();
            }

        }
    }//GEN-LAST:event_employeeIdActionPerformed

    private void employeeNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeNameActionPerformed
        String date1 = ((JTextField) payrollDate.getDateEditor().getUiComponent()).getText();
        if (employeeName.getSelectedIndex() != 0) {
            try {
                ArrayList<String> empid = ctrl.getEmployeeIds(date1);
                if (empid != null) {
                    String name1 = employeeName.getSelectedItem().toString();
                    EmployeeDTO ee = ctrl.searchEmpId(name1);
                    //System.out.println("sf");
                    for (String s : empid) {
                        //System.out.println(s);
                        if (s.equals(ee.getEmpId())) {

                            JOptionPane.showMessageDialog(this, "Cannot Launch Payroll For This Employee");
                            clear();
                        } else {
                            //if (employeeId.getSelectedIndex() != 0) {
                            try {
                                String name = employeeName.getSelectedItem().toString();
                                EmployeeDTO e = ctrl.searchEmpId(name);
                                String date = ((JTextField) payrollDate.getDateEditor().getUiComponent()).getText();
                                if (e != null) {

                                    String bs = ctrl.searchBasicSalary(e.getEmpId());
                                    String empid1 = e.getEmpId();
                                    QueryDTO b = ctrl.searchBankDetails(e.getEmpId());

                                    employeeId.setSelectedItem(empid1);
                                    bank.setText(b.getBankName());
                                    accNo.setText(b.getAcc_no());
                                    basicSalary.setText(bs);
                                    loadAllattdetails();

                                    loadAdvanceDetails();

                                    try {
                                        QueryDTO loan = ctrl.seachLoanDetails(empid1, date);
                                        loanName.setText(loan.getLoanName());
                                        loanAmount.setText(loan.getInstallment().toString());
                                    } catch (NullPointerException n) {
                                        loanName.setText("No loans yet");
                                        loanAmount.setText("No loans yet");
                                    }

                                    PayrollCalculation();

                                }
                            } catch (Exception ex) {

                            }
                            //}
                        }
                    }

                }
            } catch (Exception ex) {
                Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_employeeNameActionPerformed

    private void lb7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseClicked
        Attendance d1 = new Attendance();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb7MouseClicked

    private void lb39MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb39MouseClicked
        String date1 = ((JTextField) payrollDate.getDateEditor().getUiComponent()).getText();
        if (employeeId.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(this, "Please Select Employee");
            employeeId.requestFocus();
        } else {
            try {
                String id = employeeId.getSelectedItem().toString();
                String date = ((JTextField) payrollDate.getDateEditor().getUiComponent()).getText();

                EmployeeDTO e = ctrl.searchemp(id);
                String designation = ctrl.searchDes(e.getDeId());
                String empbank = this.bank.getText();
                String accno = this.accNo.getText();
                String bs = basicSalary.getText();
                String wd = workedDays.getText();
                String not = normalOtAmount.getText();
                String dot = doubleOtAmount.getText();
                String totlevs = noOfLeaves.getText();
                String shl = sl.getText();
                String ep8 = epf8.getText();
                String ep12 = epf12.getText();
                String et3 = etf3.getText();
                String loan = "0.00";
                String adv = "0.00";
                String bonus = "0.00";

                if (!"No Bonus Available".equals(bonusAmount.getText())) {
                    bonus = bonusAmount.getText();

                }

                if (!"No Advances yet".equals(advanceAmount.getText())) {
                    adv = advanceAmount.getText();
                }

                if (!"No loans yet".equals(loanAmount.getText())) {
                    loan = loanAmount.getText();
                }
                String gross = grossSalary.getText();
                String totDed = totalDeduction.getText();
                String Net = netSalary.getText();

                boolean added = ctrl.savePayroll(0, id, date, designation, empbank, accno, bs, wd, not, dot, totlevs, shl, ep8, ep12, et3, loan, adv, bonus, gross, totDed, Net);
                if (added) {
                    JOptionPane.showMessageDialog(this, "Payroll Process Successed");
                } else {
                    JOptionPane.showMessageDialog(this, "Payroll process UNsuccessed");
                }
                clear();
            } catch (Exception ex) {
                Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }//GEN-LAST:event_lb39MouseClicked

    private void lb40MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb40MouseClicked
        clear();
    }//GEN-LAST:event_lb40MouseClicked

    private void lb11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb11MouseClicked
        PayrollReview d1 = new PayrollReview();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb11MouseClicked

    private void lb11MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb11MouseEntered
        lb11.setForeground(Color.red);
    }//GEN-LAST:event_lb11MouseEntered

    private void lb11MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb11MouseExited
        lb11.setForeground(Color.white);
    }//GEN-LAST:event_lb11MouseExited

    private void lb4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseClicked
        Reports d1 = new Reports();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb4MouseClicked

    private void lb4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseEntered
        lb4.setForeground(Color.RED);
    }//GEN-LAST:event_lb4MouseEntered

    private void lb4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseExited
        lb4.setForeground(Color.white);
    }//GEN-LAST:event_lb4MouseExited

    private void lb10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseClicked
        Payroll d1 = new Payroll();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb10MouseClicked

    private void lb10MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseEntered
        lb10.setForeground(Color.RED);
    }//GEN-LAST:event_lb10MouseEntered

    private void lb10MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseExited
        lb10.setForeground(new Color(0, 204, 0));
    }//GEN-LAST:event_lb10MouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Payroll.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Payroll.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Payroll.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Payroll.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Payroll().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Payroll;
    private javax.swing.JTextField accNo;
    private javax.swing.JTextField advanceAmount;
    private javax.swing.JTextField advanceName;
    private javax.swing.JTable attendanceTable;
    private javax.swing.JTextField bank;
    private javax.swing.JTextField basicSalary;
    private javax.swing.JTextField bonusAmount;
    private javax.swing.JTextField bonusName;
    private javax.swing.JTextField doubleOtAmount;
    private javax.swing.JComboBox<String> employeeId;
    private javax.swing.JComboBox<String> employeeName;
    private javax.swing.JTextField epf12;
    private javax.swing.JTextField epf8;
    private javax.swing.JTextField etf3;
    private javax.swing.JTextField freeLeaves;
    private javax.swing.JTextField grossSalary;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JLabel lb1;
    private javax.swing.JLabel lb10;
    private javax.swing.JLabel lb11;
    private javax.swing.JLabel lb12;
    private javax.swing.JLabel lb13;
    private javax.swing.JLabel lb14;
    private javax.swing.JLabel lb15;
    private javax.swing.JLabel lb16;
    private javax.swing.JLabel lb17;
    private javax.swing.JLabel lb18;
    private javax.swing.JLabel lb19;
    private javax.swing.JLabel lb20;
    private javax.swing.JLabel lb21;
    private javax.swing.JLabel lb22;
    private javax.swing.JLabel lb23;
    private javax.swing.JLabel lb24;
    private javax.swing.JLabel lb25;
    private javax.swing.JLabel lb26;
    private javax.swing.JLabel lb27;
    private javax.swing.JLabel lb28;
    private javax.swing.JLabel lb29;
    private javax.swing.JLabel lb30;
    private javax.swing.JLabel lb32;
    private javax.swing.JLabel lb33;
    private javax.swing.JLabel lb34;
    private javax.swing.JLabel lb35;
    private javax.swing.JLabel lb36;
    private javax.swing.JLabel lb37;
    private javax.swing.JLabel lb38;
    private javax.swing.JLabel lb39;
    private javax.swing.JLabel lb4;
    private javax.swing.JLabel lb40;
    private javax.swing.JLabel lb41;
    private javax.swing.JLabel lb42;
    private javax.swing.JLabel lb43;
    private javax.swing.JLabel lb44;
    private javax.swing.JLabel lb45;
    private javax.swing.JLabel lb46;
    private javax.swing.JLabel lb47;
    private javax.swing.JLabel lb48;
    private javax.swing.JLabel lb5;
    private javax.swing.JLabel lb6;
    private javax.swing.JLabel lb7;
    private javax.swing.JLabel lb8;
    private javax.swing.JLabel lb9;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblTime;
    private javax.swing.JTextField loanAmount;
    private javax.swing.JTextField loanName;
    private javax.swing.JPanel main;
    private javax.swing.JLabel menu;
    private javax.swing.JTextField netSalary;
    private javax.swing.JTextField noOfLeaves;
    private javax.swing.JTextField normalOtAmount;
    private com.toedter.calendar.JDateChooser payrollDate;
    private javax.swing.JPanel pqth;
    private javax.swing.JTextField sl;
    private javax.swing.JSeparator sp1;
    private javax.swing.JTextField totalDeduction;
    private javax.swing.JTextField workedDays;
    // End of variables declaration//GEN-END:variables

    private void transpareent() {
        Payroll.setBackground(new Color(255, 255, 255, 0));
    }

    private void setTime() {
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("hh-mm-ss a");
                lblTime.setText(sdf.format(date));
            }
        }).start();
    }

    private void setDate() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        lblDate.setText(sdf.format(date));
    }

    public void PayrollCalculation() {
        String bs = basicSalary.getText();
        String not = normalOtAmount.getText();
        String dot = doubleOtAmount.getText();
        String ep8 = this.epf8.getText();
        String nol = noOfLeaves.getText();
        String shl = sl.getText();
        String bonus = "0.00";
        String adv = "0.00";
        String loan = "0.00";

        if (!"No Bonus Available".equals(bonusAmount.getText())) {
            bonus = bonusAmount.getText();

        }

        if (!"No Advances yet".equals(advanceAmount.getText())) {
            adv = advanceAmount.getText();
        }

        if (!"No loans yet".equals(loanAmount.getText())) {
            loan = loanAmount.getText();
        }

        PayrollDTO s;
        try {
            s = ctrl.calculateSalary(bs, not, dot, ep8, nol, shl, bonus, adv, loan);
            grossSalary.setText("" + s.getGross_Salary());
            totalDeduction.setText(s.getTotal_Deduction() + "");
            netSalary.setText(s.getSalary() + "");
        } catch (Exception ex) {
            Logger.getLogger(Payroll.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void clear() {
        employeeId.setSelectedIndex(0);
        employeeName.setSelectedIndex(0);
        bank.setText("");
        accNo.setText("");
        basicSalary.setText("");
        bonusAmount.setText("");
        bonusName.setText("");
        advanceAmount.setText("");
        advanceName.setText("");
        sl.setText("");
        totalDeduction.setText("");
        freeLeaves.setText("");
        netSalary.setText("");
        noOfLeaves.setText("");
        normalOtAmount.setText("");
        doubleOtAmount.setText("");
        workedDays.setText("");
        loanAmount.setText("");
        loanName.setText("");
        epf12.setText("");
        epf8.setText("");
        etf3.setText("");
        grossSalary.setText("");
        DefaultTableModel dtm = (DefaultTableModel) attendanceTable.getModel();
        dtm.setRowCount(0);
    }

}
