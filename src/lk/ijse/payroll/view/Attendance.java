/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import lk.ijse.payroll.controller.AttendanceController;
import lk.ijse.payroll.dto.AttendanceDTO;
import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.dto.LeavesDTO;
import lk.ijse.payroll.dto.shortLeaveDTO;
import lk.ijse.payroll.view.util.ComboBoxRender;

/**
 *
 * @author udara
 */
public class Attendance extends javax.swing.JFrame {

    private AttendanceController ctrl;

    /**
     * Creates new form Attendance
     */
    public Attendance() {
        initComponents();
        setTime();
        ImageIcon img=new ImageIcon("src/lk/ijse/payroll/asset/payloku.png");
        this.setIconImage(img.getImage());
        dateIn.setDateFormatString("yyyy:MM:dd");
        dateOut.setDateFormatString("yyyy:MM:dd");
        date2.setDateFormatString("yyyy:MM:dd");
        date.setDateFormatString("yyyy:MM:dd");
        setDate(new Date());
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        ctrl = new AttendanceController();
        setCellsAlignment(attendanceTable);
        setCellsAlignment(leaveTable);
        setCellsAlignment(leaveTable2);
        //attendanceId.requestFocus();
        loadItem();
        loadAllAttendance();
        LoadAllLeaves();
        loadAllShortLeaves();
        attendanceId.setText("Att01");
        employeeId.requestFocus();
        lb7.setForeground(new Color(0,204,0));
        date.setDate(new Date());
        date2.setDate(new Date());
        
    }

    public void loadAllAttendance() {
        try {
            ArrayList<AttendanceDTO> all = ctrl.getAllAttendance();
            DefaultTableModel dtm = (DefaultTableModel) attendanceTable.getModel();
            dtm.setRowCount(0);

            for (AttendanceDTO a : all) {
                System.out.println(a.getAttId());
                EmployeeDTO e = ctrl.searchempDetailsByName(a.getEmpId());
                String name = e.getEmpName();
                Object Rowdata[] = {a.getAttId(), name, a.getDay(), a.getDateIn(), a.getTimeIn(), a.getDateOut(), a.getTimeOut(), a.getWorkedHourse()};
                dtm.addRow(Rowdata);

            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void loadAllShortLeaves() {
        DefaultTableModel dtm = (DefaultTableModel) leaveTable2.getModel();
        if (employeeId7.getSelectedIndex() != -1) {
            try {
                String Id = employeeId7.getSelectedItem().toString();

                ArrayList<shortLeaveDTO> all = ctrl.getAllShortLeaves();

                dtm.setRowCount(0);

                for (shortLeaveDTO a : all) {

                    EmployeeDTO e = ctrl.searchempDetailsByName(a.getEmpId());
                    String name = e.getEmpName();
                    Object Rowdata[] = {name, a.getDate(), a.getDuration()};
                    dtm.addRow(Rowdata);

                }

            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void LoadAllLeaves() {
        try {
            ArrayList<LeavesDTO> all = ctrl.getAllLeaves();
            DefaultTableModel dtm = (DefaultTableModel) leaveTable.getModel();
            dtm.setRowCount(0);

            for (LeavesDTO a : all) {

                EmployeeDTO e = ctrl.searchempDetailsByName(a.getEmpId());
                String name = e.getEmpName();
                Object Rowdata[] = {name, a.getDate(), a.getFree_Leaves()};
                dtm.addRow(Rowdata);

            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void loadItem() {

        try {
            ArrayList<String> emp = ctrl.LoaDAllemployeeId();
            for (String e : emp) {

                //employeeId3.addItem(e);
                employeeId.addItem(e);
                employeeId2.addItem(e);
                employeeId3.addItem(e);
                employeeId4.addItem(e);
                employeeId7.addItem(e);
                employeeId8.addItem(e);

            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            ArrayList<String> emp = ctrl.LoadallemployeeName();
            for (String e : emp) {

                employeeName.addItem(e);
                EmployeeName.addItem(e);
                employeeName2.addItem(e);
                employeeName3.addItem(e);
                employeeName6.addItem(e);
                employeeName7.addItem(e);
            }
        } catch (Exception ex) {
            Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
        }

        employeeId.setRenderer(new ComboBoxRender("Select Employee"));
        employeeId.setSelectedIndex(-1);
        employeeId2.setRenderer(new ComboBoxRender("Select Employee"));
        employeeId2.setSelectedIndex(-1);
        employeeId3.setRenderer(new ComboBoxRender("Select Employee"));
        employeeId3.setSelectedIndex(-1);
        employeeId4.setRenderer(new ComboBoxRender("Select Employee"));
        employeeId4.setSelectedIndex(-1);
        employeeId7.setRenderer(new ComboBoxRender("Select Employee"));
        employeeId7.setSelectedIndex(-1);
        employeeId8.setRenderer(new ComboBoxRender("Select Employee"));
        employeeId8.setSelectedIndex(-1);

        EmployeeName.setRenderer(new ComboBoxRender("Select Employee"));
        EmployeeName.setSelectedIndex(-1);
        employeeName.setRenderer(new ComboBoxRender("Select Employee"));
        employeeName.setSelectedIndex(-1);
        employeeName2.setRenderer(new ComboBoxRender("Select Employee"));
        employeeName2.setSelectedIndex(-1);
        employeeName3.setRenderer(new ComboBoxRender("Select Employee"));
        employeeName3.setSelectedIndex(-1);
        employeeName6.setRenderer(new ComboBoxRender("Select Employee"));
        employeeName6.setSelectedIndex(-1);
        employeeName7.setRenderer(new ComboBoxRender("Select Employee"));
        employeeName7.setSelectedIndex(-1);

        dateIn.setDate(new Date());
        date.setDate(new Date());
        date2.setDate(new Date());
        timeIn.setText("Time In");
        out.setText("");
        in.setText("");
        dateOut.setDate(new Date());
        timeOut.setText("Time Out");
        year.setYear(2018);
        month.setMonth(5);
        employeeDesignation.setText("Employee Designation");
        employeeDesignation5.setText("Employee Designation");
        employeeDesignation3.setText("");
        totalAbsants.setText("");
        freeLeaves.setText("");
    }

    public static void setCellsAlignment(JTable table) {
        ((DefaultTableCellRenderer) table.getTableHeader().getDefaultRenderer()).setHorizontalAlignment(JLabel.CENTER);
        table.getTableHeader().setFont(new Font("Tahome", Font.BOLD, 15));
        table.setFont(new Font("Tahome", 1, 13));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        main = new javax.swing.JPanel();
        lb20 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        lb3 = new javax.swing.JLabel();
        lb1 = new javax.swing.JLabel();
        lb5 = new javax.swing.JLabel();
        lb7 = new javax.swing.JLabel();
        lb8 = new javax.swing.JLabel();
        lb9 = new javax.swing.JLabel();
        lb21 = new javax.swing.JLabel();
        lb22 = new javax.swing.JLabel();
        path = new javax.swing.JPanel();
        lb26 = new javax.swing.JLabel();
        lb27 = new javax.swing.JLabel();
        lb28 = new javax.swing.JLabel();
        lb29 = new javax.swing.JLabel();
        lb30 = new javax.swing.JLabel();
        lb31 = new javax.swing.JLabel();
        card = new javax.swing.JPanel();
        MarkAttendance = new javax.swing.JPanel();
        p1 = new javax.swing.JPanel();
        jSeparator4 = new javax.swing.JSeparator();
        lb32 = new javax.swing.JLabel();
        timeOut = new javax.swing.JTextField();
        employeeId = new javax.swing.JComboBox<>();
        EmployeeName = new javax.swing.JComboBox<>();
        employeeDesignation = new javax.swing.JTextField();
        lb33 = new javax.swing.JLabel();
        cancel = new javax.swing.JLabel();
        save = new javax.swing.JLabel();
        dateOut = new com.toedter.calendar.JDateChooser();
        dateIn = new com.toedter.calendar.JDateChooser();
        attendanceId = new javax.swing.JTextField();
        timeIn = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        status = new javax.swing.JLabel();
        ViewAttendance = new javax.swing.JPanel();
        p2 = new javax.swing.JPanel();
        fil = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        lb34 = new javax.swing.JLabel();
        lb35 = new javax.swing.JLabel();
        lb36 = new javax.swing.JLabel();
        employeeId2 = new javax.swing.JComboBox<>();
        lb37 = new javax.swing.JLabel();
        applyFilter = new javax.swing.JLabel();
        employeeName = new javax.swing.JComboBox<>();
        year = new com.toedter.calendar.JYearChooser();
        lb38 = new javax.swing.JLabel();
        month = new com.toedter.calendar.JMonthChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        attendanceTable = new javax.swing.JTable();
        removeAttendace = new javax.swing.JLabel();
        re = new javax.swing.JLabel();
        MakeShortLeave = new javax.swing.JPanel();
        p3 = new javax.swing.JPanel();
        lb40 = new javax.swing.JLabel();
        jSeparator10 = new javax.swing.JSeparator();
        lb41 = new javax.swing.JLabel();
        employeeId7 = new javax.swing.JComboBox<>();
        employeeName6 = new javax.swing.JComboBox<>();
        employeeDesignation5 = new javax.swing.JTextField();
        lb42 = new javax.swing.JLabel();
        lb43 = new javax.swing.JLabel();
        in = new javax.swing.JTextField();
        lb44 = new javax.swing.JLabel();
        out = new javax.swing.JTextField();
        lb45 = new javax.swing.JLabel();
        date2 = new com.toedter.calendar.JDateChooser();
        jSeparator11 = new javax.swing.JSeparator();
        lb46 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        leaveTable2 = new javax.swing.JTable();
        cancel2 = new javax.swing.JLabel();
        save2 = new javax.swing.JLabel();
        p4 = new javax.swing.JPanel();
        p5 = new javax.swing.JPanel();
        lb48 = new javax.swing.JLabel();
        applyFilter3 = new javax.swing.JLabel();
        employeeId8 = new javax.swing.JComboBox<>();
        employeeName7 = new javax.swing.JComboBox<>();
        lb47 = new javax.swing.JLabel();
        MakeLeave = new javax.swing.JPanel();
        p6 = new javax.swing.JPanel();
        lb39 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        lb50 = new javax.swing.JLabel();
        employeeId3 = new javax.swing.JComboBox<>();
        employeeName2 = new javax.swing.JComboBox<>();
        employeeDesignation3 = new javax.swing.JTextField();
        lb51 = new javax.swing.JLabel();
        lb52 = new javax.swing.JLabel();
        totalAbsants = new javax.swing.JTextField();
        lb53 = new javax.swing.JLabel();
        freeLeaves = new javax.swing.JTextField();
        lb54 = new javax.swing.JLabel();
        date = new com.toedter.calendar.JDateChooser();
        jSeparator7 = new javax.swing.JSeparator();
        lb55 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        leaveTable = new javax.swing.JTable();
        lb56 = new javax.swing.JLabel();
        save3 = new javax.swing.JLabel();
        p7 = new javax.swing.JPanel();
        p8 = new javax.swing.JPanel();
        fil2 = new javax.swing.JLabel();
        applyFilter1 = new javax.swing.JLabel();
        employeeId4 = new javax.swing.JComboBox<>();
        employeeName3 = new javax.swing.JComboBox<>();
        re2 = new javax.swing.JLabel();
        lb6 = new javax.swing.JLabel();
        lb23 = new javax.swing.JLabel();
        lb24 = new javax.swing.JLabel();
        lb25 = new javax.swing.JLabel();
        a = new javax.swing.JLabel();
        lb11 = new javax.swing.JLabel();
        lb4 = new javax.swing.JLabel();
        lb10 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        lblDate = new javax.swing.JLabel();
        lblTime = new javax.swing.JLabel();
        frame = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        main.setBackground(new java.awt.Color(255, 255, 255));
        main.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb20.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 14)); // NOI18N
        lb20.setForeground(new java.awt.Color(255, 255, 255));
        lb20.setText("Menu");
        main.add(lb20, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 60, -1));

        jSeparator1.setBackground(new java.awt.Color(204, 204, 255));
        jSeparator1.setForeground(new java.awt.Color(204, 255, 204));
        main.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 310, 10));

        lb3.setBackground(new java.awt.Color(242, 241, 239));
        lb3.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb3.setForeground(new java.awt.Color(255, 255, 255));
        lb3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/home2.png"))); // NOI18N
        lb3.setText(" DashBoard");
        lb3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb3.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lb3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb3MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb3MouseExited(evt);
            }
        });
        main.add(lb3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 240, 70));

        lb1.setBackground(new java.awt.Color(242, 241, 239));
        lb1.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb1.setForeground(new java.awt.Color(255, 255, 255));
        lb1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/admin1.png"))); // NOI18N
        lb1.setText(" Administration");
        lb1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb1MouseExited(evt);
            }
        });
        main.add(lb1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 230, 310, 70));

        lb5.setBackground(new java.awt.Color(242, 241, 239));
        lb5.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb5.setForeground(new java.awt.Color(255, 255, 255));
        lb5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/employee.png"))); // NOI18N
        lb5.setText("  Employee");
        lb5.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb5MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb5MouseExited(evt);
            }
        });
        main.add(lb5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 320, 240, 70));

        lb7.setBackground(new java.awt.Color(242, 241, 239));
        lb7.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb7.setForeground(new java.awt.Color(0, 204, 0));
        lb7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/attendance.png"))); // NOI18N
        lb7.setText("  Attendance");
        lb7.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb7MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb7MouseExited(evt);
            }
        });
        main.add(lb7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 410, 260, 70));

        lb8.setBackground(new java.awt.Color(242, 241, 239));
        lb8.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb8.setForeground(new java.awt.Color(255, 255, 255));
        lb8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/loan.png"))); // NOI18N
        lb8.setText("  Loan");
        lb8.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb8MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb8MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb8MouseExited(evt);
            }
        });
        main.add(lb8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 500, 190, 70));

        lb9.setBackground(new java.awt.Color(242, 241, 239));
        lb9.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb9.setForeground(new java.awt.Color(255, 255, 255));
        lb9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/advance.png"))); // NOI18N
        lb9.setText("  Advances");
        lb9.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb9MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb9MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb9MouseExited(evt);
            }
        });
        main.add(lb9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 590, 240, 70));

        lb21.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 30)); // NOI18N
        lb21.setForeground(new java.awt.Color(153, 153, 153));
        lb21.setText("Attendance");
        main.add(lb21, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 40, 190, 60));

        lb22.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lb22.setForeground(new java.awt.Color(204, 204, 204));
        lb22.setText("List");
        main.add(lb22, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 60, 30, 30));

        path.setBackground(new java.awt.Color(255, 255, 255));
        path.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/home.png"))); // NOI18N
        path.add(lb26, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 30, 30));

        lb27.setForeground(new java.awt.Color(153, 153, 153));
        lb27.setText("DashBoard");
        lb27.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb27.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb27MouseClicked(evt);
            }
        });
        path.add(lb27, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 16, -1, 20));

        lb28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/next.png"))); // NOI18N
        path.add(lb28, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 0, 20, 50));

        lb29.setText("Mark Attendance");
        lb29.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        path.add(lb29, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 10, -1, 30));

        lb30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/next.png"))); // NOI18N
        path.add(lb30, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 0, 20, 50));

        lb31.setForeground(new java.awt.Color(153, 153, 153));
        lb31.setText("Attendance");
        lb31.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb31.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb31MouseClicked(evt);
            }
        });
        path.add(lb31, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 16, 70, 20));

        main.add(path, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 90, 830, 50));

        card.setBackground(new java.awt.Color(255, 255, 255));
        card.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        card.setLayout(new java.awt.CardLayout());

        MarkAttendance.setBackground(new java.awt.Color(255, 255, 255));
        MarkAttendance.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MarkAttendance.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p1.setBackground(new java.awt.Color(0, 0, 102));
        p1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        MarkAttendance.add(p1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1550, 40));
        MarkAttendance.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 92, 1420, 10));

        lb32.setText("Employee Details");
        MarkAttendance.add(lb32, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 70, -1, -1));

        timeOut.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        timeOut.setText("Time Out");
        timeOut.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        timeOut.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                timeOutFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                timeOutFocusLost(evt);
            }
        });
        timeOut.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                timeOutKeyPressed(evt);
            }
        });
        MarkAttendance.add(timeOut, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 480, 240, 50));

        employeeId.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeId.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeIdActionPerformed(evt);
            }
        });
        employeeId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeIdKeyPressed(evt);
            }
        });
        MarkAttendance.add(employeeId, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 240, 240, 40));

        EmployeeName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        EmployeeName.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        EmployeeName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EmployeeNameActionPerformed(evt);
            }
        });
        EmployeeName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                EmployeeNameKeyPressed(evt);
            }
        });
        MarkAttendance.add(EmployeeName, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 240, 370, 40));

        employeeDesignation.setEditable(false);
        employeeDesignation.setBackground(new java.awt.Color(255, 255, 255));
        employeeDesignation.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeDesignation.setText("Employee Designation");
        employeeDesignation.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        employeeDesignation.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                employeeDesignationFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                employeeDesignationFocusLost(evt);
            }
        });
        employeeDesignation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeDesignationActionPerformed(evt);
            }
        });
        MarkAttendance.add(employeeDesignation, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 240, 350, 40));

        lb33.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb33.setText("Date Out");
        MarkAttendance.add(lb33, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 460, 80, 20));

        cancel.setBackground(new java.awt.Color(153, 0, 0));
        cancel.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        cancel.setForeground(new java.awt.Color(255, 255, 255));
        cancel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cancel.setText("Cancel");
        cancel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        cancel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancel.setOpaque(true);
        cancel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelMouseClicked(evt);
            }
        });
        cancel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cancelKeyPressed(evt);
            }
        });
        MarkAttendance.add(cancel, new org.netbeans.lib.awtextra.AbsoluteConstraints(1170, 780, 150, 40));

        save.setBackground(new java.awt.Color(31, 191, 21));
        save.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        save.setForeground(new java.awt.Color(255, 255, 255));
        save.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        save.setText("Save");
        save.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        save.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        save.setOpaque(true);
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveMouseClicked(evt);
            }
        });
        save.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                saveKeyPressed(evt);
            }
        });
        MarkAttendance.add(save, new org.netbeans.lib.awtextra.AbsoluteConstraints(1350, 780, 150, 40));

        dateOut.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dateOutKeyPressed(evt);
            }
        });
        MarkAttendance.add(dateOut, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 480, 230, 50));

        dateIn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dateInKeyPressed(evt);
            }
        });
        MarkAttendance.add(dateIn, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 370, 230, 50));

        attendanceId.setEditable(false);
        attendanceId.setBackground(new java.awt.Color(255, 255, 255));
        attendanceId.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        attendanceId.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        attendanceId.setText("Attendance Id");
        attendanceId.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        attendanceId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                attendanceIdFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                attendanceIdFocusLost(evt);
            }
        });
        MarkAttendance.add(attendanceId, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 140, 240, 40));

        timeIn.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        timeIn.setText("Time In");
        timeIn.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        timeIn.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                timeInFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                timeInFocusLost(evt);
            }
        });
        timeIn.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                timeInKeyPressed(evt);
            }
        });
        MarkAttendance.add(timeIn, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 370, 240, 50));

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel28.setText("Date In");
        MarkAttendance.add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 350, 80, 20));
        MarkAttendance.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 590, 1420, 10));
        MarkAttendance.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 320, 1420, 30));

        status.setBackground(new java.awt.Color(255, 255, 255));
        status.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        status.setForeground(new java.awt.Color(153, 0, 0));
        MarkAttendance.add(status, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 800, 400, 20));

        card.add(MarkAttendance, "card2");

        ViewAttendance.setBackground(new java.awt.Color(255, 255, 255));
        ViewAttendance.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ViewAttendance.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p2.setBackground(new java.awt.Color(0, 0, 102));
        p2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        ViewAttendance.add(p2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1550, 40));

        fil.setBackground(new java.awt.Color(255, 255, 255));
        fil.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        fil.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel9.setBackground(new java.awt.Color(0, 102, 204));
        jPanel9.setForeground(new java.awt.Color(255, 255, 255));
        jPanel9.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb34.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lb34.setForeground(new java.awt.Color(255, 255, 255));
        lb34.setText("Filters");
        jPanel9.add(lb34, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 0, 70, 40));

        fil.add(jPanel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1480, 40));

        lb35.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb35.setText("Employee Name :");
        fil.add(lb35, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 60, -1, -1));

        lb36.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb36.setText("Month*");
        fil.add(lb36, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 60, -1, -1));

        employeeId2.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeId2.setForeground(new java.awt.Color(51, 51, 51));
        employeeId2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        employeeId2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeId2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeId2ActionPerformed(evt);
            }
        });
        employeeId2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeId2KeyPressed(evt);
            }
        });
        fil.add(employeeId2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 90, 230, 40));

        lb37.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb37.setText("Employee ID :");
        fil.add(lb37, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, -1, -1));

        applyFilter.setBackground(new java.awt.Color(0, 153, 0));
        applyFilter.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        applyFilter.setForeground(new java.awt.Color(255, 255, 255));
        applyFilter.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        applyFilter.setText("Apply Filters");
        applyFilter.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        applyFilter.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        applyFilter.setOpaque(true);
        applyFilter.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                applyFilterMouseClicked(evt);
            }
        });
        applyFilter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                applyFilterKeyPressed(evt);
            }
        });
        fil.add(applyFilter, new org.netbeans.lib.awtextra.AbsoluteConstraints(1230, 90, 140, 40));

        employeeName.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeName.setForeground(new java.awt.Color(51, 51, 51));
        employeeName.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        employeeName.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeNameActionPerformed(evt);
            }
        });
        employeeName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeNameKeyPressed(evt);
            }
        });
        fil.add(employeeName, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 90, 250, 40));

        year.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                yearKeyPressed(evt);
            }
        });
        fil.add(year, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 90, 150, 40));

        lb38.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb38.setText("Year*");
        fil.add(lb38, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 60, -1, -1));

        month.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                monthKeyPressed(evt);
            }
        });
        fil.add(month, new org.netbeans.lib.awtextra.AbsoluteConstraints(1050, 90, 120, 40));

        ViewAttendance.add(fil, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 90, 1380, 180));

        attendanceTable.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        attendanceTable.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        attendanceTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Attendance ID", "Employee ID", "Day", "Date IN", "Time IN", "Date OUT", "Time OUT", "Worked Hourse"
            }
        ));
        attendanceTable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        attendanceTable.setGridColor(new java.awt.Color(51, 102, 255));
        attendanceTable.setRowHeight(25);
        attendanceTable.setSelectionBackground(new java.awt.Color(102, 102, 102));
        jScrollPane1.setViewportView(attendanceTable);

        ViewAttendance.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 380, 1390, 410));

        removeAttendace.setBackground(new java.awt.Color(153, 0, 0));
        removeAttendace.setFont(new java.awt.Font("Trajan Pro", 1, 18)); // NOI18N
        removeAttendace.setForeground(new java.awt.Color(255, 255, 255));
        removeAttendace.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        removeAttendace.setText("Remove Attendance");
        removeAttendace.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        removeAttendace.setOpaque(true);
        removeAttendace.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                removeAttendaceMouseClicked(evt);
            }
        });
        ViewAttendance.add(removeAttendace, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 330, 230, 30));

        re.setBackground(new java.awt.Color(255, 255, 255));
        re.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        re.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/refresh.png"))); // NOI18N
        re.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        re.setOpaque(true);
        re.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                reMouseClicked(evt);
            }
        });
        ViewAttendance.add(re, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 330, 60, 30));

        card.add(ViewAttendance, "card3");

        MakeShortLeave.setBackground(new java.awt.Color(255, 255, 255));
        MakeShortLeave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MakeShortLeave.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p3.setBackground(new java.awt.Color(0, 0, 102));
        p3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        MakeShortLeave.add(p3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1550, 40));

        lb40.setText("Employee Details");
        MakeShortLeave.add(lb40, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 50, -1, -1));
        MakeShortLeave.add(jSeparator10, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 70, 1420, 10));

        lb41.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb41.setText("Employee Name");
        MakeShortLeave.add(lb41, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 160, -1, -1));

        employeeId7.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeId7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeId7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeId7ActionPerformed(evt);
            }
        });
        employeeId7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeId7KeyPressed(evt);
            }
        });
        MakeShortLeave.add(employeeId7, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 180, 240, 40));

        employeeName6.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeName6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeName6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeName6ActionPerformed(evt);
            }
        });
        employeeName6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeName6KeyPressed(evt);
            }
        });
        MakeShortLeave.add(employeeName6, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 180, 370, 40));

        employeeDesignation5.setEditable(false);
        employeeDesignation5.setBackground(new java.awt.Color(255, 255, 255));
        employeeDesignation5.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        MakeShortLeave.add(employeeDesignation5, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 180, 350, 40));

        lb42.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb42.setText("Employee Designation");
        MakeShortLeave.add(lb42, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 160, -1, -1));

        lb43.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb43.setText("Date");
        MakeShortLeave.add(lb43, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 80, 80, 20));

        in.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        in.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        MakeShortLeave.add(in, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 280, 260, 40));

        lb44.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb44.setText("Time Out");
        MakeShortLeave.add(lb44, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 250, 120, -1));

        out.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        out.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        out.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                outActionPerformed(evt);
            }
        });
        MakeShortLeave.add(out, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 280, 250, 40));

        lb45.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb45.setText("Time In");
        MakeShortLeave.add(lb45, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 250, 120, 20));

        date2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                date2KeyPressed(evt);
            }
        });
        MakeShortLeave.add(date2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 110, 350, 40));
        MakeShortLeave.add(jSeparator11, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 360, 1440, 10));

        lb46.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb46.setText("Employee ID");
        MakeShortLeave.add(lb46, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 160, -1, -1));

        leaveTable2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        leaveTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Employee Name", "Date", "Duration"
            }
        ));
        leaveTable2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        leaveTable2.setGridColor(new java.awt.Color(255, 255, 255));
        leaveTable2.setRowHeight(25);
        leaveTable2.setSelectionBackground(new java.awt.Color(102, 102, 102));
        jScrollPane4.setViewportView(leaveTable2);

        MakeShortLeave.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 550, 1070, 250));

        cancel2.setBackground(new java.awt.Color(153, 0, 0));
        cancel2.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        cancel2.setForeground(new java.awt.Color(255, 255, 255));
        cancel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cancel2.setText("Cancel");
        cancel2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        cancel2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancel2.setOpaque(true);
        cancel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancel2MouseClicked(evt);
            }
        });
        MakeShortLeave.add(cancel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 280, 150, 40));

        save2.setBackground(new java.awt.Color(0, 153, 0));
        save2.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        save2.setForeground(new java.awt.Color(255, 255, 255));
        save2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        save2.setText("Save");
        save2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        save2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        save2.setOpaque(true);
        save2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                save2MouseClicked(evt);
            }
        });
        save2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                save2KeyPressed(evt);
            }
        });
        MakeShortLeave.add(save2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 280, 150, 40));

        p4.setBackground(new java.awt.Color(255, 255, 255));
        p4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        p4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p5.setBackground(new java.awt.Color(0, 102, 204));
        p5.setForeground(new java.awt.Color(255, 255, 255));
        p5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lb48.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lb48.setForeground(new java.awt.Color(255, 255, 255));
        lb48.setText("Filters");
        p5.add(lb48, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 0, 70, 30));

        p4.add(p5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1480, 30));

        applyFilter3.setBackground(new java.awt.Color(0, 153, 0));
        applyFilter3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        applyFilter3.setForeground(new java.awt.Color(255, 255, 255));
        applyFilter3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        applyFilter3.setText("Apply Filters");
        applyFilter3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        applyFilter3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        applyFilter3.setOpaque(true);
        applyFilter3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                applyFilter3MouseClicked(evt);
            }
        });
        applyFilter3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                applyFilter3KeyPressed(evt);
            }
        });
        p4.add(applyFilter3, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 40, 140, 40));

        employeeId8.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeId8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeId8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeId8ActionPerformed(evt);
            }
        });
        employeeId8.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeId8KeyPressed(evt);
            }
        });
        p4.add(employeeId8, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 40, 240, 40));

        employeeName7.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeName7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeName7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeName7ActionPerformed(evt);
            }
        });
        employeeName7.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeName7KeyPressed(evt);
            }
        });
        p4.add(employeeName7, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 40, 370, 40));

        MakeShortLeave.add(p4, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 400, 1070, 90));

        lb47.setBackground(new java.awt.Color(255, 255, 255));
        lb47.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb47.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/refresh.png"))); // NOI18N
        lb47.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb47.setOpaque(true);
        lb47.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb47MouseClicked(evt);
            }
        });
        MakeShortLeave.add(lb47, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 510, 60, 30));

        card.add(MakeShortLeave, "card4");

        MakeLeave.setBackground(new java.awt.Color(255, 255, 255));
        MakeLeave.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        MakeLeave.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p6.setBackground(new java.awt.Color(0, 0, 102));
        p6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        MakeLeave.add(p6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1550, 40));

        lb39.setText("Employee Details");
        MakeLeave.add(lb39, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 50, -1, -1));
        MakeLeave.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 70, 1420, 10));

        lb50.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb50.setText("Employee Name");
        MakeLeave.add(lb50, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 160, -1, -1));

        employeeId3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeId3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeId3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeId3ActionPerformed(evt);
            }
        });
        employeeId3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeId3KeyPressed(evt);
            }
        });
        MakeLeave.add(employeeId3, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 180, 240, 40));

        employeeName2.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeName2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeName2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeName2ActionPerformed(evt);
            }
        });
        employeeName2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeName2KeyPressed(evt);
            }
        });
        MakeLeave.add(employeeName2, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 180, 370, 40));

        employeeDesignation3.setEditable(false);
        employeeDesignation3.setBackground(new java.awt.Color(255, 255, 255));
        employeeDesignation3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        MakeLeave.add(employeeDesignation3, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 180, 350, 40));

        lb51.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb51.setText("Employee Designation");
        MakeLeave.add(lb51, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 160, -1, -1));

        lb52.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb52.setText("Date");
        MakeLeave.add(lb52, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 80, 80, 20));

        totalAbsants.setEditable(false);
        totalAbsants.setBackground(new java.awt.Color(255, 255, 255));
        totalAbsants.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        totalAbsants.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        MakeLeave.add(totalAbsants, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 280, 260, 40));

        lb53.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb53.setText("Total Absants");
        MakeLeave.add(lb53, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 250, 120, -1));

        freeLeaves.setEditable(false);
        freeLeaves.setBackground(new java.awt.Color(255, 255, 255));
        freeLeaves.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        freeLeaves.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        freeLeaves.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                freeLeavesActionPerformed(evt);
            }
        });
        MakeLeave.add(freeLeaves, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 280, 230, 40));

        lb54.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb54.setText("Free leaves");
        MakeLeave.add(lb54, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 250, 100, 20));

        date.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dateMouseClicked(evt);
            }
        });
        date.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dateKeyPressed(evt);
            }
        });
        MakeLeave.add(date, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 110, 350, 40));
        MakeLeave.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 360, 1440, 10));

        lb55.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lb55.setText("Employee ID");
        MakeLeave.add(lb55, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 160, -1, -1));

        leaveTable.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        leaveTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Employee Name", "Date", "Free Leaves"
            }
        ));
        leaveTable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        leaveTable.setGridColor(new java.awt.Color(255, 255, 255));
        leaveTable.setRowHeight(25);
        leaveTable.setSelectionBackground(new java.awt.Color(102, 102, 102));
        jScrollPane2.setViewportView(leaveTable);

        MakeLeave.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 560, 1070, 250));

        lb56.setBackground(new java.awt.Color(153, 0, 0));
        lb56.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        lb56.setForeground(new java.awt.Color(255, 255, 255));
        lb56.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb56.setText("Cancel");
        lb56.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb56.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb56.setOpaque(true);
        lb56.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb56MouseClicked(evt);
            }
        });
        MakeLeave.add(lb56, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 280, 150, 40));

        save3.setBackground(new java.awt.Color(0, 153, 0));
        save3.setFont(new java.awt.Font("Lucida Sans Typewriter", 1, 24)); // NOI18N
        save3.setForeground(new java.awt.Color(255, 255, 255));
        save3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        save3.setText("Save");
        save3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        save3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        save3.setOpaque(true);
        save3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                save3MouseClicked(evt);
            }
        });
        save3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                save3KeyPressed(evt);
            }
        });
        MakeLeave.add(save3, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 280, 150, 40));

        p7.setBackground(new java.awt.Color(255, 255, 255));
        p7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        p7.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        p8.setBackground(new java.awt.Color(0, 102, 204));
        p8.setForeground(new java.awt.Color(255, 255, 255));
        p8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        fil2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        fil2.setForeground(new java.awt.Color(255, 255, 255));
        fil2.setText("Filters");
        p8.add(fil2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 0, 70, 30));

        p7.add(p8, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1480, 30));

        applyFilter1.setBackground(new java.awt.Color(0, 153, 0));
        applyFilter1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        applyFilter1.setForeground(new java.awt.Color(255, 255, 255));
        applyFilter1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        applyFilter1.setText("Apply Filters");
        applyFilter1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        applyFilter1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        applyFilter1.setOpaque(true);
        applyFilter1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                applyFilter1MouseClicked(evt);
            }
        });
        applyFilter1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                applyFilter1KeyPressed(evt);
            }
        });
        p7.add(applyFilter1, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 50, 140, 40));

        employeeId4.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeId4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeId4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeId4ActionPerformed(evt);
            }
        });
        employeeId4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeId4KeyPressed(evt);
            }
        });
        p7.add(employeeId4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 50, 240, 40));

        employeeName3.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        employeeName3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeeName3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeName3ActionPerformed(evt);
            }
        });
        employeeName3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                employeeName3KeyPressed(evt);
            }
        });
        p7.add(employeeName3, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 50, 370, 40));

        MakeLeave.add(p7, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 390, 1070, 100));

        re2.setBackground(new java.awt.Color(255, 255, 255));
        re2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        re2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/refresh.png"))); // NOI18N
        re2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        re2.setOpaque(true);
        re2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                re2MouseClicked(evt);
            }
        });
        MakeLeave.add(re2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 520, 60, 30));

        card.add(MakeLeave, "card4");

        main.add(card, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 210, 1550, 840));

        lb6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/closesudu.png"))); // NOI18N
        lb6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb6MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb6MouseExited(evt);
            }
        });
        main.add(lb6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1870, 0, 40, 40));

        lb23.setBackground(new java.awt.Color(0, 0, 102));
        lb23.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 18)); // NOI18N
        lb23.setForeground(new java.awt.Color(255, 255, 255));
        lb23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb23.setText("Mark Attendance");
        lb23.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb23.setOpaque(true);
        lb23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb23MouseClicked(evt);
            }
        });
        main.add(lb23, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 170, 200, 40));

        lb24.setBackground(new java.awt.Color(250, 250, 250));
        lb24.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 18)); // NOI18N
        lb24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb24.setText("View Employee Attendance");
        lb24.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb24.setOpaque(true);
        lb24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb24MouseClicked(evt);
            }
        });
        main.add(lb24, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 170, 300, 40));

        lb25.setBackground(new java.awt.Color(250, 250, 250));
        lb25.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 18)); // NOI18N
        lb25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb25.setText("Make Employee Leaves");
        lb25.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb25.setOpaque(true);
        lb25.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb25MouseClicked(evt);
            }
        });
        main.add(lb25, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 170, 300, 40));

        a.setBackground(new java.awt.Color(250, 250, 250));
        a.setFont(new java.awt.Font("Lucida Sans Typewriter", 0, 18)); // NOI18N
        a.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        a.setText("Make Employee ShortLeaves");
        a.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        a.setOpaque(true);
        a.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                aMouseClicked(evt);
            }
        });
        main.add(a, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 170, 300, 40));

        lb11.setBackground(new java.awt.Color(242, 241, 239));
        lb11.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb11.setForeground(new java.awt.Color(255, 255, 255));
        lb11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lb11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/payroll.png"))); // NOI18N
        lb11.setText("  Payroll Review");
        lb11.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb11.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        lb11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb11MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb11MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb11MouseExited(evt);
            }
        });
        main.add(lb11, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 770, 270, 70));

        lb4.setBackground(new java.awt.Color(242, 241, 239));
        lb4.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb4.setForeground(new java.awt.Color(255, 255, 255));
        lb4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/reports.png"))); // NOI18N
        lb4.setText("  Reports");
        lb4.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb4MouseExited(evt);
            }
        });
        main.add(lb4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 860, 230, 70));

        lb10.setBackground(new java.awt.Color(242, 241, 239));
        lb10.setFont(new java.awt.Font("Times New Roman", 1, 25)); // NOI18N
        lb10.setForeground(new java.awt.Color(255, 255, 255));
        lb10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lb10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/payroll.png"))); // NOI18N
        lb10.setText("  Payroll");
        lb10.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        lb10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lb10MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lb10MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lb10MouseExited(evt);
            }
        });
        main.add(lb10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 680, 210, 70));
        main.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 940, 290, 10));

        lblDate.setFont(new java.awt.Font("DigifaceWide", 0, 18)); // NOI18N
        lblDate.setForeground(new java.awt.Color(255, 255, 255));
        main.add(lblDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 960, 220, 40));

        lblTime.setFont(new java.awt.Font("DigifaceWide", 0, 18)); // NOI18N
        lblTime.setForeground(new java.awt.Color(255, 255, 255));
        main.add(lblTime, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 1010, 220, 40));

        frame.setIcon(new javax.swing.ImageIcon(getClass().getResource("/lk/ijse/payroll/asset/employee.jpg"))); // NOI18N
        main.add(frame, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1920, 1080));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1920, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(main, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1080, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(main, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lb3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb3MouseClicked
        DashBoard d1 = new DashBoard();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb3MouseClicked

    private void lb3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb3MouseEntered
        lb3.setForeground(Color.RED);
    }//GEN-LAST:event_lb3MouseEntered

    private void lb3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb3MouseExited
        lb3.setForeground(Color.WHITE);
    }//GEN-LAST:event_lb3MouseExited

    private void lb1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseClicked
        Administation d1 = new Administation();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb1MouseClicked

    private void lb1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseEntered
        lb1.setForeground(Color.RED);
    }//GEN-LAST:event_lb1MouseEntered

    private void lb1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb1MouseExited
        lb1.setForeground(Color.white);
    }//GEN-LAST:event_lb1MouseExited

    private void lb5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseClicked
        EmployeeReview d1 = new EmployeeReview();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb5MouseClicked

    private void lb5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseEntered
        lb5.setForeground(Color.RED);
    }//GEN-LAST:event_lb5MouseEntered

    private void lb5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb5MouseExited
        lb5.setForeground(Color.white);
    }//GEN-LAST:event_lb5MouseExited

    private void lb7MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseEntered
        lb7.setForeground(Color.RED);
    }//GEN-LAST:event_lb7MouseEntered

    private void lb7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb7MouseExited
        lb7.setForeground(new Color(0,204,0));
    }//GEN-LAST:event_lb7MouseExited

    private void lb8MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseEntered
        lb8.setForeground(Color.RED);
    }//GEN-LAST:event_lb8MouseEntered

    private void lb8MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseExited
        lb8.setForeground(Color.white);
    }//GEN-LAST:event_lb8MouseExited

    private void lb9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseClicked
        PlaceAdvance d1 = new PlaceAdvance();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb9MouseClicked

    private void lb9MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseEntered
        lb9.setForeground(Color.RED);
    }//GEN-LAST:event_lb9MouseEntered

    private void lb9MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb9MouseExited
        lb9.setForeground(Color.white);
    }//GEN-LAST:event_lb9MouseExited

    private void lb27MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb27MouseClicked
        DashBoard d1 = new DashBoard();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb27MouseClicked

    private void lb31MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb31MouseClicked
        lb29.setText("Mark Attendance");
        card.removeAll();
        card.repaint();
        card.revalidate();
        card.add(MarkAttendance);
        card.repaint();
        card.revalidate();
        lb23.setBackground(new Color(0, 0, 102));
        lb24.setBackground(new Color(255, 255, 255));
        lb25.setBackground(new Color(255, 255, 255));
        lb24.setForeground(new Color(0, 0, 0));
        lb25.setForeground(new Color(0, 0, 0));
        lb23.setForeground(new Color(255, 255, 255));
    }//GEN-LAST:event_lb31MouseClicked

    private void lb23MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb23MouseClicked
        lb29.setText("Mark Attendance");
        card.removeAll();
        card.repaint();
        card.revalidate();
        card.add(MarkAttendance);
        card.repaint();
        card.revalidate();
        lb23.setBackground(new Color(0, 0, 102));
        lb24.setBackground(new Color(255, 255, 255));
        lb25.setBackground(new Color(255, 255, 255));
        a.setBackground(new Color(255, 255, 255));
        lb24.setForeground(new Color(0, 0, 0));
        lb25.setForeground(new Color(0, 0, 0));
        a.setForeground(new Color(0, 0, 0));
        lb23.setForeground(new Color(255, 255, 255));

    }//GEN-LAST:event_lb23MouseClicked

    private void lb24MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb24MouseClicked
        card.removeAll();
        lb29.setText("View Employee Attendance");
        card.repaint();
        card.revalidate();
        card.add(ViewAttendance);
        card.repaint();
        card.revalidate();
        lb24.setBackground(new Color(0, 0, 102));
        lb23.setBackground(new Color(255, 255, 255));
        lb25.setBackground(new Color(255, 255, 255));
        lb25.setForeground(new Color(0, 0, 0));
        lb23.setForeground(new Color(0, 0, 0));
        a.setForeground(new Color(0, 0, 0));
        a.setBackground(new Color(255, 255, 255));
        lb24.setForeground(new Color(255, 255, 255));
        employeeId2.requestFocus();
    }//GEN-LAST:event_lb24MouseClicked

    private void employeeIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeIdActionPerformed
        if (employeeId.getSelectedIndex() != -1) {
            try {
                String id = (String) employeeId.getSelectedItem();

                EmployeeDTO e = ctrl.searchempDetailsByName(id);
                if (e != null) {
                    String de = ctrl.searchDesignation(e.getDeId());
                    String d = e.getEmpName();

                    EmployeeName.setSelectedItem(d);

                    employeeDesignation.setText(de);

                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeIdActionPerformed

    private void EmployeeNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EmployeeNameActionPerformed
        if (EmployeeName.getSelectedIndex() != -1) {
            try {
                String name = EmployeeName.getSelectedItem().toString();
                // System.out.println(name);
                EmployeeDTO e = ctrl.searchEmpByName(name);
                if (e != null) {

                    String des = ctrl.searchDesignation(e.getDeId());
                    String empid = e.getEmpId();

                    employeeId.setSelectedItem(empid);
                    employeeDesignation.setText(des);

                }
            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_EmployeeNameActionPerformed

    private void lb6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseClicked
        int result = JOptionPane.showConfirmDialog(this, "Do You Want To Exit?", "Exit:", JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            this.dispose();
        } else if (result == JOptionPane.NO_OPTION) {
            this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        }
    }//GEN-LAST:event_lb6MouseClicked

    private void lb6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseEntered
        ImageIcon icon = new ImageIcon("src/lk/ijse/payroll/asset/closerathu.png");
        lb6.setIcon(icon);
    }//GEN-LAST:event_lb6MouseEntered

    private void lb6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb6MouseExited
        ImageIcon icon = new ImageIcon("src/lk/ijse/payroll/asset/closesudu.png");
        lb6.setIcon(icon);
    }//GEN-LAST:event_lb6MouseExited

    private void lb8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb8MouseClicked
        placeLoans d1 = new placeLoans();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();

    }//GEN-LAST:event_lb8MouseClicked

    private void lb25MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb25MouseClicked
        lb29.setText("Make Employee Leaves");
        date.requestFocus();
        card.removeAll();
        card.repaint();
        card.revalidate();
        card.add(MakeLeave);
        card.repaint();
        card.revalidate();
        lb25.setBackground(new Color(0, 0, 102));
        lb23.setBackground(new Color(255, 255, 255));
        a.setBackground(new Color(255, 255, 255));
        lb24.setBackground(new Color(255, 255, 255));
        lb23.setForeground(new Color(0, 0, 0));
        a.setForeground(new Color(0, 0, 0));
        lb25.setForeground(new Color(255, 255, 255));
        lb24.setForeground(new Color(0, 0, 0));
    }//GEN-LAST:event_lb25MouseClicked

    private void employeeId3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeId3ActionPerformed
        if (employeeId3.getSelectedIndex() != -1) {
            try {
                String id = (String) employeeId3.getSelectedItem();

                EmployeeDTO e = ctrl.searchempDetailsByName(id);
                if (e != null) {
                    String de = ctrl.searchDesignation(e.getDeId());
                    String d = e.getEmpName();
                    String ab = 21 - e.getTot_leaves() + "";
                    employeeName2.setSelectedItem(d);

                    employeeDesignation3.setText(de);
                    freeLeaves.setText("" + e.getTot_leaves());
                    totalAbsants.setText(ab);

                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeId3ActionPerformed

    private void employeeName2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeName2ActionPerformed
        if (employeeName2.getSelectedIndex() != -1) {
            try {
                String name = employeeName2.getSelectedItem().toString();
                // System.out.println(name);
                EmployeeDTO e = ctrl.searchEmpByName(name);
                if (e != null) {

                    String des = ctrl.searchDesignation(e.getDeId());
                    String empid = e.getEmpId();
                    String ab = 21 - e.getTot_leaves() + "";

                    employeeId3.setSelectedItem(empid);
                    employeeDesignation3.setText(des);
                    freeLeaves.setText(e.getTot_leaves() + "");
                    totalAbsants.setText(ab);

                }
            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeName2ActionPerformed

    private void freeLeavesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_freeLeavesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_freeLeavesActionPerformed

    private void attendanceIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_attendanceIdFocusLost
        if (attendanceId.getText().isEmpty()) {
            attendanceId.setText("Attendance Id");
        }
        attendanceId.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));

    }//GEN-LAST:event_attendanceIdFocusLost

    private void attendanceIdFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_attendanceIdFocusGained
        if (attendanceId.getText().equals("Attendance Id")) {
            attendanceId.setText("");
            attendanceId.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }
    }//GEN-LAST:event_attendanceIdFocusGained

    private void employeeDesignationFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_employeeDesignationFocusLost
        if (employeeDesignation.getText().isEmpty()) {
            employeeDesignation.setText("Employee Designation");
        }
        employeeDesignation.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));


    }//GEN-LAST:event_employeeDesignationFocusLost

    private void employeeDesignationFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_employeeDesignationFocusGained
        if (employeeDesignation.getText().equals("Employee Designation")) {
            employeeDesignation.setText("");
            employeeDesignation.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }
    }//GEN-LAST:event_employeeDesignationFocusGained

    private void timeInFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeInFocusLost
        if (timeIn.getText().isEmpty()) {
            timeIn.setText("Time In");
        }
        timeIn.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));


    }//GEN-LAST:event_timeInFocusLost

    private void timeInFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeInFocusGained
        if (timeIn.getText().equals("Time In")) {
            timeIn.setText("");
            timeIn.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }
    }//GEN-LAST:event_timeInFocusGained

    private void timeOutFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeOutFocusLost
        if (timeOut.getText().isEmpty()) {
            timeOut.setText("Time Out");
        }
        timeOut.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));


    }//GEN-LAST:event_timeOutFocusLost

    private void timeOutFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_timeOutFocusGained
        if (timeOut.getText().equals("Time Out")) {
            timeOut.setText("");
            timeOut.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, Color.red));
        }
    }//GEN-LAST:event_timeOutFocusGained

    private void employeeId4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeId4ActionPerformed
        if (employeeId4.getSelectedIndex() != -1) {
            try {
                String id = (String) employeeId4.getSelectedItem();

                EmployeeDTO e = ctrl.searchempDetailsByName(id);
                if (e != null) {
                    String de = ctrl.searchDesignation(e.getDeId());
                    String d = e.getEmpName();

                    employeeName3.setSelectedItem(d);

                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeId4ActionPerformed

    private void employeeName3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeName3ActionPerformed
        if (employeeName3.getSelectedIndex() != -1) {
            try {
                String name = employeeName3.getSelectedItem().toString();
                // System.out.println(name);
                EmployeeDTO e = ctrl.searchEmpByName(name);
                if (e != null) {

                    String des = ctrl.searchDesignation(e.getDeId());
                    String empid = e.getEmpId();

                    employeeId4.setSelectedItem(empid);

                }
            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeName3ActionPerformed

    private void employeeId2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeId2ActionPerformed
        if (employeeId2.getSelectedIndex() != -1) {
            try {
                String id = (String) employeeId2.getSelectedItem();

                EmployeeDTO e = ctrl.searchempDetailsByName(id);
                if (e != null) {
                    String de = ctrl.searchDesignation(e.getDeId());
                    String d = e.getEmpName();

                    employeeName.setSelectedItem(d);

                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeId2ActionPerformed

    private void employeeNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeNameActionPerformed
        if (employeeName.getSelectedIndex() != -1) {
            try {
                String name = employeeName.getSelectedItem().toString();
                // System.out.println(name);
                EmployeeDTO e = ctrl.searchEmpByName(name);
                if (e != null) {

                    String des = ctrl.searchDesignation(e.getDeId());
                    String empid = e.getEmpId();

                    employeeId2.setSelectedItem(empid);

                }
            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeNameActionPerformed

    private void employeeDesignationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeDesignationActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_employeeDesignationActionPerformed

    private void saveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseClicked
        String yr = ((JTextField) dateIn.getDateEditor().getUiComponent()).getText();
        String y = ((JTextField) dateOut.getDateEditor().getUiComponent()).getText();

        if (employeeId.getSelectedIndex() == -1) {
            status.setText("Please Select Employee!");
            employeeId.requestFocus();

        } else if (yr.equals("")) {
            status.setText("Please Select Valid Date before save!");
            dateIn.requestFocus();

        } else if (y.equals("")) {
            status.setText("Please Select Valid Date before save!");
            dateOut.requestFocus();

        } else if (timeIn.getText().equals("Time In")) {
            status.setText("Plesae mark timeIn before save!");
            timeIn.requestFocus();

////        } else if (!timeIn.getText().matches("[0-2]{1}[0-4].{1}[0-5]{1}[0-9]{1}")) {
////            status.setText("Plesae Check time before save!");
////            timeIn.requestFocus();
////
////        } else if (!timeOut.getText().matches("[0-2]{1}[0-4].{1}[0-5]{1}[0-9]{1}")) {
////            status.setText("Plesae Check time before save!");
////            timeOut.requestFocus();
//
        } else if (timeOut.getText().equals("Time Out")) {
            status.setText("Plesae mark timeOut before save!");
            timeOut.requestFocus();

        } else {
            status.setText("");
            try {
                String AttendanceId = attendanceId.getText();
                String EmpId = employeeId.getSelectedItem().toString();

                String designation = employeeDesignation.getText();
                String DateIn = ((JTextField) dateIn.getDateEditor().getUiComponent()).getText();

                SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy:MM:dd");

                Date now = simpleDateformat.parse(DateIn);
                System.out.println(simpleDateformat.format(now));

                DateFormat format2 = new SimpleDateFormat("EEEE");
                String day = format2.format(now);
                //System.out.println(finalDay);

                String TimeIn = timeIn.getText();
                String DateOut = ((JTextField) dateOut.getDateEditor().getUiComponent()).getText();
                String TimeOut = timeOut.getText();
                boolean isAdded = ctrl.saveAttendance(AttendanceId, EmpId, day, DateIn, TimeIn, DateOut, TimeOut, designation);
                if (isAdded) {
                    JOptionPane.showMessageDialog(this, "Attendance Mark Sucess");
                } else {
                    JOptionPane.showMessageDialog(this, "Attendance Mark faild");
                }

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage());
            }
            clean();
            loadItem();
            loadAllAttendance();
            attendanceId.setText(loadNextAttId());
        }
    }//GEN-LAST:event_saveMouseClicked

    private void save3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_save3MouseClicked
        String yr = ((JTextField) date.getDateEditor().getUiComponent()).getText();
        if (date.equals("")) {
            JOptionPane.showMessageDialog(this, "Please Select Date");
        } else if (employeeId3.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Please Select Employee");
        }
        try {

            String leavedate = ((JTextField) date.getDateEditor().getUiComponent()).getText();
            String empId = employeeId3.getSelectedItem().toString();

            int tot = Integer.parseInt(freeLeaves.getText());
            int ab = Integer.parseInt(totalAbsants.getText())+1;
            int free = tot - 1;

            boolean added = ctrl.markLeaves(0, empId, leavedate, ab, free);
            if (added) {
                JOptionPane.showMessageDialog(this, "Leave Marked");
            } else {
                JOptionPane.showMessageDialog(this, "Leave Mark faild");
            }
        } catch (Exception ex) {
            Logger.getLogger(Attendance.class.getName()).log(Level.SEVERE, null, ex);
        }
        clean();
        loadItem();
        LoadAllLeaves();
    }//GEN-LAST:event_save3MouseClicked

    private void lb56MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb56MouseClicked
        clean();
        loadItem();
    }//GEN-LAST:event_lb56MouseClicked

    private void applyFilter1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_applyFilter1KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (employeeId4.getSelectedIndex() == -1) {
                    JOptionPane.showMessageDialog(this, "Plese Select Employee");
                } else {
                    DefaultTableModel dtm = (DefaultTableModel) leaveTable.getModel();
                    try {
                        String Id = employeeId4.getSelectedItem().toString();

                        ArrayList<LeavesDTO> all = ctrl.getAllLeaves(Id);

                        dtm.setRowCount(0);

                        for (LeavesDTO a : all) {

                            EmployeeDTO e = ctrl.searchempDetailsByName(a.getEmpId());
                            String name = e.getEmpName();
                            Object Rowdata[] = {name, a.getDate(), a.getFree_Leaves()};
                            dtm.addRow(Rowdata);

                        }

                    } catch (Exception ex) {
                        Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    clean();
                    loadItem();
                }

                break;
        }
    }//GEN-LAST:event_applyFilter1KeyPressed

    private void applyFilter1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_applyFilter1MouseClicked
        if (employeeId4.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Plese Select Employee");
        } else {
            DefaultTableModel dtm = (DefaultTableModel) leaveTable.getModel();
            try {
                String Id = employeeId4.getSelectedItem().toString();

                ArrayList<LeavesDTO> all = ctrl.getAllLeaves(Id);

                dtm.setRowCount(0);

                for (LeavesDTO a : all) {

                    EmployeeDTO e = ctrl.searchempDetailsByName(a.getEmpId());
                    String name = e.getEmpName();
                    Object Rowdata[] = {name, a.getDate(), a.getFree_Leaves()};
                    dtm.addRow(Rowdata);

                }

            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }

            clean();
            loadItem();
        }
    }//GEN-LAST:event_applyFilter1MouseClicked

    private void applyFilterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_applyFilterMouseClicked

        if (employeeId2.getSelectedIndex() != -1) {
            try {
                DefaultTableModel dtm = (DefaultTableModel) attendanceTable.getModel();
                String Id = employeeId2.getSelectedItem().toString();
                int y = year.getYear();
                int m = month.getMonth() + 1;

                ArrayList<AttendanceDTO> att = ctrl.getAttendanceDetails(Id, y, m);
                dtm.setRowCount(0);

                for (AttendanceDTO a : att) {
                    EmployeeDTO e = ctrl.searchempDetailsByName(a.getEmpId());
                    String name = e.getEmpName();
                    Object Rowdata[] = {a.getAttId(), name, a.getDay(), a.getDateIn(), a.getTimeIn(), a.getDateOut(), a.getTimeOut(), a.getWorkedHourse()};
                    dtm.addRow(Rowdata);

                }

            } catch (Exception ex) {
                Logger.getLogger(Attendance.class.getName()).log(Level.SEVERE, null, ex);
            }
            clean();
            loadItem();
        } else if (employeeId2.getSelectedIndex() == -1) {
            try {
                DefaultTableModel dtm = (DefaultTableModel) attendanceTable.getModel();
                // String Id = employeeId2.getSelectedItem().toString();
                int y = year.getYear();
                int m = month.getMonth() + 1;

                ArrayList<AttendanceDTO> att = ctrl.getAttendanceDetails(y, m);
                dtm.setRowCount(0);

                for (AttendanceDTO a : att) {
                    EmployeeDTO e = ctrl.searchempDetailsByName(a.getEmpId());
                    String name = e.getEmpName();
                    Object Rowdata[] = {a.getAttId(), name, a.getDay(), a.getDateIn(), a.getTimeIn(), a.getDateOut(), a.getTimeOut(), a.getWorkedHourse()};
                    dtm.addRow(Rowdata);

                }

            } catch (Exception ex) {
                Logger.getLogger(Attendance.class.getName()).log(Level.SEVERE, null, ex);
            }
            clean();
            loadItem();
        }
    }//GEN-LAST:event_applyFilterMouseClicked

    private void cancelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cancelKeyPressed
        clean();
        loadItem();
    }//GEN-LAST:event_cancelKeyPressed

    private void cancelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelMouseClicked
        clean();
        loadItem();
    }//GEN-LAST:event_cancelMouseClicked

    private void removeAttendaceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_removeAttendaceMouseClicked
        try {
            if (attendanceTable.getSelectedRow() == -1) {
                return;
            }

            boolean result = ctrl.delete(attendanceTable.getValueAt(attendanceTable.getSelectedRow(), 0).toString());

            if (result) {
                JOptionPane.showMessageDialog(this, "Advance has been deleted successfully");
            } else {
                JOptionPane.showMessageDialog(this, "Failed to delete the Advance");
            }

            attendanceTable.getSelectionModel().clearSelection();
            loadAllAttendance();
            clean();
            loadItem();

        } catch (Exception ex) {
            Logger.getLogger(Loans.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_removeAttendaceMouseClicked

    private void aMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_aMouseClicked
        lb29.setText("Make Employee Short Leaves");
        date2.requestFocus();
        card.removeAll();
        card.repaint();
        card.revalidate();
        card.add(MakeShortLeave);
        card.repaint();
        card.revalidate();
        a.setBackground(new Color(0, 0, 102));
        lb23.setBackground(new Color(255, 255, 255));
        lb25.setBackground(new Color(255, 255, 255));
        lb24.setBackground(new Color(255, 255, 255));
        lb23.setForeground(new Color(0, 0, 0));
        lb25.setForeground(new Color(0, 0, 0));
        a.setForeground(new Color(255, 255, 255));
        lb24.setForeground(new Color(0, 0, 0));
    }//GEN-LAST:event_aMouseClicked

    private void employeeId7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeId7ActionPerformed
        if (employeeId7.getSelectedIndex() != -1) {
            try {
                String id = (String) employeeId7.getSelectedItem();

                EmployeeDTO e = ctrl.searchempDetailsByName(id);
                if (e != null) {
                    String de = ctrl.searchDesignation(e.getDeId());
                    String d = e.getEmpName();

                    employeeName6.setSelectedItem(d);

                    employeeDesignation5.setText(de);

                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeId7ActionPerformed

    private void employeeName6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeName6ActionPerformed
        if (employeeName6.getSelectedIndex() != -1) {
            try {
                String name = employeeName6.getSelectedItem().toString();
                // System.out.println(name);
                EmployeeDTO e = ctrl.searchEmpByName(name);
                if (e != null) {

                    String des = ctrl.searchDesignation(e.getDeId());
                    String empid = e.getEmpId();

                    employeeId7.setSelectedItem(empid);
                    employeeDesignation3.setText(des);

                }
            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeName6ActionPerformed

    private void outActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_outActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_outActionPerformed

    private void cancel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancel2MouseClicked
        clean();
        loadItem();
    }//GEN-LAST:event_cancel2MouseClicked

    private void save2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_save2MouseClicked
        String yr = ((JTextField) date2.getDateEditor().getUiComponent()).getText();
        if (date2.equals("")) {
            JOptionPane.showMessageDialog(this, "Please Select Date");
        } else if (employeeId7.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this, "Please Select Employee");
        }
        try {

            String leavedate = ((JTextField) date2.getDateEditor().getUiComponent()).getText();
            String empId = employeeId7.getSelectedItem().toString();

            String tot = in.getText();
            String ab = out.getText();
            System.out.println(tot);
            System.out.println(leavedate);
            System.out.println(ab);

            boolean added = ctrl.markLeaves(0, empId, leavedate, tot, ab);
            if (added) {
                JOptionPane.showMessageDialog(this, "Leave Marked");
            } else {
                JOptionPane.showMessageDialog(this, "Leave Mark faild");
            }
        } catch (Exception ex) {
            Logger.getLogger(Attendance.class.getName()).log(Level.SEVERE, null, ex);
        }
        clean();
        loadItem();
        loadAllShortLeaves();
    }//GEN-LAST:event_save2MouseClicked

    private void applyFilter3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_applyFilter3MouseClicked
        if (employeeId8.getSelectedIndex() != 0) {
            JOptionPane.showMessageDialog(this, "Plese Select Employee");
        } else {
            DefaultTableModel dtm = (DefaultTableModel) leaveTable2.getModel();
            try {
                String Id = employeeId8.getSelectedItem().toString();

                ArrayList<shortLeaveDTO> all = ctrl.getAllShortLeaves(Id);

                dtm.setRowCount(0);

                for (shortLeaveDTO a : all) {

                    EmployeeDTO e = ctrl.searchempDetailsByName(a.getEmpId());
                    String name = e.getEmpName();
                    Object Rowdata[] = {name, a.getDate(), a.getDuration()};
                    dtm.addRow(Rowdata);

                }

            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }

            clean();
            loadItem();
        }
    }//GEN-LAST:event_applyFilter3MouseClicked

    private void applyFilter3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_applyFilter3KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                if (employeeId8.getSelectedIndex() != 0) {
                    JOptionPane.showMessageDialog(this, "Plese Select Employee");
                } else {
                    DefaultTableModel dtm = (DefaultTableModel) leaveTable2.getModel();
                    try {
                        String Id = employeeId8.getSelectedItem().toString();

                        ArrayList<shortLeaveDTO> all = ctrl.getAllShortLeaves(Id);

                        dtm.setRowCount(0);

                        for (shortLeaveDTO a : all) {

                            EmployeeDTO e = ctrl.searchempDetailsByName(a.getEmpId());
                            String name = e.getEmpName();
                            Object Rowdata[] = {name, a.getDate(), a.getDuration()};
                            dtm.addRow(Rowdata);

                        }

                    } catch (Exception ex) {
                        Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    clean();
                    loadItem();
                }
                break;
        }
    }//GEN-LAST:event_applyFilter3KeyPressed

    private void employeeId8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeId8ActionPerformed
        if (employeeId8.getSelectedIndex() != -1) {
            try {
                String id = (String) employeeId8.getSelectedItem();

                EmployeeDTO e = ctrl.searchempDetailsByName(id);
                if (e != null) {
                    String de = ctrl.searchDesignation(e.getDeId());
                    String d = e.getEmpName();

                    employeeName7.setSelectedItem(d);

                }
            } catch (Exception ex) {
                Logger.getLogger(EmployeeReview.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeId8ActionPerformed

    private void employeeName7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeName7ActionPerformed
        if (employeeName7.getSelectedIndex() != -1) {
            try {
                String name = employeeName7.getSelectedItem().toString();
                // System.out.println(name);
                EmployeeDTO e = ctrl.searchEmpByName(name);
                if (e != null) {

                    String des = ctrl.searchDesignation(e.getDeId());
                    String empid = e.getEmpId();

                    employeeId8.setSelectedItem(empid);

                }
            } catch (Exception ex) {
                Logger.getLogger(PlaceAdvance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_employeeName7ActionPerformed

    private void lb47MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb47MouseClicked
        loadAllShortLeaves();
    }//GEN-LAST:event_lb47MouseClicked

    private void re2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_re2MouseClicked
        LoadAllLeaves();
    }//GEN-LAST:event_re2MouseClicked

    private void reMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reMouseClicked
        loadAllAttendance();
    }//GEN-LAST:event_reMouseClicked

    private void employeeIdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeIdKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_employeeIdKeyPressed

    private void EmployeeNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EmployeeNameKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_EmployeeNameKeyPressed

    private void dateInKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateInKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_dateInKeyPressed

    private void timeInKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_timeInKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_timeInKeyPressed

    private void dateOutKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateOutKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_dateOutKeyPressed

    private void timeOutKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_timeOutKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_timeOutKeyPressed

    private void saveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_saveKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                String yr = ((JTextField) dateIn.getDateEditor().getUiComponent()).getText();
                String y = ((JTextField) dateOut.getDateEditor().getUiComponent()).getText();

                if (employeeId.getSelectedIndex() == -1) {
                    status.setText("Please Select Employee!");
                    employeeId.requestFocus();

                } else if (yr.equals("")) {
                    status.setText("Please Select Valid Date before save!");
                    dateIn.requestFocus();

                } else if (y.equals("")) {
                    status.setText("Please Select Valid Date before save!");
                    dateOut.requestFocus();

                } else if (timeIn.getText().equals("Time In")) {
                    status.setText("Plesae mark timeIn before save!");
                    timeIn.requestFocus();

////        } else if (!timeIn.getText().matches("[0-2]{1}[0-4].{1}[0-5]{1}[0-9]{1}")) {
////            status.setText("Plesae Check time before save!");
////            timeIn.requestFocus();
////
////        } else if (!timeOut.getText().matches("[0-2]{1}[0-4].{1}[0-5]{1}[0-9]{1}")) {
////            status.setText("Plesae Check time before save!");
////            timeOut.requestFocus();
//
                } else if (timeOut.getText().equals("Time Out")) {
                    status.setText("Plesae mark timeOut before save!");
                    timeOut.requestFocus();

                } else {
                    status.setText("");
                    try {
                        String AttendanceId = attendanceId.getText();
                        String EmpId = employeeId.getSelectedItem().toString();

                        String designation = employeeDesignation.getText();
                        String DateIn = ((JTextField) dateIn.getDateEditor().getUiComponent()).getText();

                        SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy:MM:dd");

                        Date now = simpleDateformat.parse(DateIn);
                        System.out.println(simpleDateformat.format(now));

                        DateFormat format2 = new SimpleDateFormat("EEEE");
                        String day = format2.format(now);
                        //System.out.println(finalDay);

                        String TimeIn = timeIn.getText();
                        String DateOut = ((JTextField) dateOut.getDateEditor().getUiComponent()).getText();
                        String TimeOut = timeOut.getText();
                        boolean isAdded = ctrl.saveAttendance(AttendanceId, EmpId, day, DateIn, TimeIn, DateOut, TimeOut, designation);
                        if (isAdded) {
                            JOptionPane.showMessageDialog(this, "Attendance Mark Sucess");
                        } else {
                            JOptionPane.showMessageDialog(this, "Attendance Mark faild");
                        }

                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(this, ex.getMessage());
                    }
                    clean();
                    loadItem();
                    loadAllAttendance();
                    attendanceId.setText(loadNextAttId());
                }
                break;
        }
    }//GEN-LAST:event_saveKeyPressed

    private void employeeId2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeId2KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_employeeId2KeyPressed

    private void employeeNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeNameKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_employeeNameKeyPressed

    private void yearKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_yearKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_yearKeyPressed

    private void monthKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_monthKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();
                applyFilter.requestFocus();
                break;
        }
    }//GEN-LAST:event_monthKeyPressed

    private void applyFilterKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_applyFilterKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:

                if (employeeId2.getSelectedIndex() != -1) {
                    try {
                        DefaultTableModel dtm = (DefaultTableModel) attendanceTable.getModel();
                        String Id = employeeId2.getSelectedItem().toString();
                        int y = year.getYear();
                        int m = month.getMonth() + 1;

                        ArrayList<AttendanceDTO> att = ctrl.getAttendanceDetails(Id, y, m);
                        dtm.setRowCount(0);

                        for (AttendanceDTO a : att) {
                            EmployeeDTO e = ctrl.searchempDetailsByName(a.getEmpId());
                            String name = e.getEmpName();
                            Object Rowdata[] = {a.getAttId(), name, a.getDay(), a.getDateIn(), a.getTimeIn(), a.getDateOut(), a.getTimeOut(), a.getWorkedHourse()};
                            dtm.addRow(Rowdata);

                        }

                    } catch (Exception ex) {
                        Logger.getLogger(Attendance.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    clean();
                    loadItem();
                } else if (employeeId2.getSelectedIndex() == -1) {
                    try {
                        DefaultTableModel dtm = (DefaultTableModel) attendanceTable.getModel();
                        // String Id = employeeId2.getSelectedItem().toString();
                        int y = year.getYear();
                        int m = month.getMonth() + 1;

                        ArrayList<AttendanceDTO> att = ctrl.getAttendanceDetails(y, m);
                        dtm.setRowCount(0);

                        for (AttendanceDTO a : att) {
                            EmployeeDTO e = ctrl.searchempDetailsByName(a.getEmpId());
                            String name = e.getEmpName();
                            Object Rowdata[] = {a.getAttId(), name, a.getDay(), a.getDateIn(), a.getTimeIn(), a.getDateOut(), a.getTimeOut(), a.getWorkedHourse()};
                            dtm.addRow(Rowdata);

                        }

                    } catch (Exception ex) {
                        Logger.getLogger(Attendance.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    clean();
                    loadItem();
                }
                break;
        }
    }//GEN-LAST:event_applyFilterKeyPressed

    private void dateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dateKeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_dateKeyPressed

    private void employeeId3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeId3KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();
                save3.requestFocus();
                break;
        }
    }//GEN-LAST:event_employeeId3KeyPressed

    private void employeeName2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeName2KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();
                save3.requestFocus();
                break;
        }
    }//GEN-LAST:event_employeeName2KeyPressed

    private void save3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_save3KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                try {

                    String leavedate = ((JTextField) date.getDateEditor().getUiComponent()).getText();
                    String empId = employeeId3.getSelectedItem().toString();

                    int tot = Integer.parseInt(freeLeaves.getText());
                    int ab = Integer.parseInt(totalAbsants.getText())+1;
                    int free = tot - 1;

                    boolean added = ctrl.markLeaves(0, empId, leavedate, ab, free);
                    if (added) {
                        JOptionPane.showMessageDialog(this, "Leave Marked");
                    } else {
                        JOptionPane.showMessageDialog(this, "Leave Mark faild");
                    }
                } catch (Exception ex) {
                    Logger.getLogger(Attendance.class.getName()).log(Level.SEVERE, null, ex);
                }
                clean();
                loadItem();
                LoadAllLeaves();
                break;
        }
    }//GEN-LAST:event_save3KeyPressed

    private void dateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dateMouseClicked
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.getFocusOwner().transferFocus();

    }//GEN-LAST:event_dateMouseClicked

    private void employeeId4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeId4KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_employeeId4KeyPressed

    private void employeeName3KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeName3KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();
                applyFilter1.requestFocus();
                break;
        }
    }//GEN-LAST:event_employeeName3KeyPressed

    private void date2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_date2KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();

                break;
        }
    }//GEN-LAST:event_date2KeyPressed

    private void employeeId7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeId7KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();
                save2.requestFocus();
                break;
        }
    }//GEN-LAST:event_employeeId7KeyPressed

    private void employeeName6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeName6KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();
                save2.requestFocus();
                break;
        }
    }//GEN-LAST:event_employeeName6KeyPressed

    private void save2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_save2KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                try {

                    String leavedate = ((JTextField) date2.getDateEditor().getUiComponent()).getText();
                    String empId = employeeId7.getSelectedItem().toString();

                    String tot = in.getText();
                    String ab = out.getText();
                    System.out.println(tot);
                    System.out.println(leavedate);
                    System.out.println(ab);

                    boolean added = ctrl.markLeaves(0, empId, leavedate, tot, ab);
                    if (added) {
                        JOptionPane.showMessageDialog(this, "Leave Marked");
                    } else {
                        JOptionPane.showMessageDialog(this, "Leave Mark faild");
                    }
                } catch (Exception ex) {
                    Logger.getLogger(Attendance.class.getName()).log(Level.SEVERE, null, ex);
                }
                clean();
                loadItem();
                loadAllShortLeaves();
                break;
        }
    }//GEN-LAST:event_save2KeyPressed

    private void employeeId8KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeId8KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();
                applyFilter3.requestFocus();
                break;
        }
    }//GEN-LAST:event_employeeId8KeyPressed

    private void employeeName7KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_employeeName7KeyPressed
        int keyCode = evt.getKeyCode();
        switch (keyCode) {

            case KeyEvent.VK_ENTER:
                KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                manager.getFocusOwner().transferFocus();
                applyFilter3.requestFocus();
                break;
        }
    }//GEN-LAST:event_employeeName7KeyPressed

    private void lb11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb11MouseClicked
        PayrollReview d1 = new PayrollReview();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb11MouseClicked

    private void lb11MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb11MouseEntered
        lb11.setForeground(Color.red);
    }//GEN-LAST:event_lb11MouseEntered

    private void lb11MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb11MouseExited
        lb11.setForeground(Color.white);
    }//GEN-LAST:event_lb11MouseExited

    private void lb4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseClicked
        Reports d1 = new Reports();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb4MouseClicked

    private void lb4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseEntered
        lb4.setForeground(Color.RED);
    }//GEN-LAST:event_lb4MouseEntered

    private void lb4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb4MouseExited
        lb4.setForeground(Color.white);
    }//GEN-LAST:event_lb4MouseExited

    private void lb10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseClicked
        Payroll d1 = new Payroll();
        d1.setVisible(true);
        d1.pack();
        d1.setLocationRelativeTo(null);
        d1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_lb10MouseClicked

    private void lb10MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseEntered
        lb10.setForeground(Color.RED);
    }//GEN-LAST:event_lb10MouseEntered

    private void lb10MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lb10MouseExited
        lb10.setForeground(Color.white);
    }//GEN-LAST:event_lb10MouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Attendance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Attendance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Attendance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Attendance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Attendance().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> EmployeeName;
    private javax.swing.JPanel MakeLeave;
    private javax.swing.JPanel MakeShortLeave;
    private javax.swing.JPanel MarkAttendance;
    private javax.swing.JPanel ViewAttendance;
    private javax.swing.JLabel a;
    private javax.swing.JLabel applyFilter;
    private javax.swing.JLabel applyFilter1;
    private javax.swing.JLabel applyFilter3;
    private javax.swing.JTextField attendanceId;
    private javax.swing.JTable attendanceTable;
    private javax.swing.JLabel cancel;
    private javax.swing.JLabel cancel2;
    private javax.swing.JPanel card;
    private com.toedter.calendar.JDateChooser date;
    private com.toedter.calendar.JDateChooser date2;
    private com.toedter.calendar.JDateChooser dateIn;
    private com.toedter.calendar.JDateChooser dateOut;
    private javax.swing.JTextField employeeDesignation;
    private javax.swing.JTextField employeeDesignation3;
    private javax.swing.JTextField employeeDesignation5;
    private javax.swing.JComboBox<String> employeeId;
    private javax.swing.JComboBox<String> employeeId2;
    private javax.swing.JComboBox<String> employeeId3;
    private javax.swing.JComboBox<String> employeeId4;
    private javax.swing.JComboBox<String> employeeId7;
    private javax.swing.JComboBox<String> employeeId8;
    private javax.swing.JComboBox<String> employeeName;
    private javax.swing.JComboBox<String> employeeName2;
    private javax.swing.JComboBox<String> employeeName3;
    private javax.swing.JComboBox<String> employeeName6;
    private javax.swing.JComboBox<String> employeeName7;
    private javax.swing.JPanel fil;
    private javax.swing.JLabel fil2;
    private javax.swing.JLabel frame;
    private javax.swing.JTextField freeLeaves;
    private javax.swing.JTextField in;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JLabel lb1;
    private javax.swing.JLabel lb10;
    private javax.swing.JLabel lb11;
    private javax.swing.JLabel lb20;
    private javax.swing.JLabel lb21;
    private javax.swing.JLabel lb22;
    private javax.swing.JLabel lb23;
    private javax.swing.JLabel lb24;
    private javax.swing.JLabel lb25;
    private javax.swing.JLabel lb26;
    private javax.swing.JLabel lb27;
    private javax.swing.JLabel lb28;
    private javax.swing.JLabel lb29;
    private javax.swing.JLabel lb3;
    private javax.swing.JLabel lb30;
    private javax.swing.JLabel lb31;
    private javax.swing.JLabel lb32;
    private javax.swing.JLabel lb33;
    private javax.swing.JLabel lb34;
    private javax.swing.JLabel lb35;
    private javax.swing.JLabel lb36;
    private javax.swing.JLabel lb37;
    private javax.swing.JLabel lb38;
    private javax.swing.JLabel lb39;
    private javax.swing.JLabel lb4;
    private javax.swing.JLabel lb40;
    private javax.swing.JLabel lb41;
    private javax.swing.JLabel lb42;
    private javax.swing.JLabel lb43;
    private javax.swing.JLabel lb44;
    private javax.swing.JLabel lb45;
    private javax.swing.JLabel lb46;
    private javax.swing.JLabel lb47;
    private javax.swing.JLabel lb48;
    private javax.swing.JLabel lb5;
    private javax.swing.JLabel lb50;
    private javax.swing.JLabel lb51;
    private javax.swing.JLabel lb52;
    private javax.swing.JLabel lb53;
    private javax.swing.JLabel lb54;
    private javax.swing.JLabel lb55;
    private javax.swing.JLabel lb56;
    private javax.swing.JLabel lb6;
    private javax.swing.JLabel lb7;
    private javax.swing.JLabel lb8;
    private javax.swing.JLabel lb9;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblTime;
    private javax.swing.JTable leaveTable;
    private javax.swing.JTable leaveTable2;
    private javax.swing.JPanel main;
    private com.toedter.calendar.JMonthChooser month;
    private javax.swing.JTextField out;
    private javax.swing.JPanel p1;
    private javax.swing.JPanel p2;
    private javax.swing.JPanel p3;
    private javax.swing.JPanel p4;
    private javax.swing.JPanel p5;
    private javax.swing.JPanel p6;
    private javax.swing.JPanel p7;
    private javax.swing.JPanel p8;
    private javax.swing.JPanel path;
    private javax.swing.JLabel re;
    private javax.swing.JLabel re2;
    private javax.swing.JLabel removeAttendace;
    private javax.swing.JLabel save;
    private javax.swing.JLabel save2;
    private javax.swing.JLabel save3;
    private javax.swing.JLabel status;
    private javax.swing.JTextField timeIn;
    private javax.swing.JTextField timeOut;
    private javax.swing.JTextField totalAbsants;
    private com.toedter.calendar.JYearChooser year;
    // End of variables declaration//GEN-END:variables
private void setTime() {
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("hh-mm-ss a");
                lblTime.setText(sdf.format(date));
            }
        }).start();
    }

    private void setDate(Date date1) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        lblDate.setText(sdf.format(date));
    }

    public void clean() {
        attendanceId.setText("Attendance Id");

        employeeId.removeAllItems();
        employeeId2.removeAllItems();
        employeeId3.removeAllItems();
        employeeId4.removeAllItems();
        employeeId7.removeAllItems();
        employeeId8.removeAllItems();
        employeeName.removeAllItems();
        EmployeeName.removeAllItems();
        employeeName2.removeAllItems();
        employeeName3.removeAllItems();
        employeeName6.removeAllItems();
        employeeName7.removeAllItems();

        dateIn.setDate(new Date());
        date.setDate(new Date());
        date2.setDate(new Date());
        timeIn.setText("Time In");
        dateOut.setDate(new Date());
        timeOut.setText("Time Out");
        year.setYear(2018);
        month.setMonth(5);
        employeeDesignation.setText("Employee Designation");
        employeeDesignation5.setText("Employee Designation");
        employeeDesignation3.setText("");
        totalAbsants.setText("");
        freeLeaves.setText("");
        out.setText("");
        in.setText("");

    }

    private String loadNextAttId() {
        String id = getLastAttId();
        if (id == null) {
            id = "Att";
        }
        char ch = id.charAt(id.length() - 1);
        int a = Character.getNumericValue(ch) + 1;
        return id = id.substring(0, id.length() - 1) + a;

    }

    private String getLastAttId() {
        String lastId = null;
        try {
            lastId = ctrl.searchAttId();
        } catch (Exception e) {
            // e.printStackTrace();
        }
        return lastId;
    }

}
