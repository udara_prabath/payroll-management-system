/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business;

import lk.ijse.payroll.business.custom.Impl.AdvancesBOImpl;
import lk.ijse.payroll.business.custom.Impl.AttendanceBOImpl;
import lk.ijse.payroll.business.custom.Impl.BankBOImpl;
import lk.ijse.payroll.business.custom.Impl.BankDetailsBOImpl;
import lk.ijse.payroll.business.custom.Impl.BasicSalaryBOImpl;
import lk.ijse.payroll.business.custom.Impl.BonusBOImpl;
import lk.ijse.payroll.business.custom.Impl.DesignationBOImpl;
import lk.ijse.payroll.business.custom.Impl.EmpAdvancesBOImpl;


import lk.ijse.payroll.business.custom.Impl.EmpLoanBOImpl;
import lk.ijse.payroll.business.custom.Impl.EmployeeBOImpl;

import lk.ijse.payroll.business.custom.Impl.LeavesBOImpl;
import lk.ijse.payroll.business.custom.Impl.LoanBOImpl;
import lk.ijse.payroll.business.custom.Impl.OTBOImpl;
import lk.ijse.payroll.business.custom.Impl.PayrollBOImpl;
import lk.ijse.payroll.business.custom.Impl.SalaryDetailsBOImpl;
import lk.ijse.payroll.business.custom.Impl.ShortLeaveBOImpl;
import lk.ijse.payroll.business.custom.Impl.UserBOImpl;

/**
 *
 * @author udara
 */
public class BOFactory {

    public enum BOTypes {
        Advances, Attendance, Bank, BankDetails, BasicSalary, Bonus, Designation, EmpAdvances,  EmpBonus, EmpLoan, Employee,  Leaves, Loan, OT, Payroll, SalaryDetails, ShortLeave, User;
    }
    public static BOFactory bOFactory;

    private BOFactory() {

    }

    public static BOFactory getInstance() {
        if (bOFactory == null) {
            bOFactory = new BOFactory();
        }
        return bOFactory;
    }

    public <T> T getBO(BOTypes boTypes) {
        switch (boTypes) {
            case Advances:
                return (T) new AdvancesBOImpl();
            case SalaryDetails:
                return (T) new SalaryDetailsBOImpl();
            case Attendance:
                return (T) new AttendanceBOImpl();
            case Bank:
                return (T) new BankBOImpl();
            case BankDetails:
                return (T) new BankDetailsBOImpl();
            case BasicSalary:
                return (T) new BasicSalaryBOImpl();
            case Bonus:
                return (T) new BonusBOImpl();
            case Designation:
                return (T) new DesignationBOImpl();
            case EmpAdvances:
                return (T) new EmpAdvancesBOImpl();
            
            
            case EmpLoan:
                return (T) new EmpLoanBOImpl();
            case Employee:
                return (T) new EmployeeBOImpl();
            case Leaves:
                return (T) new LeavesBOImpl();
            case Loan:
                return (T) new LoanBOImpl();
            
            case OT:
                return (T) new OTBOImpl();
            case Payroll:
                return (T) new PayrollBOImpl();
            case ShortLeave:
                return (T) new ShortLeaveBOImpl();
            case User:
                return (T) new UserBOImpl();
            default:
                return null;

        }
    }

}
