/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.DesignationDTO;
import lk.ijse.payroll.entity.Designation;

/**
 *
 * @author udara
 */
public interface DesignationBO {
        public boolean add(DesignationDTO salary) throws Exception;

    public boolean update(DesignationDTO salary) throws Exception;

    public boolean delete(String salaryId) throws Exception;

    public Designation search(String salaryId) throws Exception;

    public ArrayList<DesignationDTO> getAll() throws Exception;

    public DesignationDTO searchId(String id)throws Exception;

    public ArrayList<String> getAllDesignations()throws Exception;

    public String searchDes(String deId)throws Exception;

    public String search()throws Exception;
}
