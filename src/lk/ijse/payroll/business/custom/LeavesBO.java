/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.LeavesDTO;
import lk.ijse.payroll.entity.Leaves;

/**
 *
 * @author udara
 */
public interface LeavesBO {
    public boolean add(LeavesDTO salary) throws Exception;

    public boolean update(LeavesDTO salary) throws Exception;

    public boolean delete(String salaryId) throws Exception;

    public Leaves search(String salaryId) throws Exception;

    public ArrayList<LeavesDTO> getAll() throws Exception;

    public boolean markLeave(int leaveId, String empId, String leavedate, int ab, int free)throws Exception;

    public ArrayList<LeavesDTO> getAll(String Id)throws Exception;
}
