/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.BankDetailsDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.entity.BankDetails;

/**
 *
 * @author udara
 */
public interface BankDetailsBO {
        public boolean add(BankDetailsDTO salary) throws Exception;

    public boolean update(BankDetailsDTO salary) throws Exception;

    public boolean delete(String salaryId) throws Exception;

    public BankDetailsDTO search(String salaryId) throws Exception;

    public ArrayList<BankDetailsDTO> getAll() throws Exception;

    public String searchbank() throws Exception;

     String getBdid(String toString)throws Exception;

    public QueryDTO getBankDetails(String id)throws Exception;
}
