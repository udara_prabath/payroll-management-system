/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import lk.ijse.payroll.business.custom.AttendanceBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.AttendanceDAO;
import lk.ijse.payroll.dao.custom.EmployeeDAO;
import lk.ijse.payroll.dao.custom.LeavesDAO;
import lk.ijse.payroll.dao.custom.OTDAO;
import lk.ijse.payroll.dao.custom.QueryDAO;
import lk.ijse.payroll.db.DBConnection;
import lk.ijse.payroll.dto.AttendanceDTO;
import lk.ijse.payroll.entity.Attendance;
import lk.ijse.payroll.entity.Employee;
import lk.ijse.payroll.entity.Leaves;
import lk.ijse.payroll.entity.OT;

/**
 *
 * @author udara
 */
public class AttendanceBOImpl implements AttendanceBO {

    private QueryDAO q;
    private AttendanceDAO att;
    private LeavesDAO lev;
    private EmployeeDAO emp;
    private OTDAO ot;

    public AttendanceBOImpl() {
        q = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Query);
        att = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Attendance);
        lev = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Leaves);
        emp = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Employee);
        ot = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.OT);
    }

    @Override
    public boolean add(AttendanceDTO a, String designation) throws Exception {
        Attendance at = new Attendance(a.getAttId(), a.getEmpId(), a.getDay(), a.getDateIn(), a.getTimeIn(), a.getDateOut(), a.getTimeOut(), a.getWorkedHourse());
        return att.add(at,designation);
    }

    @Override
    public boolean update(AttendanceDTO attendance) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Id) throws Exception {
        return att.delete(Id);
    }

    @Override
    public Attendance search(String Id) throws Exception {
        return null;
    }

    @Override
    public ArrayList<AttendanceDTO> getAll() throws Exception {
        ArrayList<Attendance> adv = att.getAll();
        ArrayList<AttendanceDTO> all = new ArrayList<>();
        for (Attendance a : adv) {
            System.out.println(a.getAttId());
            all.add(new AttendanceDTO(a.getAttId(), a.getEmpId(), a.getDay(), a.getDateIn(), a.getTimeIn(), a.getDateOut(), a.getTimeOut(), a.getWorkedHourse()));
        }
        return all;
    }

    @Override
    public ArrayList<AttendanceDTO> getAll(String Id, int y, int m) throws Exception {
        ArrayList<Attendance> as = att.getAll(Id, y, m);
        ArrayList<AttendanceDTO> all = new ArrayList<>();
        for (Attendance a : as) {
            all.add(new AttendanceDTO(a.getAttId(), a.getEmpId(), a.getDay(), a.getDateIn(), a.getTimeIn(), a.getDateOut(), a.getTimeOut(), a.getWorkedHourse()));
        }
        return all;
    }

    @Override
    public ArrayList<AttendanceDTO> getAll(int y, int m) throws Exception {
        ArrayList<Attendance> as = att.getAll(y, m);
        ArrayList<AttendanceDTO> all = new ArrayList<>();
        for (Attendance a : as) {
            all.add(new AttendanceDTO(a.getAttId(), a.getEmpId(), a.getDay(), a.getDateIn(), a.getTimeIn(), a.getDateOut(), a.getTimeOut(), a.getWorkedHourse()));
        }
        return all;
    }

    @Override
    public String search() throws Exception {
        Attendance a = att.searchLastId();
        AttendanceDTO at = new AttendanceDTO(a.getAttId(), a.getEmpId(), a.getDay(), a.getDateIn(), a.getTimeIn(), a.getDateOut(), a.getTimeOut(), a.getWorkedHourse());
        return at.getAttId();
    }

}
