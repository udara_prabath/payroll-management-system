/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.util.ArrayList;
import lk.ijse.payroll.business.custom.BankBO;
import lk.ijse.payroll.business.custom.BankDetailsBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.BankDetailsDAO;
import lk.ijse.payroll.dao.custom.QueryDAO;
import lk.ijse.payroll.dto.BankDetailsDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.entity.BankDetails;
import lk.ijse.payroll.entity.Query;

/**
 *
 * @author udara
 */
public class BankDetailsBOImpl implements BankDetailsBO {

    private BankDetailsDAO bd;
    private QueryDAO q;

    public BankDetailsBOImpl() {
        bd = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.BankDetails);
        q=DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Query);
    }

    @Override
    public boolean add(BankDetailsDTO bd) throws Exception {
        return false;
    }

    @Override
    public boolean update(BankDetailsDTO bd) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Id) throws Exception {
        return false;
    }

    @Override
    public BankDetailsDTO search(String Id) throws Exception {
        return null;
    }

    @Override
    public ArrayList<BankDetailsDTO> getAll() throws Exception {
        return null;
    }

    @Override
    public String searchbank() throws Exception {
        BankDetails d = bd.searchBankId();
        BankDetailsDTO c = new BankDetailsDTO(d.getBdId(), d.getEmpId(), d.getBankId(), d.getAccNo());
        return ""+c.getBdId();
    }

    @Override
    public String getBdid(String toString) throws Exception {
        BankDetails d=bd.getbankId(toString);
        BankDetailsDTO c = new BankDetailsDTO(d.getBdId(), d.getEmpId(), d.getBankId(), d.getAccNo());
        
        return ""+c.getBdId();
        
    }

    @Override
    public QueryDTO getBankDetails(String id) throws Exception {
         Query a=q.getBankDetails(id);
         QueryDTO aa=new QueryDTO(a.getBankName(), a.getAcc_no());
         return aa;
    }
    

}
