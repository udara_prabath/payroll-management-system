/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.util.ArrayList;
import lk.ijse.payroll.business.custom.BasicSalaryBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.BasicSalaryDAO;
import lk.ijse.payroll.dao.custom.QueryDAO;
import lk.ijse.payroll.dto.BasicSalaryDTO;
import lk.ijse.payroll.entity.BasicSalary;

/**
 *
 * @author udara
 */
public class BasicSalaryBOImpl implements BasicSalaryBO {

    private BasicSalaryDAO bs;
    private QueryDAO q;

    public BasicSalaryBOImpl() {
        bs = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.BasicSalary);
        q = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Query);
    }

    @Override
    public boolean addSalary(BasicSalaryDTO salary) throws Exception {
        BasicSalary basic = new BasicSalary(salary.getBsId(), salary.getAmount());
        return bs.add(basic);
    }

    @Override
    public boolean updateSalary(BasicSalaryDTO salary) throws Exception {
        BasicSalary basic = new BasicSalary(salary.getBsId(), salary.getAmount());
        return bs.update(basic);
    }

    @Override
    public boolean deleteSalary(String salaryId) throws Exception {
        return bs.delete(salaryId);
    }

    @Override
    public BasicSalary searchSalary(String salaryId) throws Exception {
        return bs.search(salaryId);
    }

    @Override
    public ArrayList<BasicSalaryDTO> getAllSalary() throws Exception {
        ArrayList<BasicSalary> allsalary = bs.getAll();
        ArrayList<BasicSalaryDTO> loans = new ArrayList<>();
        for (BasicSalary b : allsalary) {
            loans.add(new BasicSalaryDTO(b.getBsId(), b.getAmount()));
        }
        return loans;
    }

    @Override
    public ArrayList<String> getAllsalary() throws Exception {
        ArrayList<BasicSalary> allCustomers = bs.getAll();
        ArrayList<String> alldesignations = new ArrayList<>();
        for (BasicSalary bs : allCustomers) {
            alldesignations.add(bs.getAmount().toString());
        }
        return alldesignations;
    }

    @Override
    public String searchSalaryId(String toString) throws Exception {
        BasicSalary d = bs.searchId(toString);
     
        return d.getBsId();
    }

    @Override
    public String searchSalaryName(String empId) throws Exception {
        BasicSalary d = q.search(empId);
      
        BasicSalaryDTO des = new BasicSalaryDTO(d.getBsId(), d.getAmount());
        return "" + des.getAmount();
    }

    @Override
    public String searchSalaryId() throws Exception {
        BasicSalary d = bs.searchId();
        BasicSalaryDTO c = new BasicSalaryDTO(d.getBsId(), d.getAmount());
        return "" + c.getBsId();
    }
}


