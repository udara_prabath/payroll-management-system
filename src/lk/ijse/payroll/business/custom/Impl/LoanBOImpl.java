/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import jdk.nashorn.internal.scripts.JO;
import lk.ijse.payroll.business.custom.LoanBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.EmpLoanDAO;
import lk.ijse.payroll.dao.custom.LoanDAO;
import lk.ijse.payroll.dto.EmpAdvancesDTO;
import lk.ijse.payroll.dto.EmpLoanDTO;
import lk.ijse.payroll.dto.LoanDTO;
import lk.ijse.payroll.entity.EmpLoan;
import lk.ijse.payroll.entity.Loan;
import lk.ijse.payroll.view.Loans;

/**
 *
 * @author udara
 */
public class LoanBOImpl implements LoanBO {

    private LoanDAO loanDao;
    private EmpLoanDAO ld;

    public LoanBOImpl() {
        loanDao = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Loan);
        ld = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.EmpLoan);
    }

    @Override
    public boolean addLoan(LoanDTO loanDto) throws Exception {
        Loan loan = new Loan(loanDto.getLoanId(), loanDto.getLoanName(), loanDto.getAmount());
        return loanDao.add(loan);
    }

    @Override
    public boolean updateLoan(LoanDTO loanDto) throws Exception {
        Loan loan = new Loan(loanDto.getLoanId(), loanDto.getLoanName(), loanDto.getAmount());
        return loanDao.update(loan);
    }

    @Override
    public boolean deleteLoan(String loanId) throws Exception {
        return loanDao.delete(loanId);
    }

    @Override
    public ArrayList<LoanDTO> getAllLoans() throws Exception {
        ArrayList<Loan> allLoan = loanDao.getAll();
        ArrayList<LoanDTO> loans = new ArrayList<>();
        for (Loan loan : allLoan) {
            loans.add(new LoanDTO(loan.getLoanId(), loan.getLoanName(), loan.getAmount()));
        }
        return loans;
    }

    @Override
    public ArrayList<String> getAllloanNames() throws Exception {
        ArrayList<Loan> adv = loanDao.getAll();
        ArrayList<String> all = new ArrayList<>();
        for (Loan loan : adv) {
            all.add(loan.getLoanName());
        }
        return all;
    }

    @Override
    public LoanDTO searchLoanByname(String name) throws Exception {
        Loan l = loanDao.searchLoanByName(name);
        
        return new LoanDTO(l.getLoanId(), l.getLoanName(), l.getAmount());

    }

    @Override
    public boolean addLoan(EmpLoanDTO l) throws Exception {
        EmpLoan lo = new EmpLoan(l.getElId(), l.getEmpId(), l.getLoanId(), l.getDate(), l.getInstallment());
        return ld.add(lo);
    }

    @Override
    public String searchLoanName(String loanId) throws Exception {
        Loan a= loanDao.search(loanId);
        System.out.println(a.getLoanName());
        return a.getLoanName();
    }

    @Override
    public String search() throws Exception {
        Loan l=loanDao.search();
        LoanDTO a=new LoanDTO(l.getLoanId(), l.getLoanName(), l.getAmount());
        return a.getLoanId();
        
    }

}
