/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.util.ArrayList;
import lk.ijse.payroll.business.custom.BonusBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.BonusDAO;
import lk.ijse.payroll.dto.BonusDTO;

import lk.ijse.payroll.entity.Bonus;


/**
 *
 * @author udara
 */
public class BonusBOImpl implements BonusBO {

    private BonusDAO ad;

    public BonusBOImpl() {
        ad = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Bonus);
    }

    @Override
    public boolean add(BonusDTO bonus) throws Exception {
        Bonus b = new Bonus(bonus.getBonusId(), bonus.getBonusName(), bonus.getAmount());
        return ad.add(b);
    }

    @Override
    public boolean update(BonusDTO bonus) throws Exception {
        Bonus b = new Bonus(bonus.getBonusId(), bonus.getBonusName(), bonus.getAmount());
        return ad.update(b);
    }

    @Override
    public boolean delete(String Id) throws Exception {
        return ad.delete(Id);
    }

    @Override
    public Bonus search(String Id) throws Exception {
        return ad.search(Id);
    }

    @Override
    public ArrayList<BonusDTO> getAll() throws Exception {
        ArrayList<Bonus> allsalary = ad.getAll();
        ArrayList<BonusDTO> loans = new ArrayList<>();
        for (Bonus b : allsalary) {
            loans.add(new BonusDTO(b.getBonusId(), b.getBonusName(), b.getAmount()));
        }
        return loans;
    }

    @Override
    public String search() throws Exception {
        Bonus b=ad.search();
        BonusDTO a=new BonusDTO(b.getBonusId(), b.getBonusName(), b.getAmount());
        return a.getBonusId();
    }

}
