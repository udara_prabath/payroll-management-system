/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import lk.ijse.payroll.business.custom.AdvancesBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.AdvancesDAO;
import lk.ijse.payroll.dto.AdvancesDTO;
import lk.ijse.payroll.entity.Advances;

/**
 *
 * @author udara
 */
public class AdvancesBOImpl implements AdvancesBO {

    private AdvancesDAO ad;

    public AdvancesBOImpl() {
        ad = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Advances);
    }

    @Override
    public boolean add(AdvancesDTO advance) throws Exception {
        Advances add = new Advances(advance.getAdId(), advance.getAdName(), advance.getAmount());
        return ad.add(add);
    }

    @Override
    public boolean update(AdvancesDTO advance) throws Exception {
        Advances add = new Advances(advance.getAdId(), advance.getAdName(), advance.getAmount());
        return ad.update(add);
    }

    @Override
    public boolean delete(String Id) throws Exception {
        return ad.delete(Id);
    }

    @Override
    public Advances search(String Id) throws Exception {
        return ad.search(Id);
    }

    @Override
    public ArrayList<AdvancesDTO> getAll() throws Exception {
        ArrayList<Advances> adv = ad.getAll();
        ArrayList<AdvancesDTO> all = new ArrayList<>();
        for (Advances advances : adv) {
            all.add(new AdvancesDTO(advances.getAdId(), advances.getAdName(), advances.getAmount()));
        }
        return all;
    }

    @Override
    public ArrayList<String> getAllAdvanceNames() throws Exception {
        ArrayList<Advances> adv = ad.getAll();
        ArrayList<String> all = new ArrayList<>();
        for (Advances advances : adv) {
            all.add(advances.getAdName());
        }
        return all;
    }

    @Override
    public AdvancesDTO searchAdvance(String adv) throws Exception {
        Advances a = ad.searchAdvance(adv);
        System.out.println(a.getAdId());
        return new AdvancesDTO(a.getAdId(), a.getAdName(), a.getAmount());
    }

    @Override
    public String searchadid(String adId) throws Exception {
        Advances a = ad.search(adId);
        System.out.println(a.getAdName());
        return a.getAdName();
    }

    @Override
    public String search() throws Exception {
        Advances a = ad.search();
        
        AdvancesDTO s= new AdvancesDTO(a.getAdId(), a.getAdName(), a.getAmount());
        return s.getAdId();
    }

}
