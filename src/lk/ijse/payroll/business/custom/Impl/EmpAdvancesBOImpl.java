/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.util.ArrayList;
import lk.ijse.payroll.business.custom.EmpAdvancesBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.EmpAdvancesDAO;
import lk.ijse.payroll.dao.custom.QueryDAO;
import lk.ijse.payroll.dao.custom.impl.EmpAdvancesDAOImpl;
import lk.ijse.payroll.dto.EmpAdvancesDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.entity.EmpAdvances;
import lk.ijse.payroll.entity.Query;

/**
 *
 * @author udara
 */
public class EmpAdvancesBOImpl implements EmpAdvancesBO {

    private EmpAdvancesDAO ead;
    private QueryDAO q;

    public EmpAdvancesBOImpl() {
        ead = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.EmpAdvances);
        q = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Query);
    }

    @Override
    public boolean add(EmpAdvancesDTO s) throws Exception {
        EmpAdvances entity = new EmpAdvances(s.geteAdId(), s.getEmpId(), s.getaDId(), s.getMonth(), s.getYear(), s.getMonthlyInstallment());
        return ead.add(entity);
    }

    @Override
    public boolean update(EmpAdvancesDTO salary) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String salaryId) throws Exception {
        return ead.delete(salaryId);
    }

    @Override
    public EmpAdvances search(String salaryId) throws Exception {
        return null;
    }

    @Override
    public ArrayList<EmpAdvancesDTO> getAll() throws Exception {
        ArrayList<EmpAdvances> allad = ead.getAll();
        ArrayList<EmpAdvancesDTO> all = new ArrayList<>();
        for (EmpAdvances d : allad) {
            all.add(new EmpAdvancesDTO(d.geteAdId(), d.getEmpId(), d.getaDId(), d.getMonth(), d.getYear(), d.getMonthlyInstallment()));
            //System.out.println(d.getEmpId());
        }
        return all;
    }

    @Override
    public ArrayList<EmpAdvancesDTO> getEmpAdVances(String adv) throws Exception {
        ArrayList<EmpAdvances> allad = ead.searchByName(adv);
        ArrayList<EmpAdvancesDTO> all = new ArrayList<>();
        for (EmpAdvances d : allad) {
            all.add(new EmpAdvancesDTO(d.geteAdId(), d.getEmpId(), d.getaDId(), d.getMonth(), d.getYear(), d.getMonthlyInstallment()));
            
        }
        return all;
    }

    @Override
    public String searchLastId() throws Exception {
        EmpAdvances d = ead.searchLastId();
        EmpAdvancesDTO c = new EmpAdvancesDTO(d.geteAdId(), d.getEmpId(), d.getaDId(), d.getMonth(), d.getYear(), d.getMonthlyInstallment());
        return c.geteAdId();
    }

    @Override
    public ArrayList<EmpAdvancesDTO> getAdvances(String empid) throws Exception {
        ArrayList<EmpAdvances> allad = ead.searchByemp(empid);
        ArrayList<EmpAdvancesDTO> all = new ArrayList<>();
        for (EmpAdvances d : allad) {
            all.add(new EmpAdvancesDTO(d.geteAdId(), d.getEmpId(), d.getaDId(), d.getMonth(), d.getYear(), d.getMonthlyInstallment()));
            //System.out.println(d.getEmpId());
        }
        return all;
    }

    @Override
    public QueryDTO searchAdvanceDetails(String id, String date) throws Exception {

        Query a = q.searchAdvancesDetails(id, date);

        return new QueryDTO(a.getAdName(), a.getAmount());

    }

    @Override
    public ArrayList<QueryDTO> getDetails(String id) throws Exception {
        ArrayList<Query> all = q.getAdvances(id);
        ArrayList<QueryDTO> a = new ArrayList<>();
        for (Query s : all) {
            a.add(new QueryDTO(s.getAdName(), s.getYear(), s.getMonth(), s.getAmount()));
        }
        return a;
    }

    @Override
    public boolean checkAvailabiity(int year, int month, String emp) {
        String yr = ead.searchyear(emp);
        String mon = ead.searchMonth(emp);
        if (yr != null & mon != null) {
            int a = Integer.parseInt(yr);
            int b = Integer.parseInt(mon);
            if (a == year & b == month) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
