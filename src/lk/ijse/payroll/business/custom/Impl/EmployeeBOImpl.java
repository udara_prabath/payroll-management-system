/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.sql.Connection;
import java.util.ArrayList;
import lk.ijse.payroll.business.custom.EmployeeBO;
import lk.ijse.payroll.dao.DAOFactory;
import static lk.ijse.payroll.dao.DAOFactory.DAOTypes.SalaryDetails;
import lk.ijse.payroll.dao.custom.BankDetailsDAO;
import lk.ijse.payroll.dao.custom.EmployeeDAO;
import lk.ijse.payroll.dao.custom.SalaryDetailsDAO;
import lk.ijse.payroll.db.DBConnection;
import lk.ijse.payroll.dto.BankDetailsDTO;
import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.dto.SalaryDetailsDTO;
import lk.ijse.payroll.entity.BankDetails;
import lk.ijse.payroll.entity.Employee;
import lk.ijse.payroll.entity.SalaryDetails;

/**
 *
 * @author udara
 */
public class EmployeeBOImpl implements EmployeeBO {

    private EmployeeDAO emp;
    

    public EmployeeBOImpl() {
        emp = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Employee);
    }

    @Override
    public boolean add(EmployeeDTO e, BankDetailsDTO bank, SalaryDetailsDTO salary) throws Exception {

        Employee employee = new Employee(e.getEmpId(), e.getEmpName(), e.getAddress(), e.getGender(), e.getNIC(), e.getD_O_B(), Integer.parseInt(e.getPhone_No()), e.getJoin_Date(), e.getDeId(), e.getTot_leaves());
        BankDetails bankdetails = new BankDetails(bank.getBdId(), bank.getEmpId(), bank.getBankId(), bank.getAccNo());
        SalaryDetails salarydetails = new SalaryDetails(salary.getSDId(), salary.getEmpId(), salary.getBSId());
        return emp.addEmployee(employee, bankdetails, salarydetails);

    }

    @Override
    public boolean delete(String Id) throws Exception {
        return emp.delete(Id);
    }

    @Override
    public String search() throws Exception {
        Employee d = emp.searchEmpId();
        EmployeeDTO c = new EmployeeDTO(d.getEmpId(), d.getEmpName(), d.getAddress(), d.getGender(), d.getNIC(), d.getD_O_B(), "" + d.getPhone_No(), d.getJoin_Date(), d.getDeId(), d.getTot_Leaves());
        return c.getEmpId();
    }

    @Override
    public ArrayList<EmployeeDTO> getAll() throws Exception {
        ArrayList<Employee> allCustomers = emp.getAll();
        ArrayList<EmployeeDTO> alldesignations = new ArrayList<>();
        for (Employee d : allCustomers) {
            alldesignations.add(new EmployeeDTO(d.getEmpId(), d.getEmpName(), d.getAddress(), d.getGender(), d.getNIC(), d.getD_O_B(), "" + d.getPhone_No(), d.getJoin_Date(), d.getDeId(), d.getTot_Leaves()));
        }
        return alldesignations;
    }

    @Override
    public ArrayList<String> getAllemployee() throws Exception {
        ArrayList<Employee> allCustomers = emp.getAll();
        ArrayList<String> all = new ArrayList<>();
        for (Employee des : allCustomers) {
            all.add(des.getEmpId());
        }
        return all;
    }

    @Override
    public EmployeeDTO searchEmp() throws Exception {
        Employee d = emp.searchEmpId();
        return new EmployeeDTO(d.getEmpId(), d.getEmpName(), d.getAddress(), d.getGender(), d.getNIC(), d.getD_O_B(), "" + d.getPhone_No(), d.getJoin_Date(), d.getDeId(), d.getTot_Leaves());

    }

    @Override
    public boolean update(EmployeeDTO e, SalaryDetailsDTO salary, BankDetailsDTO bank) throws Exception {
        
            Employee employee = new Employee(e.getEmpId(), e.getEmpName(), e.getAddress(), e.getGender(), e.getNIC(), e.getD_O_B(), Integer.parseInt(e.getPhone_No()), e.getJoin_Date(), e.getDeId(), e.getTot_leaves());
            BankDetails bankdetails = new BankDetails(bank.getBdId(), bank.getEmpId(), bank.getBankId(), bank.getAccNo());
            SalaryDetails salarydetails = new SalaryDetails(salary.getSDId(), salary.getEmpId(), salary.getBSId());
            return emp.updateEmp(employee,bankdetails,salarydetails);
            
        
    }

    @Override
    public ArrayList<String> getAllemployeeName() throws Exception {
        ArrayList<Employee> allCustomers = emp.getAll();
        ArrayList<String> alldesignations = new ArrayList<>();
        for (Employee des : allCustomers) {
            alldesignations.add(des.getEmpName());
        }
        return alldesignations;
    }

    @Override
    public EmployeeDTO searchEmpID(String Id) throws Exception {
        Employee d = emp.searchEmpID(Id);
        return new EmployeeDTO(d.getEmpId(), d.getEmpName(), d.getAddress(), d.getGender(), d.getNIC(), d.getD_O_B(), "" + d.getPhone_No(), d.getJoin_Date(), d.getDeId(), d.getTot_Leaves());

    }

    @Override
    public EmployeeDTO searchEmpDetails(String Id) throws Exception {
        Employee d = emp.search(Id);
        return new EmployeeDTO(d.getEmpId(), d.getEmpName(), d.getAddress(), d.getGender(), d.getNIC(), d.getD_O_B(), "" + d.getPhone_No(), d.getJoin_Date(), d.getDeId(), d.getTot_Leaves());

    }

    @Override
    public EmployeeDTO searchEmployeeId(String name) throws Exception {
        Employee d = emp.searchEmpID(name);

        return new EmployeeDTO(d.getEmpId(), d.getEmpName(), d.getAddress(), d.getGender(), d.getNIC(), d.getD_O_B(), "" + d.getPhone_No(), d.getJoin_Date(), d.getDeId(), d.getTot_Leaves());

    }

    @Override
    public EmployeeDTO searchEmpName(String empId) throws Exception {
        Employee d = emp.search(empId);
        return new EmployeeDTO(d.getEmpId(), d.getEmpName(), d.getAddress(), d.getGender(), d.getNIC(), d.getD_O_B(), "" + d.getPhone_No(), d.getJoin_Date(), d.getDeId(), d.getTot_Leaves());

    }

    @Override
    public ArrayList<EmployeeDTO> getAll(String deId) throws Exception {
        ArrayList<Employee> allCustomers = emp.getAll(deId);
        ArrayList<EmployeeDTO> alldesignations = new ArrayList<>();
        for (Employee d : allCustomers) {
            alldesignations.add(new EmployeeDTO(d.getEmpId(), d.getEmpName(), d.getAddress(), d.getGender(), d.getNIC(), d.getD_O_B(), "" + d.getPhone_No(), d.getJoin_Date(), d.getDeId(), d.getTot_Leaves()));
        }
        return alldesignations;
    }

}
