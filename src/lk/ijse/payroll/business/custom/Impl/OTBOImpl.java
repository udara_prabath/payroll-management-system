/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.util.ArrayList;
import lk.ijse.payroll.business.custom.OTBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.OTDAO;
import lk.ijse.payroll.dto.AdvancesDTO;
import lk.ijse.payroll.dto.OTDTO;
import lk.ijse.payroll.entity.Advances;
import lk.ijse.payroll.entity.OT;

/**
 *
 * @author udara
 */
public class OTBOImpl implements OTBO {
    private OTDAO da;

    public OTBOImpl() {
        da=DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.OT);
    }
    

    @Override
    public boolean add(OTDTO ot) throws Exception {
        return false;
    }

    @Override
    public boolean update(OTDTO ot) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Id) throws Exception {
        return da.delete(Id);
    }

    @Override
    public OT search(String Id) throws Exception {
        return null;
    }

    @Override
    public ArrayList<OTDTO> getAll() throws Exception {
        ArrayList<OT>adv=da.getAll();
        ArrayList<OTDTO>all=new ArrayList<>();
        
        for (OT ot : adv) {
            OTDTO ot1=new OTDTO(ot.getOTId(),ot.getAttid(),ot.getEmpId(),ot.getDate(),ot.getNormal_OT(),ot.getDouble_OT(),ot.getAmount());
            all.add(ot1);
        }
        return all;
    }

    @Override
    public ArrayList<OTDTO> getAll(String id) throws Exception {
        ArrayList<OT>adv=da.getAll(id);
        ArrayList<OTDTO>all=new ArrayList<>();
        
        for (OT ot : adv) {
            OTDTO ot1=new OTDTO(ot.getOTId(),ot.getAttid(),ot.getEmpId(),ot.getDate(),ot.getNormal_OT(),ot.getDouble_OT(),ot.getAmount());
            all.add(ot1);
        }
        return all;
    }

}
