/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.sql.Connection;
import java.util.ArrayList;
import lk.ijse.payroll.business.custom.LeavesBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.EmployeeDAO;
import lk.ijse.payroll.dao.custom.LeavesDAO;
import lk.ijse.payroll.db.DBConnection;
import lk.ijse.payroll.dto.LeavesDTO;
import lk.ijse.payroll.entity.Employee;
import lk.ijse.payroll.entity.Leaves;

/**
 *
 * @author udara
 */
public class LeavesBOImpl implements LeavesBO {

    private LeavesDAO lev;
    

    public LeavesBOImpl() {
        lev = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Leaves);
        
    }

    @Override
    public boolean add(LeavesDTO leaves) throws Exception {
        return false;
    }

    @Override
    public boolean update(LeavesDTO leaves) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Id) throws Exception {
        return false;
    }

    @Override
    public Leaves search(String Id) throws Exception {
        return null;
    }

    @Override
    public ArrayList<LeavesDTO> getAll() throws Exception {
        ArrayList<Leaves> all1 = lev.getAll();
        ArrayList<LeavesDTO> all = new ArrayList<>();
        for (Leaves d : all1) {
            all.add(new LeavesDTO(d.getLId(), d.getEmpId(), d.getDate(), d.getTotal_Absants(), d.getFree_Leaves()));
        }
        return all;
    }

    @Override
    public boolean markLeave(int leaveId, String empId, String leavedate, int ab, int free) throws Exception {
        
            Leaves lea = new Leaves(leaveId, empId, leavedate, ab, free);
            return lev.add(lea);
            

            
            
    }

    @Override
    public ArrayList<LeavesDTO> getAll(String Id) throws Exception {
        ArrayList<Leaves> all1 = lev.getAll(Id);
        ArrayList<LeavesDTO> all = new ArrayList<>();
        for (Leaves d : all1) {
            all.add(new LeavesDTO(d.getLId(), d.getEmpId(), d.getDate(), d.getTotal_Absants(), d.getFree_Leaves()));
        }
        return all;
    }

}
