/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.util.ArrayList;
import lk.ijse.payroll.business.custom.ShortLeaveBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.ShortLeaveDAO;
import lk.ijse.payroll.dto.shortLeaveDTO;
import lk.ijse.payroll.entity.shortLeave;

/**
 *
 * @author udara
 */
public class ShortLeaveBOImpl implements ShortLeaveBO {

    private ShortLeaveDAO sl;

    public ShortLeaveBOImpl() {
        sl = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.ShortLeave);
    }

    @Override
    public boolean add(shortLeaveDTO a) throws Exception {
        shortLeave s = new shortLeave(a.getSLId(), a.getEmpId(), a.getDate(), a.getTimeOut(), a.getTimeIn(), a.getDuration());
        return sl.add(s);
    }

    @Override
    public ArrayList<shortLeaveDTO> getAll(String Id) throws Exception {
        ArrayList<shortLeave> all1 = sl.getAll(Id);
        ArrayList<shortLeaveDTO> all = new ArrayList<>();
        for (shortLeave d : all1) {
            all.add(new shortLeaveDTO(d.getSLId(), d.getEmpId(), d.getDate(), d.getTimeOut(), d.getTimeIn(), d.getDuration()));
        }
        return all;
    }

    @Override
    public ArrayList<shortLeaveDTO> getAll() throws Exception {
        ArrayList<shortLeave> all1 = sl.getAll();
        ArrayList<shortLeaveDTO> all = new ArrayList<>();
        for (shortLeave d : all1) {
            all.add(new shortLeaveDTO(d.getSLId(), d.getEmpId(), d.getDate(), d.getTimeOut(), d.getTimeIn(), d.getDuration()));
        }
        return all;
    }

    @Override
    public ArrayList<shortLeaveDTO> getdetails(String id, String date) throws Exception {
        ArrayList<shortLeave> all1 = sl.getAll(id,date);
        ArrayList<shortLeaveDTO> all = new ArrayList<>();
        for (shortLeave d : all1) {
            all.add(new shortLeaveDTO(d.getSLId(), d.getEmpId(), d.getDate(), d.getTimeOut(), d.getTimeIn(), d.getDuration()));
        }
        return all;
    }

}
