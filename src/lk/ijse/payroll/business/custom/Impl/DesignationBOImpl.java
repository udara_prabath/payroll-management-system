/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.util.ArrayList;
import lk.ijse.payroll.business.custom.DesignationBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.DesignationDAO;
import lk.ijse.payroll.dto.DesignationDTO;
import lk.ijse.payroll.entity.Designation;

/**
 *
 * @author udara
 */
public class DesignationBOImpl implements DesignationBO {

    private DesignationDAO de;

    public DesignationBOImpl() {
        de = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Designation);
    }

    @Override
    public boolean add(DesignationDTO des) throws Exception {
        Designation d = new Designation(des.getDeId(), des.getDeName());
        return de.add(d);
    }

    @Override
    public boolean update(DesignationDTO des) throws Exception {
        Designation d = new Designation(des.getDeId(), des.getDeName());
        return de.update(d);
    }

    @Override
    public boolean delete(String Id) throws Exception {
        return de.delete(Id);
    }

    @Override
    public Designation search(String Id) throws Exception {
        return de.search(Id);
    }

    @Override
    public DesignationDTO searchId(String id) throws Exception {
        Designation d = de.searchId(id);
        //System.out.println("aa"+d.getDeId());
        return new DesignationDTO(d.getDeId(), d.getDeName());
    }

    @Override
    public ArrayList<DesignationDTO> getAll() throws Exception {
        ArrayList<Designation> adv = de.getAll();
        ArrayList<DesignationDTO> all = new ArrayList<>();
        for (Designation advances : adv) {
            all.add(new DesignationDTO(advances.getDeId(), advances.getDeName()));
        }
        return all;
    }

    @Override
    public ArrayList<String> getAllDesignations() throws Exception {
        ArrayList<Designation> allCustomers = de.getAll();
        ArrayList<String> alldesignations = new ArrayList<>();
        for (Designation des : allCustomers) {
            alldesignations.add(des.getDeName());
        }
        return alldesignations;
    }

    @Override
    public String searchDes(String deId) throws Exception {
        Designation d = de.search(deId);
        //System.out.println("aa"+d.getDeId());
        DesignationDTO des = new DesignationDTO(d.getDeId(), d.getDeName());
        return des.getDeName();
    }

    @Override
    public String search() throws Exception {
        Designation d = de.search();
        DesignationDTO des = new DesignationDTO(d.getDeId(), d.getDeName());
        return des.getDeId();
    }

}
