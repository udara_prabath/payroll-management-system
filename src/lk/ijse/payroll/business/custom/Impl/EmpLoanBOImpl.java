/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import lk.ijse.payroll.business.custom.EmpLoanBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.EmpLoanDAO;
import lk.ijse.payroll.dao.custom.QueryDAO;
import lk.ijse.payroll.dto.EmpLoanDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.entity.EmpLoan;
import lk.ijse.payroll.entity.Query;

/**
 *
 * @author udara
 */
public class EmpLoanBOImpl implements EmpLoanBO {

    private EmpLoanDAO el;
    private QueryDAO q;

    public EmpLoanBOImpl() {
        el = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.EmpLoan);
        q = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Query);
    }

    @Override
    public boolean add(EmpLoanDTO el) throws Exception {
        return false;
    }

    @Override
    public boolean update(EmpLoanDTO el) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Id) throws Exception {
        return el.delete(Id);
    }

    @Override
    public EmpLoan search(String Id) throws Exception {
        return null;
    }

    @Override
    public ArrayList<EmpLoanDTO> getAll() throws Exception {
        ArrayList<EmpLoan> allad = el.getAll();
        ArrayList<EmpLoanDTO> all = new ArrayList<>();
        for (EmpLoan d : allad) {
            all.add(new EmpLoanDTO(d.getElId(), d.getEmpId(), d.getLoanId(), d.getDate(), d.getInstallment()));

        }
        return all;
    }

    @Override
    public ArrayList<EmpLoanDTO> getLoans(String empid) throws Exception {
        ArrayList<EmpLoan> allad = el.searchByemp(empid);
        ArrayList<EmpLoanDTO> all = new ArrayList<>();
        for (EmpLoan d : allad) {
            all.add(new EmpLoanDTO(d.getElId(), d.getEmpId(), d.getLoanId(), d.getDate(), d.getInstallment()));
        }
        return all;
    }

    @Override
    public ArrayList<EmpLoanDTO> getLoanById(String lid) throws Exception {
        ArrayList<EmpLoan> allad = el.searchByid(lid);
        ArrayList<EmpLoanDTO> all = new ArrayList<>();
        for (EmpLoan d : allad) {
            all.add(new EmpLoanDTO(d.getElId(), d.getEmpId(), d.getLoanId(), d.getDate(), d.getInstallment()));
        }
        return all;
    }

    @Override
    public QueryDTO getLoanDetails(String id, String date) throws Exception {
        Query r = q.searchLoanDetails(id, date);
        QueryDTO a = new QueryDTO(null, r.getLoanName(), r.getInstallment(), r.getEmpId());
        return a;
    }

    @Override
    public String search() throws Exception {
        EmpLoan d = el.searchId();
        EmpLoanDTO c = new EmpLoanDTO(d.getElId(), d.getEmpId(), d.getLoanId(), d.getDate(), d.getInstallment());
        return c.getElId();
    }

    @Override
    public ArrayList<QueryDTO> searchempLoans(String id) throws Exception {
        ArrayList<Query> all = q.serachDetails(id);
        ArrayList<QueryDTO> a = new ArrayList<>();
        for (Query d : all) {
            a.add(new QueryDTO(d.getLoanName(), d.getDate(), d.getLoanAmount()));
        }
        return a;
    }

    @Override
    public boolean checkAvailability(String date,String empid) {
        String dat = el.getDate(empid);
        if (!"366".equals(dat)) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy:MM:dd");

            Date d1 = null;
            Date d2 = null;
            try {
                d1 = format.parse(dat);
                d2 = format.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long diff = d2.getTime() - d1.getTime();
            long diffDays = diff / (24 * 60 * 60 * 1000);
            if(diffDays>365){
                return true;
            }else{
                return false;
            }
        }
        return true;
    }

}
