/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.util.ArrayList;
import lk.ijse.payroll.business.custom.SalaryDetailsBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.SalaryDetailsDAO;
import lk.ijse.payroll.dto.BankDetailsDTO;
import lk.ijse.payroll.dto.BasicSalaryDTO;
import lk.ijse.payroll.dto.SalaryDetailsDTO;
import lk.ijse.payroll.entity.BankDetails;
import lk.ijse.payroll.entity.BasicSalary;
import lk.ijse.payroll.entity.SalaryDetails;

/**
 *
 * @author udara
 */
public class SalaryDetailsBOImpl implements SalaryDetailsBO {

    private SalaryDetailsDAO sd;

    public SalaryDetailsBOImpl() {
        sd = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.SalaryDetails);
    }

    @Override
    public boolean add(SalaryDetailsDTO sd) throws Exception {
        return false;
    }

    @Override
    public boolean update(SalaryDetailsDTO sd) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Id) throws Exception {
        return false;
    }

    @Override
    public String search(String Id) throws Exception {
        return null;
    }

    @Override
    public ArrayList<SalaryDetailsDTO> getAll() throws Exception {
        return null;
    }

    @Override
    public String searchId() throws Exception {
        SalaryDetails d = sd.searchId();
        SalaryDetailsDTO c = new SalaryDetailsDTO(d.getSDId(), d.getEmpId(), d.getBSId());
        return "" + c.getSDId();
    }

    @Override
    public String getSalarydetailId(String toString) throws Exception {
        SalaryDetails d = sd.getSalaryDetailId(toString);
        SalaryDetailsDTO c = new SalaryDetailsDTO(d.getSDId(), d.getEmpId(), d.getBSId());

        return "" + c.getSDId();
    }

    @Override
    public String searchEmployeeSalary(String empId) throws Exception {
        SalaryDetails d = sd.getSalaryDetailId(empId);
        SalaryDetailsDTO c = new SalaryDetailsDTO(d.getSDId(), d.getEmpId(), d.getBSId());

        return "" + c.getBSId();
    }

}
