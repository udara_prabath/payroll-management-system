/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import lk.ijse.payroll.business.custom.PayrollBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.PayrollDAO;
import lk.ijse.payroll.dao.custom.QueryDAO;
import lk.ijse.payroll.dto.PayrollDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.entity.Payroll;
import lk.ijse.payroll.entity.Query;

/**
 *
 * @author udara
 */
public class PayrollBOImpl implements PayrollBO {

    private QueryDAO q;
    private PayrollDAO payroll;

    public PayrollBOImpl() {
        q = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Query);
        payroll = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Payroll);
        
    }

    @Override
    public boolean add(PayrollDTO p) throws Exception {
        Payroll pay=new Payroll(p.getPayId(),p.getEmpId(),p.getDate(),p.getDesignation(),p.getBank(),p.getAcc_no(),p.getBasic_Salary(),p.getDay_worked(),p.getNormal_OT(),p.getDouble_OT(),p.getTotal_Leaves(),p.getShort_Leaves(),p.getEPF_8(),p.getEPF_12(),p.getETF_3(),p.getLoan(),p.getAdvances(),p.getBonus(),p.getGross_Salary(),p.getTotal_Deduction(),p.getSalary());
        return payroll.add(pay);
    }

    @Override
    public boolean update(PayrollDTO pay) throws Exception {
        return false;
    }

    @Override
    public boolean delete(String Id) throws Exception {
        return false;
    }

    @Override
    public Payroll search(String Id) throws Exception {
        return null;
    }

    @Override
    public ArrayList<PayrollDTO> getAll() throws Exception {
       ArrayList<Payroll>all=payroll.getAll();
        ArrayList<PayrollDTO> details=new ArrayList<>();
        for (Payroll p : all) {
            details.add(new PayrollDTO(p.getPayId(),p.getEmpId(),p.getDate(),p.getDesignation(),p.getBank(),p.getAcc_no(),p.getBasic_Salary(),p.getDay_worked(),p.getNormal_OT(),p.getDouble_OT(),p.getTotal_Leaves(),p.getShort_Leaves(),p.getEPF_8(),p.getEPF_12(),p.getETF_3(),p.getLoan(),p.getAdvances(),p.getBonus(),p.getGross_Salary(),p.getTotal_Deduction(),p.getSalary()));
        }
        return details;
    }

    @Override
    public ArrayList<QueryDTO> getAllDetails(String id, String date) throws Exception {
        ArrayList<Query> adv = q.getAllAteendanceDetails(id, date);
        ArrayList<QueryDTO> all = new ArrayList<>();
        for (Query l : adv) {
            System.out.println(l.getEmpName());
            all.add(new QueryDTO(l.getEmpName(), l.getYear(), l.getMonth(), l.getWorked_hours(), l.getnOT(), l.getdOT()));
        }
        return all;
    }

    @Override
    public PayrollDTO calculateSalary(String bs, String not, String dot, String ep8, String nol, String shl, String bonus, String adv, String loan) throws Exception{

        int NOT = Integer.parseInt(not);
        int DOT = Integer.parseInt(dot);
        int leaves = Integer.parseInt(nol);
        int shot = Integer.parseInt(shl);

        BigDecimal basic = new BigDecimal(bs);
        BigDecimal epf8 = new BigDecimal(ep8);
        BigDecimal lon = new BigDecimal(loan);
        BigDecimal ad = new BigDecimal(adv);
        BigDecimal bon = new BigDecimal(bonus);
        
        Payroll cal=payroll.calculatePayroll(NOT,DOT,leaves,shot,basic,epf8,lon,ad,bon);
        return new PayrollDTO(cal.getGross_Salary(), cal.getTotal_Deduction(),cal.getSalary());
    }

    @Override
    public ArrayList<PayrollDTO> getAlByEmpId(String id) throws Exception {
        ArrayList<Payroll>all=payroll.getAllByEmpId(id);
        ArrayList<PayrollDTO> details=new ArrayList<>();
        for (Payroll p : all) {
            details.add(new PayrollDTO(p.getPayId(),p.getEmpId(),p.getDate(),p.getDesignation(),p.getBank(),p.getAcc_no(),p.getBasic_Salary(),p.getDay_worked(),p.getNormal_OT(),p.getDouble_OT(),p.getTotal_Leaves(),p.getShort_Leaves(),p.getEPF_8(),p.getEPF_12(),p.getETF_3(),p.getLoan(),p.getAdvances(),p.getBonus(),p.getGross_Salary(),p.getTotal_Deduction(),p.getSalary()));
        }
        return details;
    }

    @Override
    public ArrayList<PayrollDTO> getAll(int y, int m) throws Exception {
         ArrayList<Payroll>all=payroll.getAllPayroll(y,m);
        ArrayList<PayrollDTO> details=new ArrayList<>();
        for (Payroll p : all) {
            details.add(new PayrollDTO(p.getPayId(),p.getEmpId(),p.getDate(),p.getDesignation(),p.getBank(),p.getAcc_no(),p.getBasic_Salary(),p.getDay_worked(),p.getNormal_OT(),p.getDouble_OT(),p.getTotal_Leaves(),p.getShort_Leaves(),p.getEPF_8(),p.getEPF_12(),p.getETF_3(),p.getLoan(),p.getAdvances(),p.getBonus(),p.getGross_Salary(),p.getTotal_Deduction(),p.getSalary()));
        }
        return details;
    }

    @Override
    public ArrayList<PayrollDTO> getAll(String Id, int y, int m) throws Exception {
        ArrayList<Payroll>all=payroll.getAllPayroll(Id,y,m);
        ArrayList<PayrollDTO> details=new ArrayList<>();
        for (Payroll p : all) {
            details.add(new PayrollDTO(p.getPayId(),p.getEmpId(),p.getDate(),p.getDesignation(),p.getBank(),p.getAcc_no(),p.getBasic_Salary(),p.getDay_worked(),p.getNormal_OT(),p.getDouble_OT(),p.getTotal_Leaves(),p.getShort_Leaves(),p.getEPF_8(),p.getEPF_12(),p.getETF_3(),p.getLoan(),p.getAdvances(),p.getBonus(),p.getGross_Salary(),p.getTotal_Deduction(),p.getSalary()));
        }
        return details; 
    }

    @Override
    public ArrayList<String> getEmployeeIds(String date) throws Exception {
        ArrayList<String>all=payroll.getAll(date);
        ArrayList<String>ids=new ArrayList<>();
        for (String p : all) {
            ids.add(p);
        }
        return ids;
    }

}
