/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import lk.ijse.payroll.business.custom.UserBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.UserDAO;
import lk.ijse.payroll.dto.UserDTO;
import lk.ijse.payroll.entity.User;

/**
 *
 * @author udara
 */
public class UserBOImpl implements UserBO {

    private UserDAO u;

    public UserBOImpl() {
        u = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.User);
    }

    @Override
    public UserDTO search(String un, String pw) throws Exception {
        User i = u.search(un, pw);
        return new UserDTO(i.getUserName(), i.getPassword());
    }

    @Override
    public boolean update(UserDTO userDTO) throws Exception {
       User a=new User(userDTO.getUserName(),userDTO.getPassword()); 
        boolean i=u.update(a);
       
       return i;
    }

}
