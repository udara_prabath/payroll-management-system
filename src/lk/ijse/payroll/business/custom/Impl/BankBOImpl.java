/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom.Impl;

import java.util.ArrayList;
import lk.ijse.payroll.business.custom.BankBO;
import lk.ijse.payroll.dao.DAOFactory;
import lk.ijse.payroll.dao.custom.BankDAO;
import lk.ijse.payroll.dto.BankDTO;
import lk.ijse.payroll.entity.Bank;

/**
 *
 * @author udara
 */
public class BankBOImpl implements BankBO {

    private BankDAO ba;

    public BankBOImpl() {
        ba = DAOFactory.getInstance().getDAO(DAOFactory.DAOTypes.Bank);
    }

    @Override
    public boolean add(BankDTO bank) throws Exception {
        Bank b = new Bank(bank.getBankId(), bank.getBankName());
        return ba.add(b);
    }

    @Override
    public boolean update(BankDTO bank) throws Exception {
        Bank b = new Bank(bank.getBankId(), bank.getBankName());
        return ba.update(b);
    }

    @Override
    public boolean delete(String Id) throws Exception {
        return ba.delete(Id);
    }

    @Override
    public Bank search(String Id) throws Exception {
        return ba.search(Id);
    }

    @Override
    public ArrayList<BankDTO> getAll() throws Exception {
        ArrayList<Bank> adv = ba.getAll();
        ArrayList<BankDTO> all = new ArrayList<>();
        for (Bank advances : adv) {
            all.add(new BankDTO(advances.getBankId(), advances.getBankName()));
        }
        return all;
    }

    public ArrayList<String> getAllBanks() throws Exception {
        ArrayList<Bank> adv = ba.getAll();
        ArrayList<String> all = new ArrayList<>();
        for (Bank bank : adv) {
            all.add(bank.getBankName());
        }
        return all;
    }

    @Override
    public BankDTO searchBankId(String id) throws Exception {
        Bank d=ba.searchId(id);
        //System.out.println("aa"+d.getDeId());
        return new BankDTO(d.getBankId(), d.getBankName());
    }

    @Override
    public String search() throws Exception {
        Bank d=ba.searchId();
        //System.out.println("aa"+d.getDeId());
        BankDTO a=new BankDTO(d.getBankId(), d.getBankName());
        return a.getBankId();
    }

}
