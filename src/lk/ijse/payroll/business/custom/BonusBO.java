/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.BonusDTO;
import lk.ijse.payroll.entity.Bonus;

/**
 *
 * @author udara
 */
public interface BonusBO {
        public boolean add(BonusDTO salary) throws Exception;

    public boolean update(BonusDTO salary) throws Exception;

    public boolean delete(String salaryId) throws Exception;

    public Bonus search(String salaryId) throws Exception;

    public ArrayList<BonusDTO> getAll() throws Exception;

    public String search()throws Exception;
}
