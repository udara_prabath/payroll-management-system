/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.business.SuperBO;
import lk.ijse.payroll.dto.EmpAdvancesDTO;
import lk.ijse.payroll.dto.EmpLoanDTO;
import lk.ijse.payroll.dto.LoanDTO;

/**
 *
 * @author udara
 */
public interface LoanBO extends SuperBO {

    public boolean addLoan(LoanDTO loan)throws Exception;

    public boolean updateLoan(LoanDTO loan)throws Exception;

    public boolean deleteLoan(String loanId)throws Exception;

    public ArrayList<LoanDTO> getAllLoans()throws Exception;

    public ArrayList<String> getAllloanNames()throws Exception;

    public LoanDTO searchLoanByname(String name)throws Exception;

    public boolean addLoan(EmpLoanDTO advance)throws Exception;

    public String searchLoanName(String loanId)throws Exception;

    public String search()throws Exception;
    
    

}
