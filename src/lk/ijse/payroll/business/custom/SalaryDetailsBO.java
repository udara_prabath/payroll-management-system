/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.AdvancesDTO;
import lk.ijse.payroll.dto.SalaryDetailsDTO;
import lk.ijse.payroll.entity.Advances;
import lk.ijse.payroll.entity.SalaryDetails;

/**
 *
 * @author udara
 */
public interface SalaryDetailsBO {

    public String searchId()throws Exception;

    public boolean add(SalaryDetailsDTO salary) throws Exception;

    public boolean update(SalaryDetailsDTO salary) throws Exception;

    public boolean delete(String salaryId) throws Exception;

    public String search(String salaryId) throws Exception;

    public ArrayList<SalaryDetailsDTO> getAll() throws Exception;

    public String getSalarydetailId(String toString)throws Exception;

    public String searchEmployeeSalary(String empId)throws Exception;

    
}
