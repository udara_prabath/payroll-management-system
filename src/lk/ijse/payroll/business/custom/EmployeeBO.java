/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.BankDetailsDTO;
import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.dto.SalaryDetailsDTO;
import lk.ijse.payroll.entity.Employee;

/**
 *
 * @author udara
 */
public interface EmployeeBO {
    public boolean add(EmployeeDTO e,BankDetailsDTO bank,SalaryDetailsDTO salary) throws Exception;

    public boolean update(EmployeeDTO e,SalaryDetailsDTO salary,BankDetailsDTO bank) throws Exception;

    public boolean delete(String salaryId) throws Exception;

    public String search() throws Exception;

    public ArrayList<EmployeeDTO> getAll() throws Exception;

    public ArrayList<String> getAllemployee()throws Exception;

    public EmployeeDTO searchEmp()throws Exception;

    public ArrayList<String> getAllemployeeName()throws Exception;

    public EmployeeDTO searchEmpID(String Id)throws Exception;

    public EmployeeDTO searchEmpDetails(String Id)throws Exception;

    public EmployeeDTO searchEmployeeId(String name)throws Exception;

    public EmployeeDTO searchEmpName(String empId)throws Exception;

    public ArrayList<EmployeeDTO> getAll(String deId)throws Exception;
}
