/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.AdvancesDTO;
import lk.ijse.payroll.dto.EmpAdvancesDTO;
import lk.ijse.payroll.entity.Advances;

/**
 *
 * @author udara
 */
public interface AdvancesBO {
    public boolean add(AdvancesDTO salary)throws Exception;
    public boolean update(AdvancesDTO salary)throws Exception;
    public boolean delete(String salaryId)throws Exception;
    public Advances search(String salaryId)throws Exception;
    public ArrayList<AdvancesDTO> getAll()throws Exception;

    public ArrayList<String> getAllAdvanceNames()throws Exception;

    public AdvancesDTO searchAdvance(String ad)throws Exception;

    public String searchadid(String adId)throws Exception;

    public String search()throws Exception;

    

    
}
