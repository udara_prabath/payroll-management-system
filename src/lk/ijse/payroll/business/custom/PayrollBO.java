/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.PayrollDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.dto.SalaryDetailsDTO;
import lk.ijse.payroll.entity.Payroll;
import lk.ijse.payroll.entity.SalaryDetails;

/**
 *
 * @author udara
 */
public interface PayrollBO {
    public boolean add(PayrollDTO salary) throws Exception;

    public boolean update(PayrollDTO salary) throws Exception;

    public boolean delete(String salaryId) throws Exception;

    public Payroll search(String salaryId) throws Exception;

    public ArrayList<PayrollDTO> getAll() throws Exception;

    public ArrayList<QueryDTO> getAllDetails(String id,String Date)throws Exception;

    public PayrollDTO calculateSalary(String bs, String not, String dot, String ep8, String nol, String shl, String bonus, String adv, String loan)throws Exception;

    public ArrayList<PayrollDTO> getAlByEmpId(String id)throws Exception;

    public ArrayList<PayrollDTO> getAll(int y, int m)throws Exception;

    public ArrayList<PayrollDTO> getAll(String Id, int y, int m)throws Exception;

    public ArrayList<String> getEmployeeIds(String date)throws Exception;
}
