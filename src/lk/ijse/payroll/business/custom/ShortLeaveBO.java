/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dto.shortLeaveDTO;

/**
 *
 * @author udara
 */
public interface ShortLeaveBO {

    public boolean add(shortLeaveDTO a)throws Exception;

    public ArrayList<shortLeaveDTO> getAll(String Id)throws Exception;

    public ArrayList<shortLeaveDTO> getAll()throws Exception;

    public ArrayList<shortLeaveDTO> getdetails(String id, String date)throws Exception;
    
}
