/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.BankDTO;
import lk.ijse.payroll.entity.Bank;

/**
 *
 * @author udara
 */
public interface BankBO {
        public boolean add(BankDTO salary) throws Exception;

    public boolean update(BankDTO salary) throws Exception;

    public boolean delete(String salaryId) throws Exception;

    public Bank search(String salaryId) throws Exception;

    public ArrayList<BankDTO> getAll() throws Exception;

    public ArrayList<String> getAllBanks()throws Exception;

    public BankDTO searchBankId(String toString)throws Exception;

    public String search()throws Exception;
}
