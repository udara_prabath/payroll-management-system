/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.OTDTO;
import lk.ijse.payroll.dto.PayrollDTO;
import lk.ijse.payroll.entity.OT;
import lk.ijse.payroll.entity.Payroll;

/**
 *
 * @author udara
 */
public interface OTBO {

    public boolean add(OTDTO salary) throws Exception;

    public boolean update(OTDTO salary) throws Exception;

    public boolean delete(String salaryId) throws Exception;

    public OT search(String salaryId) throws Exception;

    public ArrayList<OTDTO> getAll() throws Exception;

    public ArrayList<OTDTO> getAll(String id)throws Exception;
}
