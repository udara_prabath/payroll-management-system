/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.AttendanceDTO;
import lk.ijse.payroll.entity.Attendance;

/**
 *
 * @author udara
 */
public interface AttendanceBO {

    public boolean add(AttendanceDTO salary,String designation) throws Exception;

    public boolean update(AttendanceDTO salary) throws Exception;

    public boolean delete(String salaryId) throws Exception;

    public Attendance search(String salaryId) throws Exception;

    public ArrayList<AttendanceDTO> getAll() throws Exception;

    public ArrayList<AttendanceDTO> getAll(String Id, int y, int m)throws Exception;

    public ArrayList<AttendanceDTO> getAll(int y, int m)throws Exception;

    public String search()throws Exception;

    

}
