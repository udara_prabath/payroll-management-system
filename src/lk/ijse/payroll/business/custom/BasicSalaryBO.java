/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.business.SuperBO;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.BasicSalaryDTO;
import lk.ijse.payroll.entity.BasicSalary;

/**
 *
 * @author udara
 */
public interface BasicSalaryBO extends SuperBO {

    public boolean addSalary(BasicSalaryDTO salary) throws Exception;

    public boolean updateSalary(BasicSalaryDTO salary) throws Exception;

    public boolean deleteSalary(String salaryId) throws Exception;

    public BasicSalary searchSalary(String salaryId) throws Exception;

    public ArrayList<BasicSalaryDTO> getAllSalary() throws Exception;
    
    public ArrayList<String> getAllsalary() throws Exception;

    public String searchSalaryId(String toString)throws Exception;

    public String searchSalaryName(String empId)throws Exception;

    public String searchSalaryId()throws Exception;

   

}
