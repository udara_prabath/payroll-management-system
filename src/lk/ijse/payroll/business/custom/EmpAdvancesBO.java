/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.EmpAdvancesDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.entity.EmpAdvances;

/**
 *
 * @author udara
 */
public interface EmpAdvancesBO {
        public boolean add(EmpAdvancesDTO salary) throws Exception;

    public boolean update(EmpAdvancesDTO salary) throws Exception;

    public boolean delete(String salaryId) throws Exception;

    public EmpAdvances search(String salaryId) throws Exception;

    public ArrayList<EmpAdvancesDTO> getAll() throws Exception;

    public ArrayList<EmpAdvancesDTO> getEmpAdVances(String adv)throws Exception;

    public String searchLastId()throws Exception;

    public ArrayList<EmpAdvancesDTO> getAdvances(String empid)throws Exception;

    public QueryDTO searchAdvanceDetails(String id, String date)throws Exception;

    public ArrayList<QueryDTO> getDetails(String id)throws Exception;

    public boolean checkAvailabiity(int year, int month,String emp);
}
