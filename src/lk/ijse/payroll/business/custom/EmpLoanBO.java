/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.business.custom;

import java.util.ArrayList;
import lk.ijse.payroll.dao.custom.*;
import lk.ijse.payroll.dto.EmpLoanDTO;
import lk.ijse.payroll.dto.QueryDTO;
import lk.ijse.payroll.entity.EmpLoan;

/**
 *
 * @author udara
 */
public interface EmpLoanBO {
    public boolean add(EmpLoanDTO salary) throws Exception;

    public boolean update(EmpLoanDTO salary) throws Exception;

    public boolean delete(String salaryId) throws Exception;

    public EmpLoan search(String salaryId) throws Exception;

    public ArrayList<EmpLoanDTO> getAll() throws Exception;

    public ArrayList<EmpLoanDTO> getLoans(String empid)throws Exception;

    public ArrayList<EmpLoanDTO> getLoanById(String lid)throws Exception;

    public QueryDTO getLoanDetails(String id, String date)throws Exception;

    public String search()throws Exception;

    public ArrayList<QueryDTO> searchempLoans(String id)throws Exception;

    public boolean checkAvailability(String date,String empid);
}
