/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dto;

/**
 *
 * @author udara
 */
public class AttendanceDTO {
    private String attId;
    private String EmpId;
    private String day;
    private String dateIn;
    private String timeIn;
    private String dateOut;
    private String timeOut;
    private int workedHourse;

    public AttendanceDTO(String attId, String EmpId, String day, String dateIn, String timeIn, String dateOut, String timeOut, int workedHourse) {
        this.attId = attId;
        this.EmpId = EmpId;
        this.day = day;
        this.dateIn = dateIn;
        this.timeIn = timeIn;
        this.dateOut = dateOut;
        this.timeOut = timeOut;
        this.workedHourse = workedHourse;
    }

    @Override
    public String toString() {
        return "AttendanceDTO{" + "attId=" + attId + ", EmpId=" + EmpId + ", day=" + day + ", dateIn=" + dateIn + ", timeIn=" + timeIn + ", dateOut=" + dateOut + ", timeOut=" + timeOut + ", workedHourse=" + workedHourse + '}';
    }

    public AttendanceDTO() {
    }

    /**
     * @return the attId
     */
    public String getAttId() {
        return attId;
    }

    /**
     * @param attId the attId to set
     */
    public void setAttId(String attId) {
        this.attId = attId;
    }

    /**
     * @return the EmpId
     */
    public String getEmpId() {
        return EmpId;
    }

    /**
     * @param EmpId the EmpId to set
     */
    public void setEmpId(String EmpId) {
        this.EmpId = EmpId;
    }

    /**
     * @return the day
     */
    public String getDay() {
        return day;
    }

    /**
     * @param day the day to set
     */
    public void setDay(String day) {
        this.day = day;
    }

    /**
     * @return the dateIn
     */
    public String getDateIn() {
        return dateIn;
    }

    /**
     * @param dateIn the dateIn to set
     */
    public void setDateIn(String dateIn) {
        this.dateIn = dateIn;
    }

    /**
     * @return the timeIn
     */
    public String getTimeIn() {
        return timeIn;
    }

    /**
     * @param timeIn the timeIn to set
     */
    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    /**
     * @return the dateOut
     */
    public String getDateOut() {
        return dateOut;
    }

    /**
     * @param dateOut the dateOut to set
     */
    public void setDateOut(String dateOut) {
        this.dateOut = dateOut;
    }

    /**
     * @return the timeOut
     */
    public String getTimeOut() {
        return timeOut;
    }

    /**
     * @param timeOut the timeOut to set
     */
    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    /**
     * @return the workedHourse
     */
    public int getWorkedHourse() {
        return workedHourse;
    }

    /**
     * @param workedHourse the workedHourse to set
     */
    public void setWorkedHourse(int workedHourse) {
        this.workedHourse = workedHourse;
    }
}
