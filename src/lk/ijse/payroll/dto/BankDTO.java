/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dto;

import lk.ijse.payroll.dao.custom.impl.*;
import lk.ijse.payroll.dao.custom.BankDAO;

/**
 *
 * @author udara
 */
public class BankDTO {
     private String bankId;
    private String bankName;

    @Override
    public String toString() {
        return "BankDTO{" + "bankId=" + bankId + ", bankName=" + bankName + '}';
    }

    public BankDTO(String bankId, String bankName) {
        this.bankId = bankId;
        this.bankName = bankName;
    }

    public BankDTO() {
    }

    /**
     * @return the bankId
     */
    public String getBankId() {
        return bankId;
    }

    /**
     * @param bankId the bankId to set
     */
    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}
