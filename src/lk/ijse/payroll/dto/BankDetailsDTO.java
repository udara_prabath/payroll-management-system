/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dto;

import lk.ijse.payroll.dao.custom.impl.*;
import lk.ijse.payroll.dao.custom.BankDAO;
import lk.ijse.payroll.dao.custom.BankDetailsDAO;

/**
 *
 * @author udara
 */
public class BankDetailsDTO {
    private int bdId;
    private String empId;
    private String bankId;
    private String accNo;

    public BankDetailsDTO() {
    }

    public BankDetailsDTO(int bdId, String empId, String bankId, String accNo) {
        this.bdId = bdId;
        this.empId = empId;
        this.bankId = bankId;
        this.accNo = accNo;
    }

    @Override
    public String toString() {
        return "BankDetailsDTO{" + "bdId=" + bdId + ", empId=" + empId + ", bankId=" + bankId + ", accNo=" + accNo + '}';
    }

    /**
     * @return the bdId
     */
    public int getBdId() {
        return bdId;
    }

    /**
     * @param bdId the bdId to set
     */
    public void setBdId(int bdId) {
        this.bdId = bdId;
    }

    /**
     * @return the empId
     */
    public String getEmpId() {
        return empId;
    }

    /**
     * @param empId the empId to set
     */
    public void setEmpId(String empId) {
        this.empId = empId;
    }

    /**
     * @return the bankId
     */
    public String getBankId() {
        return bankId;
    }

    /**
     * @param bankId the bankId to set
     */
    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    /**
     * @return the accNo
     */
    public String getAccNo() {
        return accNo;
    }

    /**
     * @param accNo the accNo to set
     */
    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    
   
}
