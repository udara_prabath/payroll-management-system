/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dto;

import lk.ijse.payroll.dao.custom.impl.*;
import lk.ijse.payroll.dao.custom.DesignationDAO;

/**
 *
 * @author udara
 */
public class DesignationDTO{
     private String DeId;
    private String DeName;

    public DesignationDTO(String DeId, String DeName) {
        this.DeId = DeId;
        this.DeName = DeName;
    }

    @Override
    public String toString() {
        return "DesignationDTO{" + "DeId=" + DeId + ", DeName=" + DeName + '}';
    }

    public DesignationDTO() {
    }

    /**
     * @return the DeId
     */
    public String getDeId() {
        return DeId;
    }

    /**
     * @param DeId the DeId to set
     */
    public void setDeId(String DeId) {
        this.DeId = DeId;
    }

    /**
     * @return the DeName
     */
    public String getDeName() {
        return DeName;
    }

    /**
     * @param DeName the DeName to set
     */
    public void setDeName(String DeName) {
        this.DeName = DeName;
    }
}
