/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dto;

import java.math.BigDecimal;
import lk.ijse.payroll.dao.custom.impl.*;
import lk.ijse.payroll.dao.custom.PayrollDAO;

/**
 *
 * @author udara
 */
public class PayrollDTO {

    private int PayId;
    private String EmpId;
    private String Date;
    private String Designation;
    private String Bank;
    private String Acc_no;
    private BigDecimal Basic_Salary;
    private String Day_worked;
    private BigDecimal Normal_OT;
    private BigDecimal Double_OT;
    private String Total_Leaves;
    private String Short_Leaves;
    private BigDecimal EPF_8;
    private BigDecimal EPF_12;
    private BigDecimal ETF_3;
    private BigDecimal Loan;
    private BigDecimal Advances;
    private BigDecimal Bonus;
    private BigDecimal Gross_Salary;
    private BigDecimal Total_Deduction;
    private BigDecimal salary;

    public PayrollDTO() {
    }
    
    public PayrollDTO(BigDecimal Gross_Salary,BigDecimal Total_Deduction,BigDecimal salary) {
        this.Gross_Salary=Gross_Salary;
        this.Total_Deduction=Total_Deduction;
        this.salary=salary;
    }

    public PayrollDTO(int PayId, String EmpId, String Date, String Designation, String Bank, String Acc_no, BigDecimal Basic_Salary, String Day_worked, BigDecimal Normal_OT, BigDecimal Double_OT, String Total_Leaves, String Short_Leaves, BigDecimal EPF_8, BigDecimal EPF_12, BigDecimal ETF_3, BigDecimal Loan, BigDecimal Advances, BigDecimal Bonus, BigDecimal Gross_Salary, BigDecimal Total_Deduction, BigDecimal salary) {
        this.PayId = PayId;
        this.EmpId = EmpId;
        this.Date = Date;
        this.Designation = Designation;
        this.Bank = Bank;
        this.Acc_no = Acc_no;
        this.Basic_Salary = Basic_Salary;
        this.Day_worked = Day_worked;
        this.Normal_OT = Normal_OT;
        this.Double_OT = Double_OT;
        this.Total_Leaves = Total_Leaves;
        this.Short_Leaves = Short_Leaves;
        this.EPF_8 = EPF_8;
        this.EPF_12 = EPF_12;
        this.ETF_3 = ETF_3;
        this.Loan = Loan;
        this.Advances = Advances;
        this.Bonus = Bonus;
        this.Gross_Salary = Gross_Salary;
        this.Total_Deduction = Total_Deduction;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "PayrollDTO{" + "PayId=" + PayId + ", EmpId=" + EmpId + ", Date=" + Date + ", Designation=" + Designation + ", Bank=" + Bank + ", Acc_no=" + Acc_no + ", Basic_Salary=" + Basic_Salary + ", Day_worked=" + Day_worked + ", Normal_OT=" + Normal_OT + ", Double_OT=" + Double_OT + ", Total_Leaves=" + Total_Leaves + ", Short_Leaves=" + Short_Leaves + ", EPF_8=" + EPF_8 + ", EPF_12=" + EPF_12 + ", ETF_3=" + ETF_3 + ", Loan=" + Loan + ", Advances=" + Advances + ", Bonus=" + Bonus + ", Gross_Salary=" + Gross_Salary + ", Total_Deduction=" + Total_Deduction + ", salary=" + salary + ", bs=" + bs + ", not=" + not + ", dot=" + dot + ", ep8=" + ep8 + ", ep12=" + ep12 + ", et3=" + et3 + ", nol=" + nol + ", shl=" + shl + ", bons=" + bons + ", adv=" + adv + ", lon=" + lon + '}';
    }

    

    

    /**
     * @return the PayId
     */
    public int getPayId() {
        return PayId;
    }

    /**
     * @param PayId the PayId to set
     */
    public void setPayId(int PayId) {
        this.PayId = PayId;
    }

    /**
     * @return the EmpId
     */
    public String getEmpId() {
        return EmpId;
    }

    /**
     * @param EmpId the EmpId to set
     */
    public void setEmpId(String EmpId) {
        this.EmpId = EmpId;
    }

    /**
     * @return the Date
     */
    public String getDate() {
        return Date;
    }

    /**
     * @param Date the Date to set
     */
    public void setDate(String Date) {
        this.Date = Date;
    }

    /**
     * @return the Designation
     */
    public String getDesignation() {
        return Designation;
    }

    /**
     * @param Designation the Designation to set
     */
    public void setDesignation(String Designation) {
        this.Designation = Designation;
    }

    /**
     * @return the Bank
     */
    public String getBank() {
        return Bank;
    }

    /**
     * @param Bank the Bank to set
     */
    public void setBank(String Bank) {
        this.Bank = Bank;
    }

    /**
     * @return the Acc_no
     */
    public String getAcc_no() {
        return Acc_no;
    }

    /**
     * @param Acc_no the Acc_no to set
     */
    public void setAcc_no(String Acc_no) {
        this.Acc_no = Acc_no;
    }

    /**
     * @return the Basic_Salary
     */
    public BigDecimal getBasic_Salary() {
        return Basic_Salary;
    }

    /**
     * @param Basic_Salary the Basic_Salary to set
     */
    public void setBasic_Salary(BigDecimal Basic_Salary) {
        this.Basic_Salary = Basic_Salary;
    }

    /**
     * @return the Day_worked
     */
    public String getDay_worked() {
        return Day_worked;
    }

    /**
     * @param Day_worked the Day_worked to set
     */
    public void setDay_worked(String Day_worked) {
        this.Day_worked = Day_worked;
    }

    /**
     * @return the Normal_OT
     */
    public BigDecimal getNormal_OT() {
        return Normal_OT;
    }

    /**
     * @param Normal_OT the Normal_OT to set
     */
    public void setNormal_OT(BigDecimal Normal_OT) {
        this.Normal_OT = Normal_OT;
    }

    /**
     * @return the Double_OT
     */
    public BigDecimal getDouble_OT() {
        return Double_OT;
    }

    /**
     * @param Double_OT the Double_OT to set
     */
    public void setDouble_OT(BigDecimal Double_OT) {
        this.Double_OT = Double_OT;
    }

    /**
     * @return the Total_Leaves
     */
    public String getTotal_Leaves() {
        return Total_Leaves;
    }

    /**
     * @param Total_Leaves the Total_Leaves to set
     */
    public void setTotal_Leaves(String Total_Leaves) {
        this.Total_Leaves = Total_Leaves;
    }

    /**
     * @return the Short_Leaves
     */
    public String getShort_Leaves() {
        return Short_Leaves;
    }

    /**
     * @param Short_Leaves the Short_Leaves to set
     */
    public void setShort_Leaves(String Short_Leaves) {
        this.Short_Leaves = Short_Leaves;
    }

    /**
     * @return the EPF_8
     */
    public BigDecimal getEPF_8() {
        return EPF_8;
    }

    /**
     * @param EPF_8 the EPF_8 to set
     */
    public void setEPF_8(BigDecimal EPF_8) {
        this.EPF_8 = EPF_8;
    }

    /**
     * @return the EPF_12
     */
    public BigDecimal getEPF_12() {
        return EPF_12;
    }

    /**
     * @param EPF_12 the EPF_12 to set
     */
    public void setEPF_12(BigDecimal EPF_12) {
        this.EPF_12 = EPF_12;
    }

    /**
     * @return the ETF_3
     */
    public BigDecimal getETF_3() {
        return ETF_3;
    }

    /**
     * @param ETF_3 the ETF_3 to set
     */
    public void setETF_3(BigDecimal ETF_3) {
        this.ETF_3 = ETF_3;
    }

    /**
     * @return the Loan
     */
    public BigDecimal getLoan() {
        return Loan;
    }

    /**
     * @param Loan the Loan to set
     */
    public void setLoan(BigDecimal Loan) {
        this.Loan = Loan;
    }

    /**
     * @return the Advances
     */
    public BigDecimal getAdvances() {
        return Advances;
    }

    /**
     * @param Advances the Advances to set
     */
    public void setAdvances(BigDecimal Advances) {
        this.Advances = Advances;
    }

    /**
     * @return the Bonus
     */
    public BigDecimal getBonus() {
        return Bonus;
    }

    /**
     * @param Bonus the Bonus to set
     */
    public void setBonus(BigDecimal Bonus) {
        this.Bonus = Bonus;
    }

    /**
     * @return the Gross_Salary
     */
    public BigDecimal getGross_Salary() {
        return Gross_Salary;
    }

    /**
     * @param Gross_Salary the Gross_Salary to set
     */
    public void setGross_Salary(BigDecimal Gross_Salary) {
        this.Gross_Salary = Gross_Salary;
    }

    /**
     * @return the Total_Deduction
     */
    public BigDecimal getTotal_Deduction() {
        return Total_Deduction;
    }

    /**
     * @param Total_Deduction the Total_Deduction to set
     */
    public void setTotal_Deduction(BigDecimal Total_Deduction) {
        this.Total_Deduction = Total_Deduction;
    }

    /**
     * @return the salary
     */
    public BigDecimal getSalary() {
        return salary;
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }
    private String bs;
    private String not ;
    private String dot;
    private String ep8 ;
    private String ep12 ;
    private String et3 ;
    private String nol ;
    private String shl ;
    private String bons ;
    private String adv ;
    private String lon  ;

    public PayrollDTO(String bs, String not, String dot, String ep8, String ep12, String et3, String nol, String shl, String bons, String adv, String lon) {
        this.bs = bs;
        this.not = not;
        this.dot = dot;
        this.ep8 = ep8;
        this.ep12 = ep12;
        this.et3 = et3;
        this.nol = nol;
        this.shl = shl;
        this.bons = bons;
        this.adv = adv;
        this.lon = lon;
    }

    /**
     * @return the bs
     */
    public String getBs() {
        return bs;
    }

    /**
     * @param bs the bs to set
     */
    public void setBs(String bs) {
        this.bs = bs;
    }

    /**
     * @return the not
     */
    public String getNot() {
        return not;
    }

    /**
     * @param not the not to set
     */
    public void setNot(String not) {
        this.not = not;
    }

    /**
     * @return the dot
     */
    public String getDot() {
        return dot;
    }

    /**
     * @param dot the dot to set
     */
    public void setDot(String dot) {
        this.dot = dot;
    }

    /**
     * @return the ep8
     */
    public String getEp8() {
        return ep8;
    }

    /**
     * @param ep8 the ep8 to set
     */
    public void setEp8(String ep8) {
        this.ep8 = ep8;
    }

    /**
     * @return the ep12
     */
    public String getEp12() {
        return ep12;
    }

    /**
     * @param ep12 the ep12 to set
     */
    public void setEp12(String ep12) {
        this.ep12 = ep12;
    }

    /**
     * @return the et3
     */
    public String getEt3() {
        return et3;
    }

    /**
     * @param et3 the et3 to set
     */
    public void setEt3(String et3) {
        this.et3 = et3;
    }

    /**
     * @return the nol
     */
    public String getNol() {
        return nol;
    }

    /**
     * @param nol the nol to set
     */
    public void setNol(String nol) {
        this.nol = nol;
    }

    /**
     * @return the shl
     */
    public String getShl() {
        return shl;
    }

    /**
     * @param shl the shl to set
     */
    public void setShl(String shl) {
        this.shl = shl;
    }

    /**
     * @return the bons
     */
    public String getBons() {
        return bons;
    }

    /**
     * @param bons the bons to set
     */
    public void setBons(String bons) {
        this.bons = bons;
    }

    /**
     * @return the adv
     */
    public String getAdv() {
        return adv;
    }

    /**
     * @param adv the adv to set
     */
    public void setAdv(String adv) {
        this.adv = adv;
    }

    /**
     * @return the lon
     */
    public String getLon() {
        return lon;
    }

    /**
     * @param lon the lon to set
     */
    public void setLon(String lon) {
        this.lon = lon;
    }

    
    

}
