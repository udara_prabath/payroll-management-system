/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dto;

import java.math.BigDecimal;

/**
 *
 * @author udara
 */
public class QueryDTO {
    private String extra;
    private String LoanName;
    private BigDecimal installment;
    private String empId;
    private String BankName;
    private String Acc_no;
    private String empName;
    private String year;
    private String month;
    private int worked_hours;
    private int nOT;
    private int dOT;
    private String adName;
    private BigDecimal amount;
    private String date;
    private BigDecimal loanAmount;
    
    
    private String bonusName;
    private String BonusAmount;
    
    private String PayDate;
    private BigDecimal normalOT;
    private BigDecimal doubleOT;
    private BigDecimal EPF12;
    private BigDecimal EPF8;
    private BigDecimal EtF3;
    private BigDecimal loan;
    private BigDecimal advance;
    private BigDecimal salary;
    
    public QueryDTO(String adName,String year,String month, BigDecimal amount) {
        this.adName = adName;
        this.amount = amount;
        this.year=year;
        this.month=month;
    }
    
    public QueryDTO(String date, String loanName,BigDecimal Amount) {
        this.date = date;
        this.LoanName = loanName;
        this.loanAmount=Amount;
    }
    public QueryDTO(String extra, String bonusName,String bonusAmount) {
        this.extra = extra;
        this.bonusName = bonusName;
        this.BonusAmount=bonusAmount;
    }
    
    
    public QueryDTO(String BankName, String Acc_no) {
        this.BankName = BankName;
        this.Acc_no = Acc_no;
    }
    public QueryDTO(String adName, BigDecimal amount) {
        this.adName = adName;
        this.amount = amount;
    }
    public QueryDTO() {
    }

    @Override
    public String toString() {
        return "QueryDTO{" + "extra=" + extra + ", LoanName=" + LoanName + ", installment=" + installment + ", empId=" + empId + ", BankName=" + BankName + ", Acc_no=" + Acc_no + ", empName=" + empName + ", year=" + year + ", month=" + month + ", worked_hours=" + worked_hours + ", nOT=" + nOT + ", dOT=" + dOT + ", adName=" + adName + ", amount=" + amount + ", date=" + date + ", loanAmount=" + loanAmount + ", bonusName=" + bonusName + ", BonusAmount=" + BonusAmount + '}';
    }

    

    /**
     * @return the BankName
     */
    public String getBankName() {
        return BankName;
    }

    /**
     * @param BankName the BankName to set
     */
    public void setBankName(String BankName) {
        this.BankName = BankName;
    }

    /**
     * @return the Acc_no
     */
    public String getAcc_no() {
        return Acc_no;
    }

    /**
     * @param Acc_no the Acc_no to set
     */
    public void setAcc_no(String Acc_no) {
        this.Acc_no = Acc_no;
    }

    public QueryDTO(String empName, String year, String month, int worked_hours, int nOT, int dOT) {
        this.empName = empName;
        this.year = year;
        this.month = month;
        this.worked_hours = worked_hours;
        this.nOT = nOT;
        this.dOT = dOT;
    }

    /**
     * @return the empName
     */
    public String getEmpName() {
        return empName;
    }

    /**
     * @param empName the empName to set
     */
    public void setEmpName(String empName) {
        this.empName = empName;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * @return the month
     */
    public String getMonth() {
        return month;
    }

    /**
     * @param month the month to set
     */
    public void setMonth(String month) {
        this.month = month;
    }

    /**
     * @return the worked_hours
     */
    public int getWorked_hours() {
        return worked_hours;
    }

    /**
     * @param worked_hours the worked_hours to set
     */
    public void setWorked_hours(int worked_hours) {
        this.worked_hours = worked_hours;
    }

    /**
     * @return the nOT
     */
    public int getnOT() {
        return nOT;
    }

    /**
     * @param nOT the nOT to set
     */
    public void setnOT(int nOT) {
        this.nOT = nOT;
    }

    /**
     * @return the dOT
     */
    public int getdOT() {
        return dOT;
    }

    /**
     * @param dOT the dOT to set
     */
    public void setdOT(int dOT) {
        this.dOT = dOT;
    }

    /**
     * @return the adName
     */
    public String getAdName() {
        return adName;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @return the extra
     */
    public String getExtra() {
        return extra;
    }

    /**
     * @return the bonusName
     */
    public String getBonusName() {
        return bonusName;
    }

    /**
     * @return the BonusAmount
     */
    public String getBonusAmount() {
        return BonusAmount;
    }

    /**
     * @return the LoanName
     */
    public String getLoanName() {
        return LoanName;
    }

    /**
     * @return the installment
     */
    public BigDecimal getInstallment() {
        return installment;
    }

    /**
     * @return the empId
     */
    public String getEmpId() {
        return empId;
    }

    public QueryDTO(String extra, String LoanName, BigDecimal installment, String empId) {
        this.extra = extra;
        this.LoanName = LoanName;
        this.installment = installment;
        this.empId = empId;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @return the loanAmount
     */
    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    /**
     * @return the PayDate
     */
    public String getPayDate() {
        return PayDate;
    }

    /**
     * @return the normalOT
     */
    public BigDecimal getNormalOT() {
        return normalOT;
    }

    /**
     * @return the doubleOT
     */
    public BigDecimal getDoubleOT() {
        return doubleOT;
    }

    /**
     * @return the EPF12
     */
    public BigDecimal getEPF12() {
        return EPF12;
    }

    /**
     * @return the EPF8
     */
    public BigDecimal getEPF8() {
        return EPF8;
    }

    /**
     * @return the EtF3
     */
    public BigDecimal getEtF3() {
        return EtF3;
    }

    /**
     * @return the loan
     */
    public BigDecimal getLoan() {
        return loan;
    }

    /**
     * @return the advance
     */
    public BigDecimal getAdvance() {
        return advance;
    }

    /**
     * @return the salary
     */
    public BigDecimal getSalary() {
        return salary;
    }
    public QueryDTO(String PayDate, BigDecimal normalOT, BigDecimal doubleOT, BigDecimal EPF12, BigDecimal EPF8, BigDecimal EtF3, BigDecimal loan, BigDecimal advance, BigDecimal salary) {
        this.PayDate = PayDate;
        this.normalOT = normalOT;
        this.doubleOT = doubleOT;
        this.EPF12 = EPF12;
        this.EPF8 = EPF8;
        this.EtF3 = EtF3;
        this.loan = loan;
        this.advance = advance;
        this.salary = salary;
    }
    
}
