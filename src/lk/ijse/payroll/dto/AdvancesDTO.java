/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dto;

import java.math.BigDecimal;
import lk.ijse.payroll.dao.custom.impl.*;
import lk.ijse.payroll.dao.custom.AdvancesDAO;

/**
 *
 * @author udara
 */
public class AdvancesDTO {
    private String adId;
    private String adName;
    private BigDecimal amount;

    @Override
    public String toString() {
        return "AdvancesDTO{" + "adId=" + adId + ", adName=" + adName + ", amount=" + amount + '}';
    }

    public AdvancesDTO(String adId, String adName, BigDecimal amount) {
        this.adId = adId;
        this.adName = adName;
        this.amount = amount;
    }

    public AdvancesDTO() {
    }

    /**
     * @return the adId
     */
    public String getAdId() {
        return adId;
    }

    /**
     * @param adId the adId to set
     */
    public void setAdId(String adId) {
        this.adId = adId;
    }

    /**
     * @return the adName
     */
    public String getAdName() {
        return adName;
    }

    /**
     * @param adName the adName to set
     */
    public void setAdName(String adName) {
        this.adName = adName;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
