/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dto;

import lk.ijse.payroll.dao.custom.impl.*;
import lk.ijse.payroll.dao.custom.EmpLoanDAO;
import lk.ijse.payroll.dao.custom.EmployeeDAO;

/**
 *
 * @author udara
 */
public class EmployeeDTO {
    private String empId;
    private String empName;
    private String address;
    private String gender;
    private String NIC;
    private String D_O_B;
    private String phone_No;
    private String join_Date;
    private String deId;
     private int tot_leaves;

    public EmployeeDTO() {
    }

    @Override
    public String toString() {
        return "EmployeeDTO{" + "empId=" + empId + ", empName=" + empName + ", address=" + address + ", gender=" + gender + ", NIC=" + NIC + ", D_O_B=" + D_O_B + ", phone_No=" + phone_No + ", join_Date=" + join_Date + ", deId=" + deId + ", tot_leaves=" + tot_leaves + '}';
    }

    public EmployeeDTO(String empId, String empName, String address, String gender, String NIC, String D_O_B, String phone_No, String join_Date, String deId, int tot_leaves) {
        this.empId = empId;
        this.empName = empName;
        this.address = address;
        this.gender = gender;
        this.NIC = NIC;
        this.D_O_B = D_O_B;
        this.phone_No = phone_No;
        this.join_Date = join_Date;
        this.deId = deId;
        this.tot_leaves = tot_leaves;
    }

    /**
     * @return the empId
     */
    public String getEmpId() {
        return empId;
    }

    /**
     * @param empId the empId to set
     */
    public void setEmpId(String empId) {
        this.empId = empId;
    }

    /**
     * @return the empName
     */
    public String getEmpName() {
        return empName;
    }

    /**
     * @param empName the empName to set
     */
    public void setEmpName(String empName) {
        this.empName = empName;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the NIC
     */
    public String getNIC() {
        return NIC;
    }

    /**
     * @param NIC the NIC to set
     */
    public void setNIC(String NIC) {
        this.NIC = NIC;
    }

    /**
     * @return the D_O_B
     */
    public String getD_O_B() {
        return D_O_B;
    }

    /**
     * @param D_O_B the D_O_B to set
     */
    public void setD_O_B(String D_O_B) {
        this.D_O_B = D_O_B;
    }

    /**
     * @return the phone_No
     */
    public String getPhone_No() {
        return phone_No;
    }

    /**
     * @param phone_No the phone_No to set
     */
    public void setPhone_No(String phone_No) {
        this.phone_No = phone_No;
    }

    /**
     * @return the join_Date
     */
    public String getJoin_Date() {
        return join_Date;
    }

    /**
     * @param join_Date the join_Date to set
     */
    public void setJoin_Date(String join_Date) {
        this.join_Date = join_Date;
    }

    /**
     * @return the deId
     */
    public String getDeId() {
        return deId;
    }

    /**
     * @param deId the deId to set
     */
    public void setDeId(String deId) {
        this.deId = deId;
    }

    /**
     * @return the tot_leaves
     */
    public int getTot_leaves() {
        return tot_leaves;
    }

    /**
     * @param tot_leaves the tot_leaves to set
     */
    public void setTot_leaves(int tot_leaves) {
        this.tot_leaves = tot_leaves;
    }

   
}
