/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.payroll.dto;

/**
 *
 * @author udara
 */
public class shortLeaveDTO {
    private int SLId;
    private String EmpId;
    private String Date;
    private String timeOut;
    private String TimeIn;
    private int Duration;

    public shortLeaveDTO() {
    }

    @Override
    public String toString() {
        return "shortLeaveDTO{" + "SLId=" + SLId + ", EmpId=" + EmpId + ", Date=" + Date + ", timeOut=" + timeOut + ", TimeIn=" + TimeIn + ", Duration=" + Duration + '}';
    }
    
    public shortLeaveDTO(int SLId, String EmpId, String Date, String timeOut, String TimeIn, int Duration) {
        this.SLId = SLId;
        this.EmpId = EmpId;
        this.Date = Date;
        this.timeOut = timeOut;
        this.TimeIn = TimeIn;
        this.Duration = Duration;
    }

    /**
     * @return the SLId
     */
    public int getSLId() {
        return SLId;
    }

    /**
     * @param SLId the SLId to set
     */
    public void setSLId(int SLId) {
        this.SLId = SLId;
    }

    /**
     * @return the EmpId
     */
    public String getEmpId() {
        return EmpId;
    }

    /**
     * @param EmpId the EmpId to set
     */
    public void setEmpId(String EmpId) {
        this.EmpId = EmpId;
    }

    /**
     * @return the Date
     */
    public String getDate() {
        return Date;
    }

    /**
     * @param Date the Date to set
     */
    public void setDate(String Date) {
        this.Date = Date;
    }

    /**
     * @return the timeOut
     */
    public String getTimeOut() {
        return timeOut;
    }

    /**
     * @param timeOut the timeOut to set
     */
    public void setTimeOut(String timeOut) {
        this.timeOut = timeOut;
    }

    /**
     * @return the TimeIn
     */
    public String getTimeIn() {
        return TimeIn;
    }

    /**
     * @param TimeIn the TimeIn to set
     */
    public void setTimeIn(String TimeIn) {
        this.TimeIn = TimeIn;
    }

    /**
     * @return the Duration
     */
    public int getDuration() {
        return Duration;
    }

    /**
     * @param Duration the Duration to set
     */
    public void setDuration(int Duration) {
        this.Duration = Duration;
    }
    
}
